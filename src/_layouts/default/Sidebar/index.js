import React, { Component } from 'react';

export default class Sidebar extends Component {
    constructor(props) {
        super(props)
        this.state = {
            deliverymangement: true,
            contentmanagement: true,
            categorymanagement: true,
        }
    }
    state = this.state

    deliveryChange = () => {
        const { deliverymangement } = this.state;
        if (deliverymangement) {
            this.setState({
                deliverymangement: false
            })
        } else {
            this.setState({
                deliverymangement: true
            })
        }
    }

    contentChange = () => {
        const { contentmanagement } = this.state;
        if (contentmanagement) {
            this.setState({
                contentmanagement: false
            })
        } else {
            this.setState({
                contentmanagement: true
            })
        }
    }

    categoryChange = () => {
        const { categorymanagement } = this.state;
        if (categorymanagement) {
            this.setState({
                categorymanagement: false
            })
        } else {
            this.setState({
                categorymanagement: true
            })
        }
    }

    render() {
        return (

            <aside className="main-sidebar sidebar-dark-primary elevation-4">
                {/* Brand Logo */}
                <a href="index3.html" className="brand-link">
                    {/*<img src={require('../../../assets/images/logo.png')} alt="AdminLTE Logo" className="brand-image  elevation-3" style={{ opacity: '.8' }} />*/}
                    <span className="brand-text ">myMart.qa</span>
                </a>
                {/* Sidebar */}
                <div className="sidebar">
                    {/* Sidebar user panel (optional) */}
                    <div className="user-panel mt-3 pb-3 mb-3 d-flex">
                        <div className="info">
                            <a href="#/" className="d-block">Welcome Admin</a>
                        </div>
                    </div>
                    {/* Sidebar Menu */}
                    <nav className="mt-2">
                        <ul className="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                            {/* Add icons to the links using the .nav-icon class
         with font-awesome or any other icon font library */}
                            <li className="nav-item ">
                                <a href="#/" className="nav-link active">
                                    <i className="nav-icon fas fa-tachometer-alt" />
                                    <p>
                                        Dashboard
                                        </p>
                                </a>
                            </li>
                            {/*<li className="nav-item">
                                    <a href={`${process.env.PUBLIC_URL}/seller`} className="nav-link">
                                        <i className="nav-icon fas fa-th" />
                                        <p>
                                            Sellers Management
                                        </p>
                                    </a>
                                </li>

                                <li className="nav-item">
                                    <a href={`${process.env.PUBLIC_URL}/customer`} className="nav-link">
                                        <i className="nav-icon fas fa-user" />
                                        <p>
                                            Customer Management
                                        </p>
                                    </a>
                                </li>

                                <li className= {this.state.deliverymangement ? "nav-item has-treeview menu-open":"nav-item has-treeview menu-close"}>
                                    <a href="#/" className="nav-link" onClick={this.deliveryChange}>
                                        <i className="nav-icon fas fa-truck" />
                                        <p>
                                        Delivery Management
                                        <i className="fas fa-angle-left right" />
                                        </p>
                                    </a>
                                    <ul className="nav nav-treeview ">
                                        <li className="nav-item">
                                            <a href={`${process.env.PUBLIC_URL}/deliveryexcecutive`} className="nav-link">
                                                <i className="nav-icon fas fa-users" />
                                                <p>Executive Management</p>
                                            </a>
                                        </li>
                                        <li className="nav-item">
                                            <a href={`${process.env.PUBLIC_URL}/deliverycharge`} className="nav-link">
                                                <i className="fas fa-ship nav-icon" />
                                                <p>Charge Management</p>
                                            </a>
                                        </li>
                                    </ul>
                                </li>*/}


                            <li className="nav-header">Category Management</li>
                            <span className="user-panel "></span>
                            <li className="nav-item">
                                <a href={`${process.env.PUBLIC_URL}/category`} className="nav-link">
                                    <i className="fa fa-adjust nav-icon" />
                                    <p>Category</p>
                                </a>
                            </li>
                            {/*<li className="nav-item">
                                <a href={`${process.env.PUBLIC_URL}/subcategory`} className="nav-link">
                                    <i className="fa fa-lock nav-icon" />
                                    <p>Sub Category</p>
                                </a>
                            </li>*/}


                            <li className="nav-header">Product Management</li>
                            <span className="user-panel "></span>
                            <li className="nav-item">
                                <a href={`${process.env.PUBLIC_URL}/brand`} className="nav-link">
                                    <i className="nav-icon fas fa-tag" />
                                    <p>Brand</p>
                                </a>
                            </li>
                            <li className="nav-item">
                                <a href={`${process.env.PUBLIC_URL}/request`} className="nav-link">
                                    <i className="nav-icon fas fa-tag" />
                                    <p>Request</p>
                                </a>
                            </li>
                            <li className="nav-item">
                                <a href={`${process.env.PUBLIC_URL}/productType`} className="nav-link">
                                    <i className="nav-icon fas fa-tag" />
                                    <p>Product Type</p>
                                </a>
                            </li>
                            <li className="nav-item">
                                <a href={`${process.env.PUBLIC_URL}/product`} className="nav-link">
                                    <i className="nav-icon fas fa-address-card" />
                                    <p>Product</p>
                                </a>
                            </li>
                            {/*<li className="nav-item">
                                <a href={`${process.env.PUBLIC_URL}/profile`} className="nav-link">
                                    <i className="nav-icon fas fa-address-card" />
                                    <p>Order Management</p>
                                </a>
                            </li>
                            <li className="nav-item">
                                <a href={`${process.env.PUBLIC_URL}/rating`} className="nav-link">
                                    <i className="nav-icon fas fa-address-card" />
                                    <p>Review Management</p>
                                </a>
                            </li>*/}
                            <li className="nav-header">Coupon Management</li>
                            <span className="user-panel "></span>
                            <li className="nav-item">
                                <a href={`${process.env.PUBLIC_URL}/coupon`} className="nav-link">
                                    <i className="nav-icon fas fa-percentage" />
                                    <p>Discount / Coupon</p>
                                </a>
                            </li>

                            <li className="nav-header">Banner Management</li>
                            <span className="user-panel "></span>
                            <li className="nav-item">
                                <a href={`${process.env.PUBLIC_URL}/banner`} className="nav-link">
                                    <i className="nav-icon fas fa-edit" />
                                    <p>Banner</p>
                                </a>
                            </li>

                            <li className="nav-header">Content Management</li>
                            <span className="user-panel "></span>
                            <li className="nav-item">
                                <a href={`${process.env.PUBLIC_URL}/aboutinfo`} className="nav-link">
                                    <i className="fa fa-adjust nav-icon" />
                                    <p>About Us</p>
                                </a>
                            </li>
                            <li className="nav-item">
                                <a href={`${process.env.PUBLIC_URL}/privacy`} className="nav-link">
                                    <i className="fa fa-lock nav-icon" />
                                    <p>Privacy Policy</p>
                                </a>
                            </li>
                            <li className="nav-item">
                                <a href={`${process.env.PUBLIC_URL}/terms`} className="nav-link">
                                    <i className="fa fa-info nav-icon" />
                                    <p>Terms & Conditions</p>
                                </a>
                            </li>

                            <li className="nav-header">Admin Settings</li>
                            <span className="user-panel "></span>
                            <li className="nav-item">
                                <a href={`${process.env.PUBLIC_URL}/profile`} className="nav-link">
                                    <i className="nav-icon fas fa-address-card" />
                                    <p>Profile</p>
                                </a>
                            </li>
                            <li className="nav-item">
                                <a href={`${process.env.PUBLIC_URL}/settings`} className="nav-link">
                                    <i className="nav-icon fas fa-cog" />
                                    <p>Settings</p>
                                </a>
                            </li>
                            <li className="nav-item">
                                <a href={`${process.env.PUBLIC_URL}/logout`} className="nav-link">
                                    <i className="nav-icon fas fa-power-off" />
                                    <p>Logout</p>
                                </a>
                            </li>
                        </ul>
                    </nav>
                    {/* /.sidebar-menu */}
                </div>
                {/* /.sidebar */}
            </aside>


        )
    }
}

