import React, { Component } from "react";
import playStoreIcon from "../../../assets/images/playStoreIcon.png";
import AppStoreIcon from "../../../assets/images/AppStoreIcon.png";
import envelop from "../../../assets/images/envelop.svg";
import payment_options from "../../../assets/images/payment_options.svg";
import Headphone from "../../../assets/images/headphone-footer.svg";
import AppStotr from "../../../assets/images/app-store.svg";
import PlayStore from "../../../assets/images/play-store.svg";
import { UpdateCart } from '../../../data/reducers/cart';
import mainLogo from "../../../assets/images/myMart-logo.svg";
import QRCode from "../../../assets/images/QR-code.svg";
import Location from "../../../assets/images/footer-Location.svg";
import Message from "../../../assets/images/footer-Message.svg";
import Call from "../../../assets/images/footer-Call.svg";

import Category1 from "../../../assets/images/footer-m-Category.svg";
import Category2 from "../../../assets/images/Category.png";
import Discount1 from "../../../assets/images/Feed.svg";
import Discount2 from "../../../assets/images/rss (1) 1@2x.png";
import Heart1 from "../../../assets/images/footer-m-Heart.svg";
import Heart2 from "../../../assets/images/Heart@2x.png";
import Home1 from "../../../assets/images/footer-m-Home.svg";
import Disabled1 from "../../../assets/images/Sign In.svg";
import Disabled2 from "../../../assets/images/Login Active.png";

import axios from "axios";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { connect } from 'react-redux';

class Footer extends Component {

  state = {
    email: '',
    emailError: '',
    cartCount: '',
    loggedIN: false,
    activeStatus: "Home",
    isTop: true,
    scrollTo: false
  }

  onChange = (e) => {
    const { name, value } = e.target;
    this.setState({
      [name]: value
    });
  }

  validate = () => {
    this.setState({
      emailError: ''
    });

    const regex = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
    let emailError = '';

    if (this.state.email.length < 1) {
      emailError = "Please enter your email";
    } else if (!regex.test(this.state.email.trim())) {
      emailError = "Please enter a valid Email"
    }
    if (emailError) {
      this.setState({ emailError })
      return false
    }
  }
  componentDidUpdate(prevProps) {
    if (this.props.CartProducts.cartCount !== prevProps.CartProducts.cartCount) {
      this.componentDidMount()
    }
  }

  onSubmit = async (e) => {

    e.preventDefault();
    // if (checkValidation) {
    const { email } = this.state;
    let newsLetterData = {
      "email": email
    };

    // console.log(newsLetterData);

    await axios.post(`${window.$URL}user/newsLetter`, newsLetterData)
      .then(response => {
        if (response.status === 200) {
          if (response.data.success) {
            toast.success(response.data.message, {
              position: "bottom-center",
              autoClose: 3000,
              hideProgressBar: true,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined
            })
          } else {
            toast.error(response.data.message, {
              position: "bottom-center",
              autoClose: 3000,
              hideProgressBar: true,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            })
          }
        }
      })
      .catch(error => {
        console.log(error);
      })
    // }
  }

  async componentDidMount() {
    window.addEventListener('scroll', this.listenToScroll)
    window.scrollTo(0, 0)
    document.addEventListener('scroll', () => {
      const isTop = window.scrollY < 100;
      if (isTop !== this.state.isTop) {
          this.setState({ isTop })
      }
    });
    if (window.location.href.includes('categories')) {
      localStorage.setItem("activeStatus", "")
      this.setState({ activeStatus: "categories" })
    } else if (window.location.href.includes('cart')) {
      localStorage.setItem("activeStatus", "")
      this.setState({ activeStatus: "cart" })
    } else if (window.location.href.includes('myProfile')) {
      this.setState({ activeStatus: "myProfile" })
    } else if (window.location.href) {
      this.setState({ activeStatus: "Home" })
    }
    let checkToken = await localStorage.getItem("token");
    this.setState({
      loggedIN: checkToken ? true : false,
    })

  }

  listenToScroll = () => {
    const winScroll =
      document.body.scrollTop || document.documentElement.scrollTop
  
    const height =
      document.documentElement.scrollHeight -
      document.documentElement.clientHeight
  
    const scrolled = winScroll / height
    this.setState({
        scrollTo: Number(scrolled.toFixed(2)) > 0.73
    })
    console.log("scrolled ---->", Number(scrolled.toFixed(2)) > 0.63);
  }

  render() {
    const { emailError, loggedIN, scrollTo } = this.state;
    const { CartProducts } = this.props;
    //console.log("scrollTo ----->", scrollTo);
    return (
      <div className="all-footer">
        <ToastContainer />
        <section className="footer-section">
          <div className="download-app-container">
            <div className="container">
              {/* <div className="download-content">
                <h2>Download The Mymart App</h2>
                <p>
                  The passage is attributed to an unknown typesetter in the 15th
                  century who is thought to have scrabled parts of Cicero's De
                  Finibus Bonorum et Malorum for use in a type specimen book.
                </p>
                <div className="download-button">
                  <a href="#/" className="btn playstore ">
                    <img
                      src={playStoreIcon}
                      className="img-responsive"
                      alt={playStoreIcon}
                    />
                    <div className="text">
                      <span className="download_on">Download on the</span>
                      <p>Google Play</p>
                    </div>
                  </a>
                  <a href="#/" className="btn playstore">
                    <img
                      src={AppStoreIcon}
                      className="img-responsive"
                      alt={AppStoreIcon}
                    />
                    <div className="text">
                      <span className="download_on">Download on the</span>
                      <p>Apple Store</p>
                    </div>
                  </a>
                </div>
              </div> */}
            </div>
          </div>
          {/* <div className="container">
            <div className="card news_letter">
              <div className="row">
                <div className="col-md-6">
                  <div className="signup_portion">
                    <img
                      src={envelop}
                      className="img-responsive"
                      alt={envelop}
                    />
                    <p>Sign Up For Newsletter</p>
                  </div>
                </div>

                <div className="col-md-6">
                  <div className="news-letterform">
                    <form onSubmit={this.onSubmit} className="input-withButton">
                      <input className="form-control" type="email" placeholder="Please enter your email" name="email" onChange={this.onChange}
                      />
                      {emailError ? <span style={{ color: 'red' }}> {emailError} </span> : null}
                      <button className="btn search_button">
                        Submit
                      </button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div> */}
          <footer className="footer_last_before">
            <div className="container">
              <div className="footer-wrapper">
                <div className="footer-item">
                  <ul className="nav flex-column">
                    <h2>information</h2>
                    <li className="nav-item">
                      <a className="nav-link " href={`${process.env.PUBLIC_URL}/about-us`}>
                        About Us
                      </a>
                    </li>
                    {/* <li className="nav-item">
                      <a className="nav-link" href="#/">
                        Offerst
                      </a>
                    </li> */}
                    <li className="nav-item">
                      <a className="nav-link" href={`${process.env.PUBLIC_URL}/`}>
                        New &amp; Trendin Product
                      </a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link" href={`${process.env.PUBLIC_URL}/`}>
                        Best Sellers
                      </a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link" href={`${process.env.PUBLIC_URL}/customer-faqs`}>
                        {" "}
                        Customer FAQs{" "}
                      </a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link" href={`${process.env.PUBLIC_URL}/seller-faqs`}>
                        Seller's FAQs
                      </a>
                    </li>
                  </ul>
                </div>
                <div className="footer-item">
                  <ul className="nav flex-column">
                    <h2>Other Links</h2>
                    <li className="nav-item">
                      <a className="nav-link" href="http://mymart.qa/Seller/why-join-mymart" target="_blank" rel="noopener noreferrer">
                        Sell on myMart Qatar
                      </a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link" href={`${process.env.PUBLIC_URL}/legal-notice`}>
                        {" "}
                        Legal Notice
                      </a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link" href={`${process.env.PUBLIC_URL}/Terms-and-Condition`}>
                        Terms &amp; conditions
                      </a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link" href={`${process.env.PUBLIC_URL}/Privacy-Policy`}>
                        {" "}
                        Privacy Policy
                      </a>
                    </li>
                    {/* <li className="nav-item">
                      <a className="nav-link" href="#/">
                        Secure payment
                      </a>
                    </li> */}
                  </ul>
                </div>
                <div className="footer-item">
                  <ul className="nav flex-column">
                    <h2>Your Account</h2>
                    <li className="nav-item">
                      <a className="nav-link  " href={loggedIN ? `${process.env.PUBLIC_URL}/myProfile` : `${process.env.PUBLIC_URL}/login`} >
                        Personal info
                      </a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link" href={loggedIN ? `${process.env.PUBLIC_URL}/myOrder` : `${process.env.PUBLIC_URL}/login`}>
                        {" "}
                        Orders{" "}
                      </a>
                    </li>
                    {/* <li className="nav-item">
                      <a className="nav-link" href="#/">
                        Credit slips
                      </a>
                    </li> */}
                    <li className="nav-item">
                      <a className="nav-link" href={loggedIN ? `${process.env.PUBLIC_URL}/address-book` : `${process.env.PUBLIC_URL}/login`}>
                        {" "}
                        Addresses{" "}
                      </a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link" href={loggedIN ? `${process.env.PUBLIC_URL}/myWishlist` : `${process.env.PUBLIC_URL}/login`}>
                        My wishlists
                      </a>
                    </li>
                  </ul>
                </div>
                <div className="footer-item">
                  <ul className="nav flex-column">
                    <h2>Help Links</h2>
                    <li className="nav-item">
                      <a className="nav-link  text-red" href={`${process.env.PUBLIC_URL}/shipping-delivery`}>
                        Shipping & Delivery
                      </a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link  text-red " href={`${process.env.PUBLIC_URL}/payment-methods`}>
                        Payment Methods
                      </a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link  text-red " href={`${process.env.PUBLIC_URL}/returns-refunds`}>
                        Returns & Refunds
                      </a>
                    </li>
                    {/* <li className="nav-item">
                      <a className="nav-link  " href={`${process.env.PUBLIC_URL}/contact-us`}>
                        Contact us
                      </a>
                    </li> */}
                    {/* <li className="nav-item">
                      <a className="nav-link" href={`${process.env.PUBLIC_URL}/customer-faqs`}>
                        {" "}
                        Customer FAQs{" "}
                      </a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link" href={`${process.env.PUBLIC_URL}/seller-faqs`}>
                        Seller's FAQs
                      </a>
                    </li> */}
                  </ul>
                </div>
                <div className="footer-item">
                  <ul className="nav flex-column">
                    <h2>Contact Us</h2>
                    <li className="nav-item">
                      <a className="nav-link  " href="#/">
                        <img src={Location} src={Location} />{" "}
                        Doha, Qatar
                      </a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link" href="#/">
                        <img src={Call} src={Call} />{" "}
                        +974 5029 0202 ( call / whatsapp )
                      </a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link" href="mailto:support@mymart.qa">
                        <img src={Message} src={Message} />{" "}
                        support@mymart.qa
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <a href="#/" id="back-to-top" title="Back to top">
              <i className="fa fa-angle-up" aria-hidden="true" />
              <span style={{ display: "none" }}>Back To Top</span>
            </a>
          </footer>

          <div className="footer-bottom">
            <div className="container">
              <div className="footer-chat-sec">
                <div className="online-chat">
                  <div className="onlin-chat-img">
                    <img src={Headphone} alt={Headphone} />
                  </div>
                  <div className="online-text">
                    <span>Have any question?</span>
                    <span className="chat-now">Chat Now (Online)</span>
                  </div>
                </div>
                <div className="live-chat">
                  <button className="btn">LIVE CHAT</button>
                </div>
              </div>
              <div className="row">
                <div className="col-md-6 col-sm-6">
                  <div className="left-logo-bar">
                    <div className="footer-logo">
                      <img src={mainLogo} alt={mainLogo} />
                    </div>
                    <div className="copy-right ">
                      <p>
                        {" "}
                        © 2021 myMart.qa. All rights reserved.
                      </p>
                    </div>

                    <div className="social-wrapper">
                      <a className="nav-link icon_bg" href="#/">
                        <i className="fa fa-facebook" aria-hidden="true" />
                        <span style={{ display: "none" }}>Facebook</span>
                      </a>
                      <a className="nav-link icon_bg" href="#/">
                        <i className="fa fa-twitter" aria-hidden="true" />
                        <span style={{ display: "none" }}>Twitter</span>
                      </a>
                      <a className="nav-link icon_bg" href="#/">
                        <i className="fa fa-instagram" aria-hidden="true" />
                        <span style={{ display: "none" }}>Instagram</span>
                      </a>
                      <a className="nav-link icon_bg" href="#/">
                        <i class="fa fa-whatsapp" aria-hidden="true"></i>
                        <span style={{ display: "none" }}>whatsapp</span>
                      </a>
                    </div>
                  </div>
                </div>
                <div className="col-md-6 col-sm-6">
                  <div className="row">
                    <div className="col-md-9 col-sm-9">
                      <div className="download-box">
                        <div className="download-now-inner">
                          <div className="download-number">
                            <label>Download Now</label>
                            <form className="download-now" action="">
                              <input placeholder="Your mobile number" className="number-input" type="number" />
                              <button className="btn">Download</button>
                            </form>
                          </div>
                          <div className="play-store-img">
                            <a href="#"><img src={AppStotr} alt={AppStotr} /></a>
                            <a href="#"><img src={PlayStore} alt={PlayStore} /></a>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-3 col-sm-3">
                      <div className="qr-code">
                        <label>Scan QR Code</label>
                        <a href="#"><img src={QRCode} alt={QRCode} /></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <div className="sticky-footer">
          <ul className="nav nav-pills nav-fill">
            <li className="nav-item">
              <a className={this.state.activeStatus === "Home" ? "nav-link active" : "nav-link"} href={`${process.env.PUBLIC_URL}/`} onClick={() => localStorage.setItem("activeStatus", "")}>
                {this.state.activeStatus === "Home" ? <img src={Home1} alt={Home1} /> : <img src={Home1} alt={Home1} />}
                <span>  Home </span>

              </a>
            </li>
            <li className="nav-item">
              <a className={this.state.activeStatus === "categories" ? "nav-link active" : "nav-link"} href={`${process.env.PUBLIC_URL}/categories`} onClick={() => localStorage.setItem("activeStatus", "")}>
                {this.state.activeStatus === "categories" ? <img src={Category2} alt={Category2} /> : <img src={Category1} alt={Category1} />}
                <span>Categories</span>
                {/* <span>Categories</span> */}
              </a>
            </li>
            <li className="nav-item">
              <a className={this.state.activeStatus === "cart" ? "nav-link active" : "nav-link"} href={`${process.env.PUBLIC_URL}/categories`} href={`${process.env.PUBLIC_URL}/cart`} onClick={() => localStorage.setItem("activeStatus", "")}>
                {this.state.activeStatus === "cart" ? <img src={Heart2} alt={Heart2} /> : <img src={Heart1} alt={Heart1} />}
                <span>Favourite</span>
                {/* {
                  <span className="cart-value">{this.props.CartProducts.cartCount}</span>

                } */}
                {/* <span>Cart</span> */}
              </a>
            </li>
            <li className="nav-item">
              <a className={this.state.activeStatus === "myProfile" ? "nav-link active" : "nav-link"} href={loggedIN ? `${process.env.PUBLIC_URL}/myProfile` : `${process.env.PUBLIC_URL}/login`} onClick={() => localStorage.setItem("activeStatus", "")}>
                {this.state.activeStatus === "myProfile" ? <img src={Discount2} alt={Discount2} /> : <img src={Discount1} alt={Discount1} />}
                <span>Feed</span>
                {/* <span>Account</span> */}
              </a>
            </li>
            <li className="nav-item">
              <a className={localStorage.getItem("activeStatus") === "Login" ? "nav-link active" : "nav-link"} href={loggedIN ? `${process.env.PUBLIC_URL}/myProfile` : `${process.env.PUBLIC_URL}/login`} onClick={() => localStorage.setItem("activeStatus", "Login")}>
                {localStorage.getItem("activeStatus") === "Login" ? <img src={Disabled2} alt={Disabled2} /> : <img src={Disabled1} alt={Disabled1} />}
                <span>Login</span>
                {/* <span>Account</span> */}
              </a>
            </li>
          </ul>
        </div>
        {!this.state.isTop && <div  className= {scrollTo ? "main-up-arrow scroll" : "main-up-arrow"}>
          <a onClick={() => window.scrollTo(0, 0)}><i class="fa fa-angle-up" aria-hidden="true"></i></a>
        </div>}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  CartProducts: state.Cart,
})

export default connect(
  mapStateToProps,
  { UpdateCart }
)(Footer);
