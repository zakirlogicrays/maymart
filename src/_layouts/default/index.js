import React, {useEffect} from "react";
import PropTypes from "prop-types";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.js";


import '../../assets/css/bootstrap.min.css';
import '../../assets/css/bootstrap-glyphicons.min.css';
import '../../assets/css/custom-checkbox.css';
import '../../assets/css/font-awesome.min.css';
import '../../assets/css/global-style.css';
import '../../assets/css/media-queries.css';
import '../../assets/css/responsive.css';
import '../../assets/css/time-slot.css';
import "./style.css";

import Header from './Header';
import Footer from './Footer';

export default function DefaultLayout({ children }) {
  const trackScrolling = (e) => {
    const bottom = e.target.scrollHeight - e.target.scrollTop === e.target.clientHeight;
    console.log("bottom -------------->", bottom);
    console.log("clientHeight ------->", e.target.clientHeight)
  };
  return (
    <>
    <div>
      <Header />
      <div className="body-block">
        <div className="container new-bg">
          {children}
        </div>
      </div>
      <Footer />
      </div>
    </>
  );
}

DefaultLayout.propTypes = {
  children: PropTypes.element.isRequired
};
