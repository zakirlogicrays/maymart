import React, { Component } from "react";
import axios from "axios";
import jwt_decode from "jwt-decode";
import mainLogo from "../../../assets/images/myMart-logo.svg";
import userIcon from "../../../assets/images/user-icon.svg";
import LocationIcon from "../../../assets/images/location-icon.svg";
import menuIcon from "../../../assets/images/mobile-menu-icon.svg";
import arrowLeft from "../../../assets/images/Arrow - Left.png";
import threeDots from "../../../assets/images/three dots.png";
import CloseSquare from "../../../assets/images/Close-Square.png";
import cart_icon2 from "../../../assets/images/cart-icon-2.svg";
import cartMobileIcon from "../../../assets/images/mobile-cart-icon.svg";
import Message from "../../../assets/images/Message.svg";
import Notification from "../../../assets/images/Notification.svg";
import { connect } from 'react-redux';
import { UpdateLocation } from '../../../data/reducers/location';
import { UpdateCart, UpdatelogoutCart } from '../../../data/reducers/cart';
import AddNewAddress from "../../../Component/AddNewAddress";
import groceriesImg from "../../../assets/images/grosery-nav_header.png";
import myMallImg from "../../../assets/images/Ebene 1.png";
import MoreMenuIcon from "../../../assets/images/More Square.png";
import marketplaceImg from "../../../assets/images/marketplace_nav_header.png";
import SearchResultIcon from "../../../assets/images/search-result-icon.svg";
import RightArrow from "../../../assets/images/Arrow - Right.svg";

import AccountDropIcon1 from "../../../assets/images/Bag.png";
import AccountDropIcon2 from "../../../assets/images/Heart.png";
import AccountDropIcon3 from "../../../assets/images/Vector.png";
import AccountDropIcon4 from "../../../assets/images/Star.png";
import AccountDropIcon5 from "../../../assets/images/return (2) 1.png";
import AccountDropIcon6 from "../../../assets/images/Logout.png";
import AccountDropIcon7 from "../../../assets/images/user-right-icon.png";

import MoreIcon1 from "../../../assets/images/more-heart.png";
import MoreIcon2 from "../../../assets/images/more-icon2.png";
import MoreIcon3 from "../../../assets/images/more-icon3.png";

import SearchForMobile from "../../../Component/SearchModalForMobile";

import Saudi from "../../../assets/images/saudi-arabia 1.png";

import RightMobileMenuIcon1 from "../../../assets/images/grocery-right-mobile-menu-icon1.png";
import RightMobileMenuIcon2 from "../../../assets/images/grocery-right-mobile-menu-icon2.png";
import RightMobileMenuIcon3 from "../../../assets/images/grocery-right-mobile-menu-icon3.png";
import RightMobileMenuIcon4 from "../../../assets/images/grocery-right-mobile-menu-icon4.png";
import RightMobileMenuIcon5 from "../../../assets/images/grocery-right-mobile-menu-icon5.png";
import RightMobileMenuIcon6 from "../../../assets/images/grocery-right-mobile-menu-icon6.png";
import RightMobileMenuIcon7 from "../../../assets/images/grocery-right-mobile-menu-icon7.png";
import RightMobileMenuIcon8 from "../../../assets/images/grocery-right-mobile-menu-icon8.png";
import RightMobileMenuIcon9 from "../../../assets/images/grocery-right-mobile-menu-icon9.png";
import RightMobileMenuIcon10 from "../../../assets/images/grocery-right-mobile-menu-icon10.png";

import Switch from 'rc-switch';
import { Line } from 'rc-progress';


import {
  Accordion,
  AccordionItem,
  AccordionItemButton,
  AccordionItemHeading,
  AccordionItemPanel,
} from 'react-accessible-accordion';
import TabsIcon1 from "../../../assets/images/ca-tabs-1.svg";


// import ReactMegaMenu from "react-mega-menu"


class Header extends Component {
  constructor(props) {
    super(props);
    this.wrapperRef = React.createRef();
    this.handleClickOutside = this.handleClickOutside.bind(this);
  }

  state = { menuOpen: false, mobileMenuOpen: false };

  smallMenu = () => {
    this.setState({ menuOpen: !this.state.menuOpen });
  }
  mobileMenu = () => {
    this.setState({ mobileMenuOpen: !this.state.mobileMenuOpen });
  }


  state = {
    loggedIn: false,
    showList: true,
    languageDropdown: false,
    selectedLanguage: "en",
    userName: "",
    cartCount: 0,
    toggleShow: [],
    searchedArray: [],
    categoryLevels: null,
    searchKeyword: '',
    showLocationModal: false,
    pageName: window.location.pathname.split("/"),
    showUpcomingModal: false,
    ModalMsg: '',
    fullArray: [],
    subCategory: [],
    detailofCategory: [],
    subsubCategory: [],
    selectedCategory: [],
    selectedGroceries: "",
    brandBorder: false,
    userDropdownOpen: false,
    onPage: false,
    morePopUp: false,
    selectedLevel1: undefined,
    popularCategory: [],
    rendomPopularCategory: undefined,
    searchModal: false,
    thirdLevelCategories: [],
    level1Name: undefined,
    fourthlevelCategories: [],
    GroceriesMyMallSelected: undefined,
    threeDotsMenueOpen: false
  };

  async componentDidMount() {

    await this.setState({ onPage: window.location.href.includes("/categories") })
    //document.addEventListener('mousedown', this.handleClickOutside);
    this.getUserDetails();
    //this.props.UpdateCart();

    if (this.state.loggedIn) {
      console.log("here in logggIn");
      this.props.UpdateCart();
    } else {

    }

    var addScript = document.createElement("script");
    addScript.setAttribute(
      "src",
      "//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"
    );
    document.body.appendChild(addScript);
    window.googleTranslateElementInit = this.googleTranslateElementInit;
    this.getPopularCategories();
    this.getCategoryLevels();
    this.getDynamicTitle();
    this.getAllData();
  }

  getPopularCategories = async () => {

    await axios.post(`${window.$URL}popular-category/fetch-popular-catetgory-by-pagename`, { pageName: 'Homepage' })
      .then(async response => {
        if (response.data.success) {
          await this.setState({
            popularCategory: response.data.data,
          }, () => {
            this.setState({
              rendomPopularCategory: this.state?.popularCategory[Math.floor(Math.random() * this.state?.popularCategory.length)]?.categoryName
            })
          })

          // console.log("this.state?.popularCategory ----->", Math.floor(Math.random() * this.state?.popularCategory.length));
          // localStorage.setItem("randomCategoryHeader", this.state?.popularCategory[Math.floor(Math.random() * this.state?.popularCategory.length)]?.categoryName)}
        }
      })
      .catch(error => {
        console.log(error)
      })
  }

  getDynamicTitle = async () => {
    let pathName = window.location.pathname.split("/");
    var getTitle = pathName[pathName.length - 1];
    var productListingPageName = pathName[pathName.length - 3];
    var titleSpaceRemove = getTitle.replace(/%20/g, "-");
    //console.log(getTitle,'titleSpaceRemove')
    var capitalizeTitle = this.Capitalize(titleSpaceRemove);
    console.log(capitalizeTitle, 'capitalizeTitle')
    console.log(pathName[pathName.length - 3], 'pathName')
    if (getTitle !== '' && getTitle !== 'grocery' && getTitle !== 'myMall' && getTitle !== 'marketplace' && !productListingPageName == 'productListing') {
      let title = `${capitalizeTitle} | Qatar Online Shopping, Grocery & Marketplace | myMart.qa`;
      let metaDescription = '';
      document.title = title;
      document.querySelector('meta[name="description"]').setAttribute('content', metaDescription);
    } else if (getTitle === 'grocery') {
      let title = `myMart.qa : Online Grocery Shopping Qatar - shop from your nearest grocery store or get best offers from entire country - Baladna, QBake, Dandy and more of your favorite brands`;
      let metaDescription = '';
      document.title = title;
      document.querySelector('meta[name="description"]').setAttribute('content', metaDescription);
    }
    else if (getTitle === 'MyMall') {
      let title = `myMart.qa : Online Shopping Qatar - Shop Genuine products from this online mall with 14 Days Return and money back guarantee`;
      let metaDescription = '';
      document.title = title;
      document.querySelector('meta[name="description"]').setAttribute('content', metaDescription);
    }
    else if (getTitle === 'marketplace') {
      let title = `myMart.qa : Online Shopping Qatar - shop from the marketplace with a wide variety of products available in Doha.`;
      let metaDescription = '';
      document.title = title;
      document.querySelector('meta[name="description"]').setAttribute('content', metaDescription);
    }
    else if (productListingPageName === 'productListing') {
      var titleSpaceRemoveWithOutCategory = pathName[pathName.length - 2].replace(/%20/g, "-");
      var titleSpaceRemoveWithCategory = pathName[pathName.length - 1].replace(/%20/g, "-");


      if (pathName[pathName.length - 2] !== 'category') {
        var capitalizeTitleListing = this.Capitalize(titleSpaceRemoveWithOutCategory);
      } else {
        var capitalizeTitleListing = this.Capitalize(titleSpaceRemoveWithCategory);

      }
      let title = `Buy ${capitalizeTitleListing} online | myMart.qa`;
      let metaDescription = '';
      document.title = title;
      document.querySelector('meta[name="description"]').setAttribute('content', metaDescription);

    }
    else {
      let title = `myMart.qa : Online Shopping Qatar - Electronics, Home Appliances, Groceries, Mobiles, Tablets & more`;
      let metaDescription = '';
      document.title = title;
      document.querySelector('meta[name="description"]').setAttribute('content', metaDescription);
    }
  };

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
  }

  handleClickOutside(event) {
    if (this.wrapperRef && !this.wrapperRef.current.contains(event.target)) {
      this.setState({
        searchedArray: [],
        showList: false
      })
    }
  }

  Capitalize(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }
  // componentDidUpdate(prevProps) {
  //   if (this.props.CartProducts.cartCount !== prevProps.CartProducts.cartCount) {
  //     this.componentDidMount()
  //   }
  // }
  hideComingModal = () => {
    this.setState(
      {
        showUpcomingModal: false,
      },
      () => {
        //this.componentDidMount();
      }
    );
  };
  showUpcomingModalStatus = (msg) => {
    this.setState(
      {
        showUpcomingModal: true,
        ModalMsg: msg
      },
      () => {
        //this.componentDidMount();
      }
    );
  };

  getCategoryLevels = async () => {
    await axios.get(`${window.$URL}category/categoryLevels`)
      .then(response => {
        if (response.data.success) {
          this.setState({
            categoryLevels: response.data.response
          })
          //console.log("categoryLevels ----->", this.state.categoryLevels)
        }
      })
      .catch(error => {
        console.log(error);
      })
  }

  googleTranslateElementInit() {
    //alert("test2")
    /* eslint-disable no-new */
    new window.google.translate.TranslateElement(
      {
        pageLanguage: "en",
        includedLanguages: "ar,en",
        autoDisplay: false,
        layout: window.google.translate.TranslateElement.FloatPosition.TOP_LEFT,
      },
      "google_translate_element"
    );

  }

  openLanguage = () => {

    var selectedLanguage = new window.google.translate.TranslateElement(
      {
        pageLanguage: "en",
        includedLanguages: "ar,en",
        autoDisplay: false,
        layout: window.google.translate.TranslateElement.FloatPosition.TOP_LEFT,
      },
      "google_translate_element"
    );
    console.log(selectedLanguage.j)
    setTimeout(() => {
      localStorage.setItem('language', selectedLanguage.j);
      window.location.reload();
    }, 500)
  };

  getUserDetails = async () => {
    let checkToken = await localStorage.getItem("token");
    if (checkToken) {
      const decoded = jwt_decode(checkToken);
      if (decoded.userType === "customer") {
        await axios.get(`${window.$URL}user/Details/${decoded.userid}`)
          .then((res) => {
            if (res.status === 200) {
              this.setState({
                loggedIn: true,
                userName: res.data.Firstname,
              });
            }
          })
          .catch((error) => {
            console.log(error);
          });

        if (this.props.Location.locationCoordinates.length < 1) {
          await axios.post(`${window.$URL}userAddress/fetch-user-address/`, { customerId: decoded.userid })
            .then((res) => {
              if (res.status === 200) {
                let filterDefaultAddress = res.data.data.filter((eachAddress) => {
                  return eachAddress.Default === true
                })
                if (filterDefaultAddress.length > 0) {
                  let detailsArray = [];
                  if (filterDefaultAddress[0].AddressType === "Choose on Map") {
                    let location = {
                      lat: filterDefaultAddress[0].chooseOnMapLocation.coordinates[1],
                      lng: filterDefaultAddress[0].chooseOnMapLocation.coordinates[0],
                    }
                    detailsArray = [{
                      coordinates: location,
                      name: filterDefaultAddress[0].Address
                    }]
                  } else {
                    let location = {
                      lat: filterDefaultAddress[0].Location.coordinates[1],
                      lng: filterDefaultAddress[0].Location.coordinates[0],
                    }
                    detailsArray = [{
                      coordinates: location,
                      name: filterDefaultAddress[0].Zone + filterDefaultAddress[0].Street + filterDefaultAddress[0].Building
                    }]
                  }
                  this.props.UpdateLocation(detailsArray);
                } else {

                }
              }
            })
            .catch((error) => {
              console.log(error);
            });
        }

      }
    } else {
      this.setState({
        loggedIn: false,
      });
    }
  };

  logout = async () => {
    this.props.UpdatelogoutCart()
    await localStorage.removeItem("token");
    this.setState({ userDropdownOpen: false })
    window.location.reload();
  };

  searchKey = async (e) => {
    const { value } = e.target;
    this.setState({
      showList: true
    })
    if (value.length > 0 && this.state.showList) {
      await axios.get(`https://mymart.qa:7775/api/search/searchKeyword/${value}`)
        .then(response => {
          if (response.status === 200) {
            this.setState({
              searchedArray: response.data,
              searchKeyword: value
            })
          }
        })
        .catch(error => {
          console.log(error);
        })
    } else {
      this.setState({
        searchedArray: [],
      })
    }
  }

  getHighlightedText(name) {
    const { searchKeyword } = this.state;
    if (searchKeyword.length > 1) {
      const parts = name?.split(new RegExp(`(${searchKeyword})`, 'gi'));
      return <span>{parts?.map(part => part.toLowerCase() === searchKeyword.toLowerCase() ? <b style={{ fontSize: 16 }}>{part}</b> : part)}</span>;
    } else {
      return (
        name
      )
    }
  }

  openLocationModal = () => {
    this.setState({
      showLocationModal: true
    })
  }

  hideLocModal = () => {
    this.setState({
      showLocationModal: false
    })
  }

  getAllData = async (level1) => {
    await this.setState({level1Name: level1});
    await axios
      .get(`${window.$URL}category/categoryinfo`)
      .then((response) => {
        if (response.data.success) {
          let sortArray = response.data?.response?.filter((item) => item.parent === level1 && item.categoryLevel[0] !== "" ? item : null)
          this.setState({
            fullArray: response.data?.response,
            selectedCategory: sortArray,
            selectedLevel1: level1
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  onCategoryClick = async (value) => {
    this.setState({fourthlevelCategories: []})
    let subCategory = this.state.fullArray?.filter((item) => item.parent === value ? item : null)
    await this.setState({
      subCategory: subCategory
    })
    let thirdLevelCategorie = await this.state.fullArray?.filter((item) => ((item.categoryLevel[0].split(",")[0].toString() === this.state.level1Name && item.parent === value) && item))
    await this.setState({thirdLevelCategories: thirdLevelCategorie});
    let fourthlevelCategories = await this.state.fullArray?.filter((item) => (
      thirdLevelCategorie?.filter((item1) => (
        item1.categoryName === item.parent ? 
          this.setState({fourthlevelCategories: [...this.state.fourthlevelCategories, item]}) : 
          null
        ))))
  }

  onSubCtegoryClicked = async (value) => {
    let subCategory = this.state.fullArray?.filter((item) => item.parent === value ? item : null)
    await this.setState({
      detailofCategory: subCategory
    })
    let subsubCategory = this.state.fullArray?.filter((item) => item.parent === value ?
      (this.state.fullArray?.filter((item1, idx) => item1.parent === item.categoryName ? this.setState({
        subsubCategory: [...this.state.subsubCategory, item1]
      }) : null))
      : null)

    let removedDuplicatValue = this.state.subsubCategory.filter((ele, ind) => ind === this.state.subsubCategory.findIndex(elem => elem.categoryName === ele.categoryName && elem.parent === ele.parent))
    this.setState({
      subsubCategory: removedDuplicatValue
    })
  }

  navigatePage = (e) => {
    e.preventDefault();
    const { searchedArray } = this.state;
    if (searchedArray.length > 0) {
      let data = searchedArray[0];
      let reference = '';
      if (data.type === 'Category') {
        reference = `${process.env.PUBLIC_URL}/productListing/category/${data._id}`;
      } else if (data.type === 'Brand') {
        reference = `${process.env.PUBLIC_URL}/productListing/brand/${data._id}`;
      } else if (data.type === 'Store') {
        reference = `${process.env.PUBLIC_URL}/s
        toreDetails/${data._id}`;
      } else if (data.type === 'Product') {
        reference = `${process.env.PUBLIC_URL}/productDetails/${data._id}`;
      }
      window.location.href = reference;
    }
  }

  handleSearchClicked = (data) => {
    console.log("here ----->", data);
    const bodyData = { userId: "5f173552743dec5203b612c5", keyword: data._id }
    axios.post('https://mymart.qa:7775/api/product/addSearchKeyword', bodyData)
      .then(response => {
        this.setState({
          searchedArray: [],
          showList: false
        })
      });
  }

  render() {
    const menuOpen = this.state.menuOpen;
    const mobileMenuOpen = this.state.mobileMenuOpen;
    const { loggedIn, userName, searchedArray, cartCount, categoryLevels, showLocationModal, pageName, showUpcomingModal } = this.state;
    const { CartProducts, Location } = this.props;
    return (
      <section className={window.location.href.includes("groceries") ? "header-section grocery-header-web" : "header-section" }>
        {showUpcomingModal ? (
          <div
            id="exampleModalCenter"
            className="modal fade show"
            tabIndex={-1}
            role="dialog"
            aria-labelledby="exampleModalCenterTitle"
            style={{ display: "block", paddingRight: 17 }}
          >
            <div
              className="modal-dialog modal-dialog-centered"
              role="document"
            >
              <div className="modal-content">
                <div className="modal-header">
                  {/* <h5 className="modal-title" id="exampleModalCenterTitle">Page Status</h5> */}
                  <button type="button" onClick={() => this.hideComingModal()} className="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div className="modal-body">
                  {/* <p>Promotions is Comming...</p> */}
                  <h5 className="modal-title" id="exampleModalCenterTitle">
                    {this.state.ModalMsg} coming soon...
                  </h5>
                </div>
                {/* <div className="modal-footer">
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" className="btn btn-primary">Save changes</button>
                                    </div> */}
              </div>
            </div>
          </div>
        ) : (
          ""
        )}
        <div className="container miniView desktop-header">
          <div className="top_header">
            <div className="main_top">
              <div className="row">
                <div className="col-md-3 col-sm-3">
                  <div className="logo-category">
                    <div className="logo">
                      <a href={`${process.env.PUBLIC_URL}/`}>
                        <img
                          src={mainLogo}
                          className="img-responsive"
                          alt="mainLogo"
                        />
                      </a>
                    </div>
                    <div className="category-dropdown-change">
                      <nav className="navbar navbar-expand-lg navbar-light category_header ">

                        {/* navs for large view */}
                        <div className="new-header">
                          <a className="navbar-brand mobile-menuicon" href="#/" onClick={this.mobileMenu} >
                            <img src={menuIcon} alt={menuIcon} />
                          </a>
                          <ul className="navbar-nav home_nav">
                            <li className="nav-link">


                              <a className="navbar-brand prev-menu" href="#/" onMouseEnter={() => this.setState({ selectedGroceries: undefined, detailofCategory: [] })}>
                                {/* <img src={menuIcon} alt={menuIcon} /> */}
                                <span className="sm-none"> Categories</span>
                                <i class="fa fa-angle-down" aria-hidden="true"></i>
                              </a>

                              {categoryLevels !== null ?
                                <div className="large-catmenu-view custom-groceries-menu" onMouseLeave={() => this.setState({ detailofCategory: [], selectedCategory: [], thirdLevelCategories: [], fourthlevelCategories: []})}>
                                  <div className="groceries-first-step">
                                    <ul className="menu-step1 navbar-nav flex-column ">
                                      {
                                        Object.keys(categoryLevels).map((level1, level1Index) => {
                                          return (
                                            <a className="nav-link" style={{ display: "flex", flexDirection: "column", alignItems: "flex-start" }}>
                                              <div className="main-category-dropdown">
                                                <a onClick={() => {
                                                  { level1 !== this.state.selectedLevel1 && this.getAllData(level1) }
                                                  {
                                                    level1 !== this.state.selectedLevel1 ? this.setState({
                                                      selectedLevel1: level1
                                                    }) : this.setState({ selectedLevel1: undefined })
                                                  }
                                                }}>{level1}{this.state.selectedLevel1 !== level1 ? <i className="fa fa-angle-right" aria-hidden="true"></i> : <i className="fa fa-angle-down" aria-hidden="true"></i>}</a>
                                              </div>
                                              {this.state.selectedLevel1 === level1 && this.state.selectedLevel1 &&
                                                <div className="main-category-dropdown-inner">
                                                  {
                                                    this.state.selectedCategory.map((level1, level1Index) => {
                                                      return (
                                                        <li>
                                                          <a className="nav-link" onClick={() => {
                                                            this.onCategoryClick(level1.categoryName)
                                                            {
                                                              level1.categoryName !== this.state.selectedGroceries ? this.setState({
                                                                selectedGroceries: level1.categoryName
                                                              }) : this.setState({ selectedGroceries: undefined })
                                                            }
                                                          }}>
                                                            {/* {level1} {Object.keys(categoryLevels[level1]).length > 0 ? <i className="fa fa-angle-right" aria-hidden="true"></i> : null} */}
                                                            {level1.categoryName} {this.state.selectedGroceries !== level1.categoryName ? <i className="fa fa-angle-right" aria-hidden="true"></i> : <i className="fa fa-angle-down" aria-hidden="true"></i>}
                                                          </a>
                                                        </li>
                                                      )
                                                    })
                                                  }
                                                </div>}
                                            </a>
                                          )
                                        })
                                      }
                                    </ul>
                                  </div>
                                  <div onMouseLeave={() => this.setState({thirdLevelCategories: [], fourthlevelCategories: []})}>
                                    {this.state.thirdLevelCategories.length > 0 && <ul className="menu-step2 groceries-first-step3">
                                      <div className="step2-right-bar">
                                        <ul className="step2-right-bar0 step2-right-bar1">
                                          <h2 className="main-category-bar-title">Computer & Accessories</h2>
                                         {this.state.thirdLevelCategories?.map((item) => (
                                           <div className="inner-steps">
                                              <p className="inner-step-1-text">{item.categoryName}</p>
                                              <div>
                                                <div>
                                                  {this.state.fourthlevelCategories?.map((item1) => (
                                                    <div className="inner-step-box">
                                                        {item1.parent === item.categoryName && 
                                                        <p className="inner-step-2-text">{item1.categoryName}</p>}
                                                        {item1.parent === item.categoryName && <div>
                                                          {this.state.fullArray?.map((item2) => (
                                                            item2.parent === item1.categoryName && 
                                                              <p className="inner-step-3-text">{item2.categoryName}</p>
                                                          ))}
                                                        </div>}
                                                    </div>
                                                  ))}
                                                  </div>
                                              </div>
                                           </div>
                                         ))}
                                        </ul>
                                        <ul className="step2-right-bar3 step2-right-bar1">
                                          <div className="populer-product">
                                            <li>Popular Products</li>
                                            <li> <a> <strong>Apple</strong> </a> </li>
                                            <li> <a> <strong>Dell</strong> </a> </li>
                                            <div className="view-all-btn">
                                              <a href="#" className="view-btn">View All <img src={RightArrow} alt={RightArrow} /></a>
                                            </div>
                                          </div>
                                        </ul>
                                      </div>
                                    </ul>}
                                  </div>
                                </div>
                                :
                                null
                              }

                              {/* menu for list view */}
                              {/* {categoryLevels !== null ?
                                <div className="large-catmenu-view">

                                  <ul className="menu-step1 navbar-nav flex-column ">
                                    {
                                      Object.keys(categoryLevels).map((level1, level1Index) => {
                                        return (
                                          <li className="nav-item  " key={level1Index}>
                                            <a className="nav-link" href={Object.keys(categoryLevels[level1]).length > 0 ? '#/' : `${process.env.PUBLIC_URL}/productListing/category/${level1}`}>
                                              {level1} {Object.keys(categoryLevels[level1]).length > 0 ? <i className="fa fa-angle-right" aria-hidden="true"></i> : null}
                                            </a>
                                            {
                                              Object.keys(categoryLevels[level1]).length > 0 ?
                                                <ul className="menu-step2">
                                                  {
                                                    Object.keys(categoryLevels[level1]).map((level2, level2Index) => {
                                                      return (
                                                        <li className="nav-item" key={level2Index}>
                                                          <a className="nav-link" href={Object.keys(categoryLevels[level1][level2]).length > 0 ? '#/' : `${process.env.PUBLIC_URL}/productListing/category/${level2}`}>
                                                            {level2} {Object.keys(categoryLevels[level1][level2]).length > 0 ? <i className="fa fa-angle-right" aria-hidden="true"></i> : null}
                                                          </a>
                                                          {
                                                            Object.keys(categoryLevels[level1][level2]).length > 0 ?
                                                              <ul className="menu-step3">
                                                                {
                                                                  Object.keys(categoryLevels[level1][level2]).map((level3, level3Index) => {
                                                                    return (
                                                                      <li className="nav-item" key={level3Index}>
                                                                        <a className="nav-link" href={Object.keys(categoryLevels[level1][level2][level3]).length > 0 ? '#/' : `${process.env.PUBLIC_URL}/productListing/category/${level3}`}>
                                                                          {level3} {Object.keys(categoryLevels[level1][level2][level3]).length > 0 ? <i className="fa fa-angle-right" aria-hidden="true"></i> : null}
                                                                        </a>
                                                                        {
                                                                          Object.keys(categoryLevels[level1][level2][level3]).length > 0 ?
                                                                            <ul className="menu-step4">
                                                                              {
                                                                                Object.keys(categoryLevels[level1][level2][level3]).map((level4, level4Index) => {
                                                                                  return (
                                                                                    <li className="nav-item" key={level4Index}>
                                                                                      <a className="nav-link" href={Object.keys(categoryLevels[level1][level2][level3][level4]).length > 0 ? '#/' : `${process.env.PUBLIC_URL}/productListing/category/${level4}`}>
                                                                                        {level4} {Object.keys(categoryLevels[level1][level2][level3][level4]).length > 0 ? <i className="fa fa-angle-right" aria-hidden="true"></i> : null}
                                                                                      </a>
                                                                                      {
                                                                                        Object.keys(categoryLevels[level1][level2][level3][level4]).length > 0 ?
                                                                                          <ul className="menu-step5" key={level4}>
                                                                                            {
                                                                                              Object.keys(categoryLevels[level1][level2][level3][level4]).map((level5, level5Index) => {
                                                                                                return (
                                                                                                  <li className="nav-item" key={level5Index}>
                                                                                                    <a className="nav-link" href={`${process.env.PUBLIC_URL}/productListing/category/${level5}`}>
                                                                                                      {level5}
                                                                                                    </a>
                                                                                                  </li>
                                                                                                )
                                                                                              })
                                                                                            }
                                                                                          </ul>
                                                                                          :
                                                                                          null
                                                                                      }
                                                                                    </li>
                                                                                  )
                                                                                })
                                                                              }
                                                                            </ul>
                                                                            :
                                                                            null
                                                                        }
                                                                      </li>
                                                                    )
                                                                  })
                                                                }
                                                              </ul>
                                                              :
                                                              null
                                                          }
                                                        </li>
                                                      )
                                                    })
                                                  }
                                                </ul>
                                                :
                                                null
                                            }


                                          </li>
                                        )
                                      })
                                    }

                                  </ul>
                                </div>
                                :
                                null
                              } */}
                              {/* menu for list view end */}
                            </li>
                          </ul>
                        </div>
                      </nav>
                    </div>
                  </div>
                </div>
                <div className="col-md-5 col-sm-5">
                  <div className="header-search-top">
                    <div className="search-input">
                      <form className="dlf"
                      // onSubmit={this.navigatePage}
                      >
                        <input
                          type="text"
                          className="form-control"
                          placeholder={`Search in ${this.state?.rendomPopularCategory ? this.state?.rendomPopularCategory : ""}`}
                          list="suggestion"
                          onChange={(val) => this.searchKey(val)}
                        />
                        <datalist id="suggestion" ref={this.wrapperRef} style={searchedArray.length > 0 ? { display: 'block' } : { display: 'none' }}>
                          <div className="suggestion-layer">
                            <div className="default-search-up">
                              {
                                searchedArray.slice(0, 5)?.map((data, index) => {
                                  let reference = '';
                                  if (data.type === 'Category') {
                                    reference = `${process.env.PUBLIC_URL}/productListing/category/${data._id}`;
                                  } else if (data.type === 'Brand') {
                                    reference = `${process.env.PUBLIC_URL}/productListing/brand/${data._id}`;
                                  } else if (data.type === 'Store') {
                                    reference = `${process.env.PUBLIC_URL}/storeDetails/${data._id}`;
                                  } else if (data.type === 'Product') {
                                    reference = `${process.env.PUBLIC_URL}/productDetails/${data._id}`;
                                  }
                                  return (
                                    <div>
                                      {data?.name && <a 
                                        //href={reference} 
                                        key={index} style={index === 0 ? {} : {}} onClick={() => this.handleSearchClicked(data)}
                                      >
                                        <span className="search-title" ><img src={SearchResultIcon} alt={SearchResultIcon} /> {this.getHighlightedText(data?.name?.split("-")[0])}</span> <span className="search-text"> {data.category !== undefined && data.category !== null ? ` in ${data.category.split(",")[1]}` : null}</span>
                                      </a>}
                                    </div>
                                  )
                                })
                              }
                            </div>
                            <div className="searched-item1 searched-item" onClick={() => console.log("herer")}>{
                              searchedArray?.map((data, index) => {
                                return (
                                  <div style={{ flexDirection: "row", display: "flex", borderTop: (index === 0 && data?.sellerImage?.originalURL) && "solid 1px #ddd" }} key={index}>
                                    {data?.sellerImage?.originalURL && <img src={`${"https://mymart.qa:7779/uploads/"}` + data?.sellerImage?.originalURL} alt={""} style={{ width: 25 }} />}
                                    {data?.sellerImage?.originalURL && <a style={{ color: "#5A5A5A" }}> <strong>{data?.name}</strong> </a>}
                                  </div>
                                )
                              })
                            }
                            </div>
                            <div className="searched-item2 searched-item">{
                              searchedArray?.map((data, index) => {
                                return (
                                  <div style={{ flexDirection: "row", display: "flex", borderTop: index === 0 && "solid 1px #ddd", borderBottom: index === searchedArray.length - 1 && "solid 1px #ddd" }} key={index}>
                                    {data?.brandImage?.originalURL && <img src={`${"https://mymart.qa:7779/uploads/"}` + data?.brandImage?.originalURL} alt={""} style={{ width: 25, marginTop: 8, marginBottom: 8 }} />}
                                    {data?.brandImage?.originalURL && <a style={{ color: "#5A5A5A" }}><strong>{data?.name}</strong> </a>}
                                  </div>
                                )
                              })
                            }
                            </div>
                            <div className="searched-item3 searched-item">
                              <a href="#" className="result-btn">See All Results <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                            </div>
                          </div>
                        </datalist>
                      </form>
                    </div>
                  </div>
                </div>
                <div className="col-md-4 col-sm-4">
                  <div className="top-header-right-bar">
                    <div className="favorite-icon">
                      <i class="fa fa-heart-o" aria-hidden="true"></i>
                    </div>
                    <div className="smallview">

                      <ul className="navbar-nav cart-section">
                        <li className="nav-item active">
                          <a
                            className="nav-link cart"
                            href={loggedIn ? `${process.env.PUBLIC_URL}/cart` : `${process.env.PUBLIC_URL}/cart`}
                          >
                            <img
                              src={cart_icon2}
                              alt={cart_icon2}
                              className="img-responsive"
                            />
                            {
                              <span className="cart-text">
                                Cart{" "}
                                {this.props.CartProducts.cartCount > 0 && <label className="item-count">{this.props.CartProducts.cartCount}</label>}
                              </span>


                            }
                          </a>
                        </li>

                        <i className="fa fa-ellipsis-v moreccatview" aria-hidden="true" onClick={this.smallMenu}></i>
                        <div className={menuOpen ? "moreccatview-res" : "small-menu-hide "}>
                          <ul className="nav flex-column " >
                            <li className="nav-item"><button className="nav-link" onClick={() => this.showUpcomingModalStatus('Promotions')} /*href="#/"*/>Promotions</button></li>
                            <li className="nav-item"><a className="nav-link" href={`${process.env.PUBLIC_URL}/type/grocery`}>Grocery</a></li>
                            <li className="nav-item"><a className="nav-link" onClick={() => this.showUpcomingModalStatus('Marketplace')} /*href={`${process.env.PUBLIC_URL}/type/marketplace`}*/>Marketplace</a></li>
                            <li className="nav-item"><a className="nav-link" /*href={`${process.env.PUBLIC_URL}/type/myMall`}*/ onClick={() => this.showUpcomingModalStatus('MyMall')}>MyMall</a></li>
                          </ul>
                        </div>
                      </ul>
                    </div>
                    <div className="login-signup">
                      <ul className="navbar-nav">
                        <li className="nav-item signup">
                          <img src={userIcon} alt={userIcon} />
                          {loggedIn ? (
                            // <a
                            //   href={`${process.env.PUBLIC_URL}/myProfile`}
                            //   style={{ marginRight: 10 }}
                            // >{userName}
                            // </a>
                            <div className="user-drop-down">
                              <a onClick={() => this.setState({ userDropdownOpen: !this.state.userDropdownOpen })} >John Doe <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                              {this.state.userDropdownOpen &&
                                <div className="dropdown-custom">
                                  <div className="manage-my-account-box">
                                    <div className="account-box-img">
                                      <img src={userIcon} alt={userIcon} />
                                    </div>
                                    <div className="account-box-text">
                                      <p>John Doe <img src={AccountDropIcon7} alt={AccountDropIcon7} /> </p>
                                      <sapn>Manage my account <i class="fa fa-angle-right" aria-hidden="true"></i></sapn>
                                    </div>
                                  </div>
                                  <ul>
                                    <li> <img src={AccountDropIcon1} alt={AccountDropIcon1} /> <a> My Orders </a></li>
                                    <li> <img src={AccountDropIcon2} alt={AccountDropIcon2} /> <a> My Wishlists </a></li>
                                    <li> <img src={AccountDropIcon3} alt={AccountDropIcon3} /> <a> My Followed Store </a></li>
                                    <li> <img src={AccountDropIcon4} alt={AccountDropIcon4} /> <a> My Reviews </a></li>
                                    <li> <img src={AccountDropIcon5} alt={AccountDropIcon5} /> <a> My Return & Cancellations </a></li>
                                    <li className="logout" onClick={this.logout}> <img src={AccountDropIcon6} alt={AccountDropIcon6} /> <a> <strong>Logout</strong> </a></li>
                                  </ul>
                                </div>}
                            </div>
                          ) : null}
                          {loggedIn ? (
                            <a href={`${process.env.PUBLIC_URL}/myProfile`} className="nav-link" >
                              &nbsp;
                            </a>
                          ) : (
                            <a
                              className="nav-link"
                              href={`${process.env.PUBLIC_URL}/login`}
                            >
                              Login / Signup
                            </a>
                          )}
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* bottom-header */}
          <div className="header-bottom">

            <div className="row">
              <div className="col-md-8 col-sm-6">
                <nav className="navbar navbar-expand-lg navbar-light category_header ">

                  {/* navs for large view */}
                  <div className="new-header">
                    <a className="navbar-brand mobile-menuicon" href="#/" onClick={this.mobileMenu} >
                      <img src={menuIcon} alt={menuIcon} />
                    </a>
                    <ul className="navbar-nav home_nav">

                      {/* <li className="nav-item sm-none imgage-nav">
                        <a className="nav-link " onClick={() => this.showUpcomingModalStatus('Promotions')}>
                          Promotions
                        </a>
                      </li> */}
                      <li className="nav-link" onClick={() => this.setState({GroceriesMyMallSelected: "groceries"})}>
                        {/* <a className={pageName[2] === 'grocery' ? "nav-link active" : "nav-link"} href={`${process.env.PUBLIC_URL}/type/grocery`} aria-current="page">
                          <img className="header-logo-img" src={groceriesImg} alt="Grocery" />
                          Groceryrrrrr
                        </a> */}
                        <a className={window.location.href.includes("groceries") ? "navbar-brand prev-menu active" : "navbar-brand prev-menu" } onClick={() => window.location.href = `${process.env.PUBLIC_URL}/groceries`}>
                          <img className="header-logo-img" src={groceriesImg} alt="Grocery" />
                          Groceries
                        </a>
                      </li>
                      {/* <li className="nav-item sm-none imgage-nav">
                        <a className={pageName[2] === 'marketplace' ? "nav-link active" : "nav-link"} onClick={() => this.showUpcomingModalStatus('Marketplace')} href={`${process.env.PUBLIC_URL}/type/marketplace`}>
                          <img className="header-logo-img" src={marketplaceImg} alt="Marketplace" />
                          Marketplace
                        </a>
                      </li> */}
                      <li className="nav-item sm-none imgage-nav" onClick={() => this.setState({GroceriesMyMallSelected: "myMall"})}>
                        <a className={pageName[2] === 'myMall' ? "nav-link active" : "nav-link"} onClick={() => this.showUpcomingModalStatus('MyMall')} /*href={`${process.env.PUBLIC_URL}/type/myMall`}*/>
                          <img className="header-logo-img" src={myMallImg} alt="MyMall" />
                          MyMall
                        </a>
                      </li>
                      <li className="nav-item sm-none imgage-nav more-dopdown" style={{ marginLeft: 10 }}>
                        <a className={pageName[2] === 'myMall' ? "nav-link active" : "nav-link"} onClick={() => this.setState({ morePopUp: !this.state.morePopUp })} /*href={`${process.env.PUBLIC_URL}/type/myMall`}*/>
                          <img className="header-logo-img" src={MoreMenuIcon} alt="MyMall" />
                          More
                          <i class="fa fa-angle-down" aria-hidden="true"></i>
                        </a>
                        {this.state.morePopUp && <div className="more-custom-dropdown">
                          <ul>
                            <li> <a> <img src={MoreIcon1} alt={MoreIcon1} /> Scheduled Cart <span className="">2</span> </a> </li>
                            <li> <a> <img src={MoreIcon2} alt={MoreIcon2} /> Vouchers</a> </li>
                            <li> <a> <img src={MoreIcon3} alt={MoreIcon3} /> Promotions</a>  </li>
                          </ul>
                        </div>}
                      </li>
                    </ul>
                    {window.location.href.includes("groceries") && <div className="header-progress-bar">
                      <div className="progress-text">
                        <p>Get Free Delivery above QAR 50.00 with Grocery</p>
                        <span>70%</span>
                      </div>
                      <Line percent="70" strokeWidth="1.1" strokeColor="#F9004B" />
                    </div>}
                  </div>
                </nav>
              </div>
              <div className="col-md-4 col-sm-6+">

                <div className="language-lacation">
                  {/* <div onInput={this.openLanguage} id="google_translate_element"></div> */}
                  <div class="nav-wrapper switch-language">
                    {/* <div class="sl-nav">
                      <ul>
                        <li><i class="sl-flag flag-usa"><div id="germany"></div></i> EN <i class="fa fa-angle-down" aria-hidden="true"></i>
                          <div class="triangle"></div>
                          <ul>
                            <li><i class="sl-flag flag-de"><div id="germany"></div></i> <span class="active">ةيبرعلاا</span></li>
                            <li><i class="sl-flag flag-usa"><div id="germany"></div></i> <span>English</span></li>
                          </ul>
                        </li>
                      </ul>
                    </div> */}
                    {/* <Switch
                      checkedChildren="AR"
                      unCheckedChildren="EN"
                    /> */}
                    <ul className="language-new-text">
                      <li> <a> <img src={Saudi} alt={Saudi} /> ةيبرعلاا </a> </li>
                      {/* <li> <a> <img src={United} alt={United} /> English </a> </li> */}
                    </ul>
                  </div>
                  <div className="address-bar">
                    <ul className="navbar-nav">
                      <li className="nav-item dropdown">
                        <div className="search-location">
                          <img src={LocationIcon} alt={LocationIcon} />
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Enter your address"
                            value={Location.locationCoordinates.length > 0 ? Location.locationCoordinates[0].name : ''}
                            onFocus={() => this.openLocationModal()}
                          />
                          <i class="fa fa-angle-down" aria-hidden="true"></i>
                        </div>
                      </li>
                    </ul>
                  </div>
                  <AddNewAddress modalStatus={showLocationModal} hideLocationModal={() => this.hideLocModal()} />
                </div>
              </div>
            </div>

          </div>
        </div>

        <div className="header-mobile">
          <div className="new-mobile-menu">
            <div className="category-dropdown-change">
              <nav className="navbar navbar-expand-lg navbar-light category_header ">

                {/* navs for large view */}
                <div className="new-header">
                  {!this.state.onPage ? (
                    <div>
                      { window.location.href.includes("groceries") ?  
                        <a className="navbar-brand mobile-menuicon" href="#/" >
                          <img src={arrowLeft} alt={arrowLeft} />
                        </a> :
                         <a className="navbar-brand mobile-menuicon" href="#/" onClick={() => window.location.href = `${process.env.PUBLIC_URL}/categories`} >
                            <img src={menuIcon} alt={menuIcon} />
                        </a>
                      }
                    </div>
                  ) : (
                    <a className="navbar-brand mobile-menuicon" href="#/" onClick={() => window.location.href = `${process.env.PUBLIC_URL}/`} >
                      <img src={CloseSquare} alt={CloseSquare} />
                    </a>
                  )}
                  <ul className="navbar-nav home_nav">
                    <li className="nav-link">


                      <a className="navbar-brand prev-menu" href="#/">
                        {/* <img src={menuIcon} alt={menuIcon} /> */}
                        <span className="sm-none"> Categories</span>
                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                      </a>

                      {/* menu for list view */}
                      {categoryLevels !== null ?
                        <div className="large-catmenu-view">

                          <ul className="menu-step1 navbar-nav flex-column ">
                            {
                              Object.keys(categoryLevels).map((level1, level1Index) => {
                                return (
                                  <li className="nav-item  " key={level1Index}>
                                    <a className="nav-link" href={Object.keys(categoryLevels[level1]).length > 0 ? '#/' : `${process.env.PUBLIC_URL}/productListing/category/${level1}`}>
                                      {level1} {Object.keys(categoryLevels[level1]).length > 0 ? <i className="fa fa-angle-right" aria-hidden="true"></i> : null}
                                    </a>
                                    {
                                      Object.keys(categoryLevels[level1]).length > 0 ?
                                        <ul className="menu-step2">
                                          {
                                            Object.keys(categoryLevels[level1]).map((level2, level2Index) => {
                                              return (
                                                <li className="nav-item" key={level2Index}>
                                                  <a className="nav-link" href={Object.keys(categoryLevels[level1][level2]).length > 0 ? '#/' : `${process.env.PUBLIC_URL}/productListing/category/${level2}`}>
                                                    {level2} {Object.keys(categoryLevels[level1][level2]).length > 0 ? <i className="fa fa-angle-right" aria-hidden="true"></i> : null}
                                                  </a>
                                                  {
                                                    Object.keys(categoryLevels[level1][level2]).length > 0 ?
                                                      <ul className="menu-step3">
                                                        {
                                                          Object.keys(categoryLevels[level1][level2]).map((level3, level3Index) => {
                                                            return (
                                                              <li className="nav-item" key={level3Index}>
                                                                <a className="nav-link" href={Object.keys(categoryLevels[level1][level2][level3]).length > 0 ? '#/' : `${process.env.PUBLIC_URL}/productListing/category/${level3}`}>
                                                                  {level3} {Object.keys(categoryLevels[level1][level2][level3]).length > 0 ? <i className="fa fa-angle-right" aria-hidden="true"></i> : null}
                                                                </a>
                                                                {
                                                                  Object.keys(categoryLevels[level1][level2][level3]).length > 0 ?
                                                                    <ul className="menu-step4">
                                                                      {
                                                                        Object.keys(categoryLevels[level1][level2][level3]).map((level4, level4Index) => {
                                                                          return (
                                                                            <li className="nav-item" key={level4Index}>
                                                                              <a className="nav-link" href={Object.keys(categoryLevels[level1][level2][level3][level4]).length > 0 ? '#/' : `${process.env.PUBLIC_URL}/productListing/category/${level4}`}>
                                                                                {level4} {Object.keys(categoryLevels[level1][level2][level3][level4]).length > 0 ? <i className="fa fa-angle-right" aria-hidden="true"></i> : null}
                                                                              </a>
                                                                              {
                                                                                Object.keys(categoryLevels[level1][level2][level3][level4]).length > 0 ?
                                                                                  <ul className="menu-step5" key={level4}>
                                                                                    {
                                                                                      Object.keys(categoryLevels[level1][level2][level3][level4]).map((level5, level5Index) => {
                                                                                        return (
                                                                                          <li className="nav-item" key={level5Index}>
                                                                                            <a className="nav-link" href={`${process.env.PUBLIC_URL}/productListing/category/${level5}`}>
                                                                                              {level5}
                                                                                            </a>
                                                                                          </li>
                                                                                        )
                                                                                      })
                                                                                    }
                                                                                  </ul>
                                                                                  :
                                                                                  null
                                                                              }
                                                                            </li>
                                                                          )
                                                                        })
                                                                      }
                                                                    </ul>
                                                                    :
                                                                    null
                                                                }
                                                              </li>
                                                            )
                                                          })
                                                        }
                                                      </ul>
                                                      :
                                                      null
                                                  }
                                                </li>
                                              )
                                            })
                                          }
                                        </ul>
                                        :
                                        null
                                    }


                                  </li>
                                )
                              })
                            }

                          </ul>
                        </div>
                        :
                        null
                      }
                      {/* menu for list view end */}
                    </li>
                  </ul>
                </div>
              </nav>
            </div>
            <div className={mobileMenuOpen ? "mobile-menu active" : "mobile-menu "} onClick={this.mobileMenu}>
              <ul className="navbar-nav ">
                <li className="nav-item">
                  <a href="#" className="navbar-brand">
                    <img src={mainLogo} className="img-responsive" alt="mainLogo" />
                  </a>
                </li>
                <li><hr className="dropdown-divider" /></li>
                <li className="nav-item">
                  <a href={`${process.env.PUBLIC_URL}/categories`} className="nav-link">Categories</a>
                </li>
                <li className="nav-item">
                  <a href={loggedIn ? `${process.env.PUBLIC_URL}/myWishlist` : `${process.env.PUBLIC_URL}/login`} className="nav-link">My Wishlists</a>
                </li>
                <li className="nav-item">
                  <a href={loggedIn ? `${process.env.PUBLIC_URL}/myOrder` : `${process.env.PUBLIC_URL}/login`} className="nav-link">My Orders</a>
                </li>
                <br />
                <li><hr className="dropdown-divider" /></li>
                <li className="nav-item">
                  <a href="#" className="nav-link">العربية</a>
                </li>
                <li className="nav-item">
                  <a href={`${process.env.PUBLIC_URL}/about-us`} className="nav-link">About Us </a>
                </li>
                <li className="nav-item">
                  <a href={`${process.env.PUBLIC_URL}/Terms-and-Condition`} className="nav-link">Terms & Conditions</a>
                </li>
                <li className="nav-item">
                  <a href={`${process.env.PUBLIC_URL}/Privacy-Policy`} className="nav-link">Privacy Policy</a>
                </li>
                <li className="nav-item">
                  <a href={`${process.env.PUBLIC_URL}/shipping-delivery`} className="nav-link">Shipping & Delivery </a>
                </li>
                <li className="nav-item">
                  <a href={`${process.env.PUBLIC_URL}/payment-methods`} className="nav-link">Payment Methods</a>
                </li>
                <li className="nav-item">
                  <a href={`${process.env.PUBLIC_URL}/returns-refunds`} className="nav-link">Returns & Refunds </a>
                </li>
                <li className="nav-item">
                  <a href={`${process.env.PUBLIC_URL}/contact-us`} className="nav-link">Contact Us </a>
                </li>
                <br /><li><hr className="dropdown-divider" /></li>
                <li className="nav-item">
                  <a href="http://nodeserver.mydevfactory.com/BrojiMarketPlace/Seller/why-join-mymart" className="nav-link">Sell on myMart Qatar</a>
                </li>
                <li className="nav-item">
                  <a href="http://nodeserver.mydevfactory.com/BrojiMarketPlace/Seller/" className="nav-link"> Seller Login </a>
                </li>
              </ul>
            </div>
          </div>
          <div className="mobilr-search">
            <div className="search-input">
              <form className="dlf" onSubmit={this.navigatePage} onClick={() => this.setState({ searchModal: true })}>
                <input
                  type="text"
                  className="form-control"
                  placeholder={`Search in ${this.state?.rendomPopularCategory}`} list="suggestion"
                  onChange={(val) => this.searchKey(val)}
                />
                <datalist id="suggestion" ref={this.wrapperRef} style={searchedArray.length > 0 ? { display: 'block' } : { display: 'none' }}>
                  <div className="suggestion-layer">
                    {
                      searchedArray.map((data, index) => {
                        let reference = '';
                        if (data.type === 'Category') {
                          reference = `${process.env.PUBLIC_URL}/productListing/category/${data._id}`;
                        } else if (data.type === 'Brand') {
                          reference = `${process.env.PUBLIC_URL}/productListing/brand/${data._id}`;
                        } else if (data.type === 'Store') {
                          reference = `${process.env.PUBLIC_URL}/storeDetails/${data._id}`;
                        } else if (data.type === 'Product') {
                          reference = `${process.env.PUBLIC_URL}/productDetails/${data._id}`;
                        }
                        return (
                          <a href={reference} key={index} style={index === 0 ? { backgroundColor: '#ee9621', color: '#fff' } : {}}>
                            <span className="search-title"> <img src={SearchResultIcon} alt={SearchResultIcon} /> {this.getHighlightedText(data.name)}</span> <span className="search-text"> {data.category !== undefined && data.category !== null ? ` in ${data.category}` : null}</span>
                          </a>
                        )
                      })
                    }
                  </div>
                </datalist>
              </form>
            </div>
            <SearchForMobile modalStatus={this.state.searchModal} hideLocationModal={() => this.setState({ searchModal: false })} rendomeName={this.state?.rendomPopularCategory && this.state?.rendomPopularCategory}/>
          </div>
          <div className="mobile-notification-bar">
            {!window.location.href.includes("groceries") && <div className="message-icon">
              <img src={Message} alt={Message} />
              <span>15</span>
            </div>}
            {!window.location.href.includes("groceries") && <div className="message-icon">
              <img src={Notification} alt={Notification} />
              <span>15</span>
            </div>}
            <div className="smallview">
              <ul className="navbar-nav cart-section">
                <li className="nav-item active">
                  <a
                    className="nav-link cart"
                    href={loggedIn ? `${process.env.PUBLIC_URL}/cart` : `${process.env.PUBLIC_URL}/cart`}
                  >
                    <img
                      src={cartMobileIcon}
                      alt={cartMobileIcon}
                      className="img-responsive"
                    />
                    {
                      <span className="cart-text">
                        {this.props?.CartProducts?.cartCount && <label className="item-count">{this.props.CartProducts.cartCount}</label>}
                      </span>


                    }
                  </a>
                </li>
                {/* {window.location.href.includes("groceries") && 
                <div>
                  <li className="nav-item active" onClick={() => this.setState({threeDotsMenueOpen: !this.state.threeDotsMenueOpen})}>
                    <a
                      className="nav-link cart"
                    >
                      <img
                        src={threeDots}
                        alt={threeDots}
                        className="img-responsive"
                      />
                    </a>
                  </li>
                  {this.state.threeDotsMenueOpen && <div style={{height: 50, width: 50, backgroundColor: "yellow"}}>
                    <a>three dot menue</a>
                  </div>}
                </div>
                } */}
                <i className="fa fa-ellipsis-v moreccatview" aria-hidden="true" onClick={this.smallMenu}></i>
                <div className={menuOpen ? "moreccatview-res" : "small-menu-hide "}>
                  <ul className="nav flex-column " >
                    <li className="nav-item"> <a className="nav-link"> <img src={RightMobileMenuIcon1} alt={RightMobileMenuIcon1} /> Login </a> </li>
                    <li className="nav-item"> <a className="nav-link"> <img src={RightMobileMenuIcon2} alt={RightMobileMenuIcon2} /> Home </a> </li>
                    {/* <li className="nav-item"><button className="nav-link" onClick={() => this.showUpcomingModalStatus('Promotions')}>Promotions</button></li> */}
                    <li className="nav-item"><a className="nav-link" href={`${process.env.PUBLIC_URL}/type/grocery`}> <img src={RightMobileMenuIcon3} alt={RightMobileMenuIcon3} /> Grocery</a></li>
                    <li className="nav-item"><a className="nav-link" onClick={() => this.showUpcomingModalStatus('Marketplace')} /*href={`${process.env.PUBLIC_URL}/type/marketplace`}*/> <img src={RightMobileMenuIcon4} alt={RightMobileMenuIcon4} /> Marketplace</a></li>
                    <li className="nav-item"><a className="nav-link" /*href={`${process.env.PUBLIC_URL}/type/myMall`}*/ onClick={() => this.showUpcomingModalStatus('MyMall')}><img src={RightMobileMenuIcon5} alt={RightMobileMenuIcon5} /> MyMall</a></li>
                    <li className="nav-item"> <a className="nav-link"> <img src={RightMobileMenuIcon6} alt={RightMobileMenuIcon6} /> Messages </a> </li>
                    <li className="nav-item"> <a className="nav-link"> <img src={RightMobileMenuIcon7} alt={RightMobileMenuIcon7} /> My Account </a> </li>
                    <li className="nav-item"> <a className="nav-link"> <img src={RightMobileMenuIcon8} alt={RightMobileMenuIcon8} /> My Orders </a> </li>
                    <li className="nav-item"> <a className="nav-link"> <img src={RightMobileMenuIcon9} alt={RightMobileMenuIcon9} /> My Wishlist </a> </li>
                    <li className="nav-item"> <a className="nav-link"> <img src={RightMobileMenuIcon10} alt={RightMobileMenuIcon10} /> Need Help? </a> </li>

                  </ul>
                </div>
              </ul>
            </div>
          </div>



        </div>

        <div className="location-for-mobile">
          {!this.state.onPage &&
            <div className="language-lacation">
              <div onInput={this.openLanguage} id="google_translate_element"></div>

              <div className="address-bar">
                <ul className="navbar-nav">
                  <li className="nav-item dropdown">
                    <div className="search-location">
                      <img src={LocationIcon} alt={LocationIcon} />
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Enter your address"
                        value={Location.locationCoordinates.length > 0 ? Location.locationCoordinates[0].name : ''}
                        onFocus={() => this.openLocationModal()}
                      />
                    </div>
                  </li>
                </ul>
              </div>
              <AddNewAddress modalStatus={showLocationModal} hideLocationModal={() => this.hideLocModal()} />
              <div class="nav-wrapper switch-language">
                {/* <div class="sl-nav">
                  <ul>
                    <li><i class="sl-flag flag-usa"><div id="germany"></div></i> EN <i class="fa fa-angle-down" aria-hidden="true"></i>
                      <div class="triangle"></div>
                      <ul>
                        <li><i class="sl-flag flag-de"><div id="germany"></div></i> <span class="active">ةيبرعلاا</span></li>
                        <li><i class="sl-flag flag-usa"><div id="germany"></div></i> <span>English</span></li>
                      </ul>
                    </li>
                  </ul>
                </div> */}
                <ul className="language-new-text">
                  <li> <a> <img src={Saudi} alt={Saudi} /> ةيبرعلاا </a> </li>
                  {/* <li> <a> <img src={United} alt={United} /> English </a> </li> */}
                </ul>
              </div>
            </div>}
        </div>
      </section>
    );
  }
}

const mapStateToProps = (state) => ({
  CartProducts: state.Cart,
  Location: state.Location
})

export default connect(
  mapStateToProps,
  { UpdateCart, UpdateLocation, UpdatelogoutCart }
)(Header);