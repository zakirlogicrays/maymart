import React from 'react'
import PropTypes from "prop-types";

import '../../assets/css/bootstrap.min.css';
import '../../assets/css/bootstrap-glyphicons.min.css';
import '../../assets/css/custom-checkbox.css';
import '../../assets/css/font-awesome.min.css';
import '../../assets/css/global-style.css';
import '../../assets/css/media-queries.css';


export default function AuthLayout({ children }) {
    return (
        <>
            <div className="login-page" style={{minHeight: "512.8px"}}>{children}</div>
        </>
    )
}

AuthLayout.propTypes = {
    children: PropTypes.element.isRequired
};

