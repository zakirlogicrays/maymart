import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";

const firebaseConfig = {
    apiKey: "AIzaSyB2-HfkWGum1tLY-8aI_dHNP6_ScbvLDr4",
    authDomain: "mymartchat.firebaseapp.com",
    projectId: "mymartchat",
    storageBucket: "mymartchat.appspot.com",
    messagingSenderId: "68319905836",
    appId: "1:68319905836:web:c422c7bc997a9cd4bde989",
    measurementId: "G-V4QWSKE3GP"
};

firebase.initializeApp(firebaseConfig);

export default firebase;

