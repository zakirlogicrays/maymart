import React, { Component } from "react";
import axios from "axios";

export default class ResetPassword extends Component {
  constructor() {
    super();
    this.state = {
      // email: '',

      // emailError: '',

      successMessage: "",
      success: false,
      errorMessage: "",
      error: false,
      token: "",
      verifyCodeError: "",

      username: "",
      verifyCode: "",
      confirmPassword: "",
      update: "",
    };
  }

  async componentDidMount() {
    //console.log(atob(this.props.match.params.token),'atob');

    await axios
      .get(`${window.$URL}user/seller-email-verify`, {
        params: {
          verifyToken: atob(this.props.match.params.token),
        },
      })
      .then((response) => {
        console.log(response.data, "customerlink");
        if (response.data.message === "verify link link a-ok") {
          this.setState({
            username: response.data.username,
            errorMessage: response.data,
            error: false,
          });
        } else if (response.data === "verify link is invalid") {
          this.setState({
            errorMessage: response.data,
            error: true,
          });
        } else {
          this.setState({
            error: true,
          });
        }
      })
      .catch((error) => {
        console.log(error.data);
      });
  }

  onChange = (e) => {
    const { name, value } = e.target;
    this.setState({
      [name]: value,
    });
  };
  validate = () => {
    this.setState({
      verifyCodeError: "",
    });

    let verifyCodeError = "";

    if (this.state.verifyCode.length < 1) {
      verifyCodeError = "Please enter Verify Code";
    } else if (this.state.verifyCode.length < 4) {
      verifyCodeError = "Invalid Code";
    }

    if (verifyCodeError) {
      this.setState({ verifyCodeError });
      return false;
    }

    this.setState({
      verifyCodeError,
    });

    return true;
  };

  onSubmit = async (e) => {
    e.preventDefault();
    this.setState({
      success: false,
      error: false,
      successMessage: "",
      errorMessage: "",
    });
    let checkValidation = this.validate();
    console.log(checkValidation);
    if (checkValidation) {
      //alert(this.state.confirmPassword);return false;
      axios
        .put(`${window.$URL}user/customer-update-verifyCode`, {
          username: this.state.username,
          verifyCode: this.state.verifyCode,
        })
        .then((response) => {
          if (response.data.message === "Email Verified") {
            //  console.log('hbea na kno' +response.data.message);
            this.setState({
              success: true,
              successMessage: response.data.message,
            });
            //this.props.history.push(`${process.env.PUBLIC_URL}/login/`);
          } else if (response.data.message === "Invaild Code") {
            console.log(this.state.username, "this.state.username");

            this.setState({
              errorMessage: response.data.message,
              error: true,
            });
          } else {
            this.setState({
              error: true,
            });
          }
        })
        .catch((error) => {
          console.log(error.data);
        });
    }
  };

  render() {
    const { verifyCodeError, success, error, successMessage, errorMessage } =
      this.state;
    return (
      <section className="my-mart-signup-wr">
        <div className="container">
          <div className="sinup-container">
            <h1>Enter your Verification Code</h1>
            <div className="form-sign-wrapper email-codeVarification">
              <form onSubmit={this.onSubmit}>
                <div className="header-bottom">
                  <p className="text-center">
                    <i className="fa fa-warning icon-style" /> Please check your
                    email for the verification code.
                  </p>
                </div>
                <div className="d">
                 
                  <div className="inv-sucesscode">
                    {success ? (
                      <p style={{ color: "green", fontSize: 18 }}>
                        {" "}
                        {successMessage}{" "}
                      </p>
                    ) : null}
                    {error ? (
                      <p style={{ color: "red", fontSize: 18 }}>
                        {" "}
                        {errorMessage}{" "}
                      </p>
                    ) : null}
                  </div>

                  <div className="forms-section">
                    <div className="form-group divInner">
                    <div class="msg-box">
                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                  </div>
                      <label
                        style={{ textAlign: "center", width: "100%" }}
                        className="input-heading"
                      >
                        Enter your Verification Code
                      </label>
                      <div id="divOuter">
                        <div id="divInner">
                          <input
                            id="partitioned"
                            name="verifyCode"
                            type="text"
                            maxlength="4"
                            onChange={this.onChange}
                          />
                        </div>
                      </div>
                      {verifyCodeError ? (
                        <p className="error-class"> {verifyCodeError} </p>
                      ) : null}
                    </div>
                    <div className="form-group">
                      <button type="submit" className="btn signup_btn">
                        Submit
                      </button>
                    </div>
                    <div className="form-group">
                      <p className="already_signUp">
                        Don't have an account?{" "}
                        <a href={`${process.env.PUBLIC_URL}/register`}>
                          Register
                        </a>
                      </p>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
