import React, { Component } from "react";
import axios from "axios";
import { AddToCat,UpdateloginCart,UpdateCart } from '../../data/reducers/cart';
import { connect } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import GoogleLogin from "../../Component/GoogleLogin";
import FacebookLogin from "../../Component/FacebookLogin";
//import Loader from "../../Component/Loader";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import Loader from "react-loader-spinner";



class Login extends Component {

    state = {
        loading: false,
        email: '',
        password: '',
        emailError: '',
        passwordError: '',
        successMessage: '',
        success: false,
        errorMessage: '',
        error: false,
    }

    componentDidMount(){
    }

    onChange = (e) => {
        const { name, value } = e.target;
        this.setState({
            [name]: value
        });
    }
    validate = () => {
        this.setState({
            emailError: '',
            passwordError: '',
        });
        //const regex = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
        let emailError = '';
        let passwordError = '';

        if (this.state.email.length < 1) {
            emailError = "Please enter Email or Phone";
        }
        if (emailError) {
            this.setState({ emailError })
            return false
        }

        if (this.state.password.length < 1) {
            passwordError = "Please enter Password";
        } else if (this.state.password.length < 6) {
            passwordError = "Password must be 6 characters long";

        }
        if (passwordError) {
            this.setState({ passwordError })
            return false
        }

        this.setState({
            emailError,
            passwordError,
        })
        return true
    }

    onSubmit = async (e) => {
       
        e.preventDefault();
        this.setState({
            success: false,
            error: false,
            successMessage: '',
            errorMessage: '',
        })
        const {CartProducts} = this.props;
        let checkValidation = this.validate();
        if (checkValidation) {
            this.setState({ loading: true });
            const { email, password } = this.state;
            let loginData = {
                "email": email,
                "password": password,
                "loginType": "customer"
            };
            await axios.post(`${window.$URL}user/login`, loginData)
                .then(response => {
                    if (response.status === 200) {
                        console.log(response,'response')
                        if (response.data.success) {
                            console.log(response.data.message,'true')
                            this.setState({
                                success: true,
                                successMessage: response.data.message,
                            }, async () => {
                                localStorage.setItem("token", response.data.token);
                                
                                // let lastVisit = await localStorage.getItem("lastVisited");
                                // let lastVisitParse = JSON.parse(lastVisit);
                                let TempToken = await localStorage.getItem("Temptoken")
                                if (TempToken) {
                                    this.props.UpdateloginCart();
                                    this.props.UpdateCart();
                                    // localStorage.removeItem("Temptoken");
                                    setTimeout(() => {
                                        window.location.href = `${process.env.PUBLIC_URL}/`;
                                    },1000)
                                }else{
                                    this.props.UpdateCart();
                                    setTimeout(() => {
                                        window.location.href = `${process.env.PUBLIC_URL}/`;
                                    },1000)
                                }
                            })
                        } else {
                            this.setState({
                                error: true,
                                errorMessage: response.data.message,
                                loading: false
                            })
                        }
                    }
                
            })
            .catch(error => {
                console.log(error);
                this.setState({
                    loading: false
                })
            })
        }
    }

    addToCart = async (CartProducts) => {
        //console.log("CartProducts ---->", CartProducts);
        this.props.UpdateExistingCart(CartProducts); 
    }

    render() {
        const { emailError, passwordError,errorMessage, loading} = this.state;
        
            return (
                <section className="my-mart-signup-wr">
                <ToastContainer/>
                    <div className="container">
                        <div className="sinup-container">
                            <h1>Login to your myMart Account</h1>
                            
                            <div className="form-sign-wrapper">
                            {errorMessage ? <span style={{ color: 'red',fontSize:18} }> {errorMessage} </span> : null}
                                <form onSubmit={this.onSubmit}>
                                    <div className="row">
                                        <div className="col-lg-6 col-md-6 col-sm-12 form-parts">
                                        {loading ? (
                                            <Loader
                                                type="TailSpin"
                                                color="#00BFFF"
                                                height={100}
                                                width={100}
                                                style={{
                                                    left: "85%",
                                                    position: "absolute",
                                                    top: "20%",
                                                }}
                                            />
                                        ) : null}
                                            <div className="form-group">
                                                <label className="input-heading">Phone Number or Email</label>
                                                <input type="text" placeholder="Please enter your Phone Number or Email" className="form-control" name="email" onChange={this.onChange} />
                                                {emailError ? <span style={{ color: 'red' }}> {emailError} </span> : null}
                                            </div>
                                            <div className="form-group">
                                                <label className="input-heading">Password</label>
                                                <input type="password" placeholder="Enter your Password" className="form-control" name="password" onChange={this.onChange} />
                                                {passwordError ? <span style={{ color: 'red' }}> {passwordError} </span> : null}
                                            </div>
                                            <div className="form-group" >
                                                <p className="already_signUp f-password">Forgot Password? <a href={`${process.env.PUBLIC_URL}/forgot-password`}>Click here</a></p>
                                            </div>
                                            
                                           
                                        </div>
                                        <div className="col-lg-6 col-md-6 col-sm-12">
                                            {/* <div className="form-group">
                                                <label className="input-heading">Phone Number</label>
                                                <input type="text" name="phoneNumber" className="form-control" aria-describedby="phonenumber" placeholder="Enter your phone number" pattern="[0-9]*" />
                                            </div>
                                            <div className="form-group">
                                                <button type="button" className="btn getCode">get sms code</button>
                                            </div> */}
                                            {/*<div className="form-group">
                                            <label className="c-s-checkbox ">
                                                <span className="input-heading">I'd like to receive exclusive offers and promotions via SMS</span>
                                                <input type="checkbox" />
                                                <span className="checkmark" />
                                            </label>
                                        </div>*/}
                                        
                                        <div className="lv-login"></div>
                                        <div className="form-group">
                                                <button type="submit" className="btn signup_btn">Login</button>
                                        </div>
                                      
    
                                            <div className="form-group">
                                                <div className="or-sign-section">
                                                    <hr />
                                                    <span>Or, sign up with </span>
                                                </div>
                                            </div>
    
                                            <div className=" form-group other-login-method">
                                                {/* <button className="btn signup_with_gmail">Sign Up With Email</button> */}
                                                <div className="row">
                                                    <div className="col-md-6">
                                                        <FacebookLogin  />
                                                    </div>
                                                    <div className="col-md-6 ">
                                                        <div className="google">
                                                        <GoogleLogin /></div>
                                                    </div>
                                                </div> 
                                            </div>
                                            <div className="form-group">
                                                <p className="already_signUp">Don't have an account? <a href={`${process.env.PUBLIC_URL}/register`}>Register</a></p>
                                            </div>
                                            {/* <div className="form-group">
                                                <p className="already_signUp">Forgot Password? <a href={`${process.env.PUBLIC_URL}/forgot-password`}>Click here</a></p>
                                            </div> */}
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            )
        
    }
}

const mapStateToProps = (state) => ({
    CartProducts: state.Cart,
})

export default connect(
    mapStateToProps,
    {  AddToCat, UpdateloginCart,UpdateCart }
)(Login);