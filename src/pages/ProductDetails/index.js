import React, { Component } from 'react';
import ImageGallery from "../../Component/ImageGallery";
import axios from 'axios';
import jwt_decode from 'jwt-decode';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import shopIcon from "../../assets/images/shopIcon.png";
import bestPrice from "../../assets/images/bestPrice.png";
import bestOffer from "../../assets/images/bestOffer.png";
import Loader from "../../Component/Loader";
import ProductVariantListing from "../../Component/ProductVariantListing";
import ProductRatingReviewContain from "../../Component/RatingReview";
import ProductAttributes from "../../Component/ProductAttributes";
import ProductSpecifications from "../../Component/ProductSpecifications";
import ProductListing from "../../Component/ProductListing";
import ProductDetailsContent from "../../Component/ProductDetailsContent";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
//import ReactStars from "react-rating-stars-component";
import StarRatingComponent from 'react-star-rating-component';

const LowStockQuantity = 'Quantity out of stock';

export default class ProductDetails extends Component {
    state = {
        productDetailsArray: [],
        nearestStoreArray: [],
        bestPriceArray: [],
        bestOfferArray: [],
        choosenProduct: [],
        newAndTrending: [],
        frequentproductArray: null,
        qty: 1,
        wishListedArray: [],
        variationsArray: [],
        AttributesArray: [],
        productReviewData: null,
        productTotalAverage: 0,
        titleProductName :'',
        titleLastCategoryName:''
    }
    componentDidMount = async () => {
        await axios.get(`${window.$URL}sellerProducts/getSellerProductDetails/${this.props.match.params.id}`)
            .then(response => {
                if (response.status === 200) {
                    this.setState({
                        productDetailsArray: response.data.ProductDetails,
                        nearestStoreArray: response.data.nearestStore,
                        bestPriceArray: response.data.bestPrice,
                        bestOfferArray: response.data.bestOffer,
                        variationsArray: response.data.variations,
                        AttributesArray: response.data.labels,
                        titleProductName:response.data.ProductDetails.sellerProductId.productName?response.data.ProductDetails.sellerProductId.productName :'Not Found',
                        titleLastCategoryName:response.data.ProductDetails.sellerProductId.category[response.data.ProductDetails.sellerProductId.category.length-1]
                    })
                }
            })
            .catch(error => {
                console.log(error)
            })
        this.getChooseForYou();
        this.newAndTrending();
        this.fetchFrequentProductdata();
        this.fetchProductReviewData();


        let checkToken = await localStorage.getItem("token");
        if (checkToken) {
            const decoded = jwt_decode(checkToken);
            let customerId = decoded.userid;

            await axios.post(`${window.$URL}wishlist/fetchWishList`, { customerId })
                .then(response => {
                    if (response.status === 200) {
                        if (response.data.success) {
                            this.setState({
                                wishListedArray: response.data.data
                            })
                        } else {
                            console.log('errror on fetch');
                        }
                    }
                })
                .catch(error => {
                    console.log(error);
                })
        }
    this.getDynamicTitle();

    }

    async addWishList(productId, wishListStatus) {
        let checkToken = await localStorage.getItem("token");
        if (checkToken) {
            const decoded = jwt_decode(checkToken);
            let customerId = decoded.userid;
            if (wishListStatus) {
                if (productId) {
                    let wishListData = {
                        "productVariantId": productId,
                        "customerId": customerId
                    };
                    await axios.post(`${window.$URL}wishlist/addwishlist`, wishListData)
                        .then(response => {
                            if (response.status === 200) {
                                if (response.data.success) {
                                    toast.success(response.data.message, {
                                        position: "bottom-center",
                                        autoClose: 3000,
                                        hideProgressBar: true,
                                        closeOnClick: true,
                                        pauseOnHover: true,
                                        draggable: true,
                                        progress: undefined,
                                    });
                                    this.componentDidMount();
                                } else {
                                    toast.error(response.data.message, {
                                        position: "bottom-center",
                                        autoClose: 3000,
                                        hideProgressBar: true,
                                        closeOnClick: true,
                                        pauseOnHover: true,
                                        draggable: true,
                                        progress: undefined,
                                    });
                                }
                            }
                        })
                        .catch(error => {
                            console.log(error);
                        })
                }
            } else {
                let DeletedId = {
                    "productVariantId": productId,
                };
                await axios.post(`${window.$URL}wishlist/deletewishlist`, DeletedId)
                    .then(response => {
                        if (response.status === 200) {
                            if (response.data.success) {
                                toast.error(response.data.message, {
                                    position: "bottom-center",
                                    autoClose: 3000,
                                    hideProgressBar: true,
                                    closeOnClick: true,
                                    pauseOnHover: true,
                                    draggable: true,
                                    progress: undefined,
                                });
                                this.componentDidMount();
                            } else {
                                toast.error(response.data.message, {
                                    position: "bottom-center",
                                    autoClose: 3000,
                                    hideProgressBar: true,
                                    closeOnClick: true,
                                    pauseOnHover: true,
                                    draggable: true,
                                    progress: undefined,
                                });
                            }
                        }
                    })
                    .catch(error => {
                        console.log(error);
                    })
            }
        } else {
            window.location.href = `${process.env.PUBLIC_URL}/login`
        }
    }

    renderImages = () => {
        const { productDetailsArray } = this.state;
        let newData = [];
        if (productDetailsArray !== null && productDetailsArray.variationImageId !== undefined && productDetailsArray.variationImageId.length > 0) {

            for (let i = 0; i <= productDetailsArray.variationImageId.length - 1; i++) {
                let images = productDetailsArray.variationImageId;
                newData.push({
                    original: window.$ImageURL + images[i].originalURL,
                    thumbnail: window.$ImageURL + '.' + images[i].thumbURL,
                    originalClass: 'mainImage',
                    thumbnailClass: 'thumbImage'
                });
            }

        } else if (productDetailsArray !== null && productDetailsArray.sellerProductId.productImageId !== undefined && productDetailsArray.sellerProductId.productImageId.length > 0) {
            for (let i = 0; i <= productDetailsArray.sellerProductId.productImageId.length - 1; i++) {
                let images = productDetailsArray.sellerProductId.productImageId;
                newData.push({
                    original: window.$ImageURL + images[i].originalURL,
                    thumbnail: window.$ImageURL + '.' + images[i].thumbURL,
                    originalClass: 'mainImage',
                    thumbnailClass: 'thumbImage'
                });
            }
        } else {
            newData.push({
                original: "http://nodeserver.mydevfactory.com:7779/uploads/no-image-480x480.png",
                thumbnail: "http://nodeserver.mydevfactory.com:7779/uploads/no-image-480x480.png",
                originalClass: 'mainImage',
                thumbnailClass: 'thumbImage'
            });
        }
        return newData;
    }

    getChooseForYou = async () => {
        await axios.post(`${window.$URL}sellerProducts/getChoosenProducts`)
            .then(response => {
                this.setState({
                    choosenProduct: response.data
                })
            })
            .catch(error => {
                console.log(error.data)
            })
    }

    newAndTrending = async () => {
        await axios.post(`${window.$URL}sellerProducts/newAndTrending`, { category: 'All' })
            .then(response => {
                if (response.status === 200) {
                    this.setState({
                        newAndTrending: response.data,
                    })
                }
            })
            .catch(error => {
                console.log(error)
            })
    }

    createMarkup = (html) => {
        return {
            __html: html
        };
    };

    quantityAdd = () => {
        const { productDetailsArray } = this.state;
        let newQuantity = this.state.qty + 1;
        console.log({ newQuantity }, productDetailsArray.currentInventory)
        if (parseInt(newQuantity) !== 0 && parseInt(newQuantity) <= productDetailsArray.currentInventory) {
            this.setState({
                qty: newQuantity
            })
        } else {
            toast.error(LowStockQuantity, {
                position: "bottom-center",
                autoClose: 3000,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }
    }

    quantitySubtract = () => {
        const { productDetailsArray } = this.state;
        let newQuantity = this.state.qty - 1;
        if (parseInt(newQuantity) !== 0 && parseInt(newQuantity) <= productDetailsArray.currentInventory) {
            this.setState({
                qty: newQuantity
            })
        }
    }

    fetchFrequentProductdata = async () => {
        await axios.get(`${window.$URL}couponDiscount/frequentProductList/${this.props.match.params.id}`)
            .then(response => {
                if (response.status === 200) {
                    if (response.data.success) {
                        //  console.log(response.data.data.couponAppliedToProducts)
                        this.setState({
                            frequentproductArray: response.data.data
                        })
                    } else {
                        console.log('errror on fetch');
                    }
                }
            })
            .catch(error => {
                console.log(error);
            })
    }

    fetchProductReviewData = async () => {
        await axios.get(`${window.$URL}product/fetchProductreviewData/${this.props.match.params.id}`)
            .then(response => {
                console.log(response.data.data)
                if (response.status === 200) {
                    if (response.data.success) {
                        this.setState({
                            productReviewData: response.data.data,
                            productTotalAverage: response.data.data.totalProductAveragerating
                        })
                    } else {
                        console.log('errror on fetch');
                    }
                }
            })
            .catch(error => {
                console.log(error);
            })
    }

    Capitalize(str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }

    getDynamicTitle = async () => {
        let pathName = window.location.pathname.split("/");
       
        var productDetailsPageName = pathName[pathName.length-2];
       const {titleProductName,titleLastCategoryName} =this.state;
        var capitalizeTitle = this.Capitalize(productDetailsPageName);
       
        if(productDetailsPageName=='productDetails'){
        let  title = `${titleProductName} : Buy sell online ${titleLastCategoryName} with cheap price | myMart Qatar`;
        let metaDescription = '';
        document.title = title;
        document.querySelector('meta[name="description"]').setAttribute('content', metaDescription);
        }
    }

    render() {
        const { productDetailsArray, nearestStoreArray, bestPriceArray, bestOfferArray, choosenProduct, newAndTrending, wishListedArray, qty, variationsArray, AttributesArray, frequentproductArray, productReviewData, productTotalAverage } = this.state;
        console.log({ productDetailsArray });
        if (productDetailsArray.length < 1) {
            return (
                <Loader />
            )
        } else {
            return (
                <>
                    <ToastContainer />
                    <div className="container">
                        <nav aria-label="breadcrumb">
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item">
                                    <a href={`${process.env.PUBLIC_URL}/`} > Home </a>
                                </li>
                                {
                                    productDetailsArray.sellerProductId.category.map((eachCat, eachIndex) => {
                                        if (eachCat !== "") {
                                            return (
                                                <li className={eachIndex !== productDetailsArray.sellerProductId.category.length - 1 ? "breadcrumb-item" : "breadcrumb-item active"} key={eachIndex}>
                                                    {
                                                        eachIndex !== productDetailsArray.sellerProductId.category.length - 1 ?
                                                            <a href={`${process.env.PUBLIC_URL}/productListing/category/${eachCat}`}>&nbsp; {eachCat} </a>
                                                            :
                                                            <>&nbsp; {eachCat}</>
                                                    }
                                                </li>
                                            )
                                        } else {
                                            return null;
                                        }
                                    })
                                }
                            </ol>
                        </nav>
                        <section className="product-details-wrpr">
                            <div className="row">
                                <div className="col-lg-6">
                                    <div className="p-d-img-carausal">
                                        <div className="image-slider">
                                            <ImageGallery images={this.renderImages()} />
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-6">
                                    <div className="food-description-wr">
                                        <div className="description-inner">
                                            <div className="product-details">
                                                <h2>{productDetailsArray.sellerProductId.productName} </h2>
                                                <div className="rating">
                                                    {productTotalAverage > 0 ? <StarRatingComponent value={productTotalAverage} size={30} isHalf={true} edit={false} /> : null}
                                                </div>
                                                <p className="brand-name">brand: <a href={`${process.env.PUBLIC_URL}/productListing/brand/${productDetailsArray.sellerProductId.brand}`}>{productDetailsArray.sellerProductId.brand}</a></p>
                                            </div>
                                            <div className="share-product">
                                                {wishListedArray.includes(productDetailsArray._id) ?
                                                    <button className="btn">
                                                        <i className="fa fa-heart" onClick={() => this.addWishList(productDetailsArray._id, false)} aria-hidden="true" />
                                                    </button> :
                                                    <button className="btn">
                                                        <i className="fa fa-heart-o" onClick={() => this.addWishList(productDetailsArray._id, true)} aria-hidden="true" />
                                                    </button>
                                                }
                                                {/*<button className="btn">
                                                    <i className="fa fa-share-alt" aria-hidden="true" />
                                                </button>*/}
                                            </div>
                                        </div>
                                    </div>

                                    <div className="store-content">

                                        {
                                            productDetailsArray.sellerProductId.productType === 'grocery' ?
                                                <Tabs>
                                                    <TabList className="nav nav-pills mb-3">
                                                        <Tab className="nav-item near-store" role="presentation">
                                                            <img alt="store logo" src={productDetailsArray.sellerProductId.sellerId.SellerDetails.storeLogo.originalURL !== undefined ? `${window.$ImageURL}` + productDetailsArray.sellerProductId.sellerId.SellerDetails.storeLogo.originalURL : `${window.$ImageURL}no-image-480x480.png`} style={{ height: 25 }} /> {productDetailsArray.sellerProductId.sellerId.SellerDetails.shopName}</Tab>
                                                        <Tab className="nav-item near-store" role="presentation"><img alt={shopIcon} src={shopIcon} /> Nearest Store</Tab>
                                                        <Tab className="nav-item near-store" role="presentation"><img alt={bestPrice} src={bestPrice} /> Lowest Price</Tab>
                                                        <Tab className="nav-item near-store" role="presentation"><img alt={bestOffer} src={bestOffer} /> Promotions</Tab>
                                                    </TabList>
                                                    <TabPanel className="tab-content">
                                                        <ProductDetailsContent
                                                            productDetailsArray={productDetailsArray}
                                                            variationsArray={variationsArray}
                                                            AttributesArray={AttributesArray}
                                                            qty={qty}
                                                            quantitySubtractProps={() => this.quantitySubtract()}
                                                            quantityAddProps={() => this.quantityAdd()}
                                                            productVariantId={productDetailsArray && productDetailsArray._id}
                                                        />
                                                    </TabPanel>
                                                    <TabPanel className="tab-content">
                                                        <ProductDetailsContent
                                                            productDetailsArray={nearestStoreArray}
                                                            variationsArray={variationsArray}
                                                            AttributesArray={AttributesArray}
                                                            qty={qty}
                                                            quantitySubtractProps={() => this.quantitySubtract()}
                                                            quantityAddProps={() => this.quantityAdd()}
                                                            productVariantId={nearestStoreArray && nearestStoreArray._id}
                                                        />
                                                    </TabPanel>
                                                    <TabPanel className="tab-content">
                                                        <ProductDetailsContent
                                                            productDetailsArray={bestPriceArray}
                                                            variationsArray={variationsArray}
                                                            AttributesArray={AttributesArray}
                                                            qty={qty}
                                                            quantitySubtractProps={() => this.quantitySubtract()}
                                                            quantityAddProps={() => this.quantityAdd()}
                                                            productVariantId={bestPriceArray && bestPriceArray._id}
                                                        />
                                                    </TabPanel>
                                                    <TabPanel className="tab-content">
                                                        <ProductDetailsContent
                                                            productDetailsArray={bestOfferArray}
                                                            variationsArray={variationsArray}
                                                            AttributesArray={AttributesArray}
                                                            qty={qty}
                                                            quantitySubtractProps={() => this.quantitySubtract()}
                                                            quantityAddProps={() => this.quantityAdd()}
                                                            productVariantId={bestOfferArray && bestOfferArray._id}
                                                        />
                                                    </TabPanel>
                                                </Tabs>
                                                :
                                                <ProductDetailsContent
                                                    productDetailsArray={productDetailsArray}
                                                    variationsArray={variationsArray}
                                                    AttributesArray={AttributesArray}
                                                    qty={qty}
                                                    quantitySubtractProps={() => this.quantitySubtract()}
                                                    quantityAddProps={() => this.quantityAdd()}
                                                    productVariantId={this.props.match.params.id}
                                                />
                                        }
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    {/*
                        frequentproductArray !== null && frequentproductArray.length > 0 ?
                            <section className="new_tranding">
                                <div className="container">
                                    <div className="title_section">
                                        <h2>Frequently bought together</h2>
                                    </div>
                                    <div className="freq-bought-wrapper">
                                        <ProductVariantListing productArray={frequentproductArray} />
                                    </div>
                                </div>
                            </section>
                            :
                            null
                    */}
                    {/* end frequently section  */}
                    {/* end payment details */}
                    {/*
                        productReviewData !== null && productReviewData !== undefined ?
                            <section className="payment-section-wrapper product-review-wr">
                                <div className="container">
                                    <div className="review-tabs">
                                        <Tabs>
                                            <TabList className="nav nav-pills mb-3">
                                                <Tab className="nav-link same-width" role="presentation">Product Attributes</Tab>
                                                <Tab className="nav-link same-width" role="presentation">Specifications</Tab>
                                                <Tab className="nav-link" role="presentation"> Customer Reviews </Tab>
                                                <Tab className="nav-link same-width" role="presentation">Question &amp; Answer</Tab>
                                            </TabList>

                                            <TabPanel className="tab-content">
                                                <ProductAttributes productDetailsArray={productDetailsArray} />
                                            </TabPanel>
                                            <TabPanel className="tab-content">
                                                <ProductSpecifications productDetailsArray={productDetailsArray} />
                                            </TabPanel>
                                            <TabPanel className="tab-content">
                                                {
                                                    productReviewData !== null && productReviewData !== undefined ?
                                                        <ProductRatingReviewContain ProductRatingData={productReviewData}  productDetailsArray={productDetailsArray} />
                                                        :
                                                        null
                                                }
                                            </TabPanel>
                                            <TabPanel className="tab-content">
                                                Question-answer
                                    </TabPanel>
                                        </Tabs>
                                    </div>
                                </div>
                            </section>
                    : ""*/}
                    {/* my mall start */}
                    <section className="new_tranding">
                        <div className="container">
                            <div className="title_section">
                                <h2>New and Trending</h2>
                                <a href={`${process.env.PUBLIC_URL}/productListing/malls/products`} className="btn view-all">View All</a>
                            </div>
                            <ProductListing productArray={newAndTrending} />
                        </div>
                    </section>
                    {/* my mall end */}
                    {/* Choose For You start */}
                    <section className="new_tranding">
                        <div className="container">
                            <div className="title_section">
                                <h2>Chosen for You</h2>
                                <a href={`${process.env.PUBLIC_URL}/productListing/choosen/products`} className="btn view-all">View All</a>
                            </div>
                            <ProductListing productArray={choosenProduct} />
                        </div>
                    </section>
                    {/* Choose For You end */}
                </>
            )

        }

    }
}