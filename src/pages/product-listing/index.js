import React, { Component } from "react";
import ProductListingSidebar from "../../Component/ProductListSidebar";
import ProductListing from "../../Component/ProductListing";
import Loader from "../../Component/Loader";
import axios from "axios";
import { connect } from "react-redux";
import { RemoveLocation } from "../../data/reducers/location";
import LocationModal from "../../Component/LocationModal";
import Pagination from "react-js-pagination";
import moment from "moment";
import Category7 from "../../../src/assets/images/Arrow - Right.svg";
import CloseFilterItem from "../../../src/assets/images/close-filter.png";
import blackPaper from "../../../src/assets/images/blackPaper.png";
import ProductChosePopUp from "../../Component/ProductChosePopUp";

import MultiCarousel from 'react-multi-carousel';


import GridIcon1 from "../../../src/assets/images/grid-icon1.png";
import GridIcon2 from "../../../src/assets/images/grid-icon2.png";
import ListIcon1 from "../../../src/assets/images/list-icon1.png";
import ListIcon2 from "../../../src/assets/images/list-icon2.png";

import ProductListingSliderImg from "../../../src/assets/images/product-listing-category-slider-img.png";



const CategoryListingSlider = {
  superLargeDesktop: {
    // the naming can be any, depends on you.
    breakpoint: { max: 4000, min: 3000 },
    items: 3.2
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 3.2
  },
  tablet: {
    breakpoint: { max: 1024, min: 575 },
    items: 2.2
  },
  mobile: {
    breakpoint: { max: 576, min: 0 },
    items: 1.7,
  }
};


function calculateDistance(lat1, lon1, lat2, lon2, unit) {
  var radlat1 = (Math.PI * lat1) / 180;
  var radlat2 = (Math.PI * lat2) / 180;
  var theta = lon1 - lon2;
  var radtheta = (Math.PI * theta) / 180;
  var dist =
    Math.sin(radlat1) * Math.sin(radlat2) +
    Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
  dist = Math.acos(dist);
  dist = (dist * 180) / Math.PI;
  dist = dist * 60 * 1.1515;
  if (unit === "K") {
    dist = dist * 1.609344;
  }
  if (unit === "N") {
    dist = dist * 0.8684;
  }
  return dist;
}

class Productlisting extends Component {
  state = {
    Skip: 0,
    Limit: 1000000,
    totalCount: 0,
    pageCount: 0,
    pageLimit: 20,
    postSize: "",
    ProductList: [],
    AllProductList: [],
    ProductListFilter: [],
    viewProductsBasedOn: "Store",
    TotalCategory: [],
    TotalCategoryImage: [],
    TotalCategoryWiseProductCount: [],
    TotalCountry: [],
    TotalBrand: [],
    AllAttributes: [],
    filters: [],
    loading: true,
    categoryLoading: false,
    sidebarFilter: false,
    showLocationModal: false,
    gridView: false,
    checkedBrand: [],
    checkedCon: [],
    showChosenProductPopUp: false,
    chosenProductImageData: undefined,
    chosenProductName: undefined

  };
  async componentDidMount() {
    const segment = this.props.match.params.segment;
    const id = this.props.match.params.id.replace(/%20/g, " ");
    const variables = {
      skip: this.state.Skip,
      Limit: this.state.Limit,
      order: "",
      sortBy: "",
      filters: [],
    };
    this.getProdDetails(variables, segment, id);
  }

  toggleProducts = (type) => {
    const { AllProductList } = this.state;
    const { Location } = this.props;
    this.setState(
      {
        viewProductsBasedOn: type,
        loading: true,
      },
      () => {
        this.sortProductsNearestOrBestOffer(AllProductList, Location);
      }
    );
  };

  openLocationModal = () => {
    this.props.RemoveLocation();
    this.setState({
      showLocationModal: true,
    });
  };

  hideLocModal = () => {
    this.setState(
      {
        showLocationModal: false,
        showAllMenueModal: false,
        showProductPopUp: false,
        showChosenProductPopUp: false,
      },
      () => {
        this.componentDidMount();
      }
    );
  };

  getProdDetails = async (variables, segment, id) => {
    const { Location } = this.props;
    //console.log(segment,'segment')
    this.setState({ loading: true });
    await axios
      .post(`${window.$URL}sellerProducts/getProducts`, {
        variables,
        segment,
        id,
        coordinates: [],
      })
      .then(async (response) => {
        if (response.data.products.length > 0) {
          let filterResponse = response.data.products;
          this.setState(
            {
              AllProductList: response.data.products,
            },
            () => {
              if (Location.locationCoordinates.length > 0) {
                this.sortProductsNearestOrBestOffer(filterResponse, Location);
              } else {
                this.openLocationModal();
              }
            }
          );
        } else {
          this.setState({ loading: false, categoryLoading: false });
        }
      })
      .catch((error) => {
        console.log(error.data);
        this.setState({ loading: false, categoryLoading: false });
      });
  };

  showFilteredResults = (newFilters) => {
    const segment = this.props.match.params.segment;
    const id = this.props.match.params.id.replace(/%20/g, " ");
    const variables = {
      skip: this.state.Skip,
      Limit: this.state.Limit,
      order: "",
      sortBy: "",
      filters: [newFilters],
    };
    this.getProdDetails(variables, segment, id);
  };

  redirectListing = (filtersdata) => {
    window.location.href = `${process.env.PUBLIC_URL}/productListing/category/${filtersdata}`;
  };

  getfilterData = (filtersdata, segment) => {
    this.setState({ sidebarFilter: true });
    const { filters } = this.state;
    const newFilters = { ...filters };
    newFilters[segment] = filtersdata;
    this.setState({
      filters: newFilters,
    });
    this.showFilteredResults(newFilters);
  };

  getfilterbrandData = (filtersdata, segment) => {
    this.setState({ sidebarFilter: true });
    const { filters } = this.state;
    const newFilters = { ...filters };
    newFilters[segment] = filtersdata;
    this.setState({
      filters: newFilters,
    });
    this.showFilteredResults(newFilters);
  };

  getfiltercountData = (filtersdata, segment) => {
    this.setState({ sidebarFilter: true });
    const { filters } = this.state;
    const newFilters = { ...filters };
    newFilters[segment] = filtersdata;
    this.setState({
      filters: newFilters,
    });
    this.showFilteredResults(newFilters);
  };

  getFilterAttributesData = (attriName, attriOptions) => {
    const { Location } = this.props;
    const { AllProductList } = this.state;

    let filterProduct = [];
    if (attriName.length > 0) {
      filterProduct = AllProductList.filter((data) => {
        let returnType = false;
        for (let i = 0; i <= attriName.length - 1; i++) {
          if (attriOptions[i].length > 0) {
            if (data.productType === "grocery") {
              data.sellerProductVariantId.forEach((item) => {
                item.productId.productAttribute.forEach((eachAttri) => {
                  if (eachAttri.attrName === attriName[i]) {
                    let filterOptions = eachAttri.attrValue.filter(
                      (eachOption) => {
                        return attriOptions[i].includes(eachOption);
                      }
                    );
                    if (filterOptions.length > 0) {
                      returnType = true;
                    }
                  }
                });
              });
            } else {
              if (data.attributeLabels.includes(attriName[i])) {
                let attriIndex = data.attributeLabels.indexOf(attriName[i]);
                if (attriIndex !== -1) {
                  let filterOptions = data.attributeValues[attriIndex].filter(
                    (eachOption) => {
                      return attriOptions[i].includes(eachOption);
                    }
                  );
                  if (filterOptions.length > 0) {
                    returnType = true;
                  }
                }
              }
            }
          }
        }
        return returnType;
      });
    } else {
      filterProduct = AllProductList;
    }
    this.sortProductsNearestOrBestOffer(filterProduct, Location);
  };

  getfilterPriceData = (filtersdata, segment) => {
    this.setState({ sidebarFilter: true });
    const { filters } = this.state;
    const newFilters = { ...filters };
    newFilters[segment] = filtersdata;
    this.setState({
      filters: newFilters,
    });
    this.showFilteredResults(newFilters);
  };

  getfilterRatingData = (filtersdata, segment) => {
    this.setState({ sidebarFilter: true });
    const { filters } = this.state;
    const newFilters = { ...filters };
    newFilters[segment] = filtersdata;
    this.setState({
      filters: newFilters,
    });
    this.showFilteredResults(newFilters);
  };

  fetchCategoryImage = async () => {
    const segment = this.props.match.params.segment;
    const id = this.props.match.params.id.replace(/%20/g, " ");
    const { ProductList } = this.state;
    let TotalCategory = [];
    let productCountArray = [];
    let categoryImageArray = this.state.TotalCategoryImage;

    if (segment === "category") {
      await axios
        .post(`${window.$URL}category/fetchSubLevelCategory`, {
          catName: id.replace(/%20/g, " "),
        })
        .then((response) => {
          if (response.data.success) {
            TotalCategory = response.data.dropDownOptions;
          }
        })
        .catch((error) => {
          console.log(error);
        });

      for (let i = 0; i <= TotalCategory.length - 1; i++) {
        // count products
        let prodCount = 0;
        for (let j = 0; j <= ProductList.length - 1; j++) {
          if (ProductList[j].category.includes(TotalCategory[i])) {
            prodCount++;
          }
        }
        productCountArray.push(prodCount);
        // fetch category images
        await axios
          .get(`${window.$URL}category/fetchMyMartMargin/${TotalCategory[i]}`)
          .then((response) => {
            if (response.data.success) {
              if (
                response.data.response.categoryImageId !== null &&
                response.data.response.categoryImageId !== undefined
              ) {
                categoryImageArray.push(
                  response.data.response.categoryImageId.originalURL
                );
              } else {
                categoryImageArray.push("no-image-480x480.png");
              }
            } else {
              categoryImageArray.push("no-image-480x480.png");
            }
          })
          .catch((error) => {
            console.log(error);
          });
      }
      let filterCat = TotalCategory.filter((eachCat, eachCatIndex) => {
        return productCountArray[eachCatIndex] > 0;
      });
      let filterCatImage = categoryImageArray.filter(
        (eachCat, eachCatIndex) => {
          return productCountArray[eachCatIndex] > 0;
        }
      );
      let filterCatCount = productCountArray.filter((eachCat, eachCatIndex) => {
        return productCountArray[eachCatIndex] > 0;
      });
      this.setState({
        TotalCategory: filterCat,
        TotalCategoryImage: filterCatImage,
        TotalCategoryWiseProductCount: filterCatCount,
        categoryLoading: false,
      });
    } else {
      const { TotalCategory, ProductList } = this.state;
      let categoryImageArray = this.state.TotalCategoryImage;
      let productCountArray = [];

      for (let i = 0; i <= TotalCategory.length - 1; i++) {
        // count products
        let prodCount = 0;
        for (let j = 0; j <= ProductList.length - 1; j++) {
          if (ProductList[j].category.includes(TotalCategory[i])) {
            prodCount++;
          }
        }
        productCountArray.push(prodCount);
        // fetch category images
        await axios
          .get(`${window.$URL}category/fetchMyMartMargin/${TotalCategory[i]}`)
          .then((response) => {
            if (response.data.success) {
              if (
                response.data.response.categoryImageId !== null &&
                response.data.response.categoryImageId !== undefined
              ) {
                categoryImageArray.push(
                  response.data.response.categoryImageId.originalURL
                );
              } else {
                categoryImageArray.push("no-image-480x480.png");
              }
            } else {
              categoryImageArray.push("no-image-480x480.png");
            }
          })
          .catch((error) => {
            console.log(error);
          });
      }
      this.setState({
        TotalCategoryImage: categoryImageArray,
        TotalCategoryWiseProductCount: productCountArray,
        categoryLoading: false,
      });
    }
  };

  sortProductsNearestOrBestOffer(data, userLocation) {
    const { viewProductsBasedOn } = this.state;

    if (viewProductsBasedOn === "Store") {
      let userCoordinates = userLocation.locationCoordinates[0].coordinates;

      for (let i = 0; i < data.length; i++) {
        data[i]["distance"] = calculateDistance(
          userCoordinates.lat,
          userCoordinates.lng,
          data[i].StoreLocation.coordinates[1],
          data[i].StoreLocation.coordinates[0],
          "K"
        );
      }

      data.sort(function (a, b) {
        return a.distance - b.distance;
      });
      let trimmedArray = this.removeOtherSimilarProducts(data);
      this.setState(
        {
          ProductListFilter: trimmedArray,
          ProductList: trimmedArray,
          categoryLoading: true,
        },
        () => {
          this.getSidebarData();
        }
      );
    } else {
      let bestOfferArray = [];
      data.forEach((eachProd) => {
        if (eachProd.sellerProductVariantId.length > 0) {
          if (
            eachProd.sellerProductVariantId[0].specialPriceProvided === true &&
            this.getDateValid(eachProd.sellerProductVariantId[0])
          ) {
            eachProd["displayedPrice"] =
              eachProd.sellerProductVariantId[0].specialPrice;
          } else {
            eachProd["displayedPrice"] =
              eachProd.sellerProductVariantId[0].Price;
          }
        }
        bestOfferArray.push(eachProd);
      });

      bestOfferArray.sort((a, b) => {
        return a.displayedPrice - b.displayedPrice;
      });

      let trimmedArray = this.removeOtherSimilarProducts(bestOfferArray);
      this.setState(
        {
          ProductListFilter: trimmedArray,
          ProductList: trimmedArray,
          categoryLoading: true,
        },
        () => {
          this.getSidebarData();
        }
      );
    }
  }
  getDateValid = (prodArray) => {
    let returnValidate = false;
    if (prodArray.startDate !== "" && prodArray.endDate !== "") {
      let convertStartDate = moment(prodArray.startDate).unix() * 1000;
      let convertEndDate = moment(prodArray.endDate).unix() * 1000;
      let systemTime = moment(new Date()).unix() * 1000;
      if (systemTime <= convertEndDate && systemTime >= convertStartDate) {
        returnValidate = true;
      }
    }
    return returnValidate;
  };

  sortProducts = (e) => {
    const { value } = e.target; //phtl, plth
    const { ProductList } = this.state;
    let bestOfferArray = [];
    ProductList.forEach((eachProd) => {
      if (eachProd.sellerProductVariantId.length > 0) {
        if (
          eachProd.sellerProductVariantId[0].specialPriceProvided === true &&
          this.getDateValid(eachProd.sellerProductVariantId[0])
        ) {
          eachProd["displayedPrice"] =
            eachProd.sellerProductVariantId[0].specialPrice;
        } else {
          eachProd["displayedPrice"] = eachProd.sellerProductVariantId[0].Price;
        }
      }
      bestOfferArray.push(eachProd);
    });
    if (value === "phtl") {
      bestOfferArray.sort((a, b) => {
        return b.displayedPrice - a.displayedPrice;
      });
    } else {
      bestOfferArray.sort((a, b) => {
        return a.displayedPrice - b.displayedPrice;
      });
    }
    this.setState({
      ProductListFilter: bestOfferArray,
    });
  };

  removeOtherSimilarProducts = (dataArray) => {
    let productVariantIDArray = [];
    let newDataArray = [];
    for (let i = 0; i <= dataArray.length - 1; i++) {
      if (dataArray[i].productType === "nonGrocery") {
        if (!productVariantIDArray.includes(dataArray[i].productName)) {
          productVariantIDArray.push(dataArray[i].productName);
          newDataArray.push(dataArray[i]);
        }
      } else {
        if (!productVariantIDArray.includes(dataArray[i].productId)) {
          productVariantIDArray.push(dataArray[i].productId);
          newDataArray.push(dataArray[i]);
        }
      }
    }
    return newDataArray;
  };

  async getSidebarData() {
    const { ProductList, sidebarFilter, TotalCategory } = this.state;
    let allBrand = [];
    let allCountry = [];
    let attributes = [];
    let attrName = [];
    ProductList.map(async (item) => {
      /*for (let i = 0; i <= TotalCategory.length - 1; i++) {
        if (item.category[i] !== "") {
          const findCat = allCat.indexOf(item.category[i]);
          if (findCat === -1) {
            allCat.push(item.category[i]);
          }
        }
      }*/
      if (item.country !== "") {
        const findCountry = allCountry.indexOf(item.country);
        if (findCountry === -1) {
          allCountry.push(item.country);
        }
      }
      if (item.brand !== "") {
        const findBrand = allBrand.indexOf(item.brand);
        if (findBrand === -1) {
          allBrand.push(item.brand);
        }
      }
      if (item.productType === "nonGrocery") {
        for (let i = 0; i <= item.attributeLabels.length - 1; i++) {
          // attributes
          if (item.attributeLabels[i] in attributes === false) {
            attributes[item.attributeLabels[i]] = {};
          }
          for (let j = 0; j <= item.attributeValues.length - 1; j++) {
            if (
              item.attributeValues[j] in attributes[item.attributeLabels[i]] ===
              false
            ) {
              attributes[item.attributeLabels[i]] = item.attributeValues[j];
            }
          }
        }
      } else {
        for (let i = 0; i <= item.sellerProductVariantId.length - 1; i++) {
          for (
            let j = 0;
            j <=
            item.sellerProductVariantId[i].productId.productAttribute.length -
            1;
            j++
          ) {
            if (
              !attrName.includes(
                item.sellerProductVariantId[i].productId.productAttribute[j]
                  .attrName
              )
            ) {
              attrName.push(
                item.sellerProductVariantId[i].productId.productAttribute[j]
                  .attrName
              );
            }
          }
        }
      }
      return true;
    });

    if (attrName.length > 0) {
      let arrayValue = await this.getValues(attrName);
      attributes.push(arrayValue);

      if (!sidebarFilter) {
        this.setState(
          {
            TotalCategory: TotalCategory,
            TotalCountry: allCountry,
            TotalBrand: allBrand,
            AllAttributes: attributes[0],
          },
          () => {
            this.fetchCategoryImage();
          }
        );
      }
    } else {
      if (!sidebarFilter) {
        this.setState(
          {
            TotalCategory: TotalCategory,
            TotalCountry: allCountry,
            TotalBrand: allBrand,
            AllAttributes: attributes,
          },
          () => {
            this.fetchCategoryImage();
          }
        );
      }
    }

    let totalCount = ProductList.length;
    this.setState({
      postSize: ProductList.length,
      loading: false,
      categoryLoading: false,
      sidebarFilter: true,
      pageCount: Math.ceil(ProductList.length / this.state.pageLimit),
      totalCount,
      ProductListFilter: ProductList.slice(0, this.state.pageLimit),
    });
  }

  getValues = async (attributes) => {
    let arrayValue = [];
    await axios
      .post(`${window.$URL}attribute/fetchAttributeValue`, { attributes })
      .then((response) => {
        arrayValue.push(response.data.data);
      })
      .catch((error) => {
        console.log(error);
      });
    return arrayValue[0];
  };

  handlePageChange(pageNumber) {
    const { ProductList, pageLimit } = this.state;
    const offset = (pageNumber - 1) * pageLimit;
    const ProductListFilter = ProductList.slice(offset, offset + pageLimit);
    this.setState({ ProductListFilter, activePage: pageNumber });
  }

  productType() {
    let prodListType = "";
    if (this.props.match.params.segment === "choosen") {
      prodListType = ""; //this.props.match.params.id.replace(/%20/g, " ") + this.props.match.params.segment;
    } else if (this.props.match.params.segment === "newAndTranding") {
      prodListType = this.state.postSize + " items found in New And Trending";
    } else {
      prodListType =
        this.state.postSize +
        " items found in " +
        this.props.match.params.id.replace(/%20/g, " ") +
        " " +
        this.props.match.params.segment;
    }

    return prodListType;
  }

  removeFilters = async (filterName, categoryType) => {
    if (categoryType === "country") {
      const { checkedCon } = this.state;
      const currentIndex = checkedCon.indexOf(filterName);
      const newChecked = [...checkedCon];

      if (currentIndex === -1) {
        newChecked.push(filterName)
      } else {
        newChecked.splice(currentIndex, 1)
      }
      this.setState({ checkedCon: newChecked })
      let updatedCountry = this.state?.filters?.country?.filter((item) => item !== filterName && item)
      await this.setState({
        filters: {
          ...this.state.filters, country: updatedCountry
        }
      })
      this.showFilteredResults(this.state.filters);
    }
    if (categoryType === "brand") {
      const { checkedBrand } = this.state
      const currentIndex = checkedBrand.indexOf(filterName)
      const newChecked = [...checkedBrand]
      if (currentIndex === -1) {
        newChecked.push(filterName)
      } else {
        newChecked.splice(currentIndex, 1)
      }
      this.setState({ checkedBrand: newChecked })
      let updatedBrand = this.state?.filters?.brand?.filter((item) => item !== filterName && item)
      await this.setState({
        filters: {
          ...this.state.filters, brand: updatedBrand
        }
      })
      this.showFilteredResults(this.state.filters);
    }
    if (categoryType === "rating") {
      await this.setState({
        filters: {
          ...this.state.filters, rating: undefined
        }
      })
      this.showFilteredResults(this.state.filters);
    }
    if (categoryType === "PriceRange") {
      await this.setState({
        filters: {
          ...this.state.filters, PriceRange: undefined
        }
      })
      this.showFilteredResults(this.state.filters);
    }
  }

  handleTogglebrand = (brandData, filtername) => {
    //console.log("brandData ------>", brandData);
    const { checkedBrand } = this.state
    const currentIndex = checkedBrand.indexOf(brandData)
    const newChecked = [...checkedBrand]
    if (currentIndex === -1) {
      newChecked.push(brandData)
    } else {
      newChecked.splice(currentIndex, 1)
    }
    this.setState({ checkedBrand: newChecked })
    this.getfilterbrandData(newChecked, filtername)
  }

  handleTogglecountry = (conData, filtername) => {
    const { checkedCon } = this.state;
    const currentIndex = checkedCon.indexOf(conData);
    const newChecked = [...checkedCon];

    if (currentIndex === -1) {
      newChecked.push(conData)
    } else {
      newChecked.splice(currentIndex, 1)
    }
    this.setState({ checkedCon: newChecked })
    this.getfiltercountData(newChecked, filtername)
  }

  openChosenProductPopUp = (data) => {
    console.log("data ---->", data);
    this.setState({
      showChosenProductPopUp: true,
      chosenProductImageData: data?.productImageId,
      chosenProductName: data?.productName,
    })
  }

  render() {
    const {
      TotalCategory,
      TotalCategoryImage,
      categoryLoading,
      TotalCategoryWiseProductCount,
      viewProductsBasedOn,
      showLocationModal,
      activePage,
      pageLimit,
      totalCount,
      filters,
    } = this.state;
    return (
      <section className="product-listing">
        <LocationModal
          modalStatus={showLocationModal}
          hideLocationModal={() => this.hideLocModal()}
        />
        <div className="product-listing-inner">
          <div className="top-filter-bar">
            <div className="row">
              <div className="col-md-3 col-sm-3">
                <div className="product-listin-title">
                  <h2>Search Result</h2>
                </div>
              </div>
              <div className="col-md-9 col-sm-9">
                <div className="top-filter-inner-box">
                  <div className="row">
                    <div className="col-md-6 col-sm-6">
                      <div className="shop-by-box">
                        <div className="main-srch">
                          <p className="h-name">Shop by</p>
                          <div className="nearby-srch">
                            <label>
                              <input
                                type="radio"
                                name="radio"
                                defaultValue="Store"
                                defaultChecked={
                                  viewProductsBasedOn === "Store" ? true : false
                                }
                                onChange={() => this.toggleProducts("Store")}
                              />
                              <p className="names">Nearest Store</p>
                            </label>

                            <label>
                              <input
                                type="radio"
                                name="radio"
                                defaultValue="specialPrice"
                                defaultChecked={
                                  viewProductsBasedOn === "specialPrice"
                                    ? true
                                    : false
                                }
                                onChange={() => this.toggleProducts("specialPrice")}
                              />
                              <p className="names">Best Offers</p>
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-6 col-sm-6">
                      <div className="sort-by-box">
                        <div className="rightdiv">
                          <form className="form-inline">
                            <div className="form-group">
                              <label>Sort by</label>
                              <select
                                id="inputState"
                                className="form-control"
                                onChange={(val) => this.sortProducts(val)}
                              >
                                <option value="" disabled selected>

                                </option>
                                <option value="phtl">Price High to Low</option>
                                <option value="plth">Price Low to High</option>
                              </select>
                              <i class="fa fa-angle-down" aria-hidden="true"></i>
                            </div>
                          </form>
                        </div>
                        <div className="product-list-grid-icon">
                          <a className="grid" onClick={() => this.setState({ gridView: false })}><img src={GridIcon1} alt={GridIcon1} /></a>
                          <a className="list" onClick={() => this.setState({ gridView: true })}> <img src={ListIcon1} alt={ListIcon1} /> </a>
                        </div>
                        <div className="search-result-counter">
                          <p>1 - 48 of 323 Results</p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-12 col-sm-12">
                      <div className="filtered-by">
                        <ul className="filtered-by-inner">
                          <label>Filtered by:</label>
                          {filters?.country && filters?.country?.map((item) => (
                            <li className="filter-item" >
                              <p>{item}</p>
                              <a onClick={() => this.removeFilters(item, "country")}>
                                <img src={CloseFilterItem} alt={CloseFilterItem} />
                              </a>
                            </li>
                          ))}
                          {filters?.PriceRange &&
                            <li className="filter-item" >
                              <p>{`${filters?.PriceRange?.minval} - ${filters?.PriceRange?.maxval} QAR`}</p>
                              <span className="custom-close-icon"></span>
                              <a onClick={() => this.removeFilters(filters?.PriceRange?.minval, "PriceRange")}>
                                <img src={CloseFilterItem} alt={CloseFilterItem} />
                              </a>
                            </li>
                          }
                          {filters?.brand && filters?.brand?.map((item) => (
                            <li className="filter-item" >
                              <p>{item}</p>
                              <span className="custom-close-icon"></span>
                              <a onClick={() => this.removeFilters(item, "brand")}>
                                <img src={CloseFilterItem} alt={CloseFilterItem} />
                              </a>
                            </li>
                          ))}
                          {filters?.rating &&
                            <li className="filter-item" >
                              <p>{`${filters?.rating} stars`}</p>
                              <span className="custom-close-icon"></span>
                              <a onClick={() => this.removeFilters(filters?.rating, "rating")}>
                                <img src={CloseFilterItem} alt={CloseFilterItem} />
                              </a>
                            </li>
                          }
                          <li className="filter-item clear" > <a> Clear all </a> </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* <nav aria-label="breadcrumb">
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <a href="#/">Home</a>&nbsp;
              </li>
              <li className="breadcrumb-item active" aria-current="page">
                &nbsp; Product List
              </li>
            </ol>
          </nav> */}
          {/* <!-- brad cum end--> */}

          <div className="productlisting_main">
            <div className="row">
              <ProductListingSidebar
                catData={this.state.TotalCategory}
                contryData={this.state.TotalCountry}
                brandData={this.state.TotalBrand}
                otherAttributes={this.state.AllAttributes}
                showFilteredResult={this.getfilterData}
                showFilteredbrandResult={this.getfilterbrandData}
                showFilteredcountResult={this.getfiltercountData}
                showFilteredPriceResult={this.getfilterPriceData}
                showFilteredRatingResult={this.getfilterRatingData}
                showFilteredAttributesResult={this.getFilterAttributesData}
                selectedCountryData={this.state.filters?.country}
                filteredBrand={filters?.brand}
                handleTogglebrand={(brandData, filtername) => this.handleTogglebrand(brandData, filtername)}
                checkedBrand={this.state.checkedBrand}
                handleTogglecountry={(conData, filtername) => this.handleTogglecountry(conData, filtername)}
                checkedCon={this.state.checkedCon}
              />
              <div className="col-md-9 searchView">

                {/* <h1>Product List </h1> */}
                {/* <p> */}
                {/* {this.productType()} */}
                {/*{this.state.postSize} items found in{" "}
                      {this.props.match.params.segment !== "choosen"
                        ? this.props.match.params.id.replace(/%20/g, " ")
                        : " "}{" "}
                      {this.props.match.params.segment}{" "}*/}
                {/* </p> */}

                <div className="product-listing-category-slider">
                  <div className="product-listing-top-category">
                    <div className="row">
                      <div className="col-md-8 col-sm-8">
                        <div className="listing-category-left-content">
                          <div className="category-left-content">
                            <span>Sub Categories</span>
                            <h2>Laptop & Computers </h2>
                            <p className="">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore</p>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-4 col-sm-4">
                        <div className="listing-category-right-img">
                          <img src={ProductListingSliderImg} alt={ProductListingSliderImg} />
                        </div>
                      </div>

                    </div>
                  </div>
                  <div className="category-listing-slider">
                    <MultiCarousel removeArrowOnDeviceType={["tablet", "mobile"]} responsive={CategoryListingSlider}>
                      <div className="category-listing-slider-content">
                        <div className="category-listing-slider-img">
                          <img src={ProductListingSliderImg} alt={ProductListingSliderImg} />
                        </div>
                        <div className="category-listing-slider-text">
                          <p>Laptop</p>
                          <span>24 Products</span>
                        </div>
                      </div>
                      <div className="category-listing-slider-content">
                        <div className="category-listing-slider-img">
                          <img src={ProductListingSliderImg} alt={ProductListingSliderImg} />
                        </div>
                        <div className="category-listing-slider-text">
                          <p>Laptop</p>
                          <span>24 Products</span>
                        </div>
                      </div>
                      <div className="category-listing-slider-content">
                        <div className="category-listing-slider-img">
                          <img src={ProductListingSliderImg} alt={ProductListingSliderImg} />
                        </div>
                        <div className="category-listing-slider-text">
                          <p>Laptop</p>
                          <span>24 Products</span>
                        </div>
                      </div>
                      <div className="category-listing-slider-content">
                        <div className="category-listing-slider-img">
                          <img src={ProductListingSliderImg} alt={ProductListingSliderImg} />
                        </div>
                        <div className="category-listing-slider-text">
                          <p>Laptop</p>
                          <span>24 Products</span>
                        </div>
                      </div>
                      <div className="category-listing-slider-content">
                        <div className="category-listing-slider-img">
                          <img src={ProductListingSliderImg} alt={ProductListingSliderImg} />
                        </div>
                        <div className="category-listing-slider-text">
                          <p>Laptop</p>
                          <span>24 Products</span>
                        </div>
                      </div>
                      <div className="category-listing-slider-content">
                        <div className="category-listing-slider-img">
                          <img src={ProductListingSliderImg} alt={ProductListingSliderImg} />
                        </div>
                        <div className="category-listing-slider-text">
                          <p>Laptop</p>
                          <span>24 Products</span>
                        </div>
                      </div>
                    </MultiCarousel>
                  </div>
                </div>

                {/* <div className="row">
                  {
                    Object.keys(filters).map((eachFilter, filterIndex) => {
                      let filterValues = [];
                      Object.values(filters[eachFilter]).forEach(element => {
                        filterValues.push(element);
                        filterValues.push(',');
                      })
                      return(
                        <>
                          {eachFilter}: {filterValues}
                        </>
                      )
                    })
                  }
                </div> */}
                {/* <!-- search result --> */}
                {categoryLoading ? (
                  <Loader />
                ) : (
                  <div className="row">
                    {TotalCategory.map((eachCat, eachIndex) => {
                      return (
                        <div
                          className="col-md-3 col-sm-6 col-4 padding-screen"
                          key={eachIndex}
                          onClick={() => this.redirectListing([eachCat])}
                        >
                          <div className="search-result-item">
                            <div className="image-result">
                              <img
                                src={
                                  `${window.$ImageURL}` +
                                  TotalCategoryImage[eachIndex]
                                }
                                alt={TotalCategoryImage[eachIndex]}
                              />
                            </div>
                            <div className="text-result">
                              <h2 className="title_bar"> {eachCat} </h2>
                              <p className="resultqty">
                                {" "}
                                {TotalCategoryWiseProductCount[eachIndex]}
                              </p>
                            </div>
                          </div>
                        </div>
                      );
                    })}
                  </div>
                )}
                {/* {console.log("hello ------->", this.state.ProductListFilter)} */}
                <ProductListing
                  productArray={this.state.ProductListFilter}
                  loading={this.state.loading}
                  gridView={this.state.gridView}
                  onClickChosenProductPopUp={(data) => this.openChosenProductPopUp(data)}
                />
                <ProductChosePopUp modalStatus={this.state.showChosenProductPopUp} hideLocationModal={() => this.hideLocModal()} imageData={this.state.chosenProductImageData} productName={this.state.chosenProductName} />
                <div className="pagination-product">
                  {/* <nav aria-label="Page navigation example">
                      <ul className="pagination">
                        <li className="page-item">
                          <a
                            className="page-link"
                            href="#/"
                            aria-label="Previous"
                          >
                            <span aria-hidden="true">
                              {" "}
                              <i
                                className="fa fa-angle-left"
                                aria-hidden="true"
                              ></i>
                            </span>
                          </a>
                        </li>
                        <li className="page-item active">
                          <a className="page-link" href="#/">
                            1
                          </a>
                        </li>
                        <li className="page-item">
                          <a className="page-link" href="#/">
                            2
                          </a>
                        </li>
                        <li className="page-item">
                          <a className="page-link" href="#/">
                            3
                          </a>
                        </li>
                        <li className="page-item">
                          <a className="page-link" href="#/">
                            4
                          </a>
                        </li>
                        <li className="page-item">...</li>
                        <li className="page-item">
                          <a className="page-link" href="#/" aria-label="Next">
                            <span aria-hidden="true">
                              <i
                                className="fa fa-angle-right"
                                aria-hidden="true"
                              ></i>{" "}
                            </span>
                          </a>
                        </li>
                      </ul>
                    </nav> */}
                  <Pagination
                    prevPageText="Prev"
                    nextPageText="Next"
                    firstPageText="First"
                    lastPageText="Last"
                    activePage={activePage}
                    itemsCountPerPage={pageLimit}
                    totalItemsCount={totalCount}
                    pageRangeDisplayed={5}
                    onChange={this.handlePageChange.bind(this)}
                    itemClass="page-item"
                    linkClass="page-link"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

const mapStateToProps = (state) => ({
  Location: state.Location,
});

export default connect(mapStateToProps, { RemoveLocation })(Productlisting);
