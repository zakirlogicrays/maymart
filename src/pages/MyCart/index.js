import React, { Component } from "react";
import axios from 'axios';
import jwt_decode from "jwt-decode";
import Loader from "../../Component/Loader";
import ProductListing from "../../Component/ProductListing";
import CartHeader from "../../Component/CartHeader";
import CartProducts from "../../Component/CartProducts";
import UserAddress from "../../Component/UserAddress";
import UserDeliverySlot from "../../Component/CartDeliverySlot";
import CartPayment from "../../Component/CartPayment";
import OrderSummary from "../../Component/OrderSummary";
import { RemoveCart, UpdateCart, UpdateWithOutCart, RemoveWithOutCart, RemoveSelectedCart, RemoveSelectedWithOutCart } from '../../data/reducers/cart';
import { connect } from 'react-redux';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import moment from 'moment';
import SweetAlert from 'react-bootstrap-sweetalert';

function calculateDistance(lat1, lon1, lat2, lon2, unit) {
    var radlat1 = (Math.PI * lat1) / 180;
    var radlat2 = (Math.PI * lat2) / 180;
    var theta = lon1 - lon2;
    var radtheta = (Math.PI * theta) / 180;
    var dist =
        Math.sin(radlat1) * Math.sin(radlat2) +
        Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    dist = Math.acos(dist);
    dist = (dist * 180) / Math.PI;
    dist = dist * 60 * 1.1515;
    if (unit === "K") {
        dist = dist * 1.609344;
    }
    if (unit === "N") {
        dist = dist * 0.8684;
    }
    return dist;
}

class MyCart extends Component {
    state = {
        cartArray: [],
        cartdupArray: [],
        totalCartArray: [],
        choosenProduct: [],
        loading: true,

        selectedProduct: [],
        quantityArray: [],
        myMartMarginArray: [],
        myMartProfitArray: [],
        PriceArray: [],
        sellerIDArray: [],
        productIdArray: [],
        selectedAddress: null,
        selectedTimeSlot: [],
        subTotalAmount: 0,
        shippingFee: 0,
        totalAmount: 0,
        finaltotalAmount: 0,
        couponApplied: false,
        couponAmount: 0,

        successMessage: '',
        errorMessage: '',
        tabName: 'cart',
        selectedProdarr: [],
        newCartArray: [],
        shippingType: [],
        deliveryTime: [],
        orderSuccessAlert: false,
        orderMessageAlert: '',
        showOtpModal: false,
        verifyCodeError: '',
        verifyCode: '',
    }
    async componentDidMount() {
        let checkToken = await localStorage.getItem("token");
        if (checkToken) {
            this.fetchCartData();
        } else {
            this.fetchWithOutCartData()
        }
        // this.getChooseForYou();
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.CartProducts.cartCount !== prevProps.CartProducts.cartCount) {
            // if(this.props.CartProducts.cartSuceess){
            //     toast.success('Product added to cart Successfully', {
            //         position: "bottom-center",
            //         autoClose: 3000,
            //         hideProgressBar: true,
            //         closeOnClick: true,
            //         pauseOnHover: true,
            //         draggable: true,
            //         progress: undefined,
            //     });
            // }

            if (this.props.CartProducts.removeSuccess) {
                toast.success('Product Successfully Remove From Cart', {
                    position: "bottom-center",
                    autoClose: 3000,
                    hideProgressBar: true,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });
            }
            if (this.props.CartProducts.updateSuccess) {
                toast.success('Product Successfully Updated', {
                    position: "bottom-center",
                    autoClose: 3000,
                    hideProgressBar: true,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });
            }
            this.componentDidMount();
        }

        if (prevState.totalAmount !== this.state.totalAmount) {
            if (this.state.selectedProduct.length > 0) {
                this.setState({
                    newCartArray: [...this.state.cartArray]
                }, () => {
                    this.getSelectedProdArr()
                })
            }
        }
    }

    fetchWithOutCartData = async () => {
        let checkToken = await localStorage.getItem("Temptoken");
        if (checkToken) {
            await axios.get(`${window.$URL}cart/fetchwithoutCart/${checkToken}`)
                .then(res => {
                    console.log(res);
                    //let selectedArray = [];
                    if (res.status === 200) {
                        /*for (let i = 0; i <= res.data.length - 1; i++) {
                            for(let j = 0; j <= res.data[i].productVariantId.length-1; j++){
                                selectedArray.push(res.data[i].productVariantId[j]._id);
                            }
                        }*/
                        this.setState({
                            cartArray: res.data,
                            totalCartArray: res.data,
                            cartdupArray: res.data,
                            loading: false,
                            subTotalAmount: 0,
                            totalAmount: 0
                        }, () => {
                            this.showItems('all');
                        })
                    }
                })
        } else {
            this.setState({
                cartArray: [],
                loading: false
            })
        }

    }

    fetchCartData = async () => {
        let checkToken = await localStorage.getItem("token");
        if (checkToken) {
            const decoded = jwt_decode(checkToken);
            if (decoded.userType === 'customer') {
                await axios.get(`${window.$URL}cart/fetchCart/${decoded.userid}`)
                    .then(res => {
                        console.log('cart data', res.data);
                        //let selectedArray = [];
                        if (res.status === 200) {
                            /*for (let i = 0; i <= res.data.length - 1; i++) {
                                for(let j = 0; j <= res.data[i].productVariantId.length-1; j++){
                                    selectedArray.push(res.data[i].productVariantId[j]._id);
                                }
                            }*/
                            this.setState({
                                cartArray: res.data,
                                totalCartArray: res.data,
                                cartdupArray: res.data,
                                loading: false,
                                subTotalAmount: 0,
                                totalAmount: 0
                            }, () => {
                                this.showItems('all');
                            })
                        }
                    })
                    .catch(error => {
                        console.log(error)
                    })
            }
        } else {
            this.setState({
                cartArray: [],
                loading: false
            })
        }
    }

    showItems = (type) => {
        const { totalCartArray } = this.state;
        const { Location } = this.props
        let filterCart = [];
        if (type === 'all') {
            filterCart = totalCartArray.filter(() => {
                return true;
            })
        } else if (type === 'grocery') {
            filterCart = totalCartArray.filter((eachCart) => {
                return eachCart.productId.some((eachProduct) => {
                    return eachProduct.category.includes("Grocery");
                })
            })
        } else if (type === 'nearest') {
            let distance_covered = []
            let cartDistancewise = []
            var filterCartgrocery = totalCartArray.filter((eachCart) => {
                return eachCart.productId.some((eachProduct) => {
                    return eachProduct.category.includes("Grocery");
                })
            })
            for (var i = 0; i < filterCartgrocery.length; i++) {
                const sellerLat = filterCartgrocery[i].sellerId.warehouseAddressID.Location.coordinates[1] !== 0 ? filterCartgrocery[i].sellerId.warehouseAddressID.Location.coordinates[1] : 25.19451198
                const sellerLng = filterCartgrocery[i].sellerId.warehouseAddressID.Location.coordinates[0] !== 0 ? filterCartgrocery[i].sellerId.warehouseAddressID.Location.coordinates[0] : 51.49918567
                var distance = calculateDistance(Location.locationCoordinates[0].coordinates.lat, Location.locationCoordinates[0].coordinates.lng, sellerLat, sellerLng, "K");
                distance_covered.push(distance)
                filterCartgrocery[i].distance = distance
            }
            distance_covered.sort(function (a, b) {
                return a - b;
            });

            for (var j = 0; j < distance_covered.length; j++) {
                const found = filterCartgrocery.some(el => el.distance === distance_covered[j]);
                if (found) {
                    let index;
                    filterCartgrocery.find(function (item, i) {
                        if (item.distance === distance_covered[j]) {
                            index = i;
                        }
                    });
                    cartDistancewise.push(filterCartgrocery[index])
                }
            }
            filterCart = cartDistancewise
        }
        this.setState({
            cartArray: filterCart,
            selectedProduct: [],
            quantityArray: [],
            subTotalAmount: 0,
            totalAmount: 0
        }, () => {
            this.calculatePrice();
        })
    }

    getChooseForYou = async () => {
        await axios.post(`${window.$URL}sellerProducts/getAllProducts`)
            .then(response => {
                this.setState({
                    choosenProduct: response.data
                })
            })
            .catch(error => {
                console.log(error.data)
            })
    }

    toggleSelectProductIndividualVariant = (e, eachVariant, prodVarient) => {
        let selectedProdArray = this.state.selectedProduct;
        let quantityArray = this.state.quantityArray;
        let myMartMarginArray = this.state.myMartMarginArray;
        let myMartProfitArray = this.state.myMartProfitArray;
        let PriceArray = this.state.PriceArray;
        let sellerIDArray = this.state.sellerIDArray;
        let productIdArray = this.state.productIdArray;
        const { checked } = e.target;
        if (checked) {
            console.log({ eachVariant });
            selectedProdArray.push(eachVariant._id);

            quantityArray.push(1);
            myMartMarginArray.push(eachVariant.myMartMargin);
            myMartProfitArray.push(eachVariant.myMartProfit);
            PriceArray.push(eachVariant.Price);
            sellerIDArray.push(eachVariant.sellerId);
            productIdArray.push(prodVarient);

        } else {
            let findIndex = selectedProdArray.indexOf(eachVariant._id);
            selectedProdArray.splice(findIndex, 1);
            quantityArray.splice(findIndex, 1);
            myMartMarginArray.splice(findIndex, 1);
            myMartProfitArray.splice(findIndex, 1);
            PriceArray.splice(findIndex, 1);
            sellerIDArray.splice(findIndex, 1);
            productIdArray.push(findIndex, 1);
        }
        this.setState({
            selectedProduct: selectedProdArray,
            quantityArray,
            myMartMarginArray,
            myMartProfitArray,
            PriceArray,
            sellerIDArray,
            subTotalAmount: 0,
            totalAmount: 0,
        }, () => {
            this.calculatePrice()
        })

    }

    toggleSelectProductVariant = (e, cartProductArray) => {
        let selectedProdArray = this.state.selectedProduct;
        let quantityArray = this.state.quantityArray;
        let myMartMarginArray = this.state.myMartMarginArray;
        let myMartProfitArray = this.state.myMartProfitArray;
        let PriceArray = this.state.PriceArray;
        let sellerIDArray = this.state.sellerIDArray;
        let productIdArray = this.state.productIdArray;
        const { checked } = e.target;
        if (checked) {
            for (let j = 0; j <= cartProductArray.productVariantId.length - 1; j++) {
                selectedProdArray.push(cartProductArray.productVariantId[j]._id);
                quantityArray.push(cartProductArray.quantity[j]);
                myMartMarginArray.push(cartProductArray.productVariantId[j].myMartMargin);
                myMartProfitArray.push(cartProductArray.productVariantId[j].myMartProfit);
                PriceArray.push(cartProductArray.productVariantId[j].Price);
                sellerIDArray.push(cartProductArray.productId[j].sellerId);
                productIdArray.push(cartProductArray.productId[j]);
            }
        } else {
            for (let j = 0; j <= cartProductArray.productVariantId.length - 1; j++) {
                let findIndex = selectedProdArray.indexOf(cartProductArray.productVariantId[j]._id);
                selectedProdArray.splice(findIndex, 1);
                quantityArray.splice(findIndex, 1);
                myMartMarginArray.splice(findIndex, 1);
                myMartProfitArray.splice(findIndex, 1);
                PriceArray.splice(findIndex, 1);
                sellerIDArray.splice(findIndex, 1);
                productIdArray.splice(findIndex, 1);
            }
        }
        this.setState({
            selectedProduct: selectedProdArray,
            quantityArray,
            myMartMarginArray,
            myMartProfitArray,
            PriceArray,
            sellerIDArray,
            productIdArray,
            subTotalAmount: 0,
            totalAmount: 0,
        }, () => {
            this.calculatePrice()
        })

    }

    selectAllProd = (e) => {
        const { checked } = e.target;
        const { cartArray } = this.state;
        // console.log('cartarray',cartArray)
        let selectedArray = [];
        let quantityArray = [];
        let myMartMarginArray = [];
        let myMartProfitArray = [];
        let PriceArray = [];
        let sellerIDArray = [];
        let productIdArray = [];
        if (checked) {
            for (let i = 0; i <= cartArray.length - 1; i++) {
                for (let j = 0; j <= cartArray[i].productVariantId.length - 1; j++) {
                    selectedArray.push(cartArray[i].productVariantId[j]._id);
                    productIdArray.push(cartArray[i].productId[j]);
                    quantityArray.push(cartArray[i].quantity[j]);
                    myMartMarginArray.push(cartArray[i].productVariantId[j].myMartMargin);
                    myMartProfitArray.push(cartArray[i].productVariantId[j].myMartProfit);
                    PriceArray.push(cartArray[i].productVariantId[j].Price);
                    sellerIDArray.push(cartArray[i].productId[j].sellerId);
                }
            }
        } else {
            selectedArray = [];
            quantityArray = [];
        }
        this.setState({
            selectedProduct: selectedArray,
            quantityArray,
            myMartMarginArray,
            myMartProfitArray,
            PriceArray,
            sellerIDArray,
            productIdArray,
            subTotalAmount: 0,
            totalAmount: 0,
        }, () => {
            this.calculatePrice()
        })

    }
    getDateValid = (prodArray) => {
        let returnValidate = false;
        if (prodArray.startDate !== "" && prodArray.endDate !== "") {
            let convertStartDate = moment(prodArray.startDate).unix() * 1000;
            let convertEndDate = moment(prodArray.endDate).unix() * 1000;
            let systemTime = moment(new Date()).unix() * 1000;
            if (systemTime <= convertEndDate && systemTime >= convertStartDate) {
                returnValidate = true;
            }
        }
        return returnValidate;
    }

    calculatePrice() {
        const { selectedProduct, cartArray, couponAmount, couponApplied, shippingFee } = this.state;
        let subTotalAmount = this.state.subTotalAmount;
        let totalAmount = this.state.totalAmount;

        for (let i = 0; i <= cartArray.length - 1; i++) {
            for (let j = 0; j <= cartArray[i].productVariantId.length - 1; j++) {
                if (selectedProduct.includes(cartArray[i].productVariantId[j]._id)) {

                    subTotalAmount +=
                        cartArray[i].productVariantId[j].specialPriceProvided === 'true' && this.getDateValid(cartArray[i].productVariantId[j])
                            ?
                            parseFloat(cartArray[i].productVariantId[j].specialPrice) * parseFloat(cartArray[i].quantity[j])
                            :
                            parseFloat(cartArray[i].productVariantId[j].Price) * parseFloat(cartArray[i].quantity[j]);

                    totalAmount +=
                        cartArray[i].productVariantId[j].specialPriceProvided === 'true' && this.getDateValid(cartArray[i].productVariantId[j])
                            ?
                            parseFloat(cartArray[i].productVariantId[j].specialPrice) * parseFloat(cartArray[i].quantity[j])
                            :
                            parseFloat(cartArray[i].productVariantId[j].Price) * parseFloat(cartArray[i].quantity[j]);
                }
                this.setState({
                    subTotalAmount: parseFloat(subTotalAmount).toFixed(2),
                    totalAmount: parseFloat(totalAmount).toFixed(2),
                })
            }
        }
        if (couponApplied) {

            const finaltotalAmount = (parseFloat(totalAmount) + parseFloat(shippingFee) - parseFloat(couponAmount)).toFixed(2)
            this.setState({
                finaltotalAmount: finaltotalAmount
            })
        } else {
            const finaltotalAmount = parseFloat(totalAmount + shippingFee).toFixed(2)
            this.setState({
                finaltotalAmount: finaltotalAmount,
            }, () => {
                this.getSelectedProdArr();
            })
        }


    }

    // getSelectedProdArr = () => {
    //     const { selectedProduct,totalCartArray } = this.state;
    //     let CartarrData = totalCartArray
    //     let mainarr = []
    //     for(let i=0;i<CartarrData.length;i++){
    //         let checkarr =[]
    //         for(let j=0;j<CartarrData[i].productVariantId.length;j++){
    //             if(selectedProduct.includes(CartarrData[i].productVariantId[j]._id)){
    //                 checkarr.push(CartarrData[i].productVariantId[j])
    //             }
    //         }
    //         if(checkarr.length > 0){
    //             CartarrData[i].productVariantId = checkarr
    //             mainarr.push(CartarrData[i])
    //         }
    //     }

    //     console.log(mainarr)
    //         // var selectData = []
    //         // data.map((cartData,cartindex) => {
    //         //     selectData.push(cartData)
    //         // })
    //     //     data.map((selectcartData,selectcartindex) => {
    //     //         selectcartData.productVariantId.map((varData,varIndex) => {
    //     //         const found = selectedProduct.indexOf(varData._id)
    //     //         if(found === -1){
    //     //             data[selectcartindex].productVariantId.splice(varIndex, 1)
    //     //         // console.log( selectData[cartindex].productVariantId[varIndex])
    //     //         }
    //     //     })
    //     // })
    //         // console.log('select data',data)


    //         // this.setState({
    //         //     selectedProdarr:data
    //         // })

    //     // const newData = cartData.map((data,index) => {
    //     //     const yoo = data.productVariantId.map((varData,varIndex) => {
    //     //         const found = selectedProduct.indexOf(varData._id)
    //     //         let finalData = []
    //     //         if(found === -1){
    //     //             finalData = data.productVariantId.splice(varIndex, 1)
    //     //         }
    //     //         return finalData
    //     //     })
    //     //     return yoo
    //     // })
    //     // console.log(newData)
    // }

    getSelectedProdArr = async () => {
        let dataArray = await this.getSellerWiseCartData();
        this.setState({
            selectedProdarr: dataArray.orderArraySellerWise,
        })

        // console.log('data array',dataArray.orderArraySellerWise)
    }

    deleteCart = async (cartId, variantIndex) => {
        let checkToken = await localStorage.getItem("token");
        if (checkToken) {
            this.props.RemoveCart({ cartId, variantIndex });
        } else {
            let checkTempToken = await localStorage.getItem("Temptoken");
            if (checkTempToken) {
                this.props.RemoveWithOutCart({ cartId, variantIndex });
            }
        }
    }

    showDetails = (tabname) => {
        this.setState({
            tabName: tabname
        })
    }

    quantityAdd = async (cartId, variantIndex) => {

        let checkToken = await localStorage.getItem("token");
        if (checkToken) {
            const decoded = jwt_decode(checkToken);
            await axios.post(`${window.$URL}cart/addQuantity`, { customerID: decoded.userid, cartId, variantIndex })
                .then((response) => {
                    if (response.data.success) {
                        this.props.UpdateCart();
                        toast.success(response.data.message, {
                            position: "bottom-center",
                            autoClose: 3000,
                            hideProgressBar: true,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });
                    } else {
                        toast.error(response.data.message, {
                            position: "bottom-center",
                            autoClose: 3000,
                            hideProgressBar: true,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });
                    }
                })
                .catch(error => {
                    console.log(error);
                })
        } else {
            let checkTempToken = await localStorage.getItem("Temptoken");
            if (checkTempToken) {
                await axios.post(`${window.$URL}cart/addwithoutQuantity`, { tempID: checkTempToken, cartId, variantIndex })
                    .then(response => {
                        if (response.data.success) {
                            this.props.UpdateWithOutCart();
                            toast.success(response.data.message, {
                                position: "bottom-center",
                                autoClose: 3000,
                                hideProgressBar: true,
                                closeOnClick: true,
                                pauseOnHover: true,
                                draggable: true,
                                progress: undefined,
                            });
                        } else {
                            toast.error(response.data.message, {
                                position: "bottom-center",
                                autoClose: 3000,
                                hideProgressBar: true,
                                closeOnClick: true,
                                pauseOnHover: true,
                                draggable: true,
                                progress: undefined,
                            });
                        }
                    })
                    .catch(error => {
                        console.log(error.data)
                    })
            }
        }

    }

    quantitySubtract = async (cartId, variantIndex) => {
        let checkToken = await localStorage.getItem("token");
        if (checkToken) {
            const decoded = jwt_decode(checkToken);
            await axios.post(`${window.$URL}cart/subtractQuantity`, { customerID: decoded.userid, cartId, variantIndex })
                .then((response) => {
                    if (response.data.success) {
                        this.props.UpdateCart();
                        toast.error(response.data.message, {
                            position: "bottom-center",
                            autoClose: 3000,
                            hideProgressBar: true,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });
                    } else {
                        toast.error(response.data.message, {
                            position: "bottom-center",
                            autoClose: 3000,
                            hideProgressBar: true,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });
                    }
                })
                .catch(error => {
                    console.log(error);
                })
        } else {
            let checkTempToken = await localStorage.getItem("Temptoken");
            if (checkTempToken) {
                await axios.post(`${window.$URL}cart/subtractwithoutQuantity`, { tempID: checkTempToken, cartId, variantIndex })
                    .then(response => {
                        if (response.data.success) {
                            this.props.UpdateWithOutCart();
                            toast.success(response.data.message, {
                                position: "bottom-center",
                                autoClose: 3000,
                                hideProgressBar: true,
                                closeOnClick: true,
                                pauseOnHover: true,
                                draggable: true,
                                progress: undefined,
                            });
                        } else {
                            toast.error(response.data.message, {
                                position: "bottom-center",
                                autoClose: 3000,
                                hideProgressBar: true,
                                closeOnClick: true,
                                pauseOnHover: true,
                                draggable: true,
                                progress: undefined,
                            });
                        }
                    })
                    .catch(error => {
                        console.log(error.data)
                    })
            }
        }

    }

    couponAppliedFunc = async (couponCode) => {
        const { cartArray, totalAmount } = this.state;
        let categoryArray = [];
        let productIdArray = [];
        for (let i = 0; i <= cartArray.length - 1; i++) {
            for (let j = 0; j <= cartArray[i].productId.length - 1; j++) {
                for (let k = 0; k <= cartArray[i].productId[j].category.length - 1; k++) {
                    if (!categoryArray.includes(cartArray[i].productId[j].category[k])) {
                        categoryArray.push(cartArray[i].productId[j].category[k]);
                    }
                }
                if (!productIdArray.includes(cartArray[i].productId[j].productId)) {
                    productIdArray.push(cartArray[i].productId[j].productId);
                }
            }
        }
        await axios.post(`${window.$URL}couponDiscount/checkCouponCode`, { categoryArray, productIdArray, couponCode, totalAmount })
            .then((response) => {
                if (response.data.valid) {
                    this.setState({
                        couponApplied: true,
                        couponAmount: response.data.amount
                    }, () => {
                        this.calculatePrice();
                    })
                } else {
                    this.setState({
                        couponApplied: false,
                        couponAmount: 0
                    })
                }
            })
            .catch(error => {
                console.log(error);
            })

    }

    removeCouponFunc = () => {
        this.setState({
            couponApplied: false,
            couponAmount: 0,
            subTotalAmount: 0,
            totalAmount: 0,
        }, () => {
            this.calculatePrice();
        })
    }

    fetchDeliveryAddress = (addressId) => {
        this.setState({
            selectedAddress: addressId
        })
    }

    fetchDeliveryTimeSlot = (timeSlot) => {
        this.setState({
            selectedTimeSlot: timeSlot,
            shippingFee: timeSlot.shippingFee,
            couponAmount: 0,
            subTotalAmount: 0,
            totalAmount: 0,
        }, () => {
            this.calculatePrice();
        })
    }

    proceedNextTab = () => {
        const { selectedProduct, totalAmount, selectedAddress, selectedTimeSlot, tabName, deliveryTime } = this.state;

        if (tabName === 'cart') {
            if (selectedProduct.length < 1 || totalAmount < 1) {
                toast.error('Please select product from cart', {
                    position: "bottom-center",
                    autoClose: 3000,
                    hideProgressBar: true,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });
            } else {
                this.setState({
                    tabName: 'delivery'
                })
            }
        }
        // else if(tabName === 'address'){
        //     if(selectedProduct.length < 1 || totalAmount < 1 || selectedAddress === null){
        //         toast.error('Please choose address', {
        //             position: "bottom-center",
        //             autoClose: 3000,
        //             hideProgressBar: true,
        //             closeOnClick: true,
        //             pauseOnHover: true,
        //             draggable: true,
        //             progress: undefined,
        //         });
        //     }else{
        //         this.setState({
        //             tabName: 'delivery'
        //         })
        //     }
        // }
        else if (tabName === 'delivery') {
            // if(selectedProduct.length < 1 || totalAmount < 1 || selectedAddress === null || selectedTimeSlot.length < 1){
            if (selectedProduct.length < 1 || totalAmount < 1 || selectedTimeSlot.length < 1) {
                toast.error('Please select preferred time slot', {
                    position: "bottom-center",
                    autoClose: 3000,
                    hideProgressBar: true,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });
            } else {
                this.setState({
                    tabName: 'payment'
                })
            }
        } else if (tabName === 'payment') {
            // if(selectedProduct.length < 1 || totalAmount < 1 ||  selectedTimeSlot.length < 1 || deliveryTime.length < 1){
            if (selectedProduct.length < 1 || totalAmount < 1 || selectedTimeSlot.length < 1) {
                toast.error('Please fill up all the forms', {
                    position: "bottom-center",
                    autoClose: 3000,
                    hideProgressBar: true,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });

            } else {
                if (selectedAddress !== null) {
                    this.sendOTP(selectedAddress);
                    // this.saveOrder();
                }
            }
        }
    }

    sendOTP = async (selectedAddressId) => {
        console.log({selectedAddressId});
        await axios.post(`${window.$URL}order/send-order-sms-code`, { selectedAddressId })
        .then((response) => {
            if(response.data.success){
                this.setState({
                    showOtpModal: true
                })
            }
        })
        .catch(error => {
            console.log(error);
            toast.error('Something went wrong', {
                position: "bottom-center",
                autoClose: 3000,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        })         
    }

    fetchVariantDetails = async (variantId) => {
        let filterCartData = {};
        await axios.post(`${window.$URL}sellerProducts/fetchVariantDetails`, { variantId })
            .then(response => {
                filterCartData = response.data.data;
            })
            .catch(error => {
                console.log(error);
            })
        return filterCartData;
    }

    fetchSellerDetails = async (sellerId) => {
        let filtersellerData = {};
        await axios.get(`${window.$URL}user/Details/${sellerId}`)
            .then(response => {
                filtersellerData = response.data;
            })
            .catch(error => {
                console.log(error);
            })
        return filtersellerData;

    }

    getSellerWiseCartData = async () => {
        const { couponApplied, couponAmount, shippingFee, subTotalAmount, selectedProduct, quantityArray, myMartMarginArray, myMartProfitArray, PriceArray, sellerIDArray, totalAmount, selectedAddress, selectedTimeSlot, cartArray, productIdArray, shippingType, deliveryTime } = this.state;
        let checkToken = await localStorage.getItem("token");
        if (checkToken) {
            const decoded = jwt_decode(checkToken);
            let data = {
                productVariantId: selectedProduct,
                productVariantArray: [],
                quantity: quantityArray,
                myMartMargin: myMartMarginArray,
                myMartProfit: myMartProfitArray,
                Price: PriceArray,
                sellerID: sellerIDArray,
                deliveryAddressId: selectedAddress,
                deliveryTimeSlotFrom: selectedTimeSlot.concatSlotFrom,
                deliveryTimeSlotTo: selectedTimeSlot.concatSlotTo,
                customerId: decoded.userid,
                orderStatus: 'pending',
                couponApplied: couponApplied,
                couponAmount: couponAmount,
                shippingFee: shippingFee,
                shippingType: shippingType,
                deliveryTime: '',
                subTotal: subTotalAmount,
                Total: totalAmount,
                paymentMode: 'COD'
            }

            let sellerId = [];
            let orderArraySellerWise = [];

            for (let i = 0; i <= data.sellerID.length - 1; i++) {
                let variantIdArray = [];
                let quantitySellerArray = [];
                let myMartMarginSellerArray = [];
                let myMartProfitSellerArray = [];
                let priceSellerArray = [];
                let orderProductStatusArray = [];
                let productVariantArrayzz = [];
                let sellerIdArrayzzz = [];
                let productArr = [];

                if (!sellerId.includes(data.sellerID[i])) {
                    sellerId.push(data.sellerID[i]);
                    variantIdArray.push(selectedProduct[i]);
                    quantitySellerArray.push(quantityArray[i]);
                    myMartMarginSellerArray.push(myMartMarginArray[i]);
                    myMartProfitSellerArray.push(myMartProfitArray[i]);
                    priceSellerArray.push(PriceArray[i]);
                    orderProductStatusArray.push('pending');
                    productVariantArrayzz.push(await this.fetchVariantDetails(selectedProduct[i]))
                    sellerIdArrayzzz.push(await this.fetchSellerDetails(data.sellerID[i]))
                    productArr.push(productIdArray[i]);
                    let orderData = {
                        productVariantId: variantIdArray,
                        productVariantArray: productVariantArrayzz,
                        quantity: quantitySellerArray,
                        myMartMargin: myMartMarginSellerArray,
                        myMartProfit: myMartProfitSellerArray,
                        Price: priceSellerArray,
                        sellerID: data.sellerID[i],
                        sellerIDArray: sellerIdArrayzzz,
                        productId: productArr,
                        deliveryAddressId: selectedAddress,
                        deliveryTimeSlotFrom: selectedTimeSlot.concatSlotFrom,
                        deliveryTimeSlotTo: selectedTimeSlot.concatSlotTo,
                        customerId: decoded.userid,
                        orderStatus: 'pending',
                        couponApplied: couponApplied,
                        couponAmount: couponAmount,
                        shippingFee: shippingFee,
                        shippingType: '',
                        deliveryTime: '',
                        subTotal: subTotalAmount,
                        Total: totalAmount,
                        paymentMode: 'COD',
                        orderProductStatus: orderProductStatusArray
                    }
                    orderArraySellerWise.push(orderData);
                } else {
                    let findSellerIndex = sellerId.indexOf(data.sellerID[i]);
                    orderArraySellerWise[findSellerIndex].productVariantArray.push(await this.fetchVariantDetails(selectedProduct[i]));
                    orderArraySellerWise[findSellerIndex].productId.push(productIdArray[i]);
                    orderArraySellerWise[findSellerIndex].productVariantId.push(data.productVariantId[i]);
                    orderArraySellerWise[findSellerIndex].quantity.push(data.quantity[i]);
                    orderArraySellerWise[findSellerIndex].myMartMargin.push(data.myMartMargin[i]);
                    orderArraySellerWise[findSellerIndex].myMartProfit.push(data.myMartProfit[i]);
                    orderArraySellerWise[findSellerIndex].Price.push(data.Price[i]);
                    orderArraySellerWise[findSellerIndex].orderProductStatus.push('pending');
                }
            }
            if(shippingType === 'seller'){
                for(let j=0; j < deliveryTime.length; j++){
                    let deliveryInfo = await this.fetchSellerDelivery(deliveryTime[j]);
                    orderArraySellerWise[j].deliveryTime = JSON.stringify(deliveryInfo);
                    orderArraySellerWise[j].deliveryTimeSlotFrom = this.fetchDate(deliveryInfo, 'from');
                    orderArraySellerWise[j].deliveryTimeSlotTo = this.fetchDate(deliveryInfo, 'to');
                }
            }
            orderArraySellerWise.forEach((element, index) => {
                return element.shippingType = shippingType[index];
            })
            let returnData = {
                orderArraySellerWise,
                sellerId
            };

            return returnData;
        } else {
            let returnData = {
                orderArraySellerWise: [],
                sellerId: ''
            };
            return returnData;
        }
    }

    fetchDate = (array, dateType) => {
        let deliveryDate = '';
        if(dateType === 'from') {
            if(array.deliveryTime === 'hrs'){
                deliveryDate = moment(moment().add(array.minHoursDelivery, 'h').format('YYYY-MM-DD HH:mm:ss')).unix();
            } else {
                deliveryDate = moment(moment().add(array.minDaysDelivery,'d').format('YYYY-MM-DD') + ' ' + moment('00:00', ["hh A"]).format('HH:mm:ss')).unix();
            }
        } else {
            if(array.deliveryTime === 'hrs'){
                deliveryDate = moment(moment().add(array.maxHoursDelivery, 'h').format('YYYY-MM-DD HH:mm:ss')).unix();
            } else {
                deliveryDate = moment(moment().add(array.maxDaysDelivery,'d').format('YYYY-MM-DD') + ' ' + moment('03:00', ["hh A"]).format('HH:mm:ss')).unix();
            }
        }
        return deliveryDate;
    }

    fetchSellerDelivery = async(dataId) => {
        let returnDetails = {};
        await axios.get(`${window.$URL}deliveryRule/fetchSellerDeliveryRule/${dataId}`)
        .then(response => {
            if(response.data.success){
                returnDetails = response.data.data
            }
        })
        .catch(error => {
            console.log(error);
        })
        return returnDetails;
    }

    finallyupdatedeltypetime = (deliveryData, shippingType) => {
        console.log({ deliveryData, shippingType })
        this.setState({
            shippingType: shippingType,
            deliveryTime: deliveryData
        })
    }
    createMarkup = (html) => {
        return {
            __html: html,
        };
    };
    saveOrder = async (e) => {
        e.preventDefault();
        let checkValidation = this.validateOtp();
        if(checkValidation){
            const {verifyCode, selectedAddress} = this.state;
            await axios.post(`${window.$URL}order/otp-verifyCode`, {
                selectedAddress,
                verifyCode,
            })
            .then(async response => {
                if(response.data.success){
                    this.setState({ loading: true });
                    let dataArray = await this.getSellerWiseCartData();
                    await axios.post(`${window.$URL}order/orderSave`, dataArray)
                    .then(orderResponse => {
                        if (orderResponse.data.success) {
                            toast.success(orderResponse.data.message, {
                                position: "bottom-center",
                                autoClose: 3000,
                                hideProgressBar: true,
                                closeOnClick: true,
                                pauseOnHover: true,
                                draggable: true,
                                progress: undefined,
                            });
                            setTimeout(() => {
                                this.props.UpdateCart();
                                this.setState({
                                    loading: true,
                                    totalCartArray: [],
                                    orderSuccessAlert: true,
                                    orderMessageAlert: <p>Thank you for your Cash On Delivery order <b># {orderResponse.data.data.OrderId} </b>. We have sent a email to <b>{orderResponse.data.data.Email}</b> with the confirmation. <br />You will be contacted on <b>{orderResponse.data.data.Phone}</b> for your delivery. <br /><br /> <b>support@mymart.qa</b> </p>
                                })
                            }, 2000);
                        } else {
                            toast.error(orderResponse.data.message, {
                                position: "bottom-center",
                                autoClose: 3000,
                                hideProgressBar: true,
                                closeOnClick: true,
                                pauseOnHover: true,
                                draggable: true,
                                progress: undefined,
                            });
                        }
                    })
                    .catch(error => {
                        console.log(error);
                    })
                } else {
                    toast.error(response.data.message, {
                        position: "bottom-center",
                        autoClose: 3000,
                        hideProgressBar: true,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                }
            })
            .catch(error => {
                console.log(error);
            })
        }
    }

    selectedDel = async () => {
        const { selectedProduct } = this.state
        if (selectedProduct.length > 0) {
            let checkToken = await localStorage.getItem("token");
            if (checkToken) {
                const decoded = jwt_decode(checkToken);
                const userid = decoded.userid
                // const anselectedProduct = JSON.stringify(selectedProduct)
                // console.log('selected prod',anselectedProduct)
                this.props.RemoveSelectedCart({ selectedProduct });
                this.setState({
                    selectedProduct: [],
                    quantityArray: [],
                    myMartMarginArray: [],
                    myMartProfitArray: [],
                    PriceArray: [],
                    sellerIDArray: [],
                    productIdArray: [],
                }, () => {
                    this.componentDidMount();
                })
            } else {
                let checkTempToken = await localStorage.getItem("Temptoken");
                if (checkTempToken) {
                    this.props.RemoveSelectedWithOutCart({ selectedProduct });
                    this.setState({
                        selectedProduct: [],
                        quantityArray: [],
                        myMartMarginArray: [],
                        myMartProfitArray: [],
                        PriceArray: [],
                        sellerIDArray: [],
                        productIdArray: [],
                    }, () => {
                        this.componentDidMount();
                    })
                }
            }

        } else {
            toast.error('Please select items to delete', {
                position: "bottom-center",
                autoClose: 3000,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }
    }

    getcouponseller = async (couponAmounttt) => {
        if (couponAmounttt > 0) {
            this.setState({
                couponApplied: true,
                couponAmount: couponAmounttt
            }, () => {
                this.calculatePrice();
            })
        } else {
            this.setState({
                couponApplied: false,
                couponAmount: couponAmounttt
            }, () => {
                this.calculatePrice();
            })
        }

    }

    hideComingModal() {
        this.setState({ showOtpModal: false })
    }
    validateOtp = () => {
        this.setState({
            verifyCodeError: '',
        });

        let verifyCodeError = '';
        if (this.state.verifyCode.length < 1) {
            verifyCodeError = "Please enter OTP";
        } else if (this.state.verifyCode.length < 4) {
            verifyCodeError = "Invalid Code";

        }

        if (verifyCodeError) {
            this.setState({ verifyCodeError })
            return false
        }

        this.setState({
            verifyCodeError
        })

        return true
    }

    redirectHome = () => {
        window.location.href = `${process.env.PUBLIC_URL}/`;
    }

    onChange = (e) => {
        const { name, value } = e.target;
        this.setState({
            [name]: value,
        });
        
    };

    render() {
        const { loading, cartArray, totalCartArray, choosenProduct, tabName, selectedProduct, subTotalAmount, shippingFee, totalAmount, finaltotalAmount, couponApplied, couponAmount, selectedAddress, orderSuccessAlert, orderMessageAlert, showOtpModal, verifyCodeError } = this.state;
        if (loading) {
            return (
                <section className="cart-block">
                    <div className="container">
                        <Loader />
                    </div>
                </section>
            )
        } else if (totalCartArray.length > 0) {
            return (
                <>
                    <div>
                        <section className="cart-block new-cart-modification">
                            <ToastContainer />
                            <div className="container">
                                <div className="cart-block-content">
                                    <CartHeader cartArray={totalCartArray} showTab={(pageType) => this.showDetails(pageType)} setActiveHeader={tabName} />
                                    <div className="tab-content" id="pills-tabContent">
                                        <div className={tabName === 'cart' ? "tab-pane fade show active" : "tab-pane fade"} id="pills-cart " role="tabpanel" aria-labelledby="pills-cart-tab">
                                            <div className="cart-items-content new-checkout-pannel">
                                                <div className="row">
                                                    <CartProducts
                                                        totalCartArray={totalCartArray}
                                                        cartArray={cartArray}
                                                        couponapplied={couponApplied}
                                                        showProductFilter={(type) => this.showItems(type)}
                                                        allSelect={(val) => this.selectAllProd(val)}
                                                        productVariantSelect={(val, cartId) => this.toggleSelectProductVariant(val, cartId)}
                                                        productVariantIndividualSelect={(val, eachVariant, prodVarient) => this.toggleSelectProductIndividualVariant(val, eachVariant, prodVarient)}
                                                        selectedProduct={selectedProduct}
                                                        deleteCartData={(cartId, variantIndex) => this.deleteCart(cartId, variantIndex)}
                                                        addQuantity={(cartId, variantIndex) => this.quantityAdd(cartId, variantIndex)}
                                                        subtractQuantity={(cartId, variantIndex) => this.quantitySubtract(cartId, variantIndex)}
                                                        getselectedDel={this.selectedDel}
                                                        getyooCoupon={this.getcouponseller}
                                                    />
                                                    <OrderSummary items={selectedProduct.length} subTotal={subTotalAmount} shippingFee={shippingFee} total={totalAmount} finalTotal={finaltotalAmount} couponAppliedCheck={(code) => this.couponAppliedFunc(code)} couponApplied={couponApplied} couponAmount={couponAmount} removeCoupon={() => this.removeCouponFunc()} processNext={() => this.proceedNextTab()} selectedAddress={selectedAddress} tabName={tabName} getDeliveryAddress={(addressID) => this.fetchDeliveryAddress(addressID)} />
                                                </div>
                                            </div>
                                        </div>
                                        {/* <div className={tabName === 'address' ? "tab-pane fade show active" : "tab-pane fade"} id="pills-address" role="tabpanel" aria-labelledby="pills-address-tab">
                                            <div className="cart-items-content">
                                                <div className="row">
                                                    <UserAddress getDeliveryAddress={(addressID) => this.fetchDeliveryAddress(addressID)}  colLayout={'col-md-12 col-sm-12 col-xs-12 col-lg-12'} />
                                                    <OrderSummary items={selectedProduct.length} subTotal={subTotalAmount} shippingFee={shippingFee} total={totalAmount} couponAppliedCheck={(code) => this.couponAppliedFunc(code)} couponApplied = {couponApplied} couponAmount={couponAmount} removeCoupon={() => this.removeCouponFunc()} processNext={() => this.proceedNextTab()} selectedAddress={selectedAddress} />
                                                </div>
                                            </div>
                                        </div> */}
                                        <div className={tabName === 'delivery' ? "tab-pane fade show active" : "tab-pane fade"} id="pills-delivery" role="tabpanel" aria-labelledby="pills-delivery-tab">
                                            <div className="cart-items-content ">
                                                <div className="row">
                                                    <UserDeliverySlot getDeliveryTimeSlot={(timeSlot) => this.fetchDeliveryTimeSlot(timeSlot)} selectedProduct={this.state.selectedProdarr} updateDeliverytimeType={(deliveryData, shippingType) => this.finallyupdatedeltypetime(deliveryData, shippingType)} />
                                                    <OrderSummary items={selectedProduct.length} subTotal={subTotalAmount} shippingFee={shippingFee} total={totalAmount} finalTotal={finaltotalAmount} couponAppliedCheck={(code) => this.couponAppliedFunc(code)} couponApplied={couponApplied} couponAmount={couponAmount} removeCoupon={() => this.removeCouponFunc()} processNext={() => this.proceedNextTab()} selectedAddress={selectedAddress} tabName={tabName} getDeliveryAddress={(addressID) => this.fetchDeliveryAddress(addressID)} />
                                                </div>
                                            </div>
                                        </div>
                                        <div className={tabName === 'payment' ? "tab-pane fade show active" : "tab-pane fade"} id="pills-payment" role="tabpanel" aria-labelledby="pills-payment-tab">
                                            <div className="cart-items-content">
                                                <div className="row">
                                                    <CartPayment items={selectedProduct.length} subTotal={subTotalAmount} shippingFee={shippingFee} total={totalAmount} couponApplied={couponApplied} couponAmount={couponAmount} selectedAddress={selectedAddress} />
                                                    <OrderSummary items={selectedProduct.length} subTotal={subTotalAmount} shippingFee={shippingFee} total={totalAmount} finalTotal={finaltotalAmount} couponAppliedCheck={(code) => this.couponAppliedFunc(code)} couponApplied={couponApplied} couponAmount={couponAmount} removeCoupon={() => this.removeCouponFunc()} processNext={() => this.proceedNextTab()} selectedAddress={selectedAddress} tabName={tabName} getDeliveryAddress={(addressID) => this.fetchDeliveryAddress(addressID)} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        {/* cart block end */}
                        {/* my mall start */}
                        {/*<section className="new_tranding">
                            <div className="container">
                                <div className="title_section">
                                    <h2>Related Products</h2>
                                    <a href={`${process.env.PUBLIC_URL}/productListing/choosen/products`} className="btn view-all">View All</a>
                                </div>
                                <ProductListing productArray={choosenProduct} />
                            </div>
                        </section>*/}
                        <div className="my-mart-signup-wr">
                            <div className="sinup-container">
                            {showOtpModal ? (
                                <div
                                    id="exampleModalCenter"
                                    className="modal fade show"
                                    tabIndex={-1}
                                    role="dialog"
                                    aria-labelledby="exampleModalCenterTitle"
                                    style={{ display: "block", paddingRight: 17 }}
                                >
                                    <div
                                        className="modal-dialog modal-dialog-centered"
                                        role="document"
                                    >
                                        <div className="modal-content">
                                            <div className="modal-header">
                                                {/* <h5 className="modal-title" id="exampleModalCenterTitle">Page Status</h5> */}
                                                <button
                                                    type="button"
                                                    onClick={() => this.hideComingModal()}
                                                    className="close"
                                                    data-dismiss="modal"
                                                    aria-label="Close"
                                                >
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                            <div className="modal-body">
                                                {/* <p>Promotions is Comming...</p> */}
                                                <h1>Enter your OTP </h1>
                                                <div className="form-sign-wrapper">
                                                    <form onSubmit={this.saveOrder}>
                                                        <div className="header-bottom">
                                                            <p className="text-center"><i className="fa fa-warning icon-style" /> Please check your MessageBox for the OTP.</p>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-lg-3 col-md-6 col-sm-12 form-parts">
                                                            </div>
                                                            <div className="col-lg-6 col-md-6 col-sm-12 form-parts">
                                                                <div className="form-group divInner">
                                                                    <label style={{ textAlign: 'center', width: '100%' }} className="input-heading">Enter OTP</label>
                                                                    <div id="divOuter">
                                                                        <div id="divInner">
                                                                            <input id="partitioned" name="verifyCode" type="text" maxlength="4" onChange={this.onChange} />
                                                                        </div>
                                                                    </div>
                                                                    {verifyCodeError ? <span className="error-class"> {verifyCodeError} </span> : null}
                                                                </div>
                                                                <div className="form-group">
                                                                    <button type="submit" className="btn signup_btn" >Submit</button>
                                                                </div>
                                                                <div className="form-group">
                                                                    <p className="already_signUp">Have not recived OTP? <button onClick={() => this.resendOTP(this.state.phoneNumber)}>Resend </button></p>
                                                                </div>
                                                            </div>
                                                            <div className="col-lg-3 col-md-6 col-sm-12 form-parts">
                                                            </div>

                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            ) : (
                                ""
                            )}
                            </div>
                        </div>
                    </div>
                </>
            )
        } else {
            return (
                <section className="cart-block">
                    {orderSuccessAlert ?
                        <SweetAlert
                            success
                            confirmBtnText="OK"
                            confirmBtnBsStyle="success"
                            onConfirm={() => this.redirectHome()}
                        >{orderMessageAlert}</SweetAlert>
                        :
                        null
                    }
                    <div className="container">
                        <section className="cart-block new-cart-modification">
                            <ToastContainer />
                            <div className="container">
                                <div className="cart-block-content">
                                    <h2 className="title">Your Cart is Empty </h2>
                                </div>
                            </div>
                        </section>
                    </div>
                </section>
            )
        }
    }
}
const mapStateToProps = (state) => ({
    CartProducts: state.Cart,
    Location: state.Location
})
export default connect(
    mapStateToProps,
    { RemoveCart, UpdateCart, UpdateWithOutCart, RemoveWithOutCart, RemoveSelectedCart, RemoveSelectedWithOutCart }
)(MyCart);