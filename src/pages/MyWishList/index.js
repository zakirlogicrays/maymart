import React, { Component } from "react";
import ProfileSidebar from "../../Component/ProfileSidebar";
import productImage from "../../assets/images/oranges.png";
import cartIconNew from "../../assets/images/cart-ico-new.png";
import { AddToCat } from '../../data/reducers/cart';
import { connect } from 'react-redux';
import jwt_decode from "jwt-decode";
import axios from 'axios';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Loader from "../../Component/Loader";
import ProductListing from "../../Component/ProductListing";
import moment from 'moment';

class MyWishList extends Component {

  state = {

    wishListData: [],
    loading: true,
    followedStored: [],
    choosenProduct: [],
    choosenLoading: true

  }
  async componentDidMount() {
    let checkToken = await localStorage.getItem("token");
    if (checkToken) {
      const decoded = jwt_decode(checkToken);
      let customerId = decoded.userid;

      await axios.post(`${window.$URL}wishlist/fetchWishListById`, { customerId })
        .then(response => {
          if (response.status === 200) {
            if (response.data.success) {
              console.log(response.data.data);
              this.setState({
                wishListData: response.data.data,
                loading: false
              })

            } else {
              console.log('errror on fetch');
            }
          }
        })
        .catch(error => {
          console.log(error);
        })
    }

    if (checkToken) {
      const decoded = jwt_decode(checkToken);
      let customerId = decoded.userid;

      await axios.post(`${window.$URL}followedstore/fetchStore`, { customerId })
        .then(response => {
          if (response.status === 200) {
            console.log(response.data.data)

            if (response.data.success) {

              console.log(response.data.data)

              this.setState({
                followedStored: response.data.data
              })

            } else {
              console.log('errror on fetch');
            }
          }
        })
        .catch(error => {
          console.log(error);
        })
    }
    this.getChooseForYou();
  }

  getChooseForYou = async () => {
    await axios.post(`${window.$URL}sellerProducts/getAllProducts`)
    .then(response => {
        console.log(response);
        this.setState({
            choosenProduct: response.data,
            choosenLoading: false
        })
    })
    .catch(error => {
        console.log(error.data)
    })
}


  async removeWishList(productId) {
    let checkToken = await localStorage.getItem("token");
    if (checkToken) {
      const decoded = jwt_decode(checkToken);
      let customerId = decoded.userid;
      let DeletedId = {
        "productVariantId": productId,
        "customerId": customerId
      };
      await axios.post(`${window.$URL}wishlist/deletewishlist`, DeletedId)
        .then(response => {
          if (response.status === 200) {

            if (response.data.success) {
              toast.success(response.data.message, {
                position: "bottom-center",
                autoClose: 3000,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
              });
              this.componentDidMount();
            } else {
              toast.error(response.data.message, {
                position: "bottom-center",
                autoClose: 3000,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
              });
            }
          }
        })
        .catch(error => {
          console.log(error);
        })


    } else {
      window.location.href = `${process.env.PUBLIC_URL}/login`
    }
  }
  addToCart = async (data, e) => {
    e.preventDefault();
    let addToCartData = {
      productVariantId: data.productVariantId._id,
      quantity: 1,
      productId: data.productVariantId.productId,
      sellerId: data.productVariantId.sellerProductId.sellerId,
      brand: data.productVariantId.sellerProductId.brand
    }
    this.props.AddToCat(addToCartData);
  }

  async removeFollwedStore(id) {


    let DeletedId = {
      "followedId": id,
    };
    await axios.post(`${window.$URL}followedstore/deleteFollowedStore`, DeletedId)
      .then(response => {
        if (response.status === 200) {
          if (response.data.success) {
            toast.success(response.data.message, {
              position: "bottom-center",
              autoClose: 3000,
              hideProgressBar: true,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });

            this.componentDidMount();
          } else {
            toast.error(response.data.message, {
              position: "bottom-center",
              autoClose: 3000,
              hideProgressBar: true,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });

          }
        }
      })
      .catch(error => {
        console.log(error);
      })

  }
  getDateValid = (prodArray) => {
      let returnValidate = false;
      if(prodArray.startDate !== "" && prodArray.endDate !== ""){
          let convertStartDate = moment(prodArray.startDate).unix() * 1000;
          let convertEndDate = moment(prodArray.endDate).unix() * 1000;
          let systemTime = moment(new Date()).unix() * 1000;
          if(systemTime <= convertEndDate && systemTime >= convertStartDate){
          returnValidate = true;
          }
      }
      return returnValidate;
  }
  render() {
    const { wishListData, loading, followedStored, choosenProduct } = this.state;
    if (loading) {
      return (
        <Loader />
      )
    } else {
      return (
        <>
          <section className="dbF-content">
            <ToastContainer />
            <div className="container">
              <div className="row">
                <ProfileSidebar />
                <div className="col-lg-9">
                  <div className="my-order-wr">
                    <h2 className="order-title">My Wishlist &amp; Followed Stores (2)</h2>
                    <Tabs>
                      <TabList className="nav nav-pills mb-3" id="pills-tab" role="tablist">
                        <Tab className="nav-item near-store" role="presentation">My Wishlists(2)</Tab>
                        <Tab className="nav-item near-store" role="presentation">Past Purchases</Tab>
                        <Tab className="nav-item near-store" role="presentation">Followed Stores</Tab>
                      </TabList>

                      <TabPanel className="tab-content">
                        <div className="my-order-body">
                          {
                            wishListData.length > 0 ?
                              wishListData.map((data, index) => {
                                var imageData;
                                if (data.productVariantId.sellerProductId.productImageId.length < 1 || data === undefined) {
                                  imageData = `${window.$ImageURL}no-image-480x480.png`
                                } else if (data.productVariantId.sellerProductId.primaryProductImage !== null && data.productVariantId.sellerProductId.primaryProductImage !== undefined) {
                                  imageData = `${window.$ImageURL}` + data.productVariantId.sellerProductId.primaryProductImage.originalURL
                                } else {
                                  imageData = `${window.$ImageURL}` + data.productVariantId.sellerProductId.productImageId[0].originalURL
                                }
                                return (
                                  <div key={index} className="order-items my-wishlist" >
                                    <div className="row">
                                      <div className="col-md-7 col-sm-8">
                                        <div className="items-w-image">
                                          <div className="img-wr">
                                            <img src={imageData} className="img-fluid" alt={imageData} />
                                          </div>
                                          <div className="content">
                                            <span className="mall-name">{data.productVariantId.sellerProductId.brand}</span>
                                            <h1 className="pr-name">{data.productVariantId.sellerProductId.productName}
                                            </h1>
                                            {
                                              data.productVariantId.sellerProductId.category.map((eachCat, eachIndex) => {
                                                if (eachCat !== "") {
                                                  return (
                                                    <span key={eachIndex} className="wishlist-category" > {eachCat} </span>
                                                  )
                                                } else {
                                                  return null;
                                                }
                                              })
                                            }
                                            <br />
                                            <button className="btn btn-cancel mb-0" onClick={() => this.removeWishList(data.productVariantId._id)}> X Cancel</button>
                                          </div>
                                        </div>
                                      </div>
                                      <div className="col-md-2 p-0 col-sm-4">
                                        <div className="estm-section cut-priceblock">
                                          {
                                            data.productVariantId.specialPriceProvided === "true"  && this.getDateValid(data.productVariantId)?
                                              <>
                                                <p className="price">QAR {data.productVariantId.specialPrice}</p>
                                                <span className="cut-price"> QAR {data.productVariantId.Price}</span>

                                              </>
                                              :
                                              <p className="price">QAR {data.productVariantId.Price}</p>
                                          }

                                        </div>
                                      </div>
                                      <div className="col-md-3 col-sm-12">
                                        <div className="estm-section">
                                          <button onClick={() => this.addToCart(data)} className="btn btn-addcart"><img src={cartIconNew} className="img-fluid" alt={productImage} /> Add to Cart</button>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                )
                              })
                              :
                              <div className="col-md-12 col-sm-12 flex-screen">
                                <div className="card product-card" style={{ padding: '30px' }}>
                                  <h3 style={{ textAlign: 'center' }}>Wishlist is empty.</h3>
                                </div>
                              </div>

                          }
                        </div>
                      </TabPanel>
                      <TabPanel className="tab-content">
                        <div className="my-order-body">
                          Past Purchases
                      </div>
                      </TabPanel>
                      <TabPanel className="tab-content">
                        <div className="my-order-body">
                          Followed Stores
                        {
                            followedStored.length > 0 ?
                              followedStored.map((data, index) => {
                                return (
                                  <div key={index} className="order-items my-wishlist" >
                                    <div className="row">
                                      <div className="col-md-7 col-sm-8">
                                        <div className="items-w-image">
                                          <div className="img-wr">
                                            <img src={data.storeId.storeLogo !== null && data.storeId.storeLogo !== undefined ? window.$ImageURL + data.storeId.storeLogo.originalURL : "http://nodeserver.mydevfactory.com:7779/uploads/no-image-480x480.png"} className="img-fluid" alt="logo" />

                                          </div>
                                          <div className="content">
                                            {/* <span className="mall-name">Brand</span> */}
                                            <h1 className="pr-name">{data.storeId.shopName}
                                            </h1>
                                            {/* <span className="wishlist-category" > Cate </span> */}

                                            <br />
                                            <button className="btn btn-cancel mb-0" onClick={() => this.removeFollwedStore(data._id)}> X Remove</button>
                                          </div>
                                        </div>
                                      </div>


                                    </div>
                                  </div>
                                )
                              })
                              :
                              <div className="col-md-12 col-sm-12 flex-screen">
                                <div className="card product-card" style={{ padding: '30px' }}>
                                  <h3 style={{ textAlign: 'center' }}>Wishlist is empty.</h3>
                                </div>
                              </div>

                          }
                        </div>
                      </TabPanel>
                    </Tabs>


                    <div className="tab-content" id="pills-tabContent">
                      <div className="tab-pane fade show active" id="all" role="tabpanel" aria-labelledby="all-tab">

                      </div>
                      <div className="tab-pane fade" id="to-pay" role="tabpanel" aria-labelledby="to-pay-tab">
                        2
                    </div>
                      <div className="tab-pane fade" id="to-ship" role="tabpanel" aria-labelledby="to-ship-tab">
                        3
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          <section className="new_tranding">
            <div className="container">
                <div className="title_section">
                    <h2>Chosen for You</h2>
                    <a href={`${process.env.PUBLIC_URL}/productListing/choosen/products`} className="btn view-all">View All</a>
                </div>
                <ProductListing productArray={choosenProduct} loading={this.state.choosenLoading} />
            </div>
          </section>

        </>

      )
    }
  }

}

const mapStateToProps = (state) => ({
  CartProducts: state.Cart,
})

export default connect(
  mapStateToProps,
  { AddToCat }
)(MyWishList);