import React, { Component } from "react";
import axios from "axios";
import Loader from "../../Component/Loader";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import VoucherTab from "../../Component/VoucherTab";
import jwt_decode from "jwt-decode";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export default class Vouchers extends Component {
  state = {
    selectedCategory: [],
    firstBanner: [],
    loading: true,
    adminVouchers: [],
    couponArray: [],
  };

  async componentDidMount() {
    await axios
      .get(`${window.$URL}banner/fetchBanner/homepage`)
      .then((response) => {
        if (response.status === 200) {
          let bannerArray = response.data.data;
          let firstBanner = [];

          for (let i = 0; i <= bannerArray.length - 1; i++) {
            if (
              bannerArray[i].type === "1st Banner" &&
              bannerArray[i].bannerShowesTo === "vouchers"
            ) {
              firstBanner.push({
                url:
                  `${window.$ImageURL}` +
                  bannerArray[i].bannerImageId[0].originalURL,
                bannerURL: bannerArray[i].bannerURL,
              });
            }
          }
          this.setState({
            firstBanner,
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });

    this.getAllData();
    this.fetchAdminsCoupons();
    this.fetchCoupons();
  }

  getAllData = async () => {
    let catType = this.props.match.params.categoryType;
    await axios
      .get(`${window.$URL}couponDiscount/fetch-all-Coupons`)
      .then((response) => {
        console.log(response, "for debug");
        if (response.data.success) {
          console.log(response.data);

          this.setState({
            selectedCategory: response.data,
            loading: false,
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  createMarkup = (html) => {
    return {
      __html: html,
    };
  };

  removeHTML = (html) => {
    const regex = /(<([^>]+)>)/gi;
    const result = html.replace(regex, "");
    let returnName = "";
    if (result === "Groceries") {
      returnName = "Grocery";
    } else {
      returnName = result;
    }
    return returnName;
  };

  fetchAdminsCoupons = async () => {
    // let checkToken = await localStorage.getItem("token");
    // const decoded = jwt_decode(checkToken);
    // let customerId = decoded.userid;
    await axios
      .get(`${window.$URL}couponDiscount/admin-fetchCoupons`)
      .then((response) => {
        console.log(response, "admin");
        if (response.data.success) {
          this.setState({
            adminVouchers: response.data.data,
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  async collectvoucher(voucherId, sellerId) {
    //alert(voucherId);
    let checkToken = await localStorage.getItem("token");
    if (checkToken) {
      const decoded = jwt_decode(checkToken);
      let customerId = decoded.userid;
      if (sellerId == null) {
        let voucherClaimData = {
          voucherId: voucherId,
          sellerId: sellerId,
          customerId: customerId,
        };
        await axios
          .post(
            `${window.$URL}couponDiscount/customer-voucher-claim`,
            voucherClaimData
          )
          .then((response) => {
            if (response.status === 200) {
              if (response.data.success) {
                toast.success(response.data.message, {
                  position: "bottom-center",
                  autoClose: 3000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true,
                  progress: undefined,
                });
                this.componentDidMount();
              } else {
                toast.error(response.data.message, {
                  position: "bottom-center",
                  autoClose: 3000,
                  hideProgressBar: true,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true,
                  progress: undefined,
                });
              }
            }
          })
          .catch((error) => {
            console.log(error);
          });
      }
    } else {
      window.location.href = `${process.env.PUBLIC_URL}/login`;
    }
  }

  fetchCoupons = async () => {
    let checkToken = await localStorage.getItem("token");
    if (checkToken) {
      const decoded = jwt_decode(checkToken);
      let customerId = decoded.userid;
      await axios
        .post(`${window.$URL}couponDiscount/customer-fetchCoupons/`, {
          customerId,
        })
        .then((response) => {
          //console.log(response,'array')
          let colletedIds = [];
          for (let i = 0; i < response.data.data.length; i++) {
            if (!colletedIds.includes(response.data.data[i].voucherId._id)) {
              colletedIds.push(response.data.data[i].voucherId._id);
            }
          }
          if (response.data.success) {
            this.setState({
              couponArray: colletedIds,
            });
          }
        })
        .catch((error) => {
          console.log(error);
        });
    }
  };

  render() {
    const {
      firstBanner,
      loading,
      selectedCategory,
      adminVouchers,
      couponArray,
    } = this.state;

    return (
      <>
        {/* Slider Banner */}
        <section className="banner">
          <div className="container">
            <ToastContainer />
            <div className="banner-slider">
              {firstBanner.length > 0 ? (
                <Carousel
                  autoPlay
                  infiniteLoop
                  showThumbs={false}
                  showStatus={false}
                >
                  {firstBanner.map((data, index) => {
                    return (
                      <div key={index}>
                        <a href={data.bannerURL}>
                          <img
                            src={data.url}
                            alt={data.url}
                            className="banner-image"
                          />
                        </a>
                      </div>
                    );
                  })}
                </Carousel>
              ) : null}
            </div>
          </div>
        </section>
        {loading ? (
          <Loader />
        ) : (
          <div className="container">
            <section className="collect-wrapper">
              <h3 className="head">Collect your vouchers below!</h3>
              <div className="row">
                {adminVouchers.map((adminVoucher, index) => {
                  let voucherImageURL = "";
                  if (
                    adminVoucher.couponBannerImageId !== undefined &&
                    adminVoucher.couponBannerImageId.length > 0
                  ) {
                    voucherImageURL =
                      window.$ImageURL +
                      adminVoucher.couponBannerImageId[0].originalURL;
                  } else {
                    voucherImageURL = `${window.$ImageURL}no-image-480x480.png`;
                  }

                  var months = [
                    "January",
                    "February",
                    "March",
                    "April",
                    "May",
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "December",
                  ];
                  var startMonth = new Date(adminVoucher.startDate);
                  var couponStartMonthName = months[startMonth.getMonth()];

                  var endMonth = new Date(adminVoucher.endDate);
                  var couponEndMonthName = months[endMonth.getMonth()];
                  var couponEndMonthYear = endMonth.getFullYear();

                  let startDate = new Date(adminVoucher.startDate);
                  let startDateOnly = startDate.getDate();
                  let endDate = new Date(adminVoucher.endDate);
                  let endDateOnly = endDate.getDate();

                  var currentDate = new Date();
                  //let currnetDateOnly = currentDate.getDate();

                  return (
                    <div className="col-md-4">
                      <div className="collect-voucher">
                        <div className="voucher-top">
                          <div className="top-mart">
                            <div className="image-wr">
                              <img
                                src={voucherImageURL}
                                className="img-fluid"
                                alt={voucherImageURL}
                              />
                            </div>
                            <div className="content-sec">
                              <p className="mart-date">
                                MyMart :{" "}
                                <span>
                                  {" "}
                                  {startDateOnly} {couponStartMonthName} -
                                  {endDateOnly} {couponEndMonthName} {couponEndMonthYear}
                                </span>
                              </p>
                              {adminVoucher.couponType === "%" ? (
                                <h3 className="off-price">
                                  <span>{adminVoucher.couponType}</span>
                                  {adminVoucher.couponAmount}
                                  <span> OFF</span>
                                </h3>
                              ) : (
                                <h3 className="off-price">
                                  <span>{adminVoucher.couponType}</span>
                                  {adminVoucher.couponAmount}
                                  <span> OFF</span>
                                </h3>
                              )}
                              {endDate > currentDate ? (
                                couponArray.includes(adminVoucher._id) ? (
                                  <button disabled className="btn btncollect">
                                    Collected
                                  </button>
                                ) : (
                                  <button
                                    onClick={() =>
                                      this.collectvoucher(
                                        adminVoucher._id,
                                        null
                                      )
                                    }
                                    className="btn btncollect"
                                  >
                                    Collect
                                  </button>
                                )
                              ) : (
                                "expired"
                              )}
                            </div>
                          </div>
                        </div>
                        <div className="voucher-bottom">
                          <span className="curve-shape left" />
                          <span className="curve-shape right" />
                          <div className="min-spend">
                            <p>Min. Spend  QAR{" "}
                              {parseFloat(adminVoucher.maxOrderValue).toFixed(
                                2
                              )}</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  );
                })}
              </div>
            </section>
            {/* collect end  */}
            <Tabs>
              <section className="explore-main-tab">
                <div className="tabs-section">
                  <TabList>
                    <ul
                      className="tab-bg nav nav-pills "
                      id="pills-tab"
                      role="tablist"
                    >
                      {selectedCategory.categoryGroup.map((data, index) => {
                        return (
                          <Tab key={index}>
                            <li className="nav-item" role="presentation">
                              {data}
                            </li>
                          </Tab>
                        );
                      })}
                    </ul>
                  </TabList>
                  {/* forLoop */}
                  {selectedCategory.categoryGroup.map((data, index) => {
                    return (
                      <TabPanel className="tab-content" key={index}>
                        <VoucherTab dataArray={data} />
                      </TabPanel>
                    );
                  })}
                </div>
              </section>
            </Tabs>
          </div>
        )}
      </>
    );
  }
}
