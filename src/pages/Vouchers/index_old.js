import React, { Component } from "react";
import axios from "axios";
import Loader from "../../Component/Loader";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';

export default class Vouchers extends Component {
  state = {
    selectedCategory: [],
    firstBanner: [],
    loading: true,
  };

  async componentDidMount() {
    await axios
      .get(`${window.$URL}banner/fetchBanner/homepage`)
      .then((response) => {
        if (response.status === 200) {
          let bannerArray = response.data.data;
          let firstBanner = [];

          for (let i = 0; i <= bannerArray.length - 1; i++) {
            if (
              bannerArray[i].type === "1st Banner" &&
              bannerArray[i].bannerShowesTo === "Categories"
            ) {
              firstBanner.push({
                url:
                  `${window.$ImageURL}` +
                  bannerArray[i].bannerImageId[0].originalURL,
                bannerURL: bannerArray[i].bannerURL,
              });
            }
          }
          this.setState({
            firstBanner,
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });

    this.getAllData();
  }

  getAllData = async () => {
    let catType = this.props.match.params.categoryType;
    await axios
      .get(`${window.$URL}couponDiscount/fetch-all-Coupons`)
      .then((response) => {
        if (response.data.success) {
          console.log(response.data)


          this.setState({
            selectedCategory: response.data,
            loading: false,
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  createMarkup = (html) => {
    return {
      __html: html,
    };
  };

  removeHTML = (html) => {
    const regex = /(<([^>]+)>)/gi;
    const result = html.replace(regex, "");
    let returnName = "";
    if (result === "Groceries") {
      returnName = "Grocery";
    } else {
      returnName = result;
    }
    return returnName;
  };

  render() {
    const { firstBanner, loading, selectedCategory } = this.state;

    return (
      <>
        {/* Slider Banner */}
        <section className="banner">
          <div className="container">
            <div className="banner-slider">
              {firstBanner.length > 0 ? (
                <Carousel
                  autoPlay
                  infiniteLoop
                  showThumbs={false}
                  showStatus={false}
                >
                  {firstBanner.map((data, index) => {
                    return (
                      <div key={index}>
                        <a href={data.bannerURL}>
                          <img
                            src={data.url}
                            alt={data.url}
                            className="banner-image"
                          />
                        </a>
                      </div>
                    );
                  })}
                </Carousel>
              ) : null}
            </div>
          </div>
        </section>
        {loading ? (
          <Loader />
        ) : (
          <div class="container">
            <Tabs>
              <div className="new-category-section">
                <div className="row">
                  <div class="col-md-2 col-3 tabs_left pr-0">
                    <TabList>
                      <ul className="nav flex-column">
                        {selectedCategory.categoryGroup.map((data, index) => {
                          let imageURL = '';
                          if(selectedCategory.categoryImages[index] !== null){
                            imageURL = window.$ImageURL + selectedCategory.categoryImages[index]
                          }else{
                            imageURL = `${window.$ImageURL}no-image-480x480.png`
                          }
                          return (
                            <Tab key={index}>
                              <li className="nav-item">
                                <button className="nav-link active">
                                  <img
                                    src={imageURL}
                                    className="img-fliud"
                                    alt={data}
                                  />
                                  <span>{data}</span>
                                </button>
                              </li>
                            </Tab>
                          );
                        })}
                      </ul>
                    </TabList>
                  </div>

                  <div class=" col-md-10 col-9 pl-0">
                    {
                      Object.keys(selectedCategory.data).map((index) => {
                        return (
                          <TabPanel key={index}>
                            <div className="tab-details">
                              <div className="toggle-tab-header">
                                <p className="btn-tab">{Object.keys(selectedCategory.data[index])}</p>
                              </div>
                              <div className="toggle-tab-body">
                                <div className="row cs-row-margin">
                                  <div className="voucher-wr">
                                    <h3 className="header-4-store">Store Voucher  </h3>
                                    <div className="row">
                                      {
                                      Object.values(selectedCategory.data[index])[0].map((eachVoucher, eachVoucherIndex) => {
                                        let imageURL = '';
                                        if(eachVoucher.couponBannerImageId !== undefined && eachVoucher.couponBannerImageId.length > 0){
                                          imageURL = window.$ImageURL + eachVoucher.couponBannerImageId[0].originalURL
                                        }else{
                                          imageURL = `${window.$ImageURL}no-image-480x480.png`
                                        }
                                        return (
                                          <div className="col-md-3" key={eachVoucherIndex}>
                                            <a href={`${process.env.PUBLIC_URL}/productListing/category/${selectedCategory.categoryGroup[index]}`} className="items" style={{ background: "url(" + imageURL + ")" }}>
                                              <div className="content">
                                                <p>{eachVoucher.applyTo}</p>
                                                <h2>{eachVoucher.couponName}</h2>
                                                <a className="btn" href={`${process.env.PUBLIC_URL}/productListing/category/${selectedCategory.categoryGroup[index]}`}>Shop Now</a>
                                              </div>
                                            </a>
                                          </div>
                                        );
                                      })
                                    } 
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </TabPanel>
                        );
                      })
                    }
                  </div>
                </div>

              </div>
            </Tabs>
          </div>
        )}
      </>
    );
  }
}
