import React, { Component } from 'react';
import axios from 'axios';

export default class PrivacyPolicy extends Component {
    constructor(props) {
        super(props)
        this.state = {
            privacyPolicyDetails: ""
        }
    }

    async componentDidMount() {
        this.fetchTermsCondition();
    }

    fetchTermsCondition = async () => {

        await axios.get(`${window.$URL}content/privacyInfo`)
            .then(response => {
                console.log(response.data.data)
                if (response.status === 200) {
                    this.setState({
                        privacyPolicyDetails: response.data.data.ContentDetails
                    })
                }
            })
            .catch(error => {
                console.error(error.data)
            })
    }

    createMarkup = (html) => {
        return {
            __html: html
        };
    };
    render() {
        const { privacyPolicyDetails } = this.state;
        return (
            <section className="my-mart-signup-wr">
                <div className="container">
                    <div className="sinup-container">
                        <h1>Privacy Policy</h1>
                        <div className="form-sign-wrapper">
                        <div  dangerouslySetInnerHTML={this.createMarkup(privacyPolicyDetails)} />
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

