import React, { Component } from 'react';
import axios from "axios";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class ContactUs extends Component {
    state = {

        fullName: '',
        email: '',
        phoneNumber: '',
        description: '',
        fullNameError: '',
        emailError: '',
        phoneNumberError: '',
        descriptionError: '',
        successMessage: '',
        success: false,
        errorMessage: '',
        error: false,

    }

    onChange = (e) => {
        const { name, value } = e.target;
        this.setState({
            [name]: value
        });
    }

    handleChange(evt) {
        const phoneNumber = (evt.target.validity.valid) ? evt.target.value : this.state.phoneNumber;
        this.setState({ phoneNumber });
    }

    validate = () => {
        this.setState({
            fullNameError: '',
            emailError: '',
            phoneNumberError: '',
            descriptionError: '',
        });
        const regex = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
        let fullNameError = '';
        let emailError = '';
        let phoneNumberError = '';
        let descriptionError = '';


        if (this.state.fullName.length < 1) {
            fullNameError = "Please Enter your full name";
        }
        if (fullNameError) {
            this.setState({ fullNameError })
            return false
        }

        if (this.state.email.length < 1) {
            emailError = "Please enter Email";
        } else if (!regex.test(this.state.email.trim())) {
            emailError = "Please enter a valid Email"
        }
        if (emailError) {
            this.setState({ emailError })
            return false
        }

        if (this.state.phoneNumber.length < 1) {
            phoneNumberError = "Please enter Phone Number";
        }
        if (phoneNumberError) {
            this.setState({ phoneNumberError })
            return false
        }

        if (this.state.description.length < 1) {
            descriptionError = "Please Enter you description";
        }
        if (descriptionError) {
            this.setState({ descriptionError })
            return false
        }

        this.setState({
            fullNameError,
            emailError,
            phoneNumberError,
            descriptionError,

        })
        return true
    }

    onSubmit = async (e) => {
        e.preventDefault();
        this.setState({
            success: false,
            error: false,
            successMessage: '',
            errorMessage: ''
        })
        let checkValidation = this.validate();
        if (checkValidation) {
            const { fullName, email, phoneNumber, description } = this.state;
            let enquiryData = {
                "name": fullName,
                "email": email,
                "phone": phoneNumber,
                "description": description
            };
            console.log(enquiryData)

            await axios.post(`${window.$URL}contactus/saveEnquiry`, enquiryData)
                .then(response => {
                    if (response.status === 200) {
                        if (response.data.success) {
                            this.setState({
                                fullName:'',
                                email:'',
                                phoneNumber:'',
                                description:''
                            })
                            toast.success(response.data.message, {
                                position: "bottom-center",
                                autoClose: 3000,
                                hideProgressBar: true,
                                closeOnClick: true,
                                pauseOnHover: true,
                                draggable: true,
                                progress: undefined
                            })
                        } else {
                            toast.error(response.data.message, {
                                position: "bottom-center",
                                autoClose: 3000,
                                hideProgressBar: true,
                                closeOnClick: true,
                                pauseOnHover: true,
                                draggable: true,
                                progress: undefined
                            })
                        }
                    }
                })
                .catch(error => {
                    console.log(error);
                })
        }
    }

    render() {
        const {/*fullName,email,phoneNumber,description,*/ fullNameError, emailError, phoneNumberError, descriptionError } = this.state;
        return (

            <section className="my-mart-signup-wr">
                <ToastContainer />
                <div className="container">
                    <div className="sinup-container">
                        <h1>MyMart Contact US</h1>
                        <div className="form-sign-wrapper">
                            <form onSubmit={this.onSubmit}>
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="form-group">
                                            <label className="input-heading" htmlFor="exampleInputEmail1">Full Name</label>
                                            <input type="text" className="form-control" aria-describedby="emailHelp" name="fullName" onChange={this.onChange} placeholder="Full Name"  value={this.state.fullName}/>
                                            {fullNameError ? <span style={{ color: 'red' }}> {fullNameError} </span> : null}
                                        </div>
                                    </div>
                                    <div className="col-md-6 form-parts">
                                        <div className="form-group">
                                            <label className="input-heading">Email</label>
                                            <input type="text" name="email" className="form-control" placeholder="Enter your Email Id" onChange={this.onChange} value={this.state.email} />
                                            {emailError ? <span style={{ color: 'red' }}> {emailError} </span> : null}
                                        </div>
                                        <div className="form-group">
                                            <label className="input-heading" htmlFor="exampleInputEmail1">Phone Number</label>
                                            <input type="text" name="phoneNumber" className="form-control" aria-describedby="phonenumber" placeholder="Enter your phone number" pattern="[0-9]*" onChange={this.handleChange.bind(this)} value={this.state.phoneNumber} />
                                            {phoneNumberError ? <span style={{ color: 'red' }}> {phoneNumberError} </span> : null}
                                        </div>
                                    </div>
                                    <div className="col-md-12">
                                        <div className="form-group">
                                            <label className="input-heading" htmlFor="exampleInputEmail1">Description</label>
                                            <textarea className="form-control" aria-describedby="emailHelp" name="description" onChange={this.onChange} placeholder="Enter your description" value={this.state.description} />
                                            {descriptionError ? <span style={{ color: 'red' }}> {descriptionError} </span> : null}
                                        </div>
                                    </div>
                                    <div className="col-lg-6 col-md-6 col-sm-12">
                                        <div className="form-group">
                                            <button type="submit" className="btn signup_btn">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>

        )
    }
}

export default ContactUs;