import React, { Component } from 'react';
import axios from 'axios';  
import AmazonPay from 'amazon-pay-react';

export default class TermsAndCondition extends Component {
    constructor(props) {
        super(props)
        this.state = {
            termsConditionDetails: "" 
        }
    }

    async componentDidMount() {
        this.fetchTermsCondition();
    }
    payTo = () => {
        alert('hii');
        // this.setState(
        //     {
               
        //     },
        //     () => {
        //         //this.componentDidMount();
        //     }
        // );
      };

    fetchTermsCondition = async () => {

        await axios.get(`${window.$URL}content/payments-method`) 
            .then(response => {
                console.log(response.data.data)
                if (response.status === 200) {
                    this.setState({
                        termsConditionDetails: response.data.data.ContentDetails
                    })
                }
            })
            .catch(error => {
                console.error(error.data)
            })
    }
    createMarkup = (html) => {
        return {
            __html: html
        };
    };
    render() {
        const { termsConditionDetails } = this.state;
        return (
            <section className="my-mart-signup-wr">
                <div className="container">
                    <div className="sinup-container">
                        <h1>Payment</h1>
                        <div className="form-sign-wrapper">
                        <button onClick={() => this.payTo()}>Payments</button>
                        <AmazonPay
        clientId='your-cliendId'
        sellerId='your-sellerId'
        agreementType={'BillingAgreement'}
        scope='profile payments:widget'
        btnType='PwA'
        btnColor='Gold'
        btnSize='medium'
        // onConsentChange={(hasConsent) => ...handle}
        // handleBillingAgreementId={(billingAgreementId) => ...handle}
        billingAgreementId={this.state.billingAgreementId}
        useAmazonAddressBook={true}
/>
                        
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}
