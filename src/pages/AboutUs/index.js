import React, { Component } from 'react';
import axios from 'axios';

export default class AboutUs extends Component {

    constructor(props) {
        super(props)
        this.state = {
            aboutdetails: ""
        }
    }

    async componentDidMount() {
        // {
        //     title: 'This is my title only shown on this page',
        //     metaDescription: 'With some meta description'
        // }
        // let pathName = window.location.pathname.split("/");
        // let  title = `${pathName[3]} | Qatar Online Shopping, Grocery & Marketplace | myMart.qa `;
        // let metaDescription = '';
        // document.title = title;
        // document.querySelector('meta[name="description"]').setAttribute('content', metaDescription);

        this.fetchAboutInfo();
    }

    fetchAboutInfo = async () => {

        await axios.get(`${window.$URL}content/aboutInfo`)
            .then(response => {
                console.log(response.data.data,'aboutus')
                if (response.status === 200) {
                    this.setState({
                        aboutdetails: response.data.data.ContentDetails
                    })
                }
            })
            .catch(error => {
                console.error(error.data)
            })
    }
    createMarkup = (html) => {
        return {
            __html: html
        };
    };
    render() {
        const { aboutdetails } = this.state;
        return (
            <section className="my-mart-signup-wr">
                <div className="container">
                    <div className="sinup-container">
                        <h1>About Us</h1>
                        <div className="form-sign-wrapper">
                            <div  dangerouslySetInnerHTML={this.createMarkup(aboutdetails)} />
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}
