import React, { useEffect, useState } from "react";
import back_arrow from "../../assets/images/back-arrow.png";
import store_icon from "../../assets/images/store-icon.png";
import Plus_icon from "../../assets/images/plus-icon.png";
import send_right from "../../assets/images/send-right.png";
import MessageContact from "../../Component/MessageContact";
import firebase from '../../Firebase';
import jwt_decode from 'jwt-decode';
import axios from 'axios';

export default function MessageDetails(props) {
    const [messages, setMessages] = useState([])
    const [message, setMessage] = useState('')
    const [userId, setUserId] = useState('')
    const [chatDetails, setChatDetais] = useState('')
    const [userDetails,setUserDetails] = useState('')
    const ref = firebase.firestore().collection('ChatDetails')

    useEffect(() => {
        const getMessages = async (Id) => {
            ref.orderBy('CreatedAt', 'asc').onSnapshot(querySnapshot => {
                let messageData = []
                querySnapshot.docs.map(doc => {
                    messageData.push(doc.data())
                    return true;
                })
                GetDetails(messageData,Id)
            })
        }

        getMessages(props.match.params.sellerId)
        getchatUserDetails(props.match.params.sellerId)
        getMydetails()
    }, [props.match.params.sellerId,ref])


    const getMydetails = async () => {
        let checkToken = await localStorage.getItem("token");
        const decoded = jwt_decode(checkToken);
        const UserId = decoded.userid
        await axios.get(`${window.$URL}user/Details/${UserId}`)
        .then(response => {
            setUserDetails(response.data)
        })
        .catch(error => {
            console.log(error.data)
        })
    }

    const getchatUserDetails = async (id) => {
        await axios.get(`${window.$URL}user/Details/${id}`)
        .then(resposne => {
            setChatDetais(resposne.data)
        })
    }

    
    const GetDetails = async (messageData,Id) => {
        let checkToken = await localStorage.getItem("token");
        const decoded = jwt_decode(checkToken);
        const UserId = decoded.userid
        const Trimdata = messageData.filter(data => (data.ReceiverId === UserId && data.SenderId === Id)  || (data.ReceiverId === Id && data.SenderId === UserId) )

        setMessages(Trimdata)
        setUserId(UserId)
    }
    const onChange = (e) => {
        e.preventDefault();
        const { value } = e.target;
        setMessage(value)
    }

    const addMessage = async () => {
        let checkToken = await localStorage.getItem("token");
        const decoded = jwt_decode(checkToken);
        const SenderId = decoded.userid
        const ReceiverId = props.match.params.sellerId
        const text = message
        ref.add({
            CreatedAt: new Date().getTime(),
            message: text,
            ReceiverId: ReceiverId,
            SenderId: SenderId
        });
        setMessages('')
    }
    // const getChat = async (chattrId) => {
    //     getMessages(chattrId)
    // }
    return (
        <div className="container">
            <div className="row">
                <MessageContact />
                <div className="col-md-8 col-12 message-wrapper" >
                    <div className="product-m-c new-chatbox">
                        <div className="manage-header">
                            <h2 className="title-stock">Message Details</h2>
                        </div>
                        <div class="message-header justify-flex">
                            <div class="left-content">
                                <a href={`${process.env.PUBLIC_URL}/message`}>
                                    <img src={back_arrow} alt="back-arrow" class="back-arrow" />{" "}
                                </a>
                                <div class="texts">
                                    {chatDetails.Firstname} {chatDetails.Lastname}
                                        {/* <span class="last-view">Active in : 30 mins</span> */}
                                </div>
                            </div>
                            <img src={store_icon} class="store-image" alt={store_icon} />
                        </div>

                        <div class="details-msg-wrapper">
                            <div class="chat-bot">
                                {
                                    messages.length > 0
                                    ?
                                    <>
                                    {
                                        messages.map((msgData, msgIndex) => {
                                            return(
                                                <>
                                                    {
                                                    msgData.SenderId === userId
                                                        ?
                                                        <div class="msg_sender">
                                                            <div class="sender-content">
                                                               {msgData.message}
                                                            </div>
                                                            <div class="receiver-image">
                                                                {/* <img src={representative_image} /> */}
                                                                {
                                                                    userDetails.profileImageId !== null ?
                                                                    <img src={`${window.$ImageURL}` + userDetails.profileImageId.originalURL} className="img-fluid" alt='message' />
                                                                    :
                                                                    <img src={`${window.$ImageURL}no-image-480x480.png`} className="img-fluid" alt='message' />
                                                                }   
                                                            </div>
                                                        </div>
                                                        :
                                                        <div class="receiver">
                                                            <div class="receiver-image">
                                                                {/* <img src={store_logo} /> */}
                                                                {
                                                                    chatDetails.profileImageId !== null && chatDetails.profileImageId !== undefined ?
                                                                    <img src={`${window.$ImageURL}` + chatDetails.profileImageId.originalURL} className="img-fluid" alt='message' />
                                                                    :
                                                                    <img src={`${window.$ImageURL}no-image-480x480.png`} className="img-fluid" alt='message' />
                                                                }
                                                            </div>
                                                            <div class="receiver-content">
                                                                {msgData.message}
                                                            </div>
                                                        </div>
                                                    }
                                                </>
                                            )
                                        })
                                    }
                                    </>
                                    :
                                    <>
                                    </>
                                }
                                
                            </div>

                            <footer class="msg-footer">
                                <button class="add-button">
                                    {" "}
                                    <img src={Plus_icon} alt={Plus_icon} />
                                </button>
                                <input type="text" name="message" class="input-msg" onChange={onChange} placeholder="Type Message" />
                                <button class="send-message" onClick={addMessage}>
                                    {" "}
                                    <img src={send_right} alt={send_right} />
                                </button>
                            </footer>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    )
}