import React, {Component} from "react";
import axios from "axios";
import ProfileSidebar from "../../Component/ProfileSidebar";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import jwt_decode from 'jwt-decode';

export default class ChangePassword extends Component {

    
 
    constructor()
    {
        super();
        this.state = {
            // email: '',
           
            // emailError: '',
         
            successMessage: '',
            success: false,
            errorMessage: '',
            error: false,
            
            passwordError: '',
            confirmPasswordError: '',
            oldPassword:'',
            oldPasswordError:'',
            oldPasswordInvalid:'',


            
    
            
            password:'',
            confirmPassword:'',
            update:'',
        
    
        }
    }

    


   
    onChange = (e) => {
        const { name, value } = e.target;
        this.setState({
            [name]: value
        });
    }
    validate = () => {
        this.setState({
            passwordError: '',
            confirmPasswordError:'',
            oldPassword:'',
            oldPasswordError:''
           
        });

        let passwordError = '';
        let confirmPasswordError='';
        let oldPassword= '';
        let oldPasswordError = '';
      

       
        

        if (this.state.password.length < 1) {
            passwordError = "Please enter Password";
        }else if(this.state.password.length < 6){
            passwordError = "Password must be 6 characters long";

        }

        if (passwordError) {
            this.setState({ passwordError })
            return false
        }

        if(this.state.oldPassword.length < 1 )
        {
            oldPasswordError = 'Please Enter Old Password'
        }
        if(oldPasswordError)
        {
            this.setState({oldPasswordError});
            return false;
        }


        
        if (this.state.confirmPassword.length < 1) {
            confirmPasswordError = "Please enter  Confirm Password";
        }else if( this.state.confirmPassword !== this.state.password){
            confirmPasswordError = "Confirm Password must be same as password.";

        }
        if (confirmPasswordError) {
            this.setState({ confirmPasswordError }) 
            return false
        }

        this.setState({
            passwordError,
            confirmPasswordError,
            oldPasswordError
         
        })
        
        
        return true
    }

    onSubmit = async (e) => {
       
        e.preventDefault();
        this.setState({
            success: false,
            error: false,
            successMessage: '',
            errorMessage: '',
            oldPasswordError:'',
            oldPasswordInvalid:''
        })
        let checkToken = localStorage.getItem("token");
        if(checkToken)
        {
        let decoded = jwt_decode(checkToken);
        let userEmail = decoded.email;
        let checkValidation = this.validate();
        //console.log(decoded.email,'decoded');
        //alert('go');return false;
        //support@mymart.qa
        if(checkValidation){
            //alert(this.state.confirmPassword);return false;
        axios.put(`${window.$URL}user/customer-update-password`,{ 
            username:userEmail,
            password:this.state.password,
            oldPassword:this.state.oldPassword
        }) 
        .then(response => {
            
            if(response.data.message === 'password updated') {
                //console.log('RES', response.data.message); 
                this.setState({
                    
                    success: true,
                    successMessage: response.data.message
                });
                //this.props.history.push(`${process.env.PUBLIC_URL}/login/`);

            } else {
                this.setState({
                    //errorMessage:response.data.message,
                    error: true,
                    oldPasswordInvalid:response.data.message

                    
                });
            
            }
        })
        .catch(error =>{
            console.log(error.data);
        });
    }
    }
    };

   

    render() {
        const { passwordError,confirmPasswordError,oldPasswordInvalid,oldPasswordError, success, error, successMessage, errorMessage } = this.state;
        return (
            <section className="dbF-content">
        <div className="container">
          <div className="row">
            <ProfileSidebar />
            <div className="col-lg-9">
            <ToastContainer />
              <div className="my-order-wr">
                <h2 className="order-title">Change Password </h2> 
                {
                  successMessage !== '' ?
                  <span className="text-success">{successMessage}</span>
                  :
                  null
                }
                {
                  errorMessage !== '' ?
                  <span className="text-danger">{errorMessage}</span>
                  :
                  null
                }
                <form onSubmit={this.onSubmit}>
                <div className="profile-wrapper">
                <div className="form-group">
                    <label className="input-heading" htmlFor="exampleInputEmail1">Current Password</label>
                    <input type="password" placeholder="Enter your Current Password" className="form-control" name="oldPassword" onChange={this.onChange} />
                      {oldPasswordError ? <span style={{color:'red'}}> {oldPasswordError} </span> : null }
                      {oldPasswordInvalid ? <span style={{color:'red'}}> {oldPasswordInvalid} </span> : null }

                      
                      
                    </div>
                  
                    <div className="form-group">
                    <label className="input-heading" htmlFor="exampleInputEmail1">New Password </label>
                    <input type="password" placeholder="Enter your New Password" className="form-control" name="password" onChange={this.onChange} />
                      {passwordError ? <span style={{color:'red'}}> {passwordError} </span> : null }
                    </div>
                    <div className="form-group">
                    <label className="input-heading" htmlFor="exampleInputEmail1">Confirm New Password</label>
                    <input type="password" placeholder="Enter your Confirm Password" className="form-control" name="confirmPassword" onChange={this.onChange} />
                      {confirmPasswordError ? <span style={{color:'red'}}> {confirmPasswordError} </span> : null }
                    </div>
                    
                    
                    <button type="submit" className="btn btn-delivery" >Change Password</button>
                </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </section>
            
        )
    }
}