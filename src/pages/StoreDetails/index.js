import React, { Component } from "react";
import axios from "axios";
import ProductListingSidebar from "../../Component/ProductListSidebar";
//import Category from "../../Component/Category";
import Loader from "../../Component/Loader";
import ProductList from "../../Component/ProductListing";
//import storeBanner from "../../assets/images/storeBanner.jpg";
//import storevoucher from "../../assets/images/storevoucher..jpg";
import callIcon from "../../assets/images/call-icon.png";
import whatsapp_icon from "../../assets/images/whatsapp_icon.png";
import chatStore from "../../assets/images/chat-store.png";
//import cart_icon from "../../assets/images/cart_icon.svg";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
//import { Carousel } from "react-responsive-carousel";
//import MultiCarousel from "react-multi-carousel";
import moment from 'moment';
import jwt_decode from "jwt-decode";
/*const responsive = {
  superLargeDesktop: {
    // the naming can be any, depends on you.
    breakpoint: { max: 4000, min: 3000 },
    items: 6,
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 6,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 4.2,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 4.2,
  },
};*/

export default class StoreDetails extends Component {
  state = {
    Skip: 0,
    Limit: 9,
    postSize: "",
    ProductList: [],
    TotalCategory: [],
    TotalCountry: [],
    TotalBrand: [],
    TotalAttribute: [],
    filters: [],
    storeInfo: [],
    categoryArray: [],
    sellerProducts: [],
    loading: true,
    followedStore: [],
    followId: null,
    offersProducts: [],
    couponArray: [],
    selectedcouponArray: [],
    newArrivalsProducts: [],
    popularCategory: [],
    popularCategoryLoading: true,
    topThreePicks: [],
    mainProductArray: [],
  };

  async componentDidMount() {
    const variables = {
      skip: this.state.Skip,
      Limit: this.state.Limit,
      order: "",
      sortBy: "",
      filters: [],
    };
    this.getProdDetails(variables);
    this.getStoreDetails();

    let checkToken = await localStorage.getItem("token");
    if (checkToken) {
      const decoded = jwt_decode(checkToken);
      let customerId = decoded.userid;

      await axios
        .post(`${window.$URL}followedstore/fetchStore`, { customerId })
        .then((response) => {
          if (response.status === 200) {
            //console.log(response.data.data)

            if (response.data.success) {
              let followedStore = [];
              for (let index = 0; index < response.data.data.length; index++) {
                followedStore.push(response.data.data[index].sellerId);
              }
              console.log(followedStore);
              this.setState({
                followedStore,
              });
            } else {
              console.log("errror on fetch");
            }
          }
        })
        .catch((error) => {
          console.log(error);
        });
    }
    this.fetchCoupons();
    this.getPopularCategories();
    this.fetchCollectedCoupons();
  }
  getPopularCategories = async () => {
    let storeId = this.props.match.params.storeId;
    let addToMainCategory = [...this.state.mainProductArray];
    await axios
      .post(`${window.$URL}popular-category/fetch-popular-catetgory-by-store`, {
        storeId: storeId,
      })
      .then((response) => {
        if (response.data.success) {
          addToMainCategory.push({popularCategory: response.data.data});
          this.setState({
            popularCategory: response.data.data,
            popularCategoryLoading: false,
            mainProductArray: addToMainCategory
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
  getDateValid = (prodArray) => {
      let returnValidate = false;
      if(prodArray.startDate !== "" && prodArray.endDate !== ""){
          let convertStartDate = moment(prodArray.startDate).unix() * 1000;
          let convertEndDate = moment(prodArray.endDate).unix() * 1000;
          let systemTime = moment(new Date()).unix() * 1000;
          if(systemTime <= convertEndDate && systemTime >= convertStartDate){
          returnValidate = true;
          }
      }
      return returnValidate;
  }
  getStoreDetails = async () => {
    await axios
      .get(`${window.$URL}store/Details/${this.props.match.params.storeId}`)
      .then((response) => {
        console.log(response, "store");

        var offersProducts = [];
        for (var i = 0; i < response.data.sellerProducts.length; i++) {
          if (response.data.sellerProducts[i].specialPriceProvided === true && this.getDateValid(response.data.sellerProducts[i])) {
            offersProducts.push(response.data.sellerProducts[i]);
          }
        }

        if (response.status === 200) {
          let addToMainCategory = [...this.state.mainProductArray];
          addToMainCategory.push({topThreePicks: response.data.topThreePicks})
          addToMainCategory.push({sellerProducts: response.data.sellerProducts})
          this.setState({
            storeInfo: response.data.userDetails,
            categoryArray: response.data.categoryDetailsArray,
            TotalCategory: response.data.categoryArray,
            TotalCountry: response.data.countryArray,
            TotalBrand: response.data.brandArray,
            TotalAttribute: response.data.attributes,
            sellerProducts: response.data.sellerProducts,
            ProductList: response.data.sellerProducts,
            newArrivalsProducts: response.data.last30DaysSellerProducts,
            loading: false,
            offersProducts,
            topThreePicks: response.data.topThreePicks,
            mainProductArray: addToMainCategory,
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
  getProdDetails = async (variables) => {
    const { ProductList } = this.state;

    if (variables.filters.length > 0) {
      let filterProducts = ProductList.filter((eachProduct) => {
        let returnType = false;
        if (
          eachProduct.category.length > 0 &&
          eachProduct.country &&
          eachProduct.brand
        ) {
          if (variables.filters[0].category !== undefined) {
            if (variables.filters[0].category.length > 0) {
              for (
                let i = 0;
                i <= variables.filters[0].category.length - 1;
                i++
              ) {
                if (
                  eachProduct.category.includes(
                    variables.filters[0].category[i]
                  )
                ) {
                  returnType = true;
                }
              }
            } else {
              returnType = true;
            }
          } else if (variables.filters[0].country !== undefined) {
            if (variables.filters[0].country.length > 0) {
              for (
                let i = 0;
                i <= variables.filters[0].country.length - 1;
                i++
              ) {
                if (
                  eachProduct.country.match(variables.filters[0].country[i])
                ) {
                  returnType = true;
                }
              }
            } else {
              returnType = true;
            }
          } else if (variables.filters[0].brand !== undefined) {
            if (variables.filters[0].brand.length > 0) {
              for (let i = 0; i <= variables.filters[0].brand.length - 1; i++) {
                if (eachProduct.brand.match(variables.filters[0].brand[i])) {
                  returnType = true;
                }
              }
            } else {
              returnType = true;
            }
          }
        }
        return returnType;
      });
      this.setState({
        sellerProducts: filterProducts,
      });
    } else {
      this.setState({
        sellerProducts: ProductList,
      });
    }
  };
  showFilteredResults = (newFilters) => {
    const variables = {
      skip: this.state.Skip,
      Limit: this.state.Limit,
      order: "",
      sortBy: "",
      filters: [newFilters],
    };
    this.getProdDetails(variables);
  };
  getfilterData = (filtersdata, segment) => {
    const { filters } = this.state;
    const newFilters = { ...filters };
    newFilters[segment] = filtersdata;
    this.setState({
      filters: newFilters,
    });
    this.showFilteredResults(newFilters);
  };

  getfilterbrandData = (filtersdata, segment) => {
    const { filters } = this.state;
    const newFilters = { ...filters };
    newFilters[segment] = filtersdata;
    this.setState({
      filters: newFilters,
    });
    this.showFilteredResults(newFilters);
  };

  getfiltercountData = (filtersdata, segment) => {
    const { filters } = this.state;
    const newFilters = { ...filters };
    newFilters[segment] = filtersdata;
    this.setState({
      filters: newFilters,
    });
    this.showFilteredResults(newFilters);
  };
  getfilterPriceData = (filtersdata, segment) => {
    this.setState({ sidebarFilter: true });
    const { filters } = this.state;
    const newFilters = { ...filters };
    newFilters[segment] = filtersdata;
    this.setState({
      filters: newFilters,
    });
    this.showFilteredResults(newFilters);
  }

  getfilterRatingData = (filtersdata, segment) => {
    this.setState({ sidebarFilter: true });
    const { filters } = this.state;
    const newFilters = { ...filters };
    newFilters[segment] = filtersdata;
    this.setState({
      filters: newFilters,
    });
    this.showFilteredResults(newFilters);
  }
  getFilterAttributesData = (attriName, attriOptions) => {
    const {sellerProducts} = this.state;
    
    let filterProduct = [];
    if(attriName.length > 0){
      filterProduct = sellerProducts.filter((data) => {
        let returnType = false;
        for(let i = 0; i <= attriName.length-1; i++){
          if(attriOptions[i].length > 0){
            if(data.productType === "grocery"){

              data.sellerProductVariantId.forEach((item) => {
                item.productId.productAttribute.forEach((eachAttri) => {
                  if(eachAttri.attrName === attriName[i]){
                   let filterOptions = eachAttri.attrValue.filter((eachOption) => {
                      return attriOptions[i].includes(eachOption);
                    })
                    if(filterOptions.length > 0){
                      returnType = true;
                    }
                  }
                })
                
              })
            }else{
              if(data.attributeLabels.includes(attriName[i])){
                let attriIndex = data.attributeLabels.indexOf(attriName[i]);
                if(attriIndex !== -1){
                  let filterOptions = data.attributeValues[attriIndex].filter((eachOption) => {
                    return attriOptions[i].includes(eachOption);
                  })
                  if(filterOptions.length > 0){
                    returnType = true;
                  }
                }
              }
            }
          }
        }
        return returnType;
      })
    }else{
      filterProduct = sellerProducts;
    }
    /*this.setState({
      sellerProducts: filterProduct
    })*/
  };
  fetchCoupons = async () => {
    // let checkToken = await localStorage.getItem("token");
    // const decoded = jwt_decode(checkToken);
    let customerId = this.props.match.params.storeId;
    await axios
      .post(`${window.$URL}couponDiscount/seller-store-fetchCoupons/`, {
        customerId,
      })
      .then((response) => {
        console.log(response, "fetchCoupons");
        if (response.data.success) {
          this.setState({
            couponArray: response.data.data,
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  fetchCollectedCoupons = async () => {
    let checkToken = await localStorage.getItem("token");
    if (checkToken) {
      const decoded = jwt_decode(checkToken);
      let customerId = decoded.userid;
      await axios
        .post(`${window.$URL}couponDiscount/customer-fetchCoupons/`, {
          customerId,
        })
        .then((response) => {
          console.log(response.data.data, "array");
          let selectedcouponArray = [];
          for (let i = 0; i < response.data.data.length; i++) {
            if (response.data.data[i].voucherId !== null) {
              if (
                !selectedcouponArray.includes(
                  response.data.data[i].voucherId._id
                )
              ) {
                selectedcouponArray.push(response.data.data[i].voucherId._id);
              }
            }
          }
          if (response.data.success) {
            this.setState({
              selectedcouponArray,
            });
          }
        })
        .catch((error) => {
          console.log(error);
        });
    }
  };

  async storeFollow(storeId, storeStatus) {
    //alert(storeId);
    const { storeInfo } = this.state;
    //console.log(storeInfo.SellerDetails._id);
    let checkToken = await localStorage.getItem("token");
    if (checkToken) {
      const decoded = jwt_decode(checkToken);
      let customerId = decoded.userid;

      if (storeStatus) {
        //alert(storeStatus);return false;

        let storeData = {
          sellerId: storeId,
          storeId: storeInfo.SellerDetails._id,
          customerId: customerId,
        };
        await axios
          .post(`${window.$URL}followedstore/addFollowedStore`, storeData)
          .then((response) => {
            if (response.status === 200) {
              if (response.data.success) {
                this.setState({
                  followId: response.data.data._id,
                });
                toast.success(response.data.message, {
                  position: "bottom-center",
                  autoClose: 3000,
                  hideProgressBar: true,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true,
                  progress: undefined,
                });

                this.componentDidMount();
              } else {
                toast.error(response.data.message, {
                  position: "bottom-center",
                  autoClose: 3000,
                  hideProgressBar: true,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true,
                  progress: undefined,
                });
              }
            }
          })
          .catch((error) => {
            console.log(error);
          });
      } else {
        //alert(storeStatus);return false;

        let DeletedId = {
          followedId: storeId,
        };
        await axios
          .post(`${window.$URL}followedstore/deleteFollowedStore`, DeletedId)
          .then((response) => {
            if (response.status === 200) {
              if (response.data.success) {
                toast.success(response.data.message, {
                  position: "bottom-center",
                  autoClose: 3000,
                  hideProgressBar: true,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true,
                  progress: undefined,
                });

                this.componentDidMount();
              } else {
                toast.error(response.data.message, {
                  position: "bottom-center",
                  autoClose: 3000,
                  hideProgressBar: true,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true,
                  progress: undefined,
                });
              }
            }
          })
          .catch((error) => {
            console.log(error);
          });
      }
    } else {
      window.location.href = `${process.env.PUBLIC_URL}/login`;
    }
  }

  async collectvoucher(voucherId, sellerId) {
    // alert(voucherId);
    // alert(sellerId);return false;

    let checkToken = await localStorage.getItem("token");
    if (checkToken) {
      const decoded = jwt_decode(checkToken);
      let customerId = decoded.userid;
      if (sellerId != null) {
        let voucherClaimData = {
          voucherId: voucherId,
          sellerId: sellerId,
          customerId: customerId,
        };
        await axios
          .post(
            `${window.$URL}couponDiscount/customer-voucher-claim`,
            voucherClaimData
          )
          .then((response) => {
            if (response.status === 200) {
              if (response.data.success) {
                toast.success(response.data.message, {
                  position: "bottom-center",
                  autoClose: 3000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true,
                  progress: undefined,
                });
                this.componentDidMount();
              } else {
                toast.error(response.data.message, {
                  position: "bottom-center",
                  autoClose: 3000,
                  hideProgressBar: true,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true,
                  progress: undefined,
                });
              }
            }
          })
          .catch((error) => {
            console.log(error);
          });
      }
    } else {
      window.location.href = `${process.env.PUBLIC_URL}/login`;
    }
  }

  removeHTML = (html) => {
    const regex = /(<([^>]+)>)/gi;
    const result = html.replace(regex, "");
    let returnName = "";
    if (result === "Groceries") {
      returnName = "Grocery";
    } else {
      returnName = result;
    }
    return returnName;
  };
  createMarkup = (html) => {
    return {
      __html: html,
    };
  };


  searchProduct = (e) => {
    let searchedKeyword = e.target.value;
    const {mainProductArray} = this.state;
    let filterSellerProd = [];
    let filterTop3Prod = [];
    let filterCategory = [];
    
    if(mainProductArray[2].sellerProducts.length > 0){
      filterSellerProd = mainProductArray[2].sellerProducts.filter(eachProd => {
        return eachProd.productName.toLowerCase().match(searchedKeyword.toLowerCase());
      })
    }else{
      filterSellerProd = mainProductArray[2].sellerProducts
    }

    if(mainProductArray[1].topThreePicks.length > 0){
      filterTop3Prod = mainProductArray[1].topThreePicks.filter(eachProd => {
        return eachProd.productName.toLowerCase().match(searchedKeyword.toLowerCase());
      })
    }else{
      filterSellerProd = mainProductArray[1].topThreePicks
    }

    if(mainProductArray[0].popularCategory.length > 0){
      filterCategory = mainProductArray[0].popularCategory.filter(eachProd => {
        return eachProd.categoryName.toLowerCase().match(searchedKeyword.toLowerCase());
      })
    }else{
      filterSellerProd = mainProductArray[0].popularCategory
    }
    this.setState({
      sellerProducts: filterSellerProd,
      topThreePicks: filterTop3Prod,
      popularCategory: filterCategory
    })
  }
  render() {
    const {
      storeInfo,
      popularCategory,
      topThreePicks,
      //categoryArray,
      popularCategoryLoading,
      sellerProducts,
      loading,
      followedStore,
      offersProducts,
      couponArray,
      selectedcouponArray,
      newArrivalsProducts,
    } = this.state;

    let storeBannerImgt = "";
    console.log(storeInfo,'storeInfo')

    if (storeInfo?.length > 0) {
      storeBannerImgt =
        window.$ImageURL + storeInfo.SellerDetails.storeBannerImage.originalURL;
    } else {
      storeBannerImgt = `${window.$ImageURL}no-image-480x480.png`;
    }

    //console.log('hii' +followedStore,60656cfe552a3427b01518a3);
    if (loading) {
      return <Loader />;
    } else {
      return (
        <div className="container">
          <section
            className="store-banner"
            style={{
              background: `url(` + storeBannerImgt + `) no-repeat center`,
            }}
          >
            <ToastContainer />
            <div className="market-details">
              <ul className="nav">
                <li className="nav-item brand-img">
                  <img
                    src={
                      storeInfo?.SellerDetails.storeLogo !== null &&
                        storeInfo?.SellerDetails.storeLogo !== undefined
                        ? window.$ImageURL +
                        storeInfo?.SellerDetails.storeLogo.originalURL
                        : `${window.$ImageURL}no-image-480x480.png` 
                    }
                    className="img-fluid"
                    alt="store"
                  />
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#/">
                    <img src={callIcon} alt={callIcon} /> call now
                  </a>
                </li>
                <li className="nav-item whatsapp">
                  <a className="nav-link" href="#/">
                    <img src={whatsapp_icon} alt={whatsapp_icon} /> whatsapp
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#/">
                    <img src={chatStore} alt={chatStore} /> Chat
                  </a>
                </li>
                <li className="nav-item follow ml-auto">
                  {followedStore.includes(this.props.match.params.storeId) ? (
                    <button
                      className="nav-link"
                      onClick={() =>
                        this.storeFollow(this.state.followId, false)
                      }
                    >
                      {" "}
                      UnFollow
                    </button>
                  ) : (
                    <button
                      className="nav-link"
                      onClick={() =>
                        this.storeFollow(this.props.match.params.storeId, true)
                      }
                    >
                      {" "}
                      Follow
                    </button>
                  )}
                </li>
              </ul>
            </div>
          </section>
          <div className="productlisting_main">
            <div className="row">
              <ProductListingSidebar
                catData={this.state.TotalCategory}
                contryData={this.state.TotalCountry}
                brandData={this.state.TotalBrand}
                otherAttributes={this.state.TotalAttribute}
                showFilteredResult={this.getfilterData}
                showFilteredbrandResult={this.getfilterbrandData}
                showFilteredcountResult={this.getfiltercountData}
                showFilteredPriceResult={this.getfilterPriceData}
                showFilteredRatingResult={this.getfilterRatingData}
                showFilteredAttributesResult={this.getFilterAttributesData}
              />
              <div className="col-md-9 searchView">
                <form className="srh-4-store">
                  <input
                    className="form-control"
                    type="search"
                    placeholder="search store products"
                    onChange={this.searchProduct}
                  />
                  <button className="btn" type="submit">
                    <i className="fa fa-search" aria-hidden="true" />
                  </button>
                </form>
                <div className="store-voucher-wr">
                  <Tabs>
                    <TabList className="nav nav-pills mb-3">
                      <Tab className="nav-item nav-link" role="presentation">
                        Home
                      </Tab>
                      <Tab className="nav-item nav-link" role="presentation">
                        Offers
                      </Tab>
                      <Tab className="nav-item nav-link" role="presentation">
                        New Arrivals
                      </Tab>
                    </TabList>

                    <TabPanel className="tab-content">
                      {couponArray.length > 0 ?
                      <div
                        className="tab-pane fade show active"
                        id="pills-home"
                        role="tabpanel"
                        aria-labelledby="pills-home-tab"
                      >
                        <div className="voucher-wr">
                          <h3 className="header-4-store"> Store Voucher </h3>
                          <div className="row">
                            {couponArray.map((data, index) => {
                              let imageURL = "";
                              if (data.couponBannerImageId[0] !== null) {
                                imageURL =
                                  window.$ImageURL +
                                  data.couponBannerImageId[0].originalURL;
                              } else {
                                imageURL = `${window.$ImageURL}no-image-480x480.png`;
                              }
                              return (
                                <div
                                  key={index}
                                  className="col-lg-3 col-md-4 col-sm-6"
                                >
                                  <a
                                    href="#/"
                                    className="items"
                                    style={{
                                      background: `url(${imageURL}) no-repeat center`,
                                    }}
                                  >
                                    <div className="content">
                                      <p>{data.couponName}</p>
                                      <h2>
                                        Up to {data.couponAmount}{" "}
                                        {data.couponType} OFF
                                      </h2>
                                      {/* <h6>On Groceries Online</h6> */}
                                      {selectedcouponArray.includes(
                                        data._id
                                      ) ? (
                                        <button
                                          disabled
                                          className="btn btncollect"
                                        >
                                          Collected
                                        </button>
                                      ) : (
                                        <button
                                          onClick={() =>
                                            this.collectvoucher(
                                              data._id,
                                              this.props.match.params.storeId
                                            )
                                          }
                                          className="btn btncollect"
                                        >
                                          Collect
                                        </button>
                                      )}
                                      {/* <button className="btn">Collect</button> */}
                                    </div>
                                  </a>
                                </div>
                              );
                            })}
                          </div>
                        </div>
                      </div>
                      :
                      null
                      }
                      <ProductList
                        productArray={sellerProducts}
                        loading={loading}
                      />
                    </TabPanel>

                    <TabPanel className="tab-content">
                      <ProductList
                        productArray={offersProducts}
                        loading={loading}
                      />
                    </TabPanel>
                    <TabPanel className="tab-content">
                      <ProductList
                        productArray={newArrivalsProducts}
                        loading={loading}
                      />
                    </TabPanel>
                  </Tabs>
                </div>
                <h3 className="header-4-store"> Top 3 Picks </h3>
                <ProductList productArray={topThreePicks} loading={loading} />

                {/* popular category */}
                {/* <section className="category-section">
                                    <h3 className="header-4-store">Popular Category </h3>
                                    <Category categoryDetails={categoryArray} filterCategoryWise={(variables,segment) => this.getfilterData(variables,segment)} />
                                </section> */}
                <section className="category-section">

                  <div className="title_section">
                    <h3 className="header-4-store">Popular Categories </h3>
                  </div>
                  <div className="categories-wrapper">
                      {/* Set up your HTML */}
                      {popularCategoryLoading ? (
                        <Loader />
                      ) : (
                        popularCategory.map((data, index) => {
                          return (
                            
                          <div className="product-category">
                            <a
                              href={`${process.env.PUBLIC_URL
                                }/productListing/category/${this.removeHTML(
                                  data.categoryName
                                )}`}
                              className="card"
                              key={index}
                            >
                              <div className="icon">
                                {data.categoryImageId ? (
                                  <img
                                    src={
                                      data.categoryImageId.originalURL !==
                                        null &&
                                        data.categoryImageId.originalURL !==
                                        undefined
                                        ? window.$ImageURL +
                                        data.categoryImageId.originalURL
                                        : `${window.$ImageURL}no-image-480x480.png`
                                    }
                                    alt={data.categoryName}
                                    className="store-details-popular-category"
                                  />
                                ) : (
                                  <img
                                    src={`${window.$ImageURL}no-image-480x480.png`}
                                    alt={data.categoryName}
                                    className="store-details-popular-category"
                                  />
                                )}
                              </div>
                              <p
                                className="name"
                                dangerouslySetInnerHTML={this.createMarkup(
                                  data.categoryName
                                )}
                              />
                            </a>
                          </div>
                          );
                        })
                      )}
                  </div>

                </section>
                {/* all product */}
                {/* pagination */}
                {/* <div class="pagination-product">
                                    <nav aria-label="Page navigation example">
                                        <ul class="pagination">
                                            <li class="page-item">
                                                <a class="page-link" href="#/" aria-label="Previous">
                                                    <span aria-hidden="true"> <i class="fa fa-angle-left"
                                                            aria-hidden="true"></i></span>
                                                </a>
                                            </li>
                                            <li class="page-item active"><a class="page-link" href="#/">1</a></li>
                                            <li class="page-item"><a class="page-link" href="#/">2</a></li>
                                            <li class="page-item"><a class="page-link" href="#/">3</a></li>
                                            <li class="page-item"><a class="page-link" href="#/">4</a></li>
                                            <li class="page-item">...</li>
                                            <li class="page-item">
                                                <a class="page-link" href="#/" aria-label="Next">
                                                    <span aria-hidden="true"><i class="fa fa-angle-right"
                                                            aria-hidden="true"></i>
                                                    </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div> */}
              </div>
            </div>
          </div>
        </div>
      );
    }
  }
}
