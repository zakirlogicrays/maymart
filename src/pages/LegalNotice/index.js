import React, { Component } from 'react';
import axios from 'axios';

class LegalNotice extends Component {
    constructor(props) {
        super(props)
        this.state = {
            legalNoticedetails: ""
        }
    }

    async componentDidMount() {
        this.fetchLegalNoticeInfo();
    }

    fetchLegalNoticeInfo = async () => {

        await axios.get(`${window.$URL}content/legalNoticeInfo`)
            .then(response => {
                console.log(response.data.data)
                if (response.status === 200) {
                    this.setState({
                        legalNoticedetails: response.data.data.ContentDetails
                    })
                }
            })
            .catch(error => {
                console.error(error.data)
            })
    }
    createMarkup = (html) => {
        return {
            __html: html
        };
    };
    render() {
        const { legalNoticedetails } = this.state;
        return (
            <section className="my-mart-signup-wr">
                <div className="container">
                    <div className="sinup-container">
                        <h1>Legal Notice</h1>
                        <div className="form-sign-wrapper">
                        <div  dangerouslySetInnerHTML={this.createMarkup(legalNoticedetails)} />
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default LegalNotice;