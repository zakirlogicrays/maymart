import React, {Component} from "react";
import axios from "axios";


export default class ResetPassword extends Component {

    

    constructor()
    {
        super();
        this.state = {
            // email: '',
           
            // emailError: '',
         
            successMessage: '',
            success: false,
            errorMessage: '',
            error: false,
            token:'',
            passwordError: '',
            confirmPasswordError: '',

            
    
            username:'',
            password:'',
            confirmPassword:'',
            update:'',
        
    
        }
    }

    async componentDidMount()
    {
        //console.log(this.props.match.params.token);

        await axios.get(`${window.$URL}user/customer-reset-password`,
        {
            params: 
            {
                resetPasswordToken:  atob(this.props.match.params.token) 

            },
        })
        .then(response => {
            console.log(response.data);
            if(response.data.message === 'password reset link a-ok'){
                this.setState({
                    username:response.data.username,
                    errorMessage: response.data,
                    error:false,
                });
            }else if(response.data === 'Password reset link is invalid')
            {
                this.setState({
                   
                    errorMessage: response.data,
                    error:true,
                });
            } else {
                this.setState({
                    
                    error:true,
                });
            }
        })
        .catch(error => {
            console.log(error.data); 
        });
    }


   
    onChange = (e) => {
        const { name, value } = e.target;
        this.setState({
            [name]: value
        });
    }
    validate = () => {
        this.setState({
            passwordError: '',
            confirmPasswordError:''
           
        });

        let passwordError = '';
        let confirmPasswordError='';
      

       
        

        if (this.state.password.length < 1) {
            passwordError = "Please enter Password";
        }else if(this.state.password.length < 6){
            passwordError = "Password must be 6 characters long";

        }

        if (passwordError) {
            this.setState({ passwordError })
            return false
        }

        
        if (this.state.confirmPassword.length < 1) {
            confirmPasswordError = "Please enter  Confirm Password";
        }else if( this.state.confirmPassword !== this.state.password){
            confirmPasswordError = "Confirm Password must be same as password.";

        }
        if (confirmPasswordError) {
            this.setState({ confirmPasswordError })
            return false
        }

        this.setState({
            passwordError,
            confirmPasswordError
         
        })
        
        
        return true
    }

    onSubmit = async (e) => {
       
        e.preventDefault();
        this.setState({
            success: false,
            error: false,
            successMessage: '',
            errorMessage: ''
        })
        let checkValidation = this.validate();
        console.log(checkValidation);
        if(checkValidation){
            //alert(this.state.confirmPassword);return false;
        axios.put(`${window.$URL}user/customer-update-password`,{
            username:this.state.username,
            password:this.state.password,
        })
        .then(response => {
            
            if(response.data.message === 'password updated') {
                console.log('hbea na kno' +response.data.message);
                this.setState({
                    
                    success: true,
                    successMessage: response.data.message
                });
                //this.props.history.push(`${process.env.PUBLIC_URL}/login/`);

            } else {
                this.setState({
                    error:true,
                    
                });
            
            }
        })
        .catch(error =>{
            console.log(error.data);
        });
    }
    };

   

    render() {
        const { passwordError,confirmPasswordError, success, error, successMessage, errorMessage } = this.state;
        return (
            <section className="my-mart-signup-wr">
                <div className="container">
                    <div className="sinup-container">
                        <h1>Reset Password</h1>
                        <div className="form-sign-wrapper">
                            <form onSubmit={this.onSubmit}>
                            <div className="row">
                                <div className="col-lg-12 col-md-12 col-sm-12 form-parts p-4">
                                    {
                                        success ?
                                        <p style={{color:'green', fontSize:18}}> {successMessage} </p> 
                                        :
                                        null
                                    }
                                    {
                                        error ?
                                        <p style={{color:'red', fontSize:18}}> {errorMessage} </p>
                                        :
                                        null
                                    }
                                </div>
                                <div className="col-lg-3 col-md-6 col-sm-12 form-parts">
                                </div>
                                <div className="col-lg-6 col-md-6 col-sm-12 form-parts">
                                    <div className="form-group">
                                        <label className="input-heading">Password</label>
                                        <input type="password" placeholder="Enter your Password" className="form-control" name="password" onChange={this.onChange} />
                                        {passwordError ? <span style={{color:'red'}}> {passwordError} </span> : null }

                                    </div>
                                    <div className="form-group">
                                        <label className="input-heading">Confirm Password</label>
                                        <input type="password" placeholder="Enter your Confirm Password" className="form-control" name="confirmPassword" onChange={this.onChange} />
                                        {confirmPasswordError ? <span style={{color:'red'}}> {confirmPasswordError} </span> : null }
                                    </div>
                                    
                                    <div className="form-group">
                                        <button type="submit" className="btn signup_btn" >Submit</button> 
                                    </div>
                                    <div className="form-group">
                                        <p className="already_signUp">Don't have an account? <a href={`${process.env.PUBLIC_URL}/register`}>Register</a></p>
                                    </div>
                                </div>
                                <div className="col-lg-3 col-md-6 col-sm-12 form-parts">
                                </div>
                                
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}