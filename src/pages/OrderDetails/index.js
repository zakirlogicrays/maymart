import React, { Component } from "react";
import axios from 'axios';
import jwt_decode from "jwt-decode";
import Loader from "../../Component/Loader";
import ProfileSidebar from "../../Component/ProfileSidebar";
import phoneIcon from "../../assets/images/phone-icon.png";
import whatsappIcon from "../../assets/images/whatsAppfull.png";
import chatIcon from "../../assets/images/chat-icon.png";
import creditCardImage from "../../assets/images/credit-card.png";


var moment = require('moment-timezone');

export default class OrderDetails extends Component {
  state = { isActive: false };
  handleToggle = () => {
    this.setState({ isActive: !this.state.isActive });
  };
  state = {
    OrderDetails: [],
    loading: true,
    deliveryTimeSlotFrom: '',
    deliveryDate: '',
    deliveryStartTime: '',
    deliveryEndTime: '',
    orderDate: '',
    orderTime: '',
  }
  componentDidMount() {
    console.log(this.props.match.params.orderId, 'hmm');
    //6040dcb8c5650c2e20e82db1
    this.fetchOrderDetailsData();
  }

  fetchOrderDetailsData = async () => {
    let checkToken = await localStorage.getItem("token");
    const TIME_ZONE = 'Singapore';
    const DATE_FORMAT_AVLBLTY = 'DD MMM';
    const TIME_FORMAT_AVLBLTY = 'hh:mm A';

    if (checkToken) {
      const decoded = jwt_decode(checkToken);
      if (decoded.userType === 'customer') {
        await axios.get(`http://localhost:7779/api/order/fetchorderdetail/${this.props.match.params.orderId}`)
          .then(res => {
            if (res.status === 200) {
              var deliveryDate = moment(res.data.data.deliveryTimeSlotFrom * 1000).tz(TIME_ZONE).format(DATE_FORMAT_AVLBLTY);
              var deliveryStartTime = moment(res.data.data.deliveryTimeSlotFrom * 1000).tz(TIME_ZONE).format(TIME_FORMAT_AVLBLTY);
              var deliveryEndTime = moment(res.data.data.deliveryTimeSlotTo * 1000).tz(TIME_ZONE).format(TIME_FORMAT_AVLBLTY);
              var orderDate = moment(res.data.data.orderDate * 1000).tz(TIME_ZONE).format(DATE_FORMAT_AVLBLTY);
              var orderTime = moment(res.data.data.orderDate * 1000).tz(TIME_ZONE).format(TIME_FORMAT_AVLBLTY);

              this.setState({
                OrderDetails: res.data.data,
                loading: false,
                deliveryDate,
                deliveryStartTime,
                deliveryEndTime,
                orderDate,
                orderTime,
              })

            }
          })
          .catch(error => {
            console.log(error)
          })
      }
    } else {
      this.setState({
        OrderDetails: [],
        loading: false
      })
    }
  }

  render() {
    const { OrderDetails, deliveryDate, deliveryStartTime, deliveryEndTime, isActive, orderDate, orderTime, loading } = this.state;

    if (loading) {
      return (
        <Loader />
      )
    } else {
      return (
        <section className="dbF-content">
          <div className="container">
            <div className="row">
              <ProfileSidebar />
              <div className="col-lg-9">
                <div className="my-order-wr">
                  <h2 className="order-title">Order</h2>
                  <div className="seller-order-wrap">
                    <div className="order-wrap-inner">
                      <div className="tab-content" id="pills-tabContent">
                        <div className="tab-pane fade show active" id="pills-New" role="tabpanel" aria-labelledby="pills-New-tab">
                          <div className="order-preap-section">
                            <ul className="nav nav-pills nav-fill">
                              <li className="nav-item">
                                <a className="nav-link active" href="javscript:void(0)">New</a>
                              </li>
                              <li className="nav-item">
                                <a className="nav-link active" href="javscript:void(0)">Processing</a>
                              </li>
                              <li className="nav-item">
                                <a className="nav-link" href="javscript:void(0)">Despatched</a>
                              </li>
                              <li className="nav-item">
                                <a className="nav-link  " href="javscript:void(0)">Completed</a>
                              </li>
                            </ul>
                            <div className="card-delivery-slot">
                              <div className="row">
                                <div className="col-md-8">
                                  <div className="row">
                                    <div className="col-md-10">
                                      <h2 className="credit-card-details">Credit Card Order #{OrderDetails._id}</h2>
                                      <p className="order-place-time">Order {orderDate} @{orderTime}</p>
                                    </div>
                                    <div className="col-md-2 text-center">
                                      <img src={creditCardImage} className="img-fluid" alt={creditCardImage} />
                                    </div>
                                  </div>
                                  <div className="row process-sec">
                                    <div className="col-md-6 ">
                                      <p className="money-to-pay"> Paid QAR {OrderDetails.subTotal}</p>
                                    </div>
                                    {/* <div className="col-md-6 btn-process-wrap">
                                      <button className="btn process-order">PROCESS
                                        ORDER</button>
                                    </div> */}
                                  </div>
                                </div>
                                <div className="col-md-4 d-flex">
                                  <div className="book-slot">
                                    <p className="dv-slot">Delivery Slot</p>
                                    <p className="dv-date">{deliveryDate}</p>
                                    <p className="dv-time">{deliveryStartTime} to {deliveryEndTime}</p>
                                    <p className="dv-muti-order">01:15 Singe Order </p>
                                  </div>
                                </div>
                                <div className="social-wrapd">
                                  <a href="#/"><img src={phoneIcon} alt={phoneIcon} />Contact No.</a>
                                  <a href="#/"><img src={whatsappIcon} alt={whatsappIcon} />Whatsapp</a>
                                  <a href="#/"><img src={chatIcon} alt={chatIcon} />Contact No.</a>
                                </div>
                              </div>
                            </div>
                            <div className="preview-wrapper" onClick={this.handleToggle} data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                              preview order
                            </div>
                          </div>
                          <div id="collapseOne" className={isActive ? "collapse show " : "collapse"} aria-labelledby="headingOne" data-parent="#accordionExample">
                            <div className="product-review">
                              {/* view items start */}
                              {
                                OrderDetails.productVariantId ?
                                  OrderDetails.productVariantId.map((data, index) => {
                                    var imageData;
                                    if (data.sellerProductId.productImageId.length < 1 || data === undefined) {
                                      imageData = `${window.$ImageURL}no-image-480x480.png`
                                    } else if (data.sellerProductId.primaryProductImage !== null && data.sellerProductId.primaryProductImage !== undefined) {
                                      imageData = `${window.$ImageURL}` + data.sellerProductId.primaryProductImage.originalURL
                                    } else {
                                      imageData = `${window.$ImageURL}` + data.sellerProductId.productImageId[0].originalURL
                                    }
                                    return (

                                      <div key={index} className="stock-card ">
                                        <div className="card-f-dealer">
                                          <div className="left-conte-card">
                                            <div className="img-sect">
                                              <img src={imageData} alt={imageData} />
                                            </div>
                                            <div className="content-sect">
                                              <div className="name-div">
                                                <h1 className="st-name">{data.sellerProductId.productName}
                                                </h1>
                                                <h4 className="price">QAR {data.Price} </h4>
                                              </div>
                                              <div className="fruit-stock">
                                                <div className="sep_ator">
                                                  <h4 className="s-dc">{data.sellerProductId.brand}</h4>
                                                  <h4 className="s-dc">{data.packSize}{data.packSizeMeasureUnit}</h4>
                                                </div>
                                                {/* <h4 className="stock-inhand">stock: <span>20</span>
                            </h4> */}
                                              </div>
                                              <div className="desc-barcode">
                                                {/* <p className="sdatet-desc">Stock Date / Time last
                              update: <span> 28-Nov-2020
                                / 0932 hrs</span></p> */}
                                                <p className="sdatet-desc">Barcode <span>
                                                  {data.barcodeNumber}</span></p>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    )
                                  })
                                  : null
                              }

                              {/* view items end */}
                              {/*grand total start */}
                              <div className="toal-order">
                                <div className="order-items" style={{ borderBottom: '1px solid #ddd' }}>
                                  <p className="b-items">Order Total</p>
                                  <p className="nrmal-items"> QAR {OrderDetails.Total}</p>
                                </div>
                                <div className="order-items">
                                  <p className="b-items">Delivery Fee</p>
                                  <p className="nrmal-items">QAR {OrderDetails.shippingFee}</p>
                                </div>
                                <div className="order-items">
                                  <p className="b-items">Sub Total </p>
                                  <p className="nrmal-items"> QAR {OrderDetails.subTotal}</p>
                                </div>
                                {/* <div className="order-items">
                      <p className="b-items">myMart Margin ( variable )</p>
                      <p className="nrmal-items"> QAR 12.00</p>
                    </div> */}
                                {/* <div className="order-items">
                      <p className="b-items">Final Amount Due to You</p>
                      <p className="nrmal-items"> QAR 100.00</p>
                    </div> */}
                              </div>
                              {/*grand total end */}
                            </div>
                          </div>
                        </div>
                        <div className="tab-pane fade" id="pills-Processing" role="tabpanel" aria-labelledby="pills-Processing-tab">Processing</div>
                        <div className="tab-pane fade" id="pills-Cancelled" role="tabpanel" aria-labelledby="pills-Cancelled-tab">Cancelled</div>
                        <div className="tab-pane fade" id="pills-Refund" role="tabpanel" aria-labelledby="pills-Refund-tab">Refund</div>
                        <div className="tab-pane fade" id="pills-Completed" role="tabpanel" aria-labelledby="pills-Completed-tab">Completed</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </section>
      )
    }
  }
}