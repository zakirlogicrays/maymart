import React, { Component } from "react";
import axios from 'axios';
import jwt_decode from "jwt-decode";
import Loader from "../../Component/Loader";
import ProfileSidebar from "../../Component/ProfileSidebar";

import phoneIcon from "../../assets/images/phone-icon.png";
import whatsappIcon from "../../assets/images/whatsAppfull.png";
import chatIcon from "../../assets/images/chat-icon.png";
import creditCardImage from "../../assets/images/credit-card.png";
import ReactStars from "react-rating-stars-component";
import { toast } from 'react-toastify';
import SweetAlert from "react-bootstrap-sweetalert";



var moment = require('moment-timezone');
const TIME_ZONE = 'Singapore';
const DATE_FORMAT_AVLBLTY = 'DD MMM';
const TIME_FORMAT_AVLBLTY = 'hh:mm A';


const ProductRating = {
  size: 30,
  value: 0,
  edit: true,
  isHalf: true
};


export default class MyOrder extends Component {

  state = {
    OrderList: [],
    loading: true,
    toggleId: '',
    customerID: '',
    reviewTextArea: false,
    productVariantId: null,
    orderID: null,

    showSweetAlert: false,
    showSweetAlertMainOrder: false,
    orderStatus: '',
    orderId: '',
    index: '',
    mainorderStatus: '',
    mainorderId: '',
    totalProduct: '',
    isActive: ''
  }

  componentDidMount() {
    this.fetchOrderData();
  }

  handleOnChangeProductOrderStatusCheck = (event, orderId, index) => {
    let orderStatus = event;
    console.log(orderStatus, 'value');
    this.setState({
      showSweetAlert: true,
      orderStatus: orderStatus,
      orderId: orderId,
      index: index

    });
  };

  onCancel() {
    this.setState({
      showSweetAlert: false,
      orderStatus: '',
      orderId: '',
      index: ''
    });
  }
  confirmChangeOrderStatus() {
    this.setState(
      {
        showSweetAlert: false,
      },
      () => {
        this.handleOnChangeProductOrderStatus();
      }
    );
  }


  handleOnChangeOrderStatusCheck = (event, orderId, totalProduct) => {
    //console.log(event,'value');
    let orderStatus = event;
    this.setState({
      showSweetAlertMainOrder: true,
      mainorderStatus: orderStatus,
      mainorderId: orderId,
      totalProduct: totalProduct

    });
  };

  onCancelMainOrder() {
    this.setState({
      showSweetAlertMainOrder: false,
      mainorderStatus: '',
      mainorderId: '',
      totalProduct: ''
    });
  }
  confirmChangeOrderStatusMain() {
    //console.log(this.state.mainorderStatus,this.state.mainorderId,this.state.totalProduct,'test');return false;
    this.setState(
      {
        showSweetAlertMainOrder: false,
      },
      () => {
        this.handleOnChangeOrderStatus();
      }
    );
  }

  handleToggle = (orderID) => {
    const {isActive} = this.state;
    if(isActive.length < 1 || isActive !== orderID){
      this.setState({ isActive: orderID });
    }else{
      this.setState({ isActive: '' });
    }
  };

  fetchOrderData = async () => {
    let checkToken = await localStorage.getItem("token");
    if (checkToken) {
      const decoded = jwt_decode(checkToken);
      if (decoded.userType === 'customer') {
        await axios.get(`${window.$URL}order/fetchorder/${decoded.userid}`)
          .then(res => {
            console.log(res.data.data, 'datea')
            if (res.status === 200) {
              this.setState({
                OrderList: res.data.data,
                customerID: decoded.userid,
                loading: false
              })
            }
          })
          .catch(error => {
            console.log(error)
          })
      }
    } else {
      this.setState({
        OrderList: [],
        loading: false
      })
    }
  }

  ratingSubmit = async (ratingVal, productVariantId, ProductSellerId, orderID) => {
    //  console.log({ratingVal , productVariantId,sellerId})
    //console.log(productVariantId)
    let checkToken = await localStorage.getItem("token");
    if (checkToken) {
      const decoded = jwt_decode(checkToken);

      let addProductrating = {
        customerID: decoded.userid,
        sellerID: ProductSellerId,
        productVariantID: productVariantId,
        rating: ratingVal,
        review: "",
        orderID: orderID
      }

      //console.log(addProductrating)

      if (decoded.userType === 'customer') {
        await axios.post(`${window.$URL}ratingReview/addProductRating`, addProductrating)
          .then(res => {
            if (res.status === 200) {
              console.log("rating data save succcessfully")
              this.componentDidMount();
              window.location.reload();
            } else {
              console.log("rating data save failed")
            }
          })
          .catch(error => {
            console.log(error)
          })
      }
    }

  }

  handleOnChange(event) {
    this.setState({
      review: event.target.value
    })
  }

  submitProductReview = async (productVariantId, ProductSellerId, orderID) => {

    let checkToken = await localStorage.getItem("token");
    if (checkToken) {
      const decoded = jwt_decode(checkToken);

      let addProductReview = {
        customerID: decoded.userid,
        sellerID: ProductSellerId,
        productVariantID: productVariantId,
        review: this.state.review,
        orderID: orderID
      }

      console.log(addProductReview)

      if (decoded.userType === 'customer') {
        await axios.post(`${window.$URL}ratingReview/addProductReview`, addProductReview)
          .then(res => {
            if (res.status === 200) {
              console.log("product review save succcessfully")
              this.componentDidMount();
              window.location.reload();
            } else {
              console.log("product review save failed")
            }
          })
          .catch(error => {
            console.log(error)
          })
      }
    }

  }
  async handleOnChangeOrderStatus() {

    let ordedStatusData = {
      orderId: this.state.mainorderId,
      orderStatus: this.state.mainorderStatus,
      totalProduct: this.state.totalProduct,
      cancelledBy: 'customer'
    }
    console.log(ordedStatusData, 'orderStatus')

    await axios.post(`${window.$URL}order/update-order-status`, ordedStatusData)
      .then(response => {
        if (response.data.success) {
          toast.success(response.data.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          // this.props.pageReload();
          //window.location.reload();
          this.componentDidMount();
        }
      })
      .catch(error => {
        toast.error('Something went wrong', {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      })

  }

  async handleOnChangeProductOrderStatus() {

    //alert(orderId);return false;
    let ordedStatusData = {
      orderId: this.state.orderId,
      orderStatus: this.state.orderStatus,
      index: this.state.index,
      cancelledBy: 'customer'
    }
    console.log(ordedStatusData, 'orderStatus')


    await axios.post(`${window.$URL}order/update-product-order-status`, ordedStatusData)
      .then(response => {
        if (response.data.success) {
          toast.success(response.data.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          //this.props.pageReload();
          //window.location.reload();
          this.componentDidMount();

        }
      })
      .catch(error => {
        toast.error('Something went wrong', {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      })

  }

  showTexArea = (productVariantId, orderID) => {
    console.log('view texta raea')
    this.setState({ reviewTextArea: true, productVariantId, orderID })
  }

  render() {
    const { OrderList, loading, toggleId, showSweetAlert, showSweetAlertMainOrder, isActive } = this.state;
    if (loading) {
      return (
        <Loader />
      )
    } else {
      return (

        <section className="dbF-content">
          {showSweetAlert ? (
            <SweetAlert
              warning
              showCancel
              confirmBtnText="Yes"
              confirmBtnBsStyle="danger"
              title="Confirm you wish to Change Status of this item?"
              onConfirm={() => this.confirmChangeOrderStatus()}
              onCancel={() => this.onCancel()}
              focusCancelBtn
            />
          ) : null}
          {
            showSweetAlertMainOrder ? (
              <SweetAlert
                warning
                showCancel
                confirmBtnText="Yes"
                confirmBtnBsStyle="danger"
                title="Confirm you wish to Change Status of this item?"
                onConfirm={() => this.confirmChangeOrderStatusMain()}
                onCancel={() => this.onCancelMainOrder()}
                focusCancelBtn
              />
            ) : null
          }
          <div className="container">
            <div className="row">
              <ProfileSidebar />
              <div className="col-lg-9">
                <div className="my-order-wr new-orders-pay">
                  <h2 className="order-title">My Orders</h2>
                  <div className="seller-order-wrap">
                    <div className="my-order-body">
                      {/* <div className="show-price">
                        <p>Show : </p>
                        <select className="custom-select">
                          <option value={0}>Last 5 Orders</option>
                          <option value={1}>Today</option>
                          <option value={2}>10 days Order</option>
                          <option value={3}>1month Order</option>
                        </select>
                      </div> */}
                      {
                        OrderList.length > 0 ?
                          OrderList.map((OrderDetails, indexof) => {
                            return (
                              <div className={isActive === OrderDetails._id ? "order-wrap-inner  mt-4 active" : "order-wrap-inner  mt-4"} key={indexof}>
                                <div className="order-preap-section">
                                  {/*<ul className="nav nav-pills nav-fill">
                                      <li className="nav-item">
                                        <a className="nav-link active" href="javscript:void(0)">New</a>
                                      </li>
                                      <li className="nav-item">
                                        <a className="nav-link active" href="javscript:void(0)">Processing</a>
                                      </li>
                                      <li className="nav-item">
                                        <a className="nav-link" href="javscript:void(0)">Despatched</a>
                                      </li>
                                      <li className="nav-item">
                                        <a className="nav-link  " href="javscript:void(0)">Completed</a>
                                      </li>
                                    </ul>*/}
                                  <div className="card-delivery-slot">
                                    <div className="row">
                                      <div className="col-md-8">
                                        <div className="row">
                                          <div className="col-md-10">
                                            <h2 className="credit-card-details">{OrderDetails.paymentMode} Order #{OrderDetails.orderID}</h2>
                                            {/*<p className="order-place-time">Order {orderDate} @{orderTime}</p>*/}
                                          </div>
                                          {/* <div className="col-md-2 text-center">
                                          <img src={OrderDetails.productVariantId[0].sellerProductId.sellerId.SellerDetails.storeLogo !== null &&
                                          OrderDetails.productVariantId[0].sellerProductId.sellerId.SellerDetails.storeLogo !== undefined
                                          ? window.$ImageURL +
                                          OrderDetails.productVariantId[0].sellerProductId.sellerId.SellerDetails.storeLogo.originalURL
                                          : `${window.$ImageURL}no-image-480x480.png`}
                                          className="img-fluid" alt={creditCardImage} /> 
                                          </div> */}
                                        </div>
                                        <div className="row process-sec">
                                          <div className="col-md-6 ">
                                            <p className="money-to-pay"> Paid QAR {parseFloat(OrderDetails.subTotal).toFixed(2)}</p>
                                          </div>
                                          {/* <div className="col-md-6 btn-process-wrap">
                                              <button className="btn process-order">PROCESS
                                                ORDER</button>
                                            </div> */}
                                          {OrderDetails.orderStatus === 'cancelled' ?
                                            <div className=" btn-process-wrap">

                                              {/* <select className={`form-control`} disabled name="orderStatus" onChange={(event) => this.handleOnChangeOrderStatusCheck(event, OrderDetails._id,OrderDetails.productVariantId.length ? OrderDetails.productVariantId.length:null)} defaultValue={OrderDetails.orderStatus} >
                                          <option value="">Select</option>
                                          <option value="cancelled">Cancel</option>
                                         
                                          </select>  */}

                                              <button disabled onClick={(event) => this.handleOnChangeOrderStatusCheck('cancelled', OrderDetails._id, OrderDetails.productVariantId.length ? OrderDetails.productVariantId.length : null)} className="btn btn-cancel-new">Cancelled</button>

                                            </div>

                                            : OrderDetails.orderStatus === 'completed' ?
                                              <div className=" btn-process-wrap">

                                                {/* <select className={`form-control`} name="orderStatus" onChange={(event) => this.handleOnChangeOrderStatusCheck(event, OrderDetails._id,OrderDetails.productVariantId.length ? OrderDetails.productVariantId.length:null)} defaultValue={OrderDetails.orderStatus} >
                                          <option value="">Select</option>
                                          <option value="cancelled">Cancel</option>
                                          
                                          </select>  */}
                                                <button onClick={(event) => this.handleOnChangeOrderStatusCheck('return', OrderDetails._id, OrderDetails.productVariantId.length ? OrderDetails.productVariantId.length : null)} className="btn btn-cancel-new">Return</button>

                                              </div>
                                              :
                                              OrderDetails.orderStatus === 'pending' ?

                                                <div className=" btn-process-wrap">

                                                  {/* <select className={`form-control`} name="orderStatus" onChange={(event) => this.handleOnChangeOrderStatusCheck(event, OrderDetails._id,OrderDetails.productVariantId.length ? OrderDetails.productVariantId.length:null)} defaultValue={OrderDetails.orderStatus} >
                                          <option value="">Select</option>
                                          <option value="cancelled">Cancel</option>
                                          
                                          </select>  */}
                                                  <button onClick={(event) => this.handleOnChangeOrderStatusCheck('cancelled', OrderDetails._id, OrderDetails.productVariantId.length ? OrderDetails.productVariantId.length : null)} className="btn btn-cancel-new">Cancel</button>

                                                </div>
                                                : ''
                                          }

                                        </div>
                                      </div>
                                      <div className="col-md-4 d-flex">
                                        <div className="book-slot">
                                          <p className="dv-slot">Seller Details</p>
                                          <div className="new-date-wr">
                                            <p className="dv-date">Seller Name: {OrderDetails.sellerID.Firstname ? OrderDetails.sellerID.Firstname + ' ' + OrderDetails.sellerID.Lastname : 'N/A'}</p>

                                          </div>
                                        </div>
                                      </div>
                                      <div className="col-md-4 d-flex">
                                        <div className="book-slot">
                                          <p className="dv-slot">Delivery Slot</p>
                                          <div className="new-date-wr">
                                            <p className="dv-date">{moment(OrderDetails.deliveryTimeSlotFrom * 1000).tz(TIME_ZONE).format(DATE_FORMAT_AVLBLTY)}</p>
                                            <p className="dv-time">
                                              {moment(OrderDetails.deliveryTimeSlotFrom * 1000).tz(TIME_ZONE).format(TIME_FORMAT_AVLBLTY)} to {moment(OrderDetails.deliveryTimeSlotTo * 1000).tz(TIME_ZONE).format(TIME_FORMAT_AVLBLTY)}
                                            </p>
                                          </div> <p className="dv-muti-order">{moment(OrderDetails.deliveryTimeSlotFrom * 1000).fromNow()} Single Order </p>
                                        </div>
                                      </div>
                                      <div className="social-wrapd">
                                        <a href={`tel:+974-${OrderDetails.productVariantId[0].sellerProductId.sellerId.warehouseAddressID.MobileNo}`}><img src={phoneIcon} alt={phoneIcon} />Contact No.</a>
                                        <a href={`https://web.whatsapp.com/send?phone=+974${OrderDetails.productVariantId[0].sellerProductId.sellerId.warehouseAddressID.WhatsappNo}&amp;text=Hello`} target="_blank" rel="noopener noreferrer"><img src={whatsappIcon} alt={whatsappIcon} />Whatsapp</a>
                                        {/*<a href={`${process.env.PUBLIC_URL}/messagedetails/${OrderDetails.productVariantId[0].sellerProductId.sellerId._id}`}><img src={chatIcon} alt={chatIcon} />Chat</a>*/}
                                      </div>
                                    </div>
                                  </div>
                                  <div className="preview-wrapper" onClick={() => this.handleToggle(OrderDetails._id)} data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    preview order
                                  </div>
                                </div>
                                <div id="collapseOne" className={isActive === OrderDetails._id ? "collapse show " : "collapse"} aria-labelledby="headingOne" data-parent="#accordionExample">
                                  <div className="product-review">
                                    {/* view items start */}
                                    {
                                      OrderDetails.productVariantId ?
                                        OrderDetails.productVariantId.map((data, index) => {
                                          var imageData;
                                          if (data.sellerProductId.productImageId.length < 1 || data === undefined) {
                                            imageData = `${window.$ImageURL}no-image-480x480.png`
                                          } else if (data.sellerProductId.primaryProductImage !== null && data.sellerProductId.primaryProductImage !== undefined) {
                                            imageData = `${window.$ImageURL}` + data.sellerProductId.primaryProductImage.originalURL
                                          } else {
                                            imageData = `${window.$ImageURL}` + data.sellerProductId.productImageId[0].originalURL
                                          }
                                          return (

                                            <div key={index} className="stock-card ">
                                              <div className="card-f-dealer">
                                                <div className="left-conte-card">
                                                  <div className="img-sect">
                                                    <img src={imageData} alt={imageData} />
                                                  </div>
                                                  <div className="content-sect">
                                                    <div className="name-div">
                                                      <h1 className="st-name">{data.sellerProductId.productName}
                                                      </h1>
                                                      <h4 className="price">QAR {parseFloat(OrderDetails.Price[index]).toFixed(2)} </h4>
                                                    </div>
                                                    <div className="fruit-stock">
                                                      <div className="sep_ator">
                                                        <h4 className="s-dc">{data.sellerProductId.brand}</h4>
                                                        <h4 className="s-dc">{data.packSize}{data.packSizeMeasureUnit}</h4>
                                                      </div>
                                                      {/* <h4 className="stock-inhand">stock: <span>20</span></h4> */}
                                                    </div>
                                                    {
                                                      OrderDetails.orderProductStatus[index] === 'cancelled' ?
                                                        <div className="col-md-6 btn-process-wrap">
                                                          {/* <select className={`form-control`}  disabled name="productOrderStatus" onChange={(event) => this.handleOnChangeProductOrderStatusCheck(event, OrderDetails._id,index)} defaultValue={OrderDetails.orderProductStatus[index]} >
                                                        
                                                        
                                                        <option value="cancelled">Cancel</option>
                                                        
                                                        </select>  */}
                                                          <button disabled className="btn btn-cancel-new" onClick={(event) => this.handleOnChangeProductOrderStatusCheck('cancelled', OrderDetails._id, index)} defaultValue={OrderDetails.orderProductStatus[index]}>Cancelled</button>
                                                        </div>
                                                        : OrderDetails.orderProductStatus[index] === 'completed' && data.sellerProductId.productType !== 'grocery' ?
                                                          <div className="col-md-6 btn-process-wrap">
                                                            {/* <select className={`form-control`} name="productOrderStatus" onChange={(event) => this.handleOnChangeProductOrderStatusCheck(event, OrderDetails._id,index)} defaultValue={OrderDetails.orderProductStatus[index]} >
                                                        
                                                        <option value="cancelled">Cancel</option>
                                                       
                                                        </select>  */}
                                                            <button className="btn btn-cancel-new" onClick={(event) => this.handleOnChangeProductOrderStatusCheck('return', OrderDetails._id, index)} defaultValue={OrderDetails.orderProductStatus[index]}>Return</button>

                                                          </div>
                                                          :
                                                          OrderDetails.orderStatus === 'pending' ?
                                                            <div className=" btn-process-wrap">
                                                              {/* <select className={`form-control`} name="productOrderStatus" onChange={(event) => this.handleOnChangeProductOrderStatusCheck(event, OrderDetails._id,index)} defaultValue={OrderDetails.orderProductStatus[index]} >
                                                        
                                                        <option value="cancelled">Cancel</option>
                                                       
                                                        </select>  */}
                                                              <button className="btn btn-cancel-new" onClick={(event) => this.handleOnChangeProductOrderStatusCheck('cancelled', OrderDetails._id, index)} defaultValue={OrderDetails.orderProductStatus[index]}>Cancel</button>

                                                            </div>
                                                            : ''
                                                    }
                                                    <div className="desc-barcode">
                                                      {/* <p className="sdatet-desc">Stock Date / Time last
                                                        update: <span> 28-Nov-2020
                                                          / 0932 hrs</span></p> */}
                                                      <p className="sdatet-desc">Barcode <span>
                                                        {data.barcodeNumber}</span></p>
                                                      {/*() => this.fetchProductratingDetail(data._id , data.sellerProductId.sellerId._id)*/}
                                                      {OrderDetails.orderProductStatus[index] === 'completed' ?

                                                        OrderDetails.ratingID[index] === undefined ?

                                                          <span>
                                                            <ReactStars onChange={(val) => this.ratingSubmit(val, data._id, data.sellerProductId.sellerId._id, OrderDetails._id)} {...ProductRating} />
                                                          </span>
                                                          :
                                                          <div>
                                                            <span>
                                                              <ReactStars value={OrderDetails.ratingID[index].rating} size={30} isHalf={true} edit={false} />
                                                            </span>
                                                            {
                                                              !this.state.reviewTextArea ?
                                                                <span onClick={() => this.showTexArea(data._id, OrderDetails._id)}>
                                                                  Write a Review
                                                                </span>
                                                                : null

                                                            }

                                                            {
                                                              this.state.reviewTextArea && this.state.productVariantId === data._id && this.state.orderID === OrderDetails._id ?
                                                                <div>
                                                                  <textarea className="form-control" value={this.state.review} onChange={(event) => this.handleOnChange(event)} name="review" placeholder="Please write product review"></textarea>
                                                                  <button onClick={() => this.submitProductReview(data._id, data.sellerProductId.sellerId._id, OrderDetails._id)} className="btn btn-primary">submit</button> <button onClick={() => this.setState({ reviewTextArea: false, productVariantId: null, orderID: null, review: null })} className="btn btn-danger">cancel</button>
                                                                </div>
                                                                :
                                                                null
                                                            }


                                                          </div>


                                                        : null
                                                      }

                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          )
                                        })
                                        : null
                                    }

                                    {/* view items end */}
                                    {/*grand total start */}
                                    <div className="toal-order">
                                      <div className="order-items" style={{ borderBottom: '1px solid #ddd' }}>
                                        <p className="b-items">Order Total</p>
                                        <p className="nrmal-items"> QAR {parseFloat(OrderDetails.Total).toFixed(2)}</p>
                                      </div>
                                      <div className="order-items">
                                        <p className="b-items">Delivery Fee</p>
                                        <p className="nrmal-items">QAR {parseFloat(OrderDetails.shippingFee).toFixed(2)}</p>
                                      </div>
                                      <div className="order-items">
                                        <p className="b-items">Sub Total </p>
                                        <p className="nrmal-items"> QAR {parseFloat(OrderDetails.subTotal).toFixed(2)}</p>
                                      </div>
                                      {/* <div className="order-items">
                                          <p className="b-items">myMart Margin ( variable )</p>
                                          <p className="nrmal-items"> QAR 12.00</p>
                                        </div> */}
                                      {/* <div className="order-items">
                                          <p className="b-items">Final Amount Due to You</p>
                                          <p className="nrmal-items"> QAR 100.00</p>
                                        </div> */}
                                    </div>
                                    {/*grand total end */}
                                  </div>
                                </div>
                              </div>
                            )
                          })
                          :
                          <div className="col-md-12 col-sm-12 flex-screen">
                            <div className="card product-card" style={{ padding: '30px' }}>
                              <h3 style={{ textAlign: 'center' }}>Order List is empty.</h3>
                            </div>
                          </div>
                      }
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>


      )
    }
  }
}