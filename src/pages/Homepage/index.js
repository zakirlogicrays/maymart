import React, { Component } from "react";
import MultiCarousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import axios from "axios";
import ProductListing from "../../Component/ProductListing";
import NewTranding from "../../Component/NewTranding";
import Category1 from "../../../src/assets/images/grocery 1.svg";
import Category2 from "../../../src/assets/images/All-categorys.svg";
import Category3 from "../../../src/assets/images/Maymall.svg";
import Category4 from "../../../src/assets/images/Promotions.svg";
import Category5 from "../../../src/assets/images/Vouchers.svg";
import Category6 from "../../../src/assets/images/Scheduled Cart.svg";
import Category7 from "../../../src/assets/images/Partner Store.svg";
import Category8 from "../../../src/assets/images/All Menu.svg";

import AppStor from "../../assets/images/home-play-stor2.png";
import PlayStor from "../../assets/images/home-play-stor.png";

import ElectronicDevices1 from "../../../src/assets/images/Electronic Devices.svg";
import ElectronicDevices2 from "../../../src/assets/images/Drinks.svg";
import ElectronicDevices3 from "../../../src/assets/images/Meat & Seafood.svg";
import ElectronicDevices4 from "../../../src/assets/images/Dairy & Chilled.svg";
import ElectronicDevices5 from "../../../src/assets/images/Fruit & Vegetable.svg";
import ElectronicDevices6 from "../../../src/assets/images/Bakery.svg";
import ElectronicDevices7 from "../../../src/assets/images/Automotive Parts.svg";
import ElectronicDevices8 from "../../../src/assets/images/Health & Beauty.svg";
import ElectronicDevices9 from "../../../src/assets/images/All.svg";

import BrandLogo1 from "../../../src/assets/images/brand-logo1.svg";
import BrandLogo2 from "../../../src/assets/images/brand-logo2.svg";
import BrandLogo3 from "../../../src/assets/images/brand-logo3.svg";
import BrandLogo4 from "../../../src/assets/images/brand-logo4.svg";
import BrandLogo5 from "../../../src/assets/images/brand-logo5.svg";

import AllMenueModal from "../../Component/AllMenueModal.js";
import ProductPopUp from "../../Component/ProductPopUp";
import ProductChosePopUp from "../../Component/ProductChosePopUp";


import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';
import Loader from '../../Component/Loader';

const responsiveCategory = {
    superLargeDesktop: {
        // the naming can be any, depends on you.
        breakpoint: { max: 4000, min: 3000 },
        items: 8
    },
    desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 8
    },
    tablet: {
        breakpoint: { max: 1024, min: 575 },
        items: 5.2
    },
    mobile: {
        breakpoint: { max: 576, min: 0 },
        items: 5,
    }
};

const PopulerBrandLogo = {
    superLargeDesktop: {
        // the naming can be any, depends on you.
        breakpoint: { max: 4000, min: 3000 },
        items: 5
    },
    desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 5
    },
    tablet: {
        breakpoint: { max: 1024, min: 575 },
        items: 4.2
    },
    mobile: {
        breakpoint: { max: 576, min: 0 },
        items: 4.2,
    }
};

const HighlightSlider = {
    superLargeDesktop: {
        // the naming can be any, depends on you.
        breakpoint: { max: 4000, min: 3000 },
        items: 4
    },
    desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 4
    },
    tablet: {
        breakpoint: { max: 1024, min: 575 },
        items: 2.2
    },
    mobile: {
        breakpoint: { max: 576, min: 0 },
        items: 1.7,
    }
};

const responsive = {
    superLargeDesktop: {
        // the naming can be any, depends on you.
        breakpoint: { max: 4000, min: 3000 },
        items: 8
    },
    desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 8
    },
    tablet: {
        breakpoint: { max: 1024, min: 575 },
        items: 4.2
    },
    mobile: {
        breakpoint: { max: 576, min: 0 },
        items: 4.2,
    }
};

const responsiveBigBanner = {
    superLargeDesktop: {
        // the naming can be any, depends on you.
        breakpoint: { max: 4000, min: 3000 },
        items: 5
    },
    desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 3
    },
    tablet: {
        breakpoint: { max: 1024, min: 464 },
        items: 2
    },
    mobile: {
        breakpoint: { max: 464, min: 0 },
        items: 2
    }
}

const categoryBanner = ['Grocery', 'Marketplace', 'MyMall', 'Promotions', 'Vouchers', 'Categories'];

export default class Homepage extends Component {
    state = {
        settings: {
            dots: true,
            infinite: true,
        },
        firstBanner: [],
        secondBanner: [],
        thirdBanner: [],
        fourthBanner: [],
        selectedCategory: [],
        choosenProduct: [],
        newAndTrending: [],
        newAndTrendingLoading: true,
        choosenLoading: true,
        popularCategory: [],
        tegoryLoading: true,
        showUpcomingModal: false,
        modalMsg: '',
        windowWidth: this.getWindowDimensions().width,
        parentCategoryList: undefined,
        showAllMenueModal: false,
        showProductPopUp: false,
        PopulerBrands: [],
        showChosenProductPopUp: false,
        chosenProductImageData: undefined,
        chosenProductName: undefined,
        newAndTrandingPopUpData: undefined,
        chosenData: undefined,
    }
    getWindowDimensions() {
        const { innerWidth: width } = window;
        return {
            width
        };
    }

    isBottom(el) {
        return el.getBoundingClientRect().bottom <= window.innerHeight;
      }
      


    async componentDidMount() {
        let widthofscreen = this.getWindowDimensions();
        document.addEventListener('scroll', this.trackScrolling);
        await axios.get(`${window.$URL}banner/fetchBanner/homepage`)
            .then(response => {
                if (response.status === 200) {
                    let bannerArray = response.data.data;
                    let firstBanner = [];
                    let secondBanner = [];
                    let thirdBanner = [];
                    let fourthBanner = [];

                    for (let i = 0; i <= bannerArray.length - 1; i++) {
                        if (bannerArray[i].type === "1st Banner" && bannerArray[i].bannerShowesTo === "Homepage") {
                            firstBanner.push({ url: `${window.$ImageURL}` + bannerArray[i].bannerImageId[0].originalURL, bannerURL: bannerArray[i].bannerURL })
                        } else if (bannerArray[i].type === "2nd Banner" && bannerArray[i].bannerShowesTo === "Homepage") {
                            secondBanner.push(bannerArray[i])
                        } else if (bannerArray[i].type === "3rd Banner" && bannerArray[i].bannerShowesTo === "Homepage") {
                            thirdBanner.push(bannerArray[i])
                        } else if (bannerArray[i].type === "4th Banner" && bannerArray[i].bannerShowesTo === "Homepage") {
                            fourthBanner.push(bannerArray[i])
                        }
                    }
                    this.setState({
                        firstBanner,
                        secondBanner,
                        thirdBanner,
                        fourthBanner
                    })
                }
            })
            .catch(error => {
                console.log(error);
            })
        this.getallParentCategory();
        this.getCategory();
        this.getChooseForYou();
        this.newAndTrending();
        this.getPopularCategories();
        this.fetchBrands();
        window.scrollTo(0, 0)
    }

    componentWillUnmount() {
        document.removeEventListener('scroll', this.trackScrolling);
      }
    
      trackScrolling = (e) => {
        const bottom = e.target.scrollHeight - e.target.scrollTop === e.target.clientHeight;
        const wrappedElement = document.getElementById('header');
        if (bottom) {
          console.log('header bottom reached');
          document.removeEventListener('scroll', this.trackScrolling);
        }
      };

      
    hideComingModal = () => {
        this.setState(
            {
                showUpcomingModal: false,
            },
            () => {
                //this.componentDidMount();
            }
        );
    };

    showUpcomingModalStatus = (msg) => {
        this.setState(
            {
                showUpcomingModal: true,
                modalMsg: msg
            },
            () => {
                //this.componentDidMount();
            }
        );
    };

    fetchBrands = () => {
        axios.get(`${window.$URL}brand/fetchBrand`)
            .then(response => {
                if (response.data.success) {
                    this.setState({
                        PopulerBrands: response.data.data
                    })
                }
            })
            .catch(error => {
                console.log(error)
            })
    }

    getPopularCategories = async () => {

        await axios.post(`${window.$URL}popular-category/fetch-popular-catetgory-by-pagename`, { pageName: 'Homepage' })
            .then(async response => {
                if (response.data.success) {
                    await this.setState({
                        popularCategory: response.data.data,
                        popularCategoryLoading: false
                    })
                    // console.log("this.state?.popularCategory ----->", Math.floor(Math.random() * this.state?.popularCategory.length));
                    // localStorage.setItem("randomCategoryHeader", this.state?.popularCategory[Math.floor(Math.random() * this.state?.popularCategory.length)]?.categoryName)}
            }})
            .catch(error => {
                console.log(error)
            })
    }

    getallParentCategory = async () => {

        await axios.get(`https://mymart.qa:7778/api/category/allParentCategory`)
            .then(async response => {
                if (response.data.success) {
                    //console.log("res ----->", response.data.response);
                    await this.setState({
                        parentCategoryList: response.data.response
                    })
                }
            })
            .catch(error => {
                console.log(error)
            })
    }

    getCategory = async () => {
        let selectedCat = []
        let found;
        await axios.get(`${window.$URL}category/categoryDropdown`)
            .then(response => {
                response.data.response.map((catdesc, catindex) => {
                    var nameArr = catdesc.categoryLevel[0]
                    nameArr = nameArr.split(',')
                    found = nameArr.indexOf("Grocery");
                    if (found === -1) {
                        if (catdesc.categoryLevel[0] !== '') {
                            if (catdesc.categoryLevel.categoryName !== 'Grocery') {
                                selectedCat.push(catdesc)
                            }
                        }

                    }
                    this.setState({
                        selectedCategory: selectedCat
                    })
                    return true
                })
            })
            .catch(error => {
                console.log(error.data)
            })
    }

    getChooseForYou = async () => {
        await axios.post(`${window.$URL}sellerProducts/getChoosenProducts`)
            .then(response => {
                console.log(response);
                this.setState({
                    choosenProduct: response.data,
                    choosenLoading: false
                })
            })
            .catch(error => {
                console.log(error.data)
            })
    }

    newAndTrending = async () => {
        await axios.post(`${window.$URL}sellerProducts/newAndTrending`, { category: 'All' })
            .then(response => {
                if (response.status === 200) {
                    this.setState({
                        newAndTrending: response.data,
                        newAndTrendingLoading: false
                    })
                }
            })
            .catch(error => {
                console.log(error)
            })
    }

    createMarkup = (html) => {
        return {
            __html: html
        };
    };

    openAllMenueModal = () => {
        this.setState({
            showAllMenueModal: true
        })
    }

    openProductPopUp = (data) => {
        this.setState({
            showProductPopUp: true,
            onClickProductPopUp: data
        })
    }

    openChosenProductPopUp = (data) => {
        this.setState({
            showChosenProductPopUp: true,
            chosenProductImageData: data?.productImageId,
            chosenProductName: data?.productName,
            chosenData: data
        })
    }


    hideLocModal = () => {
        this.setState({
            showAllMenueModal: false,
            showProductPopUp: false,
            showChosenProductPopUp: false,
        })
    }


    removeHTML = (html) => {
        const regex = /(<([^>]+)>)/ig;
        const result = html.replace(regex, '');
        let returnName = '';
        if (result === 'Groceries') {
            returnName = 'Grocery';
        } else if (result === 'My Mall') {
            returnName = 'MyMall';
        } else if (result === 'Marketplace&nbsp;') {
            returnName = 'Marketplace';
        } else {
            returnName = result;
        }
        return returnName;
    }
    convertFirestLetterToSmall(str) {
        return str.charAt(0).toLowerCase() + str.slice(1);
    }

    render() {
        const {
            firstBanner,
            popularCategory,
            popularCategoryLoading,
            secondBanner,
            thirdBanner,
            fourthBanner,
            /*selectedCategory,*/
            choosenProduct,
            newAndTrending,
            showUpcomingModal,
            PopulerBrands,
        } = this.state;;
        // const firstBanner = [
        //     {
        //         url: "https://mymart.qa:7779/uploads/bannerImage-1621864690304.jpg", bannerURL: ""
        //     },
        //     {
        //         url: "https://mymart.qa:7779/uploads/bannerImage-1621864690304.jpg", bannerURL: ""
        //     },
        //     {
        //         url: "https://mymart.qa:7779/uploads/bannerImage-1621864690304.jpg", bannerURL: ""
        //     }
        // ]
        return (
            <div className="home-page" onScroll={(e) => this.trackScrolling(e)}>
                {/* Slider Banner */}
                <section className="banner">
                    {showUpcomingModal ? (
                        <div
                            id="exampleModalCenter"
                            className="modal fade show"
                            tabIndex={-1}
                            role="dialog"
                            aria-labelledby="exampleModalCenterTitle"
                            style={{ display: "block", paddingRight: 17 }}
                        >
                            <div
                                className="modal-dialog modal-dialog-centered"
                                role="document"
                            >
                                <div className="modal-content">
                                    <div className="modal-header">
                                        {/* <h5 className="modal-title" id="exampleModalCenterTitle">Page Status</h5> */}
                                        <button type="button" onClick={() => this.hideComingModal()} className="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div className="modal-body">
                                        {/* <p>Promotions is Comming...</p> */}
                                        <h5 className="modal-title" id="exampleModalCenterTitle">
                                            {this.state.modalMsg} coming soon...
                                        </h5>
                                    </div>
                                    {/* <div className="modal-footer">
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" className="btn btn-primary">Save changes</button>
                                    </div> */}
                                </div>
                            </div>
                        </div>
                    ) : (
                        ""
                    )}
                    {/* <div className="container"> */}
                    <div className="banner-slider">
                        {/* {console.log ("firstBanner ----->",firstBanner)} */}
                        {
                            firstBanner.length > 0 ?
                                <Carousel
                                    // autoPlay
                                    infiniteLoop
                                    centerSlidePercentage={70}
                                    centerMode={!(this.getWindowDimensions().width < 767)}
                                    showThumbs={false}
                                    showStatus={false}
                                >
                                    {
                                        firstBanner.map((data, index) => {
                                            return (
                                                <div key={index}>
                                                    <a href={data.bannerURL}><img src={data.url} alt={data.url} className="banner-image" /></a>
                                                    <div className="banner-see-all-btn">
                                                        <a href="#" onClick={() => window.open(data.bannerURL)}> See all </a>
                                                    </div>
                                                </div>
                                            )
                                        })
                                    }
                                </Carousel>
                                :
                                null
                        }
                    </div>
                    {/* </div> */}
                </section>
                {/* end slider */}

                {/* start Mall section */}

                <div className="bg-for-mobile">
                    <section className="home-category-sec">

                        <div className="home-category">
                            <div className="home-grocery">
                                <MultiCarousel removeArrowOnDeviceType={["tablet", "mobile"]} responsive={responsiveCategory}>
                                    <a href="#">
                                        <div className="home-category-box" onClick={() => window.location.href = `${process.env.PUBLIC_URL}/groceries`}>
                                            <div className="home-category-box-img">
                                                <img src={Category1} alt={Category1} />
                                            </div>
                                            <p>Grocery</p>
                                        </div>
                                    </a>
                                    <a href="#">
                                        <div className="home-category-box">
                                            <div className="home-category-box-img">
                                                <img src={Category3} alt={Category3} />
                                            </div>
                                            <p>MyMall</p>
                                        </div>
                                    </a>
                                    <a href="#">
                                        <div className="home-category-box">
                                            <div className="home-category-box-img">
                                                <img src={Category2} alt={Category2} />
                                            </div>
                                            <p>Categories</p>
                                        </div>
                                    </a>
                                    <a href="#">
                                        <div className="home-category-box">
                                            <div className="home-category-box-img">
                                                <img src={Category4} alt={Category4} />
                                            </div>
                                            <p>Promotions</p>
                                        </div>
                                    </a>
                                    <a href="#">
                                        <div className="home-category-box">
                                            <div className="home-category-box-img">
                                                <img src={Category5} alt={Category5} />
                                            </div>
                                            <p>Vouchers</p>
                                        </div>
                                    </a>
                                    <a href="#">
                                        <div className="home-category-box">
                                            <div className="home-category-box-img">
                                                <img src={Category6} alt={Category6} />
                                            </div>
                                            <p>Scheduled Cart</p>
                                        </div>
                                    </a>
                                    <a href="#">
                                        <div className="home-category-box">
                                            <div className="home-category-box-img" onClick={() => window.location.href = `${process.env.PUBLIC_URL}/categories`}>
                                                <img src={Category7} alt={Category7} />
                                            </div>
                                            <p>Partner Store</p>
                                        </div>
                                    </a>
                                    <a href="#">
                                        <div className="home-category-box" onClick={() => this.openAllMenueModal()}>
                                            <div className="home-category-box-img">
                                                <img src={Category8} alt={Category8} />
                                            </div>
                                            <p>All Menu</p>
                                        </div>
                                    </a>
                                </MultiCarousel>
                            </div>
                            <AllMenueModal modalStatus={this.state.showAllMenueModal} hideLocationModal={() => this.hideLocModal()} />
                            <div className="home-electronic">
                                <ul>
                                    {this.state.parentCategoryList?.slice(0, 5)?.map((item, idx) => {
                                        return (
                                            <li>
                                                <a href="#">
                                                    <div className="electronic-box" >
                                                        <img src={`https://mymart.qa:7779/uploads/${item.categoryImageId.originalURL}`} alt={ElectronicDevices1} style={{ height: 20, width: 20 }} />
                                                        <p>{item.categoryName}</p>
                                                    </div>
                                                </a>
                                            </li>
                                        )
                                    })}
                                    <li>
                                        <a href="#">
                                            <div className="electronic-box">
                                                <img src={ElectronicDevices9} alt={ElectronicDevices9} />
                                                <p> All </p>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </section>
                </div>
                {/* end Mall section */}

                {/* new tranding start */}
                <section className="new_tranding">
                    {/* <div className="container"> */}
                    <div className="title_section">
                        <div>
                            <h2>New & Trending</h2>
                            <p>List of Popular New Products</p>
                        </div>
                        <a href={`${process.env.PUBLIC_URL}/productListing/newAndTranding/products`} className="btn view-all">See all</a>
                    </div>

                    {/* <ProductListing productArray={newAndTrending} loading={this.state.newAndTrendingLoading} /> */}
                    <NewTranding productArray={newAndTrending} loading={this.state.newAndTrendingLoading} onClickProductPopUp={(data) => this.openProductPopUp(data)} />

                    {/* </div> */}
                    <ProductPopUp data={this.state.onClickProductPopUp} modalStatus={this.state.showProductPopUp} hideLocationModal={() => this.hideLocModal()} />
                </section>
                {/* new tranding end */}

                {/* Maymart Qatar app section start here */}
                <section className="mymart-qatar-app for-mobile">
                    {/* <div className="container"> */}
                    <div className="mymart-qatar-app-bg">
                        <div className="mymart-qatar-content">
                            <h2>Download now on</h2>
                            <div className="play-store-img">
                                <a href="#"> <img src={AppStor} alt={AppStor} /> </a>
                                <a href="#"> <img src={PlayStor} alt={PlayStor} /> </a>
                            </div>
                        </div>
                    </div>
                    {/* </div> */}
                </section>
                {/* Maymart Qatar app section end here */}


                {/* Popular section start */}
                <div className="bg-for-mobile-populer">
                    <section className="new_tranding populer-category-sec">

                        <div className="title_section">
                            <div>
                                <h2>Popular Categories </h2>
                                <p className="for-desktop">Most visited categories</p>
                                <p className="for-mobile">Most visited categories</p>
                            </div>
                            <a href={`${process.env.PUBLIC_URL}/productListing/newAndTranding/${Math.random()}`} className="btn view-all">See all</a>
                        </div>

                        <div className="mall-cat-wr">
                            {/* Set up your HTML */}
                            {
                                popularCategoryLoading ?
                                    <Loader />
                                    :
                                    // (this.state.windowWidth < 76 ?
                                    //     <MultiCarousel
                                    //         swipeable={true}
                                    //         draggable={false}
                                    //         showDots={false}
                                    //         responsive={responsive}
                                    //         ssr={true} // means to render carousel on server-side.
                                    //         infinite={false}
                                    //         className="home-populer-category-slider"
                                    //         autoPlaySpeed={1000}
                                    //         keyBoardControl={true}
                                    //         customTransition="all .5"
                                    //         transitionDuration={500}
                                    //         containerClass="carousel-container"
                                    //         itemClass="carousel-item-padding-40-px"
                                    //         removeArrowOnDeviceType={["desktop"]}
                                    //     >
                                    //         {

                                    //             popularCategory.map((data, index) => {
                                    //                 //console.log("popularCategory ------->", popularCategory);
                                    //                 return (
                                    //                     <div className="home-populer-category-slider-inner">
                                    //                         <a href={`${process.env.PUBLIC_URL}/productListing/category/${this.removeHTML(data.categoryName)}`} className="card" key={index}>
                                    //                             <div className="icon">
                                    //                                 {
                                    //                                     data.categoryImageId ?
                                    //                                         <img src={data.categoryImageId.originalURL !== null && data.categoryImageId.originalURL !== undefined ? window.$ImageURL + data.categoryImageId.originalURL : `${window.$ImageURL}no-image-480x480.png`} alt={data.categoryName} />
                                    //                                         :
                                    //                                         <img src={`${window.$ImageURL}no-image-480x480.png`} alt={data.categoryName} />
                                    //                                 }
                                    //                             </div>
                                    //                             <p className="name" dangerouslySetInnerHTML={this.createMarkup(data.categoryName)} />
                                    //                         </a>
                                    //                         {/* <a href={`${process.env.PUBLIC_URL}/productListing/category/${this.removeHTML(data.categoryName)}`} className="card" key={index}>
                                    //                     <div className="icon">
                                    //                         {
                                    //                             data.categoryImageId ?
                                    //                                 <img src={data.categoryImageId.originalURL !== null && data.categoryImageId.originalURL !== undefined ? window.$ImageURL + data.categoryImageId.originalURL : `${window.$ImageURL}no-image-480x480.png`} alt={data.categoryName} />
                                    //                                 :
                                    //                                 <img src={`${window.$ImageURL}no-image-480x480.png`} alt={data.categoryName} />
                                    //                         }
                                    //                     </div>
                                    //                     <p className="name" dangerouslySetInnerHTML={this.createMarkup(data.categoryName)} />
                                    //                 </a> */}
                                    //                     </div>
                                    //                 )
                                    //             })
                                    //         }
                                    //     </MultiCarousel>
                                    //     :
                                    <div className="populer-category-main">
                                        {
                                            popularCategory.slice(0, 16).map((data, index) => {
                                                //console.log("popularCategory ------->", popularCategory);
                                                return (
                                                    <div className="populer-category-inner">
                                                        <a href={`${process.env.PUBLIC_URL}/productListing/category/${this.removeHTML(data.categoryName)}`} className="card" key={index}>
                                                            <div className="icon" >
                                                                {
                                                                    data.categoryImageId ?
                                                                        <img src={data.categoryImageId.originalURL !== null && data.categoryImageId.originalURL !== undefined ? window.$ImageURL + data.categoryImageId.originalURL : `${window.$ImageURL}no-image-480x480.png`} alt={data.categoryName} />
                                                                        :
                                                                        <img src={`${window.$ImageURL}no-image-480x480.png`} alt={data.categoryName} />
                                                                }
                                                            </div>
                                                            <div style={{display: "flex", flexDirection: "row"}}>
                                                                <p className="name" dangerouslySetInnerHTML={this.createMarkup(data.categoryName.slice(0, 15))} />
                                                                {data.categoryName.length > 15 && <p style={{marginTop: 8, color: "black", marginLeft: 4}}>...</p>}
                                                            </div>
                                                        </a>
                                                    </div>

                                                )
                                            })
                                        }
                                    </div>
                            }
                        </div>

                    </section>
                </div>
                {/* Popular section  end */}


                {/* populer brand section start */}
                <section className="populer-brand new_tranding">
                    {/* <div className="container"> */}
                    <div className="title_section">
                        <h2>Popular Brands</h2>
                        <a href={`#`} className="btn view-all">See all</a>
                    </div>
                    <div className="populer-barand-slider">
                        <MultiCarousel
                            removeArrowOnDeviceType={["tablet", "mobile"]}
                            responsive={PopulerBrandLogo}>
                            {PopulerBrands?.map((item) => (
                                <div className="brand-item">
                                    <img src={window.$ImageURL + item?.brandImageId?.originalURL} alt={BrandLogo1} />
                                </div>
                            ))}
                        </MultiCarousel>
                    </div>
                    {/* </div> */}
                </section>
                {/* populer brand section end */}

                {/* populer brand section start */}
                <div className="bg-for-mobile">
                    <section className="new_tranding highlight-product-sec">

                        <div className="title_section">
                            <div>
                                <h2>Highlights</h2>
                                <p>Focus your shopping with these highlights</p>
                            </div>
                            <a href={`#`} className="btn view-all">See all</a>
                        </div>
                        <div className="populer-barand-slider">
                            <MultiCarousel
                                removeArrowOnDeviceType={["tablet", "mobile"]}
                                responsive={HighlightSlider}
                            >
                                {this.state?.thirdBanner?.map((item) => (
                                    <div className="highlight-box" style={{ backgroundImage: `url(${process.env.REACT_APP_LIVE_IMAGE_URL}${item.bannerImageId[0].originalURL})` }}>
                                        <div className="highlight-content" >
                                            <div dangerouslySetInnerHTML={{ __html: item.title }} />
                                            {/* <p>{item.title.replace}</p> */}
                                            {/* <h2>Camera</h2> */}
                                            <div className="shop-btn">
                                                <a href="#">Shop Now <i className="fa fa-angle-right" aria-hidden="true" /></a>
                                            </div>
                                        </div>
                                    </div>
                                ))}
                                {/* <div className="highlight-box" style={{ backgroundImage: 'url("../../src/assets/images/brand-logo5.svg")' }}>
                                    <div className="highlight-content">
                                        <p>Exclusive </p>
                                        <h2>Camera</h2>
                                        <div className="shop-btn">
                                            <a href="#">Shop Now <i className="fa fa-angle-right" aria-hidden="true" /></a>
                                        </div>
                                    </div>
                                </div>
                                <div className="highlight-box" style={{ backgroundImage: 'url("../../src/assets/images/brand-logo5.svg")' }}>
                                    <div className="highlight-content">
                                        <p>Exclusive </p>
                                        <h2>Camera</h2>
                                        <div className="shop-btn">
                                            <a href="#">Shop Now <i className="fa fa-angle-right" aria-hidden="true" /></a>
                                        </div>
                                    </div>
                                </div>
                                <div className="highlight-box" style={{ backgroundImage: 'url("../../src/assets/images/brand-logo5.svg")' }}>
                                    <div className="highlight-content">
                                        <p>Exclusive </p>
                                        <h2>Camera</h2>
                                        <div className="shop-btn">
                                            <a href="#">Shop Now <i className="fa fa-angle-right" aria-hidden="true" /></a>
                                        </div>
                                    </div>
                                </div> */}
                            </MultiCarousel>
                        </div>

                    </section>

                </div>
                {/* populer brand section end */}

                {/* laptop section start */}
                {/* <section className="new_tranding">
                    <div className="container">
                        <div className="title_section">
                            <h2> </h2>
                        </div>
                        <div className="mall-cat-wr exclusiveOffer"> */}
                {/* Set up your HTML */}
                {/* <div className="owl-carousel">
                                <MultiCarousel
                                    swipeable={true}
                                    draggable={true}
                                    showDots={false}
                                    responsive={responsiveBigBanner}
                                    ssr={true} 
                                    infinite={false}
                                    autoPlaySpeed={1000}
                                    keyBoardControl={true}
                                    customTransition="all .5"
                                    transitionDuration={500}
                                    containerClass="carousel-container"
                                    itemClass="carousel-item-padding-80-px"
                                    style={{ height: 400 }}
                                    removeArrowOnDeviceType={["tablet", "mobile"]}
                                >
                                    {
                                        thirdBanner.map((data, index) => {
                                            return (
                                                <div className="offer-item " key={index} style={{ backgroundImage: "url(" + window.$ImageURL + data.bannerImageId[0].originalURL + ")", backgroundPosition: 'center', backgroundRepeat: 'no-repeat', backgroundSize: 'cover' }}>
                                                    <div className="icontent">
                                                        <p className="name" dangerouslySetInnerHTML={this.createMarkup(data.title)} />
                                                        <a href={data.bannerURL}>Shop Now <i className="fa fa-angle-right" aria-hidden="true" /></a>
                                                    </div>
                                                </div>
                                            )
                                        })
                                    }

                                </MultiCarousel>
                            </div> */}
                {/* </div>
                    </div>
                </section> */}
                {/* laptop section  end */}
                {/* trending section end */}
                {/* game zone offer start */}
                {/* <section className="gamezone_wrapper">
                    <div className="container">
                        <div className="row">
                            {
                                fourthBanner.map((data, index) => {
                                    return (
                                        <div className="col-md-6 d-flex" key={index}>
                                            <div className={index % 2 !== 0 ? "gaming-console" : "peper-area"} style={{ backgroundImage: "url(" + window.$ImageURL + data.bannerImageId[0].originalURL + ")", backgroundPosition: 'center', backgroundRepeat: 'no-repeat', backgroundSize: 'cover' }}>
                                                <div className="in-content0">
                                                    <p className="name" style={index % 2 !== 0 ? { textAlign: 'start' } : { textAlign: 'end' }} dangerouslySetInnerHTML={this.createMarkup(data.title)} />
                                                    <a href={data.bannerURL} className={index % 2 !== 0 ? ' btn right' : 'btn left'}  >Shop Now <i className="fa fa-angle-right" aria-hidden="true" /></a>
                                                </div></div>
                                        </div>
                                    )
                                })
                            }
                        </div>
                    </div>
                </section> */}
                {/* categories start */}
                {/* <section className="category-section">
                    <div className="container">
                        <h2 className="header-title">Categories
                            <div className="categories-wrapper">
                                {
                                    selectedCategory.map((catData, catIndex) => {
                                        var imageData;
                                        if (catData.categoryImageId === undefined) {
                                            imageData = `${window.$ImageURL}no-image-480x480.png`
                                        } else {
                                            imageData = `${window.$ImageURL}` + catData.categoryImageId.originalURL
                                        }

                                        return (
                                            <a href={`${process.env.PUBLIC_URL}/productListing/category/${catData.categoryName}`} className="product-category" key={catIndex}>
                                                <div className="inner_category">
                                                    <div className="image-product">
                                                        <img src={imageData} className="img-responsive" alt="categories" />
                                                    </div>
                                                    <p>{catData.categoryName}</p>
                                                </div>
                                            </a>
                                        )
                                    })
                                }

                            </div>
                        </h2></div>
                </section> */}

                {/* categories end */}

                {/* Choose For You start */}
                <section className="new_tranding">
                    {/* <div className="container"> */}
                    <div className="title_section">
                        <div>
                            <h2>Chosen for You</h2>
                            <p>List of products which might interest you.</p>
                        </div>
                        <a href={`${process.env.PUBLIC_URL}/productListing/choosen/products`} className="btn view-all">See All</a>
                    </div>
                    <ProductListing productArray={choosenProduct} loading={this.state.choosenLoading} onClickChosenProductPopUp={(data) => this.openChosenProductPopUp(data)} />
                    {/* </div> */}
                        <ProductChosePopUp modalStatus={this.state.showChosenProductPopUp} hideLocationModal={() => this.hideLocModal()} imageData={this.state.chosenProductImageData} productName={this.state.chosenProductName} chosenData={this.state.chosenData}/>
                </section>
                {/* Choose For You end */}
                {/* {!this.state.isTop && <div className="main-up-arrow" >
                    <a onClick={() =>  window.scrollTo(0, 0)}><i class="fa fa-angle-up" aria-hidden="true"></i></a>
                </div>} */}
            </div>
        )
    }
}