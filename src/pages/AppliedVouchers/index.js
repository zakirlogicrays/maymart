import React, { Component } from 'react';
import { ToastContainer, /*toast*/ } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import axios from 'axios';
import ProfileSidebar from "../../Component/ProfileSidebar";
import jwt_decode from "jwt-decode";

export default class VoucherLisitng extends Component {
    state = {
        couponArray: []
    }
    componentDidMount() {
        this.fetchCoupons();
    }

    fetchCoupons = async () => {
        let checkToken = await localStorage.getItem("token");
        const decoded = jwt_decode(checkToken);
        let customerId = decoded.userid;
        await axios.post(`${window.$URL}couponDiscount/customer-fetchCoupons/`, { customerId })
            .then(response => {
                console.log(response,'fetchCoupons')
                if (response.data.success) {
                    this.setState({
                        couponArray: response.data.data
                    })
                }
            })
            .catch(error => {
                console.log(error);
            })
    }
    render() {
        const { couponArray } = this.state;
        return (
            <section className="dbF-content">
                <div className="container">
                    <div className="row">
                        <ProfileSidebar />
                        <div className="col-lg-9">
                            <ToastContainer />
                            <div className="my-order-wr">
                                <div className="nw-ad-book address-section">
                                    <div className="row">
                                        <h2 className="col-8 order-title">Vouchers</h2>

                                    </div>
                                    <div className="table-responsive p-0">
                                        <table className="table table-bordered table-head-fixed">
                                            <thead>
                                                <tr>
                                                    <th style={{ width: '15%' }}>Modified Date </th>
                                                    <th style={{ width: '15%' }}>Voucher Name</th>
                                                    <th style={{ width: '15%' }}>Voucher Code</th>
                                                    <th>Voucher Image</th>
                                                    <th style={{ width: '20%' }}>Discount Amount </th>
                                                    <th style={{ width: '20%' }}>Voucher Status </th>


                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    couponArray.length > 0 ?
                                                    couponArray.map((data, index) => {  
                                                        let imageURL = '';
                                                        if (data.voucherId !== null) 
                                                        {
                                                            imageURL = window.$ImageURL + data.voucherId.couponBannerImageId[0].originalURL
                                                        } else 
                                                        {
                                                            imageURL = `${window.$ImageURL}no-image-480x480.png`
                                                        }
                                                        return (
                                                            <tr key={index}>
                                                                <td>
                                                                    {new Intl.DateTimeFormat('en-GB', { day: '2-digit', month: '2-digit', year: 'numeric', timeZone: 'Singapore' }).format(data.UpdatedDate)}
                                                                    <br />
                                                                    {new Intl.DateTimeFormat('en-GB', { hour: 'numeric', minute: 'numeric', timeZone: 'Singapore' }).format(data.UpdatedDate)}
                                                                </td>
                                                                <td>{data.voucherId ? data.voucherId.couponName :null}</td>
                                                                <td>{data.voucherId ? data.voucherId.couponCode :null }</td>
                                                                <td>
                                                                    <img src={imageURL} className="img-fluid" alt={data.voucherId ? data.voucherId.couponName :null } />
                                                                </td>

                                                                <td>{data.voucherId ? data.voucherId.couponAmount :null} {data.couponType}</td>
                                                                <td>
                                                                    {
                                                                        data.couponAppliedStatus === false ?
                                                                            "Not Claimed"
                                                                            :
                                                                            "Claimed"
                                                                    }
                                                                </td>


                                                            </tr>
                                                        )
                                                    })
                                                    :null
                                                }
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        )
    }
}