import React, { Component } from "react";
import axios from "axios";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import GoogleLogin from "../../Component/GoogleLogin";
import FacebookLogin from "../../Component/FacebookLogin";
import { connect } from "react-redux";
import { AddToCat, UpdateCart } from "../../data/reducers/cart";

import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import Loader from "react-loader-spinner";

class Register extends Component {
    state = {
        birthDay: "",
        fName: "",
        lName: "",
        birthMonth: "",
        birthYear: "",
        email: "",
        password: "",
        gender: "",
        phoneNumber: "",
        birthDayError: "",
        fNameError: "",
        lNameError: "",
        // birthYearError: '',
        emailError: "",
        passwordError: "",
        confirmPassword: "",

        //genderError: '',
        phoneNumberError: "",
        successMessage: "",
        success: false,
        errorMessage: "",
        error: false,
        emailShow: true,
        mobileShow: false,
        confirmPasswordError: "",
        loading: false,
        showOtpModal: false,
        verifyCodeError: '',
        verifyCode: '',
        signButtionDisabled: false,
        verifyCodeButton: false,
    };

    onChange = (e) => {
        const { name, value } = e.target;
        this.setState({
            [name]: value,
        });

        // if(name === 'gender'){
        //     if(value !== 'Individual'){
        //         this.setState({
        //             birthMonth: true
        //         })
        //     }else{
        //         this.setState({
        //             birthMonth: false,
        //             birthYear: ''
        //         })
        //     }
        // }
    };

    handleChange(evt) {
        const phoneNumber = evt.target.validity.valid
            ? evt.target.value
            : this.state.phoneNumber;
        this.setState({ phoneNumber });
    }

    validate = () => {
        this.setState({
            birthDayError: "",
            fNameError: "",
            lNameError: "",
            emailError: "",
            passwordError: "",
            phoneNumberError: "",
            confirmPasswordError: "",
        });
        const regex = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
        let fNameError = "";
        let lNameError = "";
        let emailError = "";
        let passwordError = "";
        let phoneNumberError = "";
        let confirmPasswordError = "";

        if (this.state.fName.length < 1) {
            fNameError = "Please Enter First Name";
        }
        if (fNameError) {
            this.setState({ fNameError });
            return false;
        }
        if (this.state.lName.length < 1) {
            lNameError = "Please Enter Last Name";
        }
        if (lNameError) {
            this.setState({ lNameError });
            return false;
        }

        if (this.state.email.length < 1 && this.state.emailShow === true) {
            emailError = "Please enter Email";
        } else if (
            !regex.test(this.state.email.trim()) &&
            this.state.emailShow === true
        ) {
            emailError = "Please enter a valid Email";
        }
        if (emailError) {
            this.setState({ emailError });
            return false;
        }

        if (this.state.password.length < 1) {
            passwordError = "Please enter Password";
        } else if (this.state.password.length < 6) {
            passwordError = "Password must be 6 characters long";
        }
        if (passwordError) {
            this.setState({ passwordError });
            return false;
        }

        if (this.state.confirmPassword.length < 1) {
            confirmPasswordError = "Please enter Confirm Password";
        } else if (this.state.confirmPassword !== this.state.password) {
            confirmPasswordError = "Password didn't match";
        }
        if (confirmPasswordError) {
            this.setState({ confirmPasswordError });
            return false;
        }

        if (this.state.phoneNumber.length < 1 && this.state.mobileShow === true) {
            phoneNumberError = "Please enter Phone Number";
        }
        if (phoneNumberError) {
            this.setState({ phoneNumberError });
            return false;
        }

        this.setState({
            fNameError,
            lNameError,
            emailError,
            passwordError,
            confirmPasswordError,
            phoneNumberError,
        });
        return true;
    };
    validateOtp = () => {
        this.setState({
            verifyCodeError: '',
        });

        let verifyCodeError = '';
        if (this.state.verifyCode.length < 1) {
            verifyCodeError = "Please enter OTP";
        } else if (this.state.verifyCode.length < 4) {
            verifyCodeError = "Invalid Code";

        }

        if (verifyCodeError) {
            this.setState({ verifyCodeError })
            return false
        }

        this.setState({
            verifyCodeError
        })

        return true
    }

    onSubmitVerifyCode = async (e) => {

        e.preventDefault();
        this.setState({
            success: false,
            error: false,
            successMessage: '',
            errorMessage: ''
        })
        let checkValidation = this.validateOtp();
        if (checkValidation) {
            //alert(this.state.confirmPassword);return false;
            axios.put(`${window.$URL}user/customer-update-otp-verifyCode`, {
                Phone: this.state.phoneNumber,
                verifyCode: this.state.verifyCode,
            })
                .then(response => {

                    if (response.data.message === 'OTP Verified') {
                        //  console.log('hbea na kno' +response.data.message);
                        toast.success(response.data.message, {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });
                        this.setState({
                            showOtpModal: false,
                            success: true,
                            //successMessage: response.data.message,
                            signButtionDisabled: false,
                            verifyCodeButton: true
                        });
                        //this.props.history.push(`${process.env.PUBLIC_URL}/login/`);

                    } else if (response.data.message === 'Invaild Code') {

                        toast.error(response.data.message, {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });
                        this.setState({
                            // errorMessage: response.data.message,
                            error: true,

                        });

                    } else {
                        this.setState({

                            error: true

                        });

                    }
                })
                .catch(error => {
                    console.log(error.data);
                });
        }
    };


    hideComingModal = () => {
        this.setState(
            {
                showOtpModal: false,
            },
            () => {
                //this.componentDidMount();
            }
        );
    };

    onSubmit = async (e) => {
        e.preventDefault();
        this.setState({
            success: false,
            error: false,
            successMessage: "",
            errorMessage: "",
        });
        let checkValidation = this.validate();
        if (checkValidation) {
            this.setState({ loading: true });
            const { fName, lName, email, password, phoneNumber } = this.state;
            let registerData = {
                fname: fName,
                lname: lName,
                email: email,
                password: password,
                phone: phoneNumber,
                UserType: "customer",
            };

            //alert(registerData);return false;
            await axios
                .post(`${window.$URL}user/customersignup`, registerData)
                .then((response) => {
                    if (response.status === 200) {
                        if (response.data.success && response.data.registerBy === 'email') { //register by Email redirection
                            //console.log(btoa(response.data.emailVerifyCode),'endode')
                            let encodedVerifyCode = btoa(response.data.emailVerifyCode);
                            this.setState(
                                {
                                    success: true,
                                    successMessage: response.data.message,
                                    loading: false,
                                },
                                async () => {
                                    //localStorage.setItem("token", response.data.token);

                                    let lastVisit = await localStorage.getItem("lastVisited");
                                    let lastVisitParse = JSON.parse(lastVisit);
                                    if (lastVisitParse) {
                                        this.addToCart(lastVisitParse);
                                    } else {
                                        this.props.UpdateCart();
                                        setTimeout(() => {
                                            window.location.href = `${process.env.PUBLIC_URL}/verify-email/${encodedVerifyCode}`;
                                        }, 1000);
                                    }
                                }
                            );
                        } else if (response.data.success && response.data.registerBy === 'phone') { //register by Phone Redirection
                            this.setState(
                                {
                                    success: true,
                                    successMessage: response.data.message,
                                    loading: false,
                                },
                                async () => {
                                    localStorage.setItem("token", response.data.token);

                                    let lastVisit = await localStorage.getItem("lastVisited");
                                    let lastVisitParse = JSON.parse(lastVisit);
                                    if (lastVisitParse) {
                                        this.addToCart(lastVisitParse);
                                    } else {
                                        this.props.UpdateCart();
                                        setTimeout(() => {
                                            window.location.href = `${process.env.PUBLIC_URL}/`;
                                        }, 1000);
                                    }
                                }
                            );

                        }
                        else {
                            toast.error(response.data.message, {
                                position: "top-right",
                                autoClose: 5000,
                                hideProgressBar: false,
                                closeOnClick: true,
                                pauseOnHover: true,
                                draggable: true,
                                progress: undefined,
                            });
                            this.setState({
                                error: true,
                                loading: false,
                                //errorMessage: response.data.message,
                            });
                        }
                    }
                })
                .catch((error) => {
                    console.log(error);
                });
        }
    };

    addToCart = async (lastVisitParse) => {
        this.props.AddToCat(lastVisitParse);
    };

    async buttonShow(value) {
        //alert(value);
        if (value === "email") {
            this.setState({
                emailShow: true,
                mobileShow: false,
                phoneNumberError: '',
                phoneNumber: '',
                signButtionDisabled: false

            });
        } else if (value === "mobile") {
            this.setState({
                mobileShow: true,
                emailShow: false,
                signButtionDisabled: true
            });
        }
    }

    async getSmsCode(phoneNumber) {
        //alert(phonenumber)
        // let phoneNumber ={
        //     phoneNumber:phonenumber
        // }
        console.log(phoneNumber, "array");
        if (!this.state.phoneNumber.length < 1) {
            await axios
                .post(`${window.$URL}user/get-sms-code`, { phoneNumber })
                .then((response) => {
                    console.log(response, "sms code");
                    if (response.status === 200) {
                        if (response.data.success) {
                            toast.success(response.data.message, {
                                position: "top-right",
                                autoClose: 5000,
                                hideProgressBar: false,
                                closeOnClick: true,
                                pauseOnHover: true,
                                draggable: true,
                                progress: undefined,
                            });
                            this.setState({
                                success: true,
                                //successMessage: response.data.message,
                                loading: false,
                                showOtpModal: true
                            }, async () => {
                                //localStorage.setItem("token", response.data.token);
                                //console.log(response)

                            })
                        } else {
                            toast.error(response.data.message, {
                                position: "top-right",
                                autoClose: 5000,
                                hideProgressBar: false,
                                closeOnClick: true,
                                pauseOnHover: true,
                                draggable: true,
                                progress: undefined,
                            });
                            this.setState({
                                error: true,
                                //errorMessage: response.data.message
                            })
                        }
                    }
                })
                .catch((error) => {
                    console.log(error);
                });
        } else {
            //alert('please enter mobile number');
            let phoneNumberError = "Please enter Phone Number";
            this.setState({ phoneNumberError });
        }
    }
    async resendOTP(phoneNumber) {

        //console.log(phoneNumber, "array");
        await axios
            .post(`${window.$URL}user/resend-sms-code`, { phoneNumber })
            .then((response) => {
                console.log(response, "resend code");
                if (response.status === 200) {
                    if (response.data.success) {
                        toast.success(response.data.message, {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });
                        this.setState({
                            success: true,
                            //successMessage: response.data.message,
                        }, async () => {
                            //localStorage.setItem("token", response.data.token);
                            //console.log(response)

                        })
                    } else {
                        this.setState({
                            error: true,
                            errorMessage: response.data.message
                        })
                    }
                }
            })
            .catch((error) => {
                console.log(error);
            });

    }


    render() {
        const {
            birthMonthError,
            birthDayError,
            fNameError,
            lNameError,
            emailError,
            confirmPasswordError,
            passwordError,
            phoneNumberError,
            success,
            error,
            successMessage,
            errorMessage,
      /*emailShow,*/ mobileShow,
            loading,
            showOtpModal,
            verifyCodeError,
            signButtionDisabled,
            verifyCodeButton
        } = this.state;

        return (
            <section className="my-mart-signup-wr">
                <ToastContainer />
                <div className="container">
                    <div className="sinup-container">
                        <h1>Create your myMart Account</h1>

                        <div className="form-sign-wrapper">
                            <form onSubmit={this.onSubmit}>
                                <div className="row">
                                    <div className="col-lg-12 col-md-12 col-sm-12 form-parts p-4">
                                        {success ? (
                                            <p style={{ color: "green", fontSize: 18 }}>
                                                {" "}
                                                {successMessage}{" "}
                                            </p>
                                        ) : null}
                                        {error ? (
                                            <p style={{ color: "red", fontSize: 18 }}>
                                                {" "}
                                                {errorMessage}{" "}
                                            </p>
                                        ) : null}
                                    </div>
                                    <div className="col-lg-6 col-md-6 col-sm-12 form-parts">
                                        {loading ? (
                                            <Loader
                                                type="TailSpin"
                                                color="#00BFFF"
                                                height={100}
                                                width={100}
                                                style={{
                                                    left: "45%",
                                                    position: "absolute",
                                                    top: "45%",
                                                }}
                                            />
                                        ) : null}
                                        <div className="form-group">
                                            <label
                                                className="input-heading"
                                                htmlFor="exampleInputEmail1"
                                            >
                                                First Name
                      </label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                aria-describedby="emailHelp"
                                                name="fName"
                                                onChange={this.onChange}
                                                placeholder="First Name"
                                            />
                                            {fNameError ? (
                                                <span style={{ color: "red" }}> {fNameError} </span>
                                            ) : null}
                                        </div>
                                        <div className="form-group">
                                            <label
                                                className="input-heading"
                                                htmlFor="exampleInputEmail1"
                                            >
                                                Last Name
                      </label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                aria-describedby="emailHelp"
                                                name="lName"
                                                onChange={this.onChange}
                                                placeholder="Last Name"
                                            />
                                            {lNameError ? (
                                                <span style={{ color: "red" }}> {lNameError} </span>
                                            ) : null}
                                        </div>
                                        {mobileShow === true ? (
                                            <>
                                                {/* <div className="form-group">
                                                    <label
                                                        className="input-heading"
                                                        htmlFor="exampleInputEmail1"
                                                    >
                                                        Phone Number
                                                    </label>
                                                    
                                                    <input
                                                        type="text"
                                                        name="phoneNumber"
                                                        className="form-control"
                                                        aria-describedby="phonenumber"
                                                        placeholder="Enter your phone number"
                                                        pattern="[0-9]*"
                                                        onChange={this.handleChange.bind(this)}
                                                        value={this.state.phoneNumber}
                                                        disabled={this.state.verifyCodeButton}
                                                        
                                                    />
                                                    {phoneNumberError ? (
                                                        <span style={{ color: "red" }}>
                                                            {" "}
                                                            {phoneNumberError}{" "}
                                                        </span>
                                                    ) : null}
                                                </div> */}
                                                <div className="form-group">
                                                    <label
                                                        className="input-heading"
                                                        htmlFor="exampleInputEmail1"
                                                    >
                                                        Phone Number
                                                    </label>
                                                    <div className="input-group">
                                                        <div className="input-group-append">
                                                            <div style={{ height: 50, borderRadius: 10, background: '#F5F5F5', marginTop: 10 }} className="input-group-text"><span>+974</span></div>
                                                        </div>
                                                        <input type="text"
                                                            name="phoneNumber"
                                                            className="form-control"
                                                            aria-describedby="phonenumber"
                                                            placeholder="Enter your phone number"
                                                            pattern="[0-9]*"
                                                            onChange={this.handleChange.bind(this)}
                                                            value={this.state.phoneNumber}
                                                            disabled={this.state.verifyCodeButton} style={{ height: 50, borderRadius: 10, background: '#F5F5F5', marginTop: 10 }} />


                                                    </div>
                                                    {phoneNumberError ? (
                                                        <span style={{ color: "red" }}>
                                                            {" "}
                                                            {phoneNumberError}{" "}
                                                        </span>
                                                    ) : null}
                                                </div>
                                                <div className="form-group">

                                                    <button
                                                        onClick={() =>
                                                            this.getSmsCode(this.state.phoneNumber)
                                                        }
                                                        type="button" disabled={this.state.verifyCodeButton}
                                                        className="btn getCode"
                                                    >
                                                        {this.state.verifyCodeButton ? 'Verified' : 'Get OTP'}
                                                    </button>



                                                </div>
                                            </>
                                        ) : (
                                            
                                            <div className="form-group">
                                                <label className="input-heading">Email</label>
                                                <input
                                                    type="email"
                                                    name="email"
                                                    className="form-control"
                                                    placeholder="Enter your Email Id"
                                                    onChange={this.onChange}
                                                />
                                                {emailError ? (
                                                    <span style={{ color: "red" }}> {emailError} </span>
                                                ) : null}
                                            </div>
                                        )}

                                        {/*  modal upcoming */}
                                        {showOtpModal ? (
                                            <div
                                                id="exampleModalCenter"
                                                className="modal fade show"
                                                tabIndex={-1}
                                                role="dialog"
                                                aria-labelledby="exampleModalCenterTitle"
                                                style={{ display: "block", paddingRight: 17 }}
                                            >
                                                <div
                                                    className="modal-dialog modal-dialog-centered"
                                                    role="document"
                                                >
                                                    <div className="modal-content">
                                                        <div className="modal-header">
                                                            {/* <h5 className="modal-title" id="exampleModalCenterTitle">Page Status</h5> */}
                                                            <button
                                                                type="button"
                                                                onClick={() => this.hideComingModal()}
                                                                className="close"
                                                                data-dismiss="modal"
                                                                aria-label="Close"
                                                            >
                                                                <span aria-hidden="true">×</span>
                                                            </button>
                                                        </div>
                                                        <div className="modal-body">
                                                            {/* <p>Promotions is Comming...</p> */}

                                                            <h1>Enter your OTP </h1>
                                                            <div className="form-sign-wrapper">
                                                                <form onSubmit={this.onSubmitVerifyCode}>
                                                                    <div className="header-bottom">
                                                                        <p className="text-center"><i className="fa fa-warning icon-style" /> Please check your MessageBox for the OTP.</p>
                                                                    </div>
                                                                    <div className="row">
                                                                        {/* <div className="col-lg-12 col-md-12 col-sm-12 form-parts p-2">
                                                                            {
                                                                                success ?
                                                                                    <p style={{ color: 'green', fontSize: 18 }}> {successMessage} </p>
                                                                                    :
                                                                                    null
                                                                            }
                                                                            {
                                                                                error ?
                                                                                    <p style={{ color: 'red', fontSize: 18 }}> {errorMessage} </p>
                                                                                    :
                                                                                    null
                                                                            }
                                                                        </div> */}
                                                                        <div className="col-lg-3 col-md-6 col-sm-12 form-parts">
                                                                        </div>
                                                                        <div className="col-lg-6 col-md-6 col-sm-12 form-parts">
                                                                            <div className="form-group divInner">
                                                                                <label style={{ textAlign: 'center', width: '100%' }} className="input-heading">Enter OTP</label>
                                                                                <div id="divOuter">
                                                                                    <div id="divInner">
                                                                                        <input id="partitioned" name="verifyCode" type="text" maxlength="4" onChange={this.onChange} />
                                                                                    </div>
                                                                                </div>
                                                                                {verifyCodeError ? <span className="error-class"> {verifyCodeError} </span> : null}
                                                                            </div>
                                                                            <div className="form-group">
                                                                                <button type="submit" className="btn signup_btn" >Submit</button>
                                                                            </div>
                                                                            <div className="form-group">
                                                                                <p className="already_signUp">Have not recived OTP? <button onClick={() => this.resendOTP(this.state.phoneNumber)}>Resend </button></p>
                                                                            </div>
                                                                        </div>
                                                                        <div className="col-lg-3 col-md-6 col-sm-12 form-parts">
                                                                        </div>

                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>
                                                        {/* <div className="modal-footer">
        <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" className="btn btn-primary">Save changes</button>
      </div> */}
                                                    </div>
                                                </div>
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                        {/* modal upcoming */}

                                        <div className="form-group">
                                            <label className="input-heading">Password</label>
                                            <input
                                                type="password"
                                                name="password"
                                                className="form-control"
                                                placeholder="Min. 6 characters"
                                                onChange={this.onChange}
                                            />
                                            {passwordError ? (
                                                <span style={{ color: "red" }}> {passwordError} </span>
                                            ) : null}
                                        </div>
                                        <div className="form-group">
                                            <label className="input-heading">Confirm Password</label>
                                            <input
                                                type="password"
                                                name="confirmPassword"
                                                className="form-control"
                                                placeholder="Min. 6 characters"
                                                onChange={this.onChange}
                                            />
                                            {confirmPasswordError ? (
                                                <span style={{ color: "red" }}>
                                                    {" "}
                                                    {confirmPasswordError}{" "}
                                                </span>
                                            ) : null}
                                        </div>
                                        <div className="form-group">
                                            <label className="c-s-checkbox ">
                                                <span className="input-heading">
                                                    I'd like to receive exclusive offers and promotions
                                                    via SMS
                        </span>
                                                <input type="checkbox" />
                                                <span className="checkmark" />
                                            </label>
                                        </div>

                                        <div className="form-group">

                                                <button type="submit" disabled={signButtionDisabled} className="btn signup_btn">
                                                    Sign Up
                                            </button>

                                            <p className="agreement">
                                                By clicking "SIGN UP"; I agree to myMart's Terms of Use
                                                and Privacy Policy
                      </p>
                                        </div>
                                    </div>

                                    <div className="col-lg-6 col-md-6 col-sm-12">
                                        <br />
                                        <br />
                                        <div className="form-group">
                                            <div className="or-sign-section">
                                                <hr />
                                                <span>Or, sign up with </span>
                                            </div>
                                        </div>
                                        <div className="form-group other-login-method">
                                            {mobileShow === true ? (
                                                <button
                                                    type="button"
                                                    onClick={() => this.buttonShow("email")}
                                                    className="btn signup_with_gmail"
                                                >
                                                    Sign Up With Email
                                                </button>
                                            ) : (
                                                <button
                                                    type="button"
                                                    onClick={() => this.buttonShow("mobile")}
                                                    className="btn signup_with_gmail"
                                                >
                                                    Sign Up With Mobile
                                                </button>
                                            )}
                                            {/* <button className="btn signup_with_facebook">Sign in with Facebook</button> */}

                                            <div className="row">
                                                <div className="col-md-6">
                                                    <FacebookLogin />
                                                </div>
                                                <div className="col-md-6 ">
                                                    <div className="google">
                                                        <GoogleLogin />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <p className="already_signUp">
                                                Already member?{" "}
                                                <a href={`${process.env.PUBLIC_URL}/login`}>Sign In</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

const mapStateToProps = (state) => ({
    CartProducts: state.Cart,
});

export default connect(mapStateToProps, { AddToCat, UpdateCart })(Register);
