import React, { Component } from 'react';
import axios from 'axios';
import {
    Accordion,
    AccordionItem,
    AccordionItemHeading,
    AccordionItemButton,
    AccordionItemPanel,
} from 'react-accessible-accordion';
import 'react-accessible-accordion/dist/fancy-example.css';

class SellerFaq extends Component {

    constructor(props) {
        super(props)
        this.state = {
            sellerFaqsList: ""
        }
    }

    async componentDidMount() {
        this.ftechSellerFaq();
    }

    ftechSellerFaq = async () => {

        await axios.get(`${window.$URL}faqs/ftechSellerFaq`)
            .then(response => {
                console.log(response.data.data)
                if (response.status === 200) {
                    this.setState({
                        sellerFaqsList: response.data.data
                    })
                }
            })
            .catch(error => {
                console.error(error.data)
            })
    }


    render() {
        return (
            <section className="my-mart-signup-wr">
                <div className="container">
                    <div className="sinup-container">
                        <h1>Seller FAQs</h1>
                        <div className="form-sign-wrapper">
                            <Accordion>
                            
                                {
                                    this.state.sellerFaqsList ?
                                    this.state.sellerFaqsList.map((data, index) => {
                                        return (
                                    
                                            <AccordionItem key={index}>
                                                <AccordionItemHeading>
                                                    <AccordionItemButton>
                                                        {data.faqsQuestion}
                                                </AccordionItemButton>
                                                </AccordionItemHeading>
                                                <AccordionItemPanel>
                                                    <p>{data.faqsAnswer}</p>
                                                </AccordionItemPanel>
                                            </AccordionItem>
                                    
                                        )
                                    }):null
                                
                                }
                            </Accordion>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default SellerFaq;