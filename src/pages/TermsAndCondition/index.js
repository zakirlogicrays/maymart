import React, { Component } from 'react';
import axios from 'axios';  

export default class TermsAndCondition extends Component {
    constructor(props) {
        super(props)
        this.state = {
            termsConditionDetails: ""
        }
    }

    async componentDidMount() {
        this.fetchTermsCondition();
    }

    fetchTermsCondition = async () => {

        await axios.get(`${window.$URL}content/termsInfo`)
            .then(response => {
                console.log(response.data.data)
                if (response.status === 200) {
                    this.setState({
                        termsConditionDetails: response.data.data.ContentDetails
                    })
                }
            })
            .catch(error => {
                console.error(error.data)
            })
    }
    createMarkup = (html) => {
        return {
            __html: html
        };
    };
    render() {
        const { termsConditionDetails } = this.state;
        return (
            <section className="my-mart-signup-wr">
                <div className="container">
                    <div className="sinup-container">
                        <h1>Terms & Conditions</h1>
                        <div className="form-sign-wrapper">
                        <div  dangerouslySetInnerHTML={this.createMarkup(termsConditionDetails)} />
                        
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}
