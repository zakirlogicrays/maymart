import React, { Component } from 'react';
import axios from 'axios';
import {
    Accordion,
    AccordionItem,
    AccordionItemHeading,
    AccordionItemButton,
    AccordionItemPanel,
} from 'react-accessible-accordion';
import 'react-accessible-accordion/dist/fancy-example.css';

class CustomerFaq extends Component {

    constructor(props) {
        super(props)
        this.state = {
            customerFaqsList: ""
        }
    }

    async componentDidMount() {
        this.ftechCustomerFaq();
    }

    ftechCustomerFaq = async () => {

        await axios.get(`${window.$URL}faqs/ftechCustomerFaq`)
            .then(response => {
                console.log(response.data.data)
                if (response.status === 200) {
                    this.setState({
                        customerFaqsList: response.data.data
                    })
                }
            })
            .catch(error => {
                console.error(error.data)
            })
    }

    render() {
        //const { customerFaqsList } = this.state;
        return (
            <section className="my-mart-signup-wr">
                <div className="container">
                    <div className="sinup-container">
                        <h1>Customer FAQs</h1>
                        <div className="form-sign-wrapper">
                        <Accordion >
                            {
                                this.state.customerFaqsList ?
                                this.state.customerFaqsList.map((data, index) => {
                                    return (
                                   
                                        <AccordionItem key={index}>
                                            <AccordionItemHeading>
                                                <AccordionItemButton>
                                                    {data.faqsQuestion}
                                            </AccordionItemButton>
                                            </AccordionItemHeading>
                                            <AccordionItemPanel>
                                                <p>{data.faqsAnswer}</p>
                                            </AccordionItemPanel>
                                        </AccordionItem>
                                   
                                    )
                                }):null
                               
                            }
                        </Accordion>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default CustomerFaq;