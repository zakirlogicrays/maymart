import React, { Component } from "react";
import axios from "axios";
import Loader from "../../Component/Loader";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import ForYou from "../../assets/images/ca-tabs-1.svg";
import {
  Accordion,
  AccordionItem,
  AccordionItemButton,
  AccordionItemHeading,
  AccordionItemPanel,
} from "react-accessible-accordion";

import TabsIcon1 from "../../assets/images/ca-tabs-1.svg";

export default class Category extends Component {
  state = {
    selectedCategory: [],
    firstBanner: [],
    loading: true,
    fullArray: [],
    subCategory: [],
    subCategoryItems: [],
    forYousubCategoryItems: [],
    arrownotShowen: [],
    checkArray: [],
    forYouCategory: [],
    forYouSubCategories: [],
    forYouArrownotShowen: [],
    forYouCheckArray: [],
    popularCategory: []
  };

  async componentDidMount() {
    await axios
      .get(`${window.$URL}banner/fetchBanner/homepage`)
      .then((response) => {
        if (response.status === 200) {
          let bannerArray = response.data.data;
          let firstBanner = [];

          for (let i = 0; i <= bannerArray.length - 1; i++) {
            if (
              bannerArray[i].type === "1st Banner" &&
              bannerArray[i].bannerShowesTo === "Categories"
            ) {
              firstBanner.push({
                url:
                  `${window.$ImageURL}` +
                  bannerArray[i].bannerImageId[0].originalURL,
                bannerURL: bannerArray[i].bannerURL,
              });
            }
          }
          this.setState({
            firstBanner,
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });

    await this.getAllData();
    await this.getForYouCategorys();
    await this.getPopularCategories();
  }

  getPopularCategories = async () => {

    await axios.post(`${window.$URL}popular-category/fetch-popular-catetgory-by-pagename`, { pageName: 'Homepage' })
        .then(async response => {
            if (response.data.success) {
                await this.setState({
                    popularCategory: response.data.data,
                })
        }})
        .catch(error => {
            console.log(error)
        })
}

  getForYouCategorys = async () => {
    await axios
      .get(
        `https://mymart.qa:7775/api/product/fetchKeywordByUser?userId=5f173552743dec5203b612c5&limit=10&skip=0`
      )
      .then((response) => {
        if(response.data.success) {
          this.setState({
            forYouCategory: response.data.data
          }, async() => {
            let FinalArray = await this.state.fullArray?.filter((item) => (
              this.state.forYouCategory?.some((item1) => item1.keyword === item.categoryName)
            ))
            this.setState({forYouSubCategories: FinalArray})
          })
        }
      })
      .catch((error) => {
        console.log(error)
      })
      let array = this.state.fullArray?.map((item) =>
        this.state.forYouSubCategories?.filter((item1) =>
          item1.categoryName === item.parent ? item.parent : null
        )
        );
        let refectoredArray = array.map(
          (item) => item.length > 0 && item[0].categoryName
          );
        let finalFilter = refectoredArray.filter((item) => item !== false && item);
        this.setState({ forYouArrownotShowen: finalFilter });
        let lastArray = this.state.forYouSubCategories?.map((item) => item.categoryName);
        this.setState({ forYouCheckArray: this.prepareBooleans(lastArray, finalFilter) });
      }

  getAllData = async () => {
    let catType = this.props.match.params.categoryType;
    await axios
      .get(`${window.$URL}category/categoryinfo`)
      .then((response) => {
        if (response.data.success) {
          console.log(response.data);
          let sortArray = response.data?.response?.filter((item) =>
            (item.parent === "Grocery" || item.parent === "electronic Devices") && item.categoryLevel[0] !== ""
              ? item
              : null
          );
          this.setState({
            fullArray: response.data?.response,
            selectedCategory: sortArray,
            loading: false,
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  prepareBooleans = (master, keys) => {
    const booleans = master.map((el) => {
      return keys.includes(el);
    });
    return booleans;
  };

  getSelectedCategory = async (categoryName) => {
    let subCategory = await this.state.fullArray?.filter((item) =>
      item.parent === categoryName ? item : null
    );
    await this.setState({
      subCategory: subCategory,
    });
    let array = this.state.fullArray?.map((item) =>
      this.state.subCategory?.filter((item1) =>
        item1.categoryName === item.parent ? item.parent : null
      )
    );
    let refectoredArray = array.map(
      (item) => item.length > 0 && item[0].categoryName
    );
    let finalFilter = refectoredArray.filter((item) => item !== false && item);
    this.setState({ arrownotShowen: finalFilter });
    let lastArray = this.state.subCategory?.map((item) => item.categoryName);
    this.setState({ checkArray: this.prepareBooleans(lastArray, finalFilter) });
  };

  getSelectedSubCategory = async (categoryName) => {
    let selectedSubCategory1 = await this.state.fullArray?.filter((item) =>
      item.parent === categoryName ? item : null
    );
    this.setState({
      subCategoryItems: selectedSubCategory1,
    });
  };

  getForYouSelectedSubCategory = async (categoryName) => {
    let selectedSubCategory1 = await this.state.fullArray?.filter((item) =>
      item.parent === categoryName ? item : null
    );
    this.setState({
      forYousubCategoryItems: selectedSubCategory1,
    });
  };

  createMarkup = (html) => {
    return {
      __html: html,
    };
  };

  removeHTML = (html) => {
    const regex = /(<([^>]+)>)/gi;
    const result = html.replace(regex, "");
    let returnName = "";
    if (result === "Groceries") {
      returnName = "Grocery";
    } else {
      returnName = result;
    }
    return returnName;
  };

  render() {
    const {
      firstBanner,
      loading,
      selectedCategory,
      fullArray,
    } = this.state;
    console.log("popularCategory -------->", this.state.popularCategory);
    return (
      <div className="category-page-main">
        {/* Tabs section start here */}

        <section className="category-tabs-section">
          {/* <div className="container"> */}
          <Tabs>
            <div className="row">
              <div className="col-md-2 col-sm-2 pr-0 left-tabs">
                <TabList defaultIndex="1">
                  <div className="category-title">
                    {" "}
                    <h2>Categories</h2>{" "}
                  </div>
                  <Tab>
                    {" "}
                    <img src={ForYou} alt={TabsIcon1} />
                    <span>For You</span>
                  </Tab>
                  {selectedCategory?.map((item) => (
                    <Tab
                      onClick={() =>
                        this.getSelectedCategory(item?.categoryName)
                      }
                      //id="react-tabs-0"
                    >
                      {" "}
                      <img
                        src={
                          `${process.env.REACT_APP_LIVE_IMAGE_URL}` +
                          item?.categoryImageId?.originalURL
                        }
                        alt={TabsIcon1}
                      />{" "}
                      <span>{item?.categoryName}</span>
                    </Tab>
                  ))}
                </TabList>
              </div>
              <div className="col-md-10 col-sm-10 pl-0 right-tabs">
                <div className="all-tabs">
                  <TabPanel>
                    <h2>{"For You"}</h2>
                    <Accordion allowZeroExpanded>
                    {this.state.forYouSubCategories && 
                      this.state.forYouSubCategories?.map((item, idx) => (
                        <div>
                            {this.state.forYouCheckArray.length > 0 && (
                              <div>
                                {this.state.forYouCheckArray[idx] !== false ? (
                                <AccordionItem>
                                    <AccordionItemHeading 
                                      onClick={() =>
                                        {
                                          this.getForYouSelectedSubCategory(
                                          item.categoryName
                                        )
                                      }
                                      }
                                    >
                                      <AccordionItemButton>
                                      {item?.categoryImageId ? (
                                                  <img
                                                    src={
                                                      `${process.env.REACT_APP_LIVE_IMAGE_URL}` +
                                                      item?.categoryImageId?.originalURL
                                                    }
                                                    alt={TabsIcon1}
                                                    style={{ height: 25, width: 25 }}
                                                  />
                                                ) : (
                                                  <img
                                                    src="https://upload.wikimedia.org/wikipedia/commons/1/14/No_Image_Available.jpg"
                                                    alt={TabsIcon1}
                                                    style={{ height: 25, width: 25 }}
                                                  />
                                                )}{" "}
                                                {item.categoryName}
                                      </AccordionItemButton>
                                    </AccordionItemHeading>
                                  <AccordionItemPanel>
                                  <div className="tabs-inner-item">
                                      <ul>
                                        {this.state?.forYousubCategoryItems?.map(
                                          (item, idx) => {
                                            return (
                                              <li>
                                                {" "}
                                                {item?.categoryImageId ? (
                                                  <img
                                                    src={
                                                      `${process.env.REACT_APP_LIVE_IMAGE_URL}` +
                                                      item?.categoryImageId
                                                        ?.originalURL
                                                    }
                                                    alt={TabsIcon1}
                                                  />
                                                ) : (
                                                  <img
                                                    src="https://upload.wikimedia.org/wikipedia/commons/1/14/No_Image_Available.jpg"
                                                    alt={TabsIcon1}
                                                  />
                                                )}{" "}
                                                <span>
                                                  {item.categoryName}
                                                </span>{" "}
                                              </li>
                                            );
                                          }
                                        )}
                                      </ul>
                                    </div>
                                  </AccordionItemPanel>
                                </AccordionItem>
                                ) : (
                                  <div>
                                    <a className="no-subcategory-accordion">
                                      {item?.categoryImageId ? (
                                        <img
                                          src={
                                            `${process.env.REACT_APP_LIVE_IMAGE_URL}` +
                                            item?.categoryImageId?.originalURL
                                          }
                                          alt={TabsIcon1}
                                          style={{ height: 25, width: 25 }}
                                        />
                                      ) : (
                                        <img
                                          src="https://upload.wikimedia.org/wikipedia/commons/1/14/No_Image_Available.jpg"
                                          alt={TabsIcon1}
                                          style={{ height: 25, width: 25 }}
                                        />
                                      )}{" "}
                                      {item.categoryName}
                                    </a>
                                  </div>
                                )}
                              </div>
                            )}
                          </div>
                      ))}
                    </Accordion>
                  </TabPanel>
                  {selectedCategory?.map((item) => (
                    <TabPanel>
                      <h2>{item.categoryName}</h2>
                      <Accordion allowZeroExpanded>
                        {this.state.subCategory?.map((item, idx) => (
                          <div>
                            {this.state.checkArray.length > 0 && (
                              <div>
                                {this.state.checkArray[idx] !== false ? (
                                  <AccordionItem>
                                    <AccordionItemHeading
                                      expanded={false}
                                      onClick={() =>
                                        {
                                          this.getSelectedSubCategory(
                                          item.categoryName
                                        )
                                      }
                                      }
                                    >
                                      <AccordionItemButton
                                        //content={false}
                                      >
                                        {item?.categoryImageId ? (
                                          <img
                                            src={
                                              `${process.env.REACT_APP_LIVE_IMAGE_URL}` +
                                              item?.categoryImageId?.originalURL
                                            }
                                            alt={TabsIcon1}
                                            style={{ height: 25, width: 25 }}
                                          />
                                        ) : (
                                          <img
                                            src="https://upload.wikimedia.org/wikipedia/commons/1/14/No_Image_Available.jpg"
                                            alt={TabsIcon1}
                                            style={{ height: 25, width: 25 }}
                                          />
                                        )}{" "}
                                        {item.categoryName}
                                      </AccordionItemButton>
                                    </AccordionItemHeading>
                                    <AccordionItemPanel>
                                      <div className="tabs-inner-item">
                                        <ul>
                                          {this.state?.subCategoryItems?.map(
                                            (item, idx) => {
                                              return (
                                                <li>
                                                  {" "}
                                                  {item?.categoryImageId ? (
                                                    <img
                                                      src={
                                                        `${process.env.REACT_APP_LIVE_IMAGE_URL}` +
                                                        item?.categoryImageId
                                                          ?.originalURL
                                                      }
                                                      alt={TabsIcon1}
                                                    />
                                                  ) : (
                                                    <img
                                                      src="https://upload.wikimedia.org/wikipedia/commons/1/14/No_Image_Available.jpg"
                                                      alt={TabsIcon1}
                                                    />
                                                  )}{" "}
                                                  <span>
                                                    {item.categoryName}
                                                  </span>{" "}
                                                </li>
                                              );
                                            }
                                          )}
                                        </ul>
                                      </div>
                                    </AccordionItemPanel>
                                  </AccordionItem>
                                ) : (
                                  <div>
                                    <a className="no-subcategory-accordion">
                                      {item?.categoryImageId ? (
                                        <img
                                          src={
                                            `${process.env.REACT_APP_LIVE_IMAGE_URL}` +
                                            item?.categoryImageId?.originalURL
                                          }
                                          alt={TabsIcon1}
                                          style={{ height: 25, width: 25 }}
                                        />
                                      ) : (
                                        <img
                                          src="https://upload.wikimedia.org/wikipedia/commons/1/14/No_Image_Available.jpg"
                                          alt={TabsIcon1}
                                          style={{ height: 25, width: 25 }}
                                        />
                                      )}{" "}
                                      {item.categoryName}
                                    </a>
                                  </div>
                                )}
                              </div>
                            )}
                          </div>
                        ))}
                      </Accordion>
                    </TabPanel>
                  ))}
                </div>
              </div>
            </div>
          </Tabs>
          {/* </div> */}
        </section>

        {/* Tabs section end here */}

        {/* Slider Banner */}
        {/* <section className="banner">
          <div className="container">
            <div className="banner-slider">
              {firstBanner.length > 0 ? (
                <Carousel
                  autoPlay
                  infiniteLoop
                  showThumbs={false}
                  showStatus={false}
                >
                  {firstBanner.map((data, index) => {
                    return (
                      <div key={index}>
                        <a href={data.bannerURL}>
                          <img
                            src={data.url}
                            alt={data.url}
                            className="banner-image"
                          />
                        </a>
                      </div>
                    );
                  })}
                </Carousel>
              ) : null}
            </div>
          </div>
        </section> */}

        {/* end slider */}
        {/* my mall section start */}

        {/* my mall section  end */}
        {/* laptop section start */}

        {/* laptop section  end */}
        {/* Choose For You start */}

        {/* Choose For You end */}

        {/* Choose For You end */}
        {/* {loading ? (
          <Loader />
        ) : (
          <div class="container">
            <Tabs>
              <div className="new-category-section">

                <div className="row">


                  <div class="col-md-2 col-4 tabs_left pr-0">
                    <TabList>
                      <ul className="nav flex-column">

                        {selectedCategory.map((data, index) => {
                          if (data.categoryName === data.parent) {
                            return (
                              <Tab key={index}>
                                <li className="nav-item">
                                  <div className="nav-link">
                                    <img
                                      src={
                                        data.categoryImageId !== undefined
                                          ? window.$ImageURL +
                                          data.categoryImageId.originalURL
                                          : `${window.$ImageURL}no-image-480x480.png`
                                      }
                                      className="img-fliud"
                                      alt={data.categoryName}
                                    />
                                    <span>{data.parent}</span>
                                  </div>
                                </li>
                              </Tab>
                            );
                          }
                          return true;
                        })}


                      </ul>
                    </TabList>
                  </div>

                  <div class=" col-md-10 col-8 pl-0">
                    {selectedCategory.map((data, index) => {
                      if (data.categoryName === data.parent) {
                        return (
                          <TabPanel key={index}>
                            <div className="tab-details">
                              <div className="toggle-tab-header">
                                <p className="btn-tab">{data.parent}</p>
                                <i class="fa fa-angle-down fa-2x" aria-hidden="true"></i>
                              </div>
                              <div className="toggle-tab-body">
                                <div className="row cs-row-margin">

                                  {selectedCategory.map((data2, index) => {
                                    if (data.parent === data2.parent) {
                                      if (data2.parent !== data2.categoryName) {
                                        return (
                                          <div className="col-lg-2 col-md-3 col-6 cs-col-padding" key={index}>

                                            <a href={`${process.env.PUBLIC_URL
                                              }/productListing/category/${this.removeHTML(
                                                data2.categoryName
                                              )}`} class="sing-items">
                                              <div className="cat-image">
                                                <img src={
                                                  data2.categoryImageId !== undefined
                                                    ? window.$ImageURL +
                                                    data2.categoryImageId.originalURL
                                                    : `${window.$ImageURL}no-image-480x480.png`
                                                } className="img-fluid" alt={data2.categoryName} />
                                              </div>
                                              <p className="title">{data2.categoryName}</p>
                                            </a>

                                          </div>
                                        );
                                      }
                                    }
                                    return true;
                                  })}

                                </div>
                              </div>
                            </div>
                          </TabPanel>



                        );
                      }
                      return true;
                    })}
                  </div>
                </div>

              </div>
            </Tabs>
          </div>
        )} */}
      </div>
    );
  }
}
