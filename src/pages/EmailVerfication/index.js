import React, {Component} from "react";
import axios from "axios";


export default class EmailVerification extends Component { 

    state = {
        email: '',
       
        emailError: '',
     
        successMessage: '',
        success: false,
        errorMessage: '',
        error: false,
        token:''

    }

    onChange = (e) => {
        const { name, value } = e.target;
        this.setState({
            [name]: value
        });
    }
    validate = () => {
        this.setState({
            emailError: '',
           
        });
        const regex = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
        let emailError = '';
      

        if (this.state.email.length < 1) {
            emailError = "Please enter Email";
        } else if (!regex.test(this.state.email.trim())) {
            emailError = "Please enter a valid Email"
        }
        if (emailError) {
            this.setState({ emailError })
            return false
        }

        
        

        this.setState({
            emailError,
            
        })
        return true
    }

    onSubmit = async (e) => {
        e.preventDefault();
        this.setState({
            success: false,
            error: false,
            successMessage: '',
            token: '',

            errorMessage: ''
        })
        let checkValidation = this.validate();
        if (checkValidation) {
            const { email } = this.state;
            let loginData = {
                "email": email,
               
                //"loginType": "customer"
            };
            await axios.post(`${window.$URL}user/customer-forgot-password`, loginData)   
            .then(response => {
                if(response.status === 200){
                    if(response.data.success){
                        console.log(response);
                        
                        this.setState({
                            success: true,
                            successMessage: response.data.message,
                            token:response.data.passwordChangeToken
                        },() => {
                            
                            //this.props.history.push(`${process.env.PUBLIC_URL}/reset-password/${this.state.token}`);
                        })
                    }else{
                        console.log('errorAmi'+response.data.message)
                        this.setState({
                            error: true,
                            errorMessage: response.data.message
                        })
                    }
                }
            })
            .catch(error => {
                console.log(error);
            })
        }
    }

    render() {
        const { emailError, success, successMessage, errorMessage } = this.state;
        return (
            <section className="my-mart-signup-wr">
                <div className="container">
                    <div className="sinup-container">
                        <h1>Email Verification</h1>
                        <div className="form-sign-wrapper">
                            <form onSubmit={this.onSubmit}>
                            <div className="row">
                                <div className="col-lg-12 col-md-12 col-sm-12 form-parts p-4">
                                    {
                                        success ?
                                        <p style={{color:'green', fontSize:18}}> {successMessage} </p>
                                        :
                                        null
                                    }
                                    {/* {
                                        error ?
                                        <p style={{color:'red', fontSize:18}}> {errorMessage} </p>
                                        :
                                        null
                                    } */}
                                </div>
                                <div className="col-lg-3 col-md-6 col-sm-12 form-parts">
                                </div>
                                <div className="col-lg-6 col-md-6 col-sm-12 form-parts">
                                    <div className="form-group">
                                        <label className="input-heading">Verification Code</label>
                                        <input type="text" placeholder="Enter your Code" className="form-control" name="email_verification_code" onChange={this.onChange} />
                                        {emailError ? <span style={{color:'red'}}> {emailError} </span> : null }
                                        {errorMessage ? <span style={{color:'red'}}> {errorMessage} </span> : null }

                                    </div>
                                    
                                    <div className="form-group">
                                        <button type="submit" className="btn signup_btn">Submit</button> 
                                    </div>
                                    
                                </div>
                                <div className="col-lg-3 col-md-6 col-sm-12 form-parts">
                                </div>
                                
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}