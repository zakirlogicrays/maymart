import React, { Component } from "react";
import ProfileSidebar from "../../Component/ProfileSidebar";
import AddressBook from "../../Component/UserAddress";


export default class UserAddress extends Component {
  

  render() {
   
    return (
      <section className="dbF-content">
        <div className="container">
          <div className="row">
            <ProfileSidebar />
            <div className="col-lg-9">
            <AddressBook  colLayout={'col-md-12 col-sm-12 col-xs-12 col-lg-12'}/>
            </div>
          </div>
        </div>
      </section>
    )
  }
}  