import React, { Component } from "react";
import MultiCarousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import axios from "axios";
import ProductListing from "../../Component/ProductListing";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from "react-responsive-carousel";
import Loader from "../../Component/Loader";
import { connect } from "react-redux";
import { RemoveLocation } from "../../data/reducers/location";
import LocationModal from "../../Component/LocationModal";
import moment from 'moment';

const responsive = {
    superLargeDesktop: {
        // the naming can be any, depends on you.
        breakpoint: { max: 4000, min: 3000 },
        items: 6,
    },
    desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 6,
    },
    tablet: {
        breakpoint: { max: 1024, min: 464 },
        items: 4.2,
    },
    mobile: {
        breakpoint: { max: 464, min: 0 },
        items: 4.2,
    },
};

const responsiveBigBanner = {
    superLargeDesktop: {
        // the naming can be any, depends on you.
        breakpoint: { max: 4000, min: 3000 },
        items: 5,
    },
    desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 3,
    },
    tablet: {
        breakpoint: { max: 1024, min: 464 },
        items: 2,
    },
    mobile: {
        breakpoint: { max: 464, min: 0 },
        items: 2,
    },
};

function calculateDistance(lat1, lon1, lat2, lon2, unit) {
    var radlat1 = (Math.PI * lat1) / 180;
    var radlat2 = (Math.PI * lat2) / 180;
    var theta = lon1 - lon2;
    var radtheta = (Math.PI * theta) / 180;
    var dist =
        Math.sin(radlat1) * Math.sin(radlat2) +
        Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    dist = Math.acos(dist);
    dist = (dist * 180) / Math.PI;
    dist = dist * 60 * 1.1515;
    if (unit === "K") {
        dist = dist * 1.609344;
    }
    if (unit === "N") {
        dist = dist * 0.8684;
    }
    return dist;
}

class Homepage extends Component {
    state = {
        settings: {
            dots: true,
            infinite: true,
        },
        firstBanner: [],
        secondBanner: [],
        thirdBanner: [],
        fourthBanner: [],
        selectedCategory: [],
        TotalCategoryImage: [],

        featuredProducts: [],
        recommendedProducts: [],
        newAndTrending: [],
        bestSellers: [],
        featuredProductsFilter: [],
        recommendedProductsFilter: [],
        newAndTrendingFilter: [],
        bestSellersFilter: [],
        loading: true,
        newAndTrendingLoading: true,
        popularCategoryLoading: true,
        popularCategory: [],
        showLocationModal: false,
        viewProductsBasedOn: "Store",
        showUpcomingModal: false,
    };

    componentDidMount() {
        const { Location } = this.props;
        if (Location.locationCoordinates.length > 0) {
          
            this.fetchBanners();
            this.getAllData();
            this.getPopularCategories();
        } else {
           

            this.openLocationModal();
        }
        

        if (this.props.match.params.categoryType === "promotions") {
            this.setState({ showUpcomingModal: true });
        }
    }

    Capitalize(str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }

    openLocationModal = () => {
        this.props.RemoveLocation();
        this.setState({
            showLocationModal: true,
        });
    };

    hideLocModal = () => {
        this.setState(
            {
                showLocationModal: false,
            },
            () => {
                this.componentDidMount();
            }
        );
    };

    fetchBanners = async () => {
        let catType = this.Capitalize(this.props.match.params.categoryType);
        let bannerType = "";
        if (catType === "Grocery") {
            bannerType = "Grocery";
        }
        if (catType === "MyMall") {
            bannerType = "My Mall";
        }
        if (catType === "Marketplace") {
            bannerType = "Marketplace";
        }
        await axios
            .get(`${window.$URL}banner/fetchBanner`)
            .then((response) => {
                if (response.status === 200) {
                    let bannerArray = response.data.data;
                    let firstBanner = [];
                    let secondBanner = [];
                    let thirdBanner = [];
                    let fourthBanner = [];

                    for (let i = 0; i <= bannerArray.length - 1; i++) {
                        if (
                            bannerArray[i].type === "1st Banner" &&
                            bannerArray[i].bannerShowesTo === bannerType
                        ) {
                            firstBanner.push({
                                url:
                                    `${window.$ImageURL}` +
                                    bannerArray[i].bannerImageId[0].originalURL,
                                bannerURL: bannerArray[i].bannerURL,
                            });
                        } else if (
                            bannerArray[i].type === "2nd Banner" &&
                            bannerArray[i].bannerShowesTo === bannerType
                        ) {
                            secondBanner.push(bannerArray[i]);
                        } else if (
                            bannerArray[i].type === "3rd Banner" &&
                            bannerArray[i].bannerShowesTo === bannerType
                        ) {
                            thirdBanner.push(bannerArray[i]);
                        } else if (
                            bannerArray[i].type === "4th Banner" &&
                            bannerArray[i].bannerShowesTo === bannerType
                        ) {
                            fourthBanner.push(bannerArray[i]);
                        }
                    }
                    this.setState({
                        firstBanner,
                        secondBanner,
                        thirdBanner,
                        fourthBanner,
                    });
                }
            })
            .catch((error) => {
                console.log(error);
            });
    };

    getAllData = async () => {
        this.newAndTrending();
        let catType = this.Capitalize(this.props.match.params.categoryType);
        await axios
            .post(`${window.$URL}sellerProducts/homepageCategoryWise`, {
                category: catType,
            })
            .then((response) => {
      

                if (response.data.success) {
                    //console.log('a');
                    this.setState(
                        {
                            selectedCategory: response.data.popularCategories,
                            TotalCategoryImage: response.data.popularCategoryImages,
                            featuredProducts: response.data.featuredProducts,
                            recommendedProducts: response.data.recommendedProducts,
                            bestSellers: response.data.bestSellers,
                            loading:false
                           
                        },
                        () => {
                            this.renderProducts();
                        }
                    );
                }else{
                    console.log('b');

                    this.setState({
                        loading:false
                    })
                }
            })
            .catch((error) => {
                console.log('c');

                console.log(error);
                this.setState({
                    loading:false
                })
            });
    };

    toggleProducts = (type) => {
        console.log(type);
        this.setState(
            {
                viewProductsBasedOn: type,
            },
            () => {
                this.renderProducts();
            }
        );
    };

    renderProducts = () => {
        const { Location } = this.props;
        const {
            featuredProducts,
            recommendedProducts,
            newAndTrending,
            bestSellers,
        } = this.state;
        let productFeatured = this.sortProductsNearestOrBestOffer(
            featuredProducts,
            Location
        );
        let productRecommended = this.sortProductsNearestOrBestOffer(
            recommendedProducts,
            Location
        );
        let productNewAndTrending = this.sortProductsNearestOrBestOffer(
            newAndTrending,
            Location
        );
        let productBestSellers = this.sortProductsNearestOrBestOffer(
            bestSellers,
            Location
        );

        this.setState({
            featuredProductsFilter: productFeatured,
            recommendedProductsFilter: productRecommended,
            newAndTrendingFilter: productNewAndTrending,
            bestSellersFilter: productBestSellers,
            loading: false,
            newAndTrendingLoading: false,
        });
       

    };

    sortProductsNearestOrBestOffer(data, userLocation) {
        const { viewProductsBasedOn } = this.state;
        let returnArray = [];
        let returnType = false;
        if (viewProductsBasedOn === "Store") {
            let userCoordinates = userLocation.locationCoordinates[0].coordinates;

            for (let i = 0; i < data.length; i++) {
                data[i]["distance"] = calculateDistance(
                    userCoordinates.lat,
                    userCoordinates.lng,
                    data[i].StoreLocation.coordinates[1],
                    data[i].StoreLocation.coordinates[0],
                    "K"
                );
            }

            data.sort(function (a, b) {
                return a.distance - b.distance;
            });
            let trimmedArray = this.removeOtherSimilarProducts(data);
            returnArray = trimmedArray;
            returnType = true;
        } else {
            let filterOfferData = data.filter((eachData) => {
                return eachData.sellerProductVariantId[0].specialPriceProvided && this.getDateValid(eachData.sellerProductVariantId[0]);
            });

            filterOfferData.sort((a, b) => {
                return (
                    b.sellerProductVariantId[0].specialPriceDiscount -
                    a.sellerProductVariantId[0].specialPriceDiscount
                );
            });
            returnArray = filterOfferData;
            returnType = true;
        }
        if (returnType === true) {
            return returnArray;
        }
    }
    getDateValid = (prodArray) => {
        let returnValidate = false;
        if(prodArray.startDate !== "" && prodArray.endDate !== ""){
            let convertStartDate = moment(prodArray.startDate).unix() * 1000;
            let convertEndDate = moment(prodArray.endDate).unix() * 1000;
            let systemTime = moment(new Date()).unix() * 1000;
            if(systemTime <= convertEndDate && systemTime >= convertStartDate){
            returnValidate = true;
            }
        }
        return returnValidate;
    }

    removeOtherSimilarProducts = (dataArray) => {
        let productVariantIDArray = [];
        let newDataArray = [];
        for (let i = 0; i <= dataArray.length - 1; i++) {
            if (!productVariantIDArray.includes(dataArray[i].productId)) {
                productVariantIDArray.push(dataArray[i].productId);
                newDataArray.push(dataArray[i]);
            }
        }
        return newDataArray;
    };

    getPopularCategories = async () => {
        let catType = this.Capitalize(this.props.match.params.categoryType);
        await axios
            .post(
                `${window.$URL}popular-category/fetch-popular-catetgory-by-pagename`,
                { pageName: catType }
            )
            .then((response) => {
                if (response.data.success) {
                    this.setState({
                        popularCategory: response.data.data,
                        popularCategoryLoading: false,
                    });
                }
            })
            .catch((error) => {
                console.log(error);
            });
    };

    newAndTrending = async () => {
        let newTrendingProducts = [];
        let catType = this.Capitalize(this.props.match.params.categoryType);
        await axios
            .post(`${window.$URL}sellerProducts/newAndTrending`, {
                category: catType,
            })
            .then((response) => {
                if (response.status === 200) {
                    newTrendingProducts = response.data;
                    this.setState({
                        newAndTrending: response.data,
                    });
                }
            })
            .catch((error) => {
                console.log(error);
            });
        return newTrendingProducts;
    };

    createMarkup = (html) => {
        return {
            __html: html,
        };
    };

    removeHTML = (html) => {
        const regex = /(<([^>]+)>)/gi;
        const result = html.replace(regex, "");
        let returnName = "";
        if (result === "Groceries") {
            returnName = "Grocery";
        } else {
            returnName = result;
        }
        return returnName;
    };

    hideComingModal = () => {
        this.setState(
            {
                showUpcomingModal: false,
            },
            () => {
                //this.componentDidMount();
            }
        );
    };
    render() {
        const {
            firstBanner,
            showUpcomingModal,
            popularCategoryLoading,
            thirdBanner,
            fourthBanner,
            featuredProductsFilter,
            recommendedProductsFilter,
            newAndTrendingFilter,
            bestSellersFilter,
            popularCategory,
            showLocationModal,
            viewProductsBasedOn,
        } = this.state;
        const { categoryType } = this.props.match.params;
        
        return (
            <>
                {/* Slider Banner */}

                <section className="banner">
                    <LocationModal
                        modalStatus={showLocationModal}
                        hideLocationModal={() => this.hideLocModal()}
                    />
                    {/*  modal upcoming */}
                    {showUpcomingModal ? (
                        <div
                            id="exampleModalCenter"
                            className="modal fade show"
                            tabIndex={-1}
                            role="dialog"
                            aria-labelledby="exampleModalCenterTitle"
                            style={{ display: "block", paddingRight: 17 }}
                        >
                            <div
                                className="modal-dialog modal-dialog-centered"
                                role="document"
                            >
                                <div className="modal-content">
                                    <div className="modal-header">
        {/* <h5 className="modal-title" id="exampleModalCenterTitle">Page Status</h5> */}
        <button type="button" onClick={() => this.hideComingModal()} className="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
                                    <div className="modal-body">
                                        {/* <p>Promotions is Comming...</p> */}
                                        <h5 className="modal-title" id="exampleModalCenterTitle">
                                            Promotions coming soon...
                    </h5>
                                    </div>
                                    {/* <div className="modal-footer">
        <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" className="btn btn-primary">Save changes</button>
      </div> */}
                                </div>
                            </div>
                        </div>
                    ) : (
                        ""
                    )}
                    {/* modal upcoming */}
                    <div className="container">
                        <div className="banner-slider">
                            {firstBanner.length > 0 ? (
                                <Carousel
                                    autoPlay
                                    infiniteLoop
                                    showThumbs={false}
                                    showStatus={false}
                                >
                                    {firstBanner.map((data, index) => {
                                        return (
                                            <div key={index}>
                                                <a href={data.bannerURL}>
                                                    <img
                                                        src={data.url}
                                                        alt={data.url}
                                                        className="banner-image"
                                                    />
                                                </a>
                                            </div>
                                        );
                                    })}
                                </Carousel>
                            ) : null}
                        </div>
                    </div>
                </section>
                {/* end slider */}
                {/* my mall section start */}
                <section className="new_tranding">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-8 col-lg-8 col-sm-6 col-xs-6 title_section">
                                <h2>Popular Categories </h2>
                            </div>
                            <div className="col-md-4 col-lg-4 col-sm-6 col-xs-6 title_section pull-right">
                                {categoryType === "grocery" ? (
                                    <div className="main-srch">
                                        <p className="h-name">Shop by:</p>
                                        <div className="nearby-srch">
                                            <label>
                                                <input
                                                    type="radio"
                                                    name="radio"
                                                    defaultValue="Store"
                                                    defaultChecked={
                                                        viewProductsBasedOn === "Store" ? true : false
                                                    }
                                                    onChange={() => this.toggleProducts("Store")}
                                                />
                                                <p className="names">Nearest Store</p>
                                            </label>

                                            <label>
                                                <input
                                                    type="radio"
                                                    name="radio"
                                                    defaultValue="specialPrice"
                                                    defaultChecked={
                                                        viewProductsBasedOn === "specialPrice"
                                                            ? true
                                                            : false
                                                    }
                                                    onChange={() => this.toggleProducts("specialPrice")}
                                                />
                                                <p className="names">Best Offers</p>
                                            </label>
                                        </div>
                                    </div>
                                ) : null}
                            </div>
                        </div>

                        <div className="mall-cat-wr">
                            {/* Set up your HTML */}
                            {popularCategoryLoading ? (
                                <Loader />
                            ) : (
                                <MultiCarousel
                                    swipeable={false}
                                    draggable={false}
                                    showDots={false}
                                    responsive={responsive}
                                    ssr={true} // means to render carousel on server-side.
                                    infinite={false}
                                    autoPlaySpeed={1000}
                                    keyBoardControl={true}
                                    customTransition="all .5"
                                    transitionDuration={500}
                                    containerClass="carousel-container"
                                    itemClass="carousel-item-padding-40-px"
                                    removeArrowOnDeviceType={["tablet", "mobile"]}
                                >
                                    {popularCategory.map((data, index) => {
                                        console.log(data);
                                        return (
                                            <a
                                                href={`${process.env.PUBLIC_URL
                                                    }/productListing/category/${this.removeHTML(
                                                        data.categoryName
                                                    )}`}
                                                className="card"
                                                key={index}
                                            >
                                                <div className="icon">
                                                    {data.categoryImageId ? (
                                                        <img
                                                            src={
                                                                data.categoryImageId.originalURL !== null &&
                                                                    data.categoryImageId.originalURL !== undefined
                                                                    ? window.$ImageURL +
                                                                    data.categoryImageId.originalURL
                                                                    : `${window.$ImageURL}no-image-480x480.png`
                                                            }
                                                            alt={data.categoryName}
                                                        />
                                                    ) : (
                                                        <img
                                                            src={`${window.$ImageURL}no-image-480x480.png`}
                                                            alt={data.categoryName}
                                                        />
                                                    )}
                                                </div>
                                                <p
                                                    className="name"
                                                    dangerouslySetInnerHTML={this.createMarkup(
                                                        data.categoryName
                                                    )}
                                                />
                                            </a>
                                        );
                                    })}
                                </MultiCarousel>
                            )}
                        </div>
                    </div>
                </section>
                {/* my mall section  end */}
                {/* laptop section start */}
                <section className="new_tranding">
                    <div className="container">
                        <div className="title_section">
                            <h2> Highlights </h2>
                        </div>
                        <div className="mall-cat-wr exclusiveOffer">
                            {/* Set up your HTML */}
                            <div className="owl-carousel">
                                <MultiCarousel
                                    swipeable={true}
                                    draggable={true}
                                    showDots={false}
                                    responsive={responsiveBigBanner}
                                    ssr={true} // means to render carousel on server-side.
                                    infinite={false}
                                    autoPlaySpeed={1000}
                                    keyBoardControl={true}
                                    customTransition="all .5"
                                    transitionDuration={500}
                                    containerClass="carousel-container"
                                    itemClass="carousel-item-padding-80-px"
                                    removeArrowOnDeviceType={["tablet", "mobile"]}
                                >
                                    {thirdBanner.map((data, index) => {
                                        return (
                                            <div
                                                className="offer-item"
                                                key={index}
                                                style={{
                                                    backgroundImage:
                                                        "url(" +
                                                        window.$ImageURL +
                                                        data.bannerImageId[0].originalURL +
                                                        ")",
                                                    backgroundPosition: "center",
                                                    backgroundRepeat: "no-repeat",
                                                    backgroundSize: "cover",
                                                }}
                                            >
                                                <div className="icontent">
                                                    <p
                                                        className="name"
                                                        dangerouslySetInnerHTML={this.createMarkup(
                                                            data.title
                                                        )}
                                                    />
                                                    <a href={data.bannerURL}>
                                                        Shop Now{" "}
                                                        <i
                                                            className="fa fa-angle-right"
                                                            aria-hidden="true"
                                                        />
                                                    </a>
                                                </div>
                                            </div>
                                        );
                                    })}
                                </MultiCarousel>
                            </div>
                        </div>
                    </div>
                </section>
                {/* laptop section  end */}
                {/* Choose For You start */}
                <section className="new_tranding">
                    <div className="container">
                        <div className="title_section">
                            <h2>Featured Products</h2>
                            <a href="#/" className="btn view-all">
                                View All
              </a>
                        </div>
                        <ProductListing
                            productArray={featuredProductsFilter}
                            loading={this.state.loading}
                        />
                    </div>
                </section>
                {/* Choose For You end */}
                {/* trending section end */}
                {/* game zone offer start */}
                <section className="gamezone_wrapper">
                    <div className="container">
                        <div className="row">
                            {fourthBanner.map((data, index) => {
                                return (
                                    <div className="col-md-6 " key={index}>
                                        <div
                                            className={
                                                index % 2 !== 0 ? "gaming-console" : "peper-area"
                                            }
                                            style={{
                                                backgroundImage:
                                                    "url(" +
                                                    window.$ImageURL +
                                                    data.bannerImageId[0].originalURL +
                                                    ")",
                                                backgroundPosition: "center",
                                                backgroundRepeat: "no-repeat",
                                                backgroundSize: "cover",
                                            }}
                                        >
                                            <div className="in-content0">
                                                <p
                                                    className="name"
                                                    style={
                                                        index % 2 !== 0
                                                            ? { textAlign: "start" }
                                                            : { textAlign: "end" }
                                                    }
                                                    dangerouslySetInnerHTML={this.createMarkup(
                                                        data.title
                                                    )}
                                                />
                                                <a
                                                    href={data.bannerURL}
                                                    className={
                                                        index % 2 !== 0 ? " btn right" : "btn left"
                                                    }
                                                >
                                                    Shop Now{" "}
                                                    <i className="fa fa-angle-right" aria-hidden="true" />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                </section>
                {/* my mall start */}
                {/* Choose For You start */}
                <section className="new_tranding">
                    <div className="container">
                        <div className="title_section">
                            <h2>Recommended Products</h2>
                            <a href="#/" className="btn view-all">
                                View All
              </a>
                        </div>
                        <ProductListing
                            productArray={recommendedProductsFilter}
                            loading={this.state.loading}
                        />
                    </div>
                </section>
                {/* Choose For You end */}
                <section className="new_tranding">
                    <div className="container">
                        <div className="title_section">
                            <h2>New and Trending</h2>
                            <a href="#/" className="btn view-all">
                                View All
              </a>
                        </div>
                        <ProductListing
                            productArray={newAndTrendingFilter}
                            loading={this.state.newAndTrendingLoading}
                        />
                    </div>
                </section>
                {/* my mall end */}
                {/* Choose For You start */}
                <section className="new_tranding">
                    <div className="container">
                        <div className="title_section">
                            <h2>Best Sellers</h2>
                            <a href="#/" className="btn view-all">
                                View All
              </a>
                        </div>
                        <ProductListing
                            productArray={bestSellersFilter}
                            loading={this.state.loading}
                        />
                    </div>
                </section>
                {/* Choose For You end */}
            </>
        );
    }
}

const mapStateToProps = (state) => ({
    Location: state.Location,
});

export default connect(mapStateToProps, { RemoveLocation })(Homepage);
