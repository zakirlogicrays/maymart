
import React, { Component } from 'react'
import TimeSlot from '../../Component/TimeSlot';
import axios from 'axios';
import { MdLocationOn } from 'react-icons/md';
import { ImPhone } from 'react-icons/im';
import { BsFillEnvelopeFill } from 'react-icons/bs';
import deliveryBox from '../../assets/images/delivery-box.png';
import raspberry from '../../assets/images/raspberry.png';

export default class Checkout extends Component {
    state = {
        timeSlotShow: false,
        deliveryRule: [],
        deliveryRuleSelected: [],
        timeSlot: 0,
        editForm:false
    }

    componentDidMount(){
        this.getDeliveryRule();
    }

    getDeliveryRule = async() => {
        await axios.get(`${window.$URL}deliveryRule/fetchDeliveryRule`)
        .then(response => {
            if(response.data.success){
                this.setState({
                    deliveryRule: response.data.data,
                    deliveryRuleSelected: response.data.data[0],
                    timeSlot: response.data.data[0].timeSlot
                })
            }
        })
        .catch(error => {
            console.log(error.data)
        })
    }

    timeSlot = (slot, index) => {
        this.setState({
            timeSlot : slot,
            deliveryRuleSelected: this.state.deliveryRule[index]
        })
    }


    toggleTimeSlot = () => {
        const {timeSlotShow} = this.state;
        if(timeSlotShow){
            this.setState({
                timeSlotShow : false
            })
        }else{
            this.setState({
                timeSlotShow : true
            })
        }
    }

    render() {
        const{deliveryRule,deliveryRuleSelected,timeSlot,timeSlotShow} = this.state;
        return (
            <section className="cart-block checkout-block">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-8 flex-screen">
                            <div className="shipment-Block">
                                <div className="shipment-wr">
                                    <div className="top-header">
                                        <div className="row">
                                            <div className="col-lg-9 col-md-9 col-sm-12">
                                                <h2 className="gross-title">Groceries Shipment</h2>
                                                <p className="gros-subtitle">Want all your groceries delivered together?</p>
                                            </div>
                                            <div className="col-lg-3  col-md-3 col-sm-12">
                                                <div className="check-form">
                                                    <div className="form-check">
                                                        <input className="form-check-input" type="radio" name="gridRadios" id="gridRadios1" defaultValue="option1" defaultChecked />
                                                        <label className="form-check-label" htmlFor="gridRadios1"> Yes</label>
                                                    </div>
                                                    <div className="form-check">
                                                        <input className="form-check-input" type="radio" name="gridRadios" id="gridRadios2" defaultValue="option2" />
                                                        <label className="form-check-label" htmlFor="gridRadios2"> No</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="new-shipment-wr">
  
                                        <div className="user-name-wr">
                                            <h2 className="names"><span className="location-icon"><MdLocationOn/></span> Broji</h2>
                                            <p className="address"><span className="add-type">home</span> lorem address <span className="edit">Edit</span></p>
                                        </div>
                                        <div className="edit-form">
                                            <div className="formf-row">
                                                <ImPhone className="form-icon bl-icon"/>  
                                                <input type="number" className="form-control" placeholder="Phone number"/>
                                                
                                            </div>
                                            <div className="formf-row">
                                                <BsFillEnvelopeFill className="form-icon bl-icon"/>  
                                                <input type="email" className="form-control" placeholder="Email address"/>
                                                
                                            </div>
                                        </div>
                                        </div>
                                    <div className="shipment-inner si-newInner">
                                    <div className="pack-details">
                                        <div className="pack-with-image">
                                            <img src={deliveryBox} className="img-fluid"/>
                                            <h4 className="pack-head">Package 1 of 2</h4>
                                        </div>
                                        <p className="paked-by">Shipped by WUAN</p>
                                    </div>
                                        <div className="inner-padding">
                                        <p className="choose-delivery-opt">Choose your delivery option  </p>
                                        <div className="row">
                                            {
                                                deliveryRule.map((data,index) => {
                                                    return(
                                                        <div className="col-lg-4 col-md-4 col-sm-6 equal" key={index}>
                                                            <label className="schedule-slot">
                                                                <input type="radio" name="test" value={data.timmeSlot} checked={data.timeSlot === timeSlot ? true : false} onChange={() => this.timeSlot(data.timeSlot, index)} />
                                                                <div className="sch-slot-slct">
                                                                    <h2>{`QAR ${data.deliveryCharge}`}</h2>
                                                                    <h3>Scheduled ({data.timeSlot}-hour slots) </h3>
                                                                    <p>Select your preferred slot</p>
                                                                </div>
                                                            </label>
                                                        </div>
                                                    )
                                                })
                                            }
                                        </div>
                                        

                                        <div className="slect-time-slot-wr">
                                            <p>Delivery Time  </p>
                                            <button className="btn btn-delivery" onClick={this.toggleTimeSlot}> Select your slot</button>
                                        </div>



                                        {
                                            timeSlotShow ?
                                            <TimeSlot deliveryRule={deliveryRule} deliveryRuleSelected={deliveryRuleSelected} timeSlot={timeSlot} />
                                            :
                                            null
                                        }
                                        <div className="row">
                                            <div className="col-3 col-md-2">
                                                <div className="ship-image">
                                                    <img src={raspberry} alt="raspberry" />
                                                </div>
                                            </div>
                                            <div className="col-7 col-md-8">
                                                <div className="ship-prodetails">
                                                    <h2 className="pro-name">Quis Aliquam Quaerat Voluptatem</h2>
                                                    <h4 className="price">QAR 50.00 <span> 80.00</span></h4>
                                                    <p>1 kg</p>
                                                </div>
                                            </div>
                                            <div className="col-2 d-flex">
                                                <div className="ship-prodetails">
                                                    <h4 className="price">Qty: 1</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="total-price">
                                        <h2>1 Item(s), Total : <span>QAR 100.00</span></h2>
                                        <p>Price dropped, Saved : Rp 4,000</p>
                                    </div>
                                    </div>
                                    </div>
                            </div>
                        </div>
                        <div className="col-lg-4 flex-screen">
                            <div className="shipment-Block">
                                <div className="shipment-wr">
                                    <div className="top-header placeHolder">
                                        <h2 className="gross-title">Place Order Now</h2>
                                    </div>
                                    <div className="shipment-inner billing-section">
                                        <p className="Ship-Bill">Shipping &amp; Billing</p>
                                        <p className="buyer-name">Broji</p>
                                        <p className="address-1">13 South Poll, Near RD, Singapore 72200 </p>
                                        <div className="buyer-address">
                                            <p> Bill to the same address</p>
                                            <p>123 456 7890</p>
                                            <p>broji@dummy.com</p>
                                            <button className="btn edit"><i className="fa fa-pencil" aria-hidden="true" /></button>
                                        </div>
                                        <p className="Ship-Bill">Shipping &amp; Billing</p>
                                        <div className="sb-total">
                                            <p>Subtotal (2 items)</p>
                                            <p>QAR 38.00</p>
                                        </div>
                                        <div className="sb-total">
                                            <p>Shipping Fee</p>
                                            <p>QAR 8.00</p>
                                        </div>
                                        <form className="form-wrapper">
                                            <input className="form-control " type="search" placeholder="Enter Voucher Code" />
                                            <button className="btn btn-apply" type="submit">Apply</button>
                                        </form>
                                        <div className="net-price">
                                            <span>Total: QAR 50.00</span>
                                        </div>
                                        <button className="btn checkout">place order</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        )
    }
}