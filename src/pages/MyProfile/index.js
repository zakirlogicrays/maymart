import React, { Component } from "react";
import ProfileSidebar from "../../Component/ProfileSidebar";
import axios from "axios";
import jwt_decode from 'jwt-decode';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export default class MyProfile extends Component {
  state = {
    profileInfo: [],
    fName: '',
    lName: '',
    fNameError: '',
    lNameError: '',
    successMessage: '',
    errorMessage: '',
    prevProfileImg: '',
    ProfileImg: null,
    profileImageInDB: ''
  }

  componentDidMount(){
    this.fetchProfileDetails();
  }

  fetchProfileDetails = async() => {
    let checkToken = await localStorage.getItem("token");
    const decoded = jwt_decode(checkToken);
    await  axios.get(`${window.$URL}user/Details/${decoded.userid}`)
    .then(res => {
      console.log(res);
        if(res.status === 200){
          this.setState({
            profileInfo: res.data,
            fName: res.data.Firstname,
            lName: res.data.Lastname,
            profileImageInDB: res.data.profileImageId !== null ? res.data.profileImageId.originalURL : null
          })
        }
    })
    .catch(error => {
        console.log(error)
    })
  }

  onChange = (e) => {
    e.preventDefault();
    const {name, value} = e.target;
    this.setState({
      [name] : value
    })
  }

  handleProfileImageChange = (e) => {
      e.preventDefault();
      const img = e.target.files[0];

      this.setState({
          prevProfileImg: URL.createObjectURL(img),
          ProfileImg: img,
      })
  }

  validate = () => {
    this.setState({
      fNameError: '',
      lNameError: '',
      successMessage: '',
      errorMessage : ''
    })
    let fNameError = '';
    let lNameError = '';

    if (this.state.fName === '') {
      fNameError = "Please Enter First Name"
    }
    if (fNameError) {
        this.setState({ fNameError })
        return false
    }

    if (this.state.lName === '') {
      lNameError = "Please Enter Last Name"
    }
    if (lNameError) {
        this.setState({ lNameError })
        return false
    }
    return true
}

  onUpdate = async(e) => {
    e.preventDefault();
    let validate = this.validate();
    if(validate){
      const {ProfileImg, fName, lName} = this.state;
      //console.log(fName,lName,'ami')
      let checkToken = localStorage.getItem("token");
      let decoded = jwt_decode(checkToken);
      let userId = decoded.userid;

      await axios.post(`${window.$URL}user/updateprofile`, { fName, lName, userId})
      .then(async response => {
          if(response.data.success){
     
          
            if (response.data.success) {
              toast.success(response.data.message, {
                  position: "bottom-center",
                  autoClose: 3000,
                  hideProgressBar: true,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true,
                  progress: undefined,
              });
              this.props.cancelAdd(true);
          } else {
              toast.error(response.data.message, {
                  position: "bottom-center",
                  autoClose: 3000,
                  hideProgressBar: true,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true,
                  progress: undefined,
              });
          }

            if(ProfileImg.name !== undefined){
              var bodyLogoFormData = new FormData();
              bodyLogoFormData.set('userId', userId);
              bodyLogoFormData.append('uploadProfileImage', ProfileImg);
              await axios({
                  method: 'post',
                  url: `${window.$URL}user/uploadProfileImage`,
                  data: bodyLogoFormData,
                  headers: { 'Content-Type': 'multipart/form-data' }  
              })
              .then(() => {
                // this.setState({
                //   successMessage: response.data.message
                // })
                if (response.data.success) {
                  // toast.success(response.data.message, {
                  //     position: "bottom-center",
                  //     autoClose: 3000,
                  //     hideProgressBar: true,
                  //     closeOnClick: true,
                  //     pauseOnHover: true,
                  //     draggable: true,
                  //     progress: undefined,
                  // });
                  this.props.cancelAdd(true);
              } else {
                  toast.error(response.data.message, {
                      position: "bottom-center",
                      autoClose: 3000,
                      hideProgressBar: true,
                      closeOnClick: true,
                      pauseOnHover: true,
                      draggable: true,
                      progress: undefined,
                  });
              }
                
              })
              .catch(imageError => {
                  console.log(imageError);
              })
            }else{
              this.setState({
                successMessage: response.data.message
              })
            }
            
          }else{
            this.setState({
              errorMessage: response.data.message
            })
          }
      })
      .catch(error => {
          console.log(error);
      })
    }
  }

  render() {
    const {profileInfo, fNameError, lNameError, successMessage, errorMessage, profileImageInDB} = this.state;
    return (
      <section className="dbF-content">
        <div className="container">
          <div className="row">
            <ProfileSidebar />
            <div className="col-lg-9">
            <ToastContainer />
              <div className="my-order-wr">
                <h2 className="order-title">My Account </h2>
                {
                  successMessage !== '' ?
                  <span className="text-success">{successMessage}</span>
                  :
                  null
                }
                {
                  errorMessage !== '' ?
                  <span className="text-danger">{errorMessage}</span>
                  :
                  null
                }
                <div className="profile-wrapper">
                  <div className="profile-image-wr">
                    <div className="pofile-image">
                      {
                          profileImageInDB !== null && !this.state.prevProfileImg ?
                          <img src={`${window.$ImageURL}` + profileImageInDB} className="img-fluid" alt={profileImageInDB} />
                          :
                          <img src={(!this.state.prevProfileImg) ? `${window.$ImageURL}no-image-480x480.png` : this.state.prevProfileImg} className="img-fluid" alt={this.state.prevProfileImg} />
                      }
                    </div>
                    <input type="file" hidden id="choose-photo" onChange={(event) => this.handleProfileImageChange(event)} />
                    <label className="file-upload" htmlFor="choose-photo"><i className="fa fa-pencil" aria-hidden="true" /></label> 
                  </div>
                    <div className="form-group">
                    <label className="input-heading" htmlFor="exampleInputEmail1">First Name</label>
                      <input type="text" className="form-control" name="fName" placeholder="Customer first name" defaultValue={profileInfo.Firstname} onChange={this.onChange} />
                      {fNameError ? (<div className="text-danger">{fNameError}</div>) : null}
                    </div>
                    <div className="form-group">
                    <label className="input-heading" htmlFor="exampleInputEmail1">Last Name</label>
                      <input type="text" className="form-control" name="lName" placeholder="Customer last name" defaultValue={profileInfo.Lastname} onChange={this.onChange} />
                      {lNameError ? (<div className="text-danger">{lNameError}</div>) : null}
                    </div>
                    <div className="form-group">
                    <label className="input-heading" htmlFor="exampleInputEmail1">Email</label>
                      <input type="email" className="form-control" placeholder="Customer email" defaultValue={profileInfo.Email} readOnly disabled />
                    </div>
                    <div className="form-group ">
                    <label className="input-heading" htmlFor="exampleInputEmail1">Phone</label>
                      <input type="text" className="form-control" placeholder="Customer phone no" defaultValue={profileInfo.Phone} readOnly disabled />
                    </div>
                    <button type="submit" className="btn btn-delivery" onClick={this.onUpdate}>Update</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
  }
}  