import React from 'react';
import { Router } from "react-router-dom";
import history from "./services/history";
import Routes from "./routes";
//redux
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import {store, persistor} from './data/store';

function App() {
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <Router history={history}  basename="/" > 
          <Routes />
        </Router>
      </PersistGate>
    </Provider>
  );
}

export default App;
