import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
require('dotenv').config();
window.$URL = process.env.NODE_ENV === 'production' ? process.env.REACT_APP_PROD_API_URL : process.env.REACT_APP_DEV_API_URL;
window.$ImageURL = process.env.NODE_ENV === 'production' ? process.env.REACT_APP_LIVE_IMAGE_URL : process.env.REACT_APP_DEV_IMAGE_URL;

let languageChoosen = localStorage.getItem('language');
// console.log(languageChoosen,'index')

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
// document.getElementsByTagName('html')[0].setAttribute("dir", "rtl");
document.getElementsByTagName('html')[0].setAttribute("dir", languageChoosen === "ar" ? "rtl" : "ltr");
