import { API_BASE_URL } from '../constants';
import axios from 'axios';

const request = (options) => {
    const headers = new Headers({
        'Content-Type': 'application/json',
    })
}


export function checkEmailAvailability(email) {
   return  axios.get(`${API_BASE_URL}user/checkEmailAvailability/${email}`)
}

export function signup(signupRequest) {

    return  axios.post(`${API_BASE_URL}user/customersignup`, signupRequest)

}

export function login(loginRequest) {

    return  axios.post(`${API_BASE_URL}user/customerlogin`, loginRequest)

}