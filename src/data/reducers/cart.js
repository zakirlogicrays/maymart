import axios from 'axios';
import jwt_decode from 'jwt-decode';
import { toast } from 'react-toastify';
//Actions Types
const ADD_TO_CART_SUCCESS = 'ADD_TO_CART_SUCCESS';
const ADD_TO_CART_FAILED = 'ADD_TO_CART_FAILED';
const REMOVE_FROM_CART_SUCCESS = 'REMOVE_FROM_CART_SUCCESS';
const REMOVE_FROM_CART_FAILED = 'REMOVE_FROM_CART_FAILED';
const UPDATE_CART_SUCCESS = 'UPDATE_CART_SUCCESS';
const UPDATE_FROM_CART_FAILED = 'UPDATE_FROM_CART_FAILED';
const UPDATE_EXISTING_CART_SUCCESS = 'UPDATE_CART_SUCCESS';
const UPDATE_EXISTING_FROM_CART_FAILED = 'UPDATE_FROM_CART_FAILED';
//Reducers

export default function (state = {cartItem:[],cartCount:'', cartSuceess:'',updateSuccess:'', removeSuccess:''}, action ){

    const {type,payload,countData} = action;

    switch(type){
        case ADD_TO_CART_SUCCESS:
            return { cartItem: payload,cartCount:countData, cartSuceess:true, updateSuccess:'', removeSuccess:'' };

        case REMOVE_FROM_CART_SUCCESS:
            return{cartItem: payload,cartCount:'', cartSuceess:'',updateSuccess:'',  removeSuccess:true};

        case UPDATE_CART_SUCCESS:
            return{cartItem: payload,cartCount:countData, cartSuceess:'',updateSuccess:true, removeSuccess:''}

        case UPDATE_EXISTING_CART_SUCCESS:
            return{cartItem: payload,cartCount:'',cartSuceess:true,updateSuccess:'', removeSuccess:''}

        case UPDATE_EXISTING_FROM_CART_FAILED:
            return{cartItem: payload,cartCount:'', cartSuceess:false,updateSuccess:'', removeSuccess:''}

        case ADD_TO_CART_FAILED:
            return{cartItem: payload,cartCount:'', cartSuceess:false,updateSuccess:'', removeSuccess:''}

        default:
            return state;

    }
}



//Actions

export const AddToCat = (detailsArray) => async(dispatch) => {
    try{
        let checkToken = await localStorage.getItem("token");
        if (checkToken) {
            const decoded = jwt_decode(checkToken);
            detailsArray.customerId = decoded.userid;
            detailsArray.TempcustomerId = ''
        }else{
            let tempToken = await localStorage.getItem("Temptoken");
            if(tempToken){
                detailsArray.TempcustomerId = tempToken
            }else{
                var result =  Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
                detailsArray.TempcustomerId = result
                localStorage.setItem("Temptoken", result);
            }
        }
        await axios.post(`${window.$URL}cart/addToCart`, detailsArray)
        .then(async (response) => {
            if(response.data.success){
                let checkToken = await localStorage.getItem("token");
                if(checkToken){
                    const decoded = jwt_decode(checkToken);
                    let customerID = decoded.userid;
                    axios.get(`${window.$URL}cart/fetchCart/`+customerID)
                    .then(response => {
                        var CartData = response.data
                        axios.get(`${window.$URL}cart/getCartCounts/${customerID}`)
                        .then(totalCartResponse => {
                            if(totalCartResponse.status === 200){
                                dispatch({
                                    type: ADD_TO_CART_SUCCESS,
                                    payload: CartData,
                                    countData: totalCartResponse.data.data
                                });
                            }
                        })
                        .catch(error => {
                            console.log(error.data)
                            dispatch({
                                type: ADD_TO_CART_FAILED
                            })
                        })
                    })
                    .catch(error => {
                        console.log(error.data)
                        dispatch({
                            type: ADD_TO_CART_FAILED
                        })
                    })
                }else{
                    let tempToken = await localStorage.getItem("Temptoken");
                    axios.get(`${window.$URL}cart/fetchwithoutCart/`+tempToken)
                    .then(response => {
                        var CartData = response.data
                        axios.get(`${window.$URL}cart/getCartWithOutCounts/${tempToken}`)
                        .then(totalCartResponse => {
                            if(totalCartResponse.status === 200){
                                dispatch({
                                    type: ADD_TO_CART_SUCCESS,
                                    payload: CartData,
                                    countData: totalCartResponse.data.data
                                });
                            }
                        })
                        .catch(error => {
                            console.log(error.data)
                            dispatch({
                                type: ADD_TO_CART_FAILED
                            })
                        })
                    })
                    .catch(error => {
                        console.log(error.data)
                        dispatch({
                            type: ADD_TO_CART_FAILED
                        })
                    })
                }   
            }else{
                dispatch({
                    type: ADD_TO_CART_FAILED
                    
                })
            }
        })
    }   
    catch(err){
        dispatch({
            type: ADD_TO_CART_FAILED
        })
    }
}


export const RemoveSelectedCart = (selectedProd) => async(dispatch) => {
    try{
        let checkToken = await localStorage.getItem("token");
        if (checkToken) {
            const decoded = jwt_decode(checkToken);
            let customerID = decoded.userid;
            const object = {
                selectedProdanother : JSON.stringify(selectedProd.selectedProduct),
                userId:customerID
            }
            await axios.post(`${window.$URL}cart/deleteselectedAll`, object)
            .then(response => {
                if(response.data.success){
                    axios.get(`${window.$URL}cart/fetchCart/`+customerID)
                    .then(response => {
                        var CartData = response.data
                        axios.get(`${window.$URL}cart/getCartCounts/${customerID}`)
                        .then(totalCartResponse => {
                            if(totalCartResponse.status === 200){
                                dispatch({
                                    type: REMOVE_FROM_CART_SUCCESS,
                                    payload: CartData,
                                    countData: totalCartResponse.data.data
                                });
                            }
                        })
                        .catch(error => {
                            console.log(error.data)
                            dispatch({
                                type: ADD_TO_CART_FAILED
                            })
                        })
                    })
                    .catch(error => {
                        console.log(error.data)
                        dispatch({
                            type: ADD_TO_CART_FAILED
                        })
                    })
                }
            })
            .catch(error => {
                console.log(error.data)
                dispatch({
                    type: ADD_TO_CART_FAILED
                })
            })
        }
    }
    catch(err){
        // toast.error('Add to Cart failed', {
        //     position: "bottom-center",
        //     autoClose: 3000,
        //     hideProgressBar: false,
        //     closeOnClick: true,
        //     pauseOnHover: true,
        //     draggable: true,
        //     progress: undefined,
        // });
        dispatch({
            type: REMOVE_FROM_CART_FAILED
        })
    }
    
}

export const RemoveSelectedWithOutCart = (selectedProd) => async(dispatch) => {
    try{
        let checkToken = await localStorage.getItem("Temptoken");
        if (checkToken) {
            const TempId = checkToken
            const object = {
                selectedProdanother : JSON.stringify(selectedProd.selectedProduct),
                tempId:TempId
            }
            await axios.post(`${window.$URL}cart/deleteselectedwithoutAll`, object)
            .then(response => {
                if(response.data.success){
                    axios.get(`${window.$URL}cart/fetchwithoutCart/`+TempId)
                    .then(response => {
                        var CartData = response.data
                        axios.get(`${window.$URL}cart/getCartWithOutCounts/${TempId}`)
                        .then(totalCartResponse => {
                            if(totalCartResponse.status === 200){
                                dispatch({
                                    type: REMOVE_FROM_CART_SUCCESS,
                                    payload: CartData,
                                    countData: totalCartResponse.data.data
                                });
                            }
                        })
                        .catch(error => {
                            console.log(error.data)
                            dispatch({
                                type: ADD_TO_CART_FAILED
                            })
                        })
                    })
                    .catch(error => {
                        console.log(error.data)
                        dispatch({
                            type: ADD_TO_CART_FAILED
                        })
                    })
                }
            })
            .catch(error => {
                console.log(error.data)
                dispatch({
                    type: ADD_TO_CART_FAILED
                })
            })
        }
    }
    catch(err){
        // toast.error('Add to Cart failed', {
        //     position: "bottom-center",
        //     autoClose: 3000,
        //     hideProgressBar: false,
        //     closeOnClick: true,
        //     pauseOnHover: true,
        //     draggable: true,
        //     progress: undefined,
        // });
        dispatch({
            type: REMOVE_FROM_CART_FAILED
        })
    }
}

export const RemoveCart = ({cartId, variantIndex }) => async(dispatch) => {
    try{
        let checkToken = await localStorage.getItem("token");
        if (checkToken) {
            const decoded = jwt_decode(checkToken);
            let customerID = decoded.userid;
            
            await axios.post(`${window.$URL}cart/deleteAll`, { customerID, cartId, variantIndex })
            .then(async (response) => {
                if(response.data.success){
                    // await axios.get(`${window.$URL}cart/getCartCounts/${customerID}`)
                    // .then(totalCartResponse => {
                    //     if(totalCartResponse.status === 200){
                    //         // toast.error(response.data.message, {
                    //         //     position: "bottom-center",
                    //         //     autoClose: 3000,
                    //         //     hideProgressBar: false,
                    //         //     closeOnClick: true,
                    //         //     pauseOnHover: true,
                    //         //     draggable: true,
                    //         //     progress: undefined,
                    //         // });
                    //         dispatch({
                    //             type: REMOVE_FROM_CART_SUCCESS,
                    //             payload: totalCartResponse.data.data
                    //         })
                    //     }
                    // })

                    axios.get(`${window.$URL}cart/fetchCart/`+customerID)
                    .then(response => {
                        var CartData = response.data
                        axios.get(`${window.$URL}cart/getCartCounts/${customerID}`)
                        .then(totalCartResponse => {
                            if(totalCartResponse.status === 200){
                                dispatch({
                                    type: REMOVE_FROM_CART_SUCCESS,
                                    payload: CartData,
                                    countData: totalCartResponse.data.data
                                });
                            }
                        })
                        .catch(error => {
                            console.log(error.data)
                            dispatch({
                                type: ADD_TO_CART_FAILED
                            })
                        })
                    })
                    .catch(error => {
                        console.log(error.data)
                        dispatch({
                            type: ADD_TO_CART_FAILED
                        })
                    })
                }else{
                    // toast.error(response.data.message, {
                    //     position: "bottom-center",
                    //     autoClose: 3000,
                    //     hideProgressBar: false,
                    //     closeOnClick: true,
                    //     pauseOnHover: true,
                    //     draggable: true,
                    //     progress: undefined,
                    // });
                }
            })
        }else{
            dispatch({
                type: REMOVE_FROM_CART_SUCCESS,
                payload: []
            })
           // window.location.href = `${process.env.PUBLIC_URL}/login`
        }
    }   
    catch(err){
        // toast.error('Add to Cart failed', {
        //     position: "bottom-center",
        //     autoClose: 3000,
        //     hideProgressBar: false,
        //     closeOnClick: true,
        //     pauseOnHover: true,
        //     draggable: true,
        //     progress: undefined,
        // });
        dispatch({
            type: REMOVE_FROM_CART_FAILED
        })
    }
}

export const RemoveWithOutCart = ({cartId, variantIndex }) => async(dispatch) => {
    try{
        let checkToken = await localStorage.getItem("Temptoken");
        if (checkToken) {
            const TempId = checkToken
            await axios.post(`${window.$URL}cart/deleteAllTemp`, { TempId, cartId, variantIndex })
            .then(async (response) => {
                if(response.data.success){
                    // await axios.get(`${window.$URL}cart/getCartWithOutCounts/${TempId}`)
                    // .then(totalCartResponse => {
                    //     if(totalCartResponse.status === 200){
                    //         // toast.error(response.data.message, {
                    //         //     position: "bottom-center",
                    //         //     autoClose: 3000,
                    //         //     hideProgressBar: false,
                    //         //     closeOnClick: true,
                    //         //     pauseOnHover: true,
                    //         //     draggable: true,
                    //         //     progress: undefined,
                    //         // });
                    //         dispatch({
                    //             type: REMOVE_FROM_CART_SUCCESS,
                    //             payload: totalCartResponse.data.data
                    //         })
                    //     }
                    // })

                    axios.get(`${window.$URL}cart/fetchwithoutCart/`+TempId)
                    .then(response => {
                        var CartData = response.data
                        axios.get(`${window.$URL}cart/getCartWithOutCounts/${TempId}`)
                        .then(totalCartResponse => {
                            if(totalCartResponse.status === 200){
                                dispatch({
                                    type: REMOVE_FROM_CART_SUCCESS,
                                    payload: CartData,
                                    countData: totalCartResponse.data.data
                                });
                            }
                        })
                        .catch(error => {
                            console.log(error.data)
                            dispatch({
                                type: ADD_TO_CART_FAILED
                            })
                        })
                    })
                    .catch(error => {
                        console.log(error.data)
                        dispatch({
                            type: ADD_TO_CART_FAILED
                        })
                    })
                }else{
                    // toast.error(response.data.message, {
                    //     position: "bottom-center",
                    //     autoClose: 3000,
                    //     hideProgressBar: false,
                    //     closeOnClick: true,
                    //     pauseOnHover: true,
                    //     draggable: true,
                    //     progress: undefined,
                    // });
                }
            })
        }else{
            dispatch({
                type: REMOVE_FROM_CART_SUCCESS,
                payload: []
            })
           // window.location.href = `${process.env.PUBLIC_URL}/login`
        }
    }   
    catch(err){
        // toast.error('Add to Cart failed', {
        //     position: "bottom-center",
        //     autoClose: 3000,
        //     hideProgressBar: false,
        //     closeOnClick: true,
        //     pauseOnHover: true,
        //     draggable: true,
        //     progress: undefined,
        // });
        dispatch({
            type: REMOVE_FROM_CART_FAILED
        })
    }
}

export const UpdateCart = () => async(dispatch) => {
    try{
        let checkToken = await localStorage.getItem("token");
        if (checkToken) {
            const decoded = jwt_decode(checkToken);
            let customerID = decoded.userid;
            // await axios.get(`${window.$URL}cart/getCartCounts/${customerID}`)
            // .then(totalCartResponse => {
            //     if(totalCartResponse.status === 200){
            //         dispatch({
            //             type: UPDATE_CART_SUCCESS,
            //             payload: totalCartResponse.data.data
            //         })
            //     }
            // })

            await axios.get(`${window.$URL}cart/fetchCart/`+customerID)
            .then(response => {
                var CartData = response.data
                axios.get(`${window.$URL}cart/getCartCounts/${customerID}`)
                .then(totalCartResponse => {
                    if(totalCartResponse.status === 200){
                        dispatch({
                            type: UPDATE_CART_SUCCESS,
                            payload: CartData,
                            countData: totalCartResponse.data.data
                        });
                    }
                })
                .catch(error => {
                    console.log(error.data)
                    dispatch({
                        type: ADD_TO_CART_FAILED
                    })
                })
            })
            .catch(error => {
                console.log(error.data)
                dispatch({
                    type: ADD_TO_CART_FAILED
                })
            })
        }else{
           // window.location.href = `${process.env.PUBLIC_URL}/login`
        }
    }   
    catch(err){
        dispatch({
            type: UPDATE_FROM_CART_FAILED
        })
    }
}


export const UpdateWithOutCart = () => async(dispatch) =>{
    try{
        let tempToken = await localStorage.getItem("Temptoken");
        if (tempToken) {
           
            // await axios.get(`${window.$URL}cart/getCartWithOutCounts/${tempToken}`)
            // .then(totalCartResponse => {
            //     if(totalCartResponse.status === 200){
            //         dispatch({
            //             type: UPDATE_CART_SUCCESS,
            //             payload: totalCartResponse.data.data
            //         })
            //     }
            // })

            axios.get(`${window.$URL}cart/fetchwithoutCart/`+tempToken)
            .then(response => {
                var CartData = response.data
                axios.get(`${window.$URL}cart/getCartWithOutCounts/${tempToken}`)
                .then(totalCartResponse => {
                    if(totalCartResponse.status === 200){
                        dispatch({
                            type: UPDATE_CART_SUCCESS,
                            payload: CartData,
                            countData: totalCartResponse.data.data
                        });
                    }
                })
                .catch(error => {
                    console.log(error.data)
                    dispatch({
                        type: ADD_TO_CART_FAILED
                    })
                })
            })
            .catch(error => {
                console.log(error.data)
                dispatch({
                    type: ADD_TO_CART_FAILED
                })
            })
        }else{
           // window.location.href = `${process.env.PUBLIC_URL}/login`
        }
    }   
    catch(err){
        dispatch({
            type: UPDATE_FROM_CART_FAILED
        })
    }
}

export const UpdateExistingCart = (existingCartProducts) => async(dispatch) => {
    try{
        let checkToken = await localStorage.getItem("token");
        if (checkToken) {
            const decoded = jwt_decode(checkToken);
            let customerID = decoded.userid;
            
            for (let i=0; i<=existingCartProducts.length-1; i++){
                existingCartProducts[i].customerId = customerID;
                await axios.post(`${window.$URL}cart/addToCart`, existingCartProducts[i])
                .then(async (response) => {
                    if(response.data.success){
                        dispatch({
                            type: UPDATE_EXISTING_CART_SUCCESS,
                            payload: response.data.data
                        });
                    }else{
                        // toast.error(response.data.message, {
                        //     position: "bottom-center",
                        //     autoClose: 3000,
                        //     hideProgressBar: false,
                        //     closeOnClick: true,
                        //     pauseOnHover: true,
                        //     draggable: true,
                        //     progress: undefined,
                        // });
                    }
                })
            }
        }
    }   
    catch(err){
        dispatch({
            type: UPDATE_EXISTING_FROM_CART_FAILED
        })
    }
}
export const UpdateloginCart = () => async (dispatch) => {
    let checkToken = await localStorage.getItem("token");
    const decoded = jwt_decode(checkToken);
    let customerID = decoded.userid;
    let TempToken = await localStorage.getItem("Temptoken");

    const object ={
        customer : customerID,
        tempToken : TempToken
    }
    await axios.post(`${window.$URL}cart/updateLoginCart`, object)
    .then(response => {
        if(response.data.success){
             axios.get(`${window.$URL}cart/fetchCart/`+customerID)
            .then(response => {
                var CartData = response.data
                axios.get(`${window.$URL}cart/getCartCounts/${customerID}`)
                .then(totalCartResponse => {
                    if(totalCartResponse.status === 200){
                        dispatch({
                            type: ADD_TO_CART_SUCCESS,
                            payload: CartData,
                            countData: totalCartResponse.data.data
                        });
                    }
                })
                .catch(error => {
                    console.log(error.data)
                    dispatch({
                        type: ADD_TO_CART_FAILED
                    })
                })
            })
            .catch(error => {
                console.log(error.data)
                dispatch({
                    type: ADD_TO_CART_FAILED
                })
            })

            // axios.get(`${window.$URL}cart/getCartCounts/${customerID}`)
            // .then(totalCartResponse => {
            //     if(totalCartResponse.status === 200){
            //         localStorage.removeItem("Temptoken");
            //         dispatch({
            //             type: UPDATE_CART_SUCCESS,
            //             payload: totalCartResponse.data.data
            //         })
            //     }
            // })
        }
    })
    .catch(error => {
        console.log(error.data)
    })
    
}
export const UpdatelogoutCart = () => async (dispatch) => {
    dispatch({
        type: ADD_TO_CART_SUCCESS,
        payload: [],
        countData: 0
    });
}
