import { toast } from 'react-toastify';
//Actions Types
const ADD_TO_LOCATION_SUCCESS = 'ADD_TO_LOCATION_SUCCESS';
const ADD_TO_LOCATION_FAILED = 'ADD_TO_LOCATION_FAILED';
const REMOVE_FROM_LOCATION_SUCCESS = 'REMOVE_FROM_LOCATION_SUCCESS';
const REMOVE_FROM_LOCATION_FAILED = 'REMOVE_FROM_LOCATION_FAILED';
const UPDATE_LOCATION_SUCCESS = 'UPDATE_LOCATION_SUCCESS';
const UPDATE_FROM_LOCATION_FAILED = 'UPDATE_FROM_LOCATION_FAILED';

//Reducers

export default function (state = {locationCoordinates:[], locationSuceess:'', removeSuccess:''}, action ){

    const {type,payload} = action;

    switch(type){
        case ADD_TO_LOCATION_SUCCESS:
            return {locationCoordinates: payload, locationSuceess:true, removeSuccess:'' };

        case REMOVE_FROM_LOCATION_SUCCESS:
            return{locationCoordinates: payload, locationSuceess:true, removeSuccess:'true'};

        case UPDATE_LOCATION_SUCCESS:
            return{locationCoordinates: payload, locationSuceess:true, removeSuccess:''}

        case ADD_TO_LOCATION_FAILED:
            return{locationCoordinates: payload, locationSuceess:false, removeSuccess:''}

        default:
            return state;

    }
}
//Actions

export const AddToLocation = (detailsArray) => async(dispatch) => {
    try{ 
        toast.success('Location Received Successfully', {
            position: "bottom-center",
            autoClose: 3000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        });
        dispatch({
            type: ADD_TO_LOCATION_SUCCESS,
            payload: detailsArray
        });
    }catch(err){
        toast.error('Location Receive Failed', {
            position: "bottom-center",
            autoClose: 3000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        });
        dispatch({
            type: ADD_TO_LOCATION_FAILED
        })
    }
}

export const RemoveLocation = () => async(dispatch) => {
    try{
        dispatch({
            type: REMOVE_FROM_LOCATION_SUCCESS,
            payload: []
        })
    }catch(err){
        dispatch({
            type: REMOVE_FROM_LOCATION_FAILED
        })
    }
}

export const UpdateLocation = (detailsArray) => async(dispatch) => {
    try{
        toast.success('Location Updated Successfully', {
            position: "bottom-center",
            autoClose: 3000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        });
        dispatch({
            type: UPDATE_LOCATION_SUCCESS,
            payload: detailsArray
        })
    }catch(err){
        toast.error('Location Update Failed', {
            position: "bottom-center",
            autoClose: 3000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        });
        dispatch({
            type: UPDATE_FROM_LOCATION_FAILED
        })
    }
}