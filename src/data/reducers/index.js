import {combineReducers} from 'redux';
import {persistReducer} from 'redux-persist';
import storage from 'redux-persist/lib/storage'
import Cart from './cart';
import Location from './location';

const persistConfig = {
    key:'root',
    storage,
    whitelist:['Cart','Location']
}

const rootReducer = combineReducers({
    Cart,
    Location,
});


export default persistReducer(persistConfig,rootReducer)