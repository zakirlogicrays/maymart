import React, { Component } from 'react';
import cart_icon from "../../assets/images/cart_icon.svg";
import Loader from "../../Component/Loader";
import { AddToCat } from '../../data/reducers/cart';
import { connect } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import moment from 'moment';

class ProductVariantListing extends Component {

    constructor(props) {
        super(props);
        this.state = {
            wishListedArray: [],
            loggedIn: false
        }
    }

    addToCart = async (produtArray) => {
        let addToCartData = {
            productVariantId: produtArray._id,
            quantity: 1,
            productId: produtArray.sellerProductId._id,
            sellerId: produtArray.sellerProductId.sellerId,
            brand: produtArray.sellerProductId.brand
        }
        this.props.AddToCat(addToCartData);
    }
    getDateValid = (prodArray) => {
        let returnValidate = false;
        if(prodArray.startDate !== "" && prodArray.endDate !== ""){
            let convertStartDate = moment(prodArray.startDate).unix() * 1000;
            let convertEndDate = moment(prodArray.endDate).unix() * 1000;
            let systemTime = moment(new Date()).unix() * 1000;
            if(systemTime <= convertEndDate && systemTime >= convertStartDate){
            returnValidate = true;
            }
        }
        return returnValidate;
    }
    render() {
        const { productArray, loading } = this.props;
        if (loading) {
            return (
                <Loader />
            )
        } else {
            return (
                <div className="product-container">
                    <ToastContainer />
                    <div className="row">
                        {
                            productArray !== null && productArray.couponAppliedToProducts.length > 0 ?
                                productArray.couponAppliedToProducts.map((prodData, prodIndex) => {
                                    var imageData;
                                    // console.log(prodData)
                                    if (prodData.sellerProductId.primaryProductImage === undefined || prodData.sellerProductId.productImageId[0] === undefined) {
                                        imageData = `${window.$ImageURL}no-image-480x480.png`
                                    } else if (prodData.sellerProductId.primaryProductImage !== null && prodData.sellerProductId.primaryProductImage !== undefined) {
                                        imageData = `${window.$ImageURL}` + prodData.sellerProductId.primaryProductImage.originalURL
                                    } else {
                                        imageData = `${window.$ImageURL}` + prodData.sellerProductId.productImageId[0].originalURL
                                    }
                                    if (prodData.sellerProductId) {
                                        return (
                                            <div className="col-lg-3 col-md-4 col-sm-12 col-xs-12" style={{ marginTop: 30 }} key={prodIndex}>
                                                <div className="card product-card no-qty">
                                                    {
                                                        prodData.specialPriceProvided === true && this.getDateValid(prodData) ?
                                                            <div className="offer-area">
                                                                <span>Offer</span>
                                                            </div>
                                                            :
                                                            null
                                                    }
                                                    <a href={`${process.env.PUBLIC_URL}/productDetails/${prodData.sellerProductId._id}`} className="product-image">
                                                        <img src={imageData} className="img-responsive" alt="homepageImages" style={{ height: '100%' }} />
                                                    </a>
                                                    <div className="iner-container">

                                                        <div className="price-with-fav">
                                                            {
                                                                prodData.specialPriceProvided === true && this.getDateValid(prodData) ?
                                                                    <h4 className="offer_quote" > {prodData.specialPriceDiscount}% Off </h4>
                                                                    :
                                                                    null
                                                            }
                                                            <button type="button" className="favorite_button" style={{ position: 'absolute', background: '#fff' }}>
                                                                {/* {wishListedArray.includes(prodData.sellerProductVariantId[0]._id)
                                                                    ? <i className="fa fa-heart" onClick={() => this.addWishList(prodData.sellerProductVariantId[0]._id, false)} aria-hidden="true" />
                                                                    : <i className="fa fa-heart-o" onClick={() => this.addWishList(prodData.sellerProductVariantId[0]._id, true)} aria-hidden="true"></i>
                                                                } */}

                                                                {/*<i className="fa fa-heart" aria-hidden="true"></i>*/}
                                                            </button>
                                                        </div>
                                                        {/* {this.fetchVariationProduct(prodData.sellerProductVariantId, prodData.productType)} */}
                                                        {
                                                            prodData.sellerProductId.productName !== null ?
                                                                <h5 className="card-title">
                                                                    <a href={`${process.env.PUBLIC_URL}/productDetails/${prodData.sellerProductId._id}`}>{prodData.sellerProductId.productName.length > 20 ? prodData.sellerProductId.productName.substring(0, 20) + '...' : prodData.sellerProductId.productName}</a>
                                                                    {/* <a href={`${process.env.PUBLIC_URL}/productDetails/${prodData.sellerProductId._id}`}>{prodData.sellerProductId.productName}</a> */}
                                                                </h5>
                                                                :
                                                                null
                                                        }
                                                        {
                                                            prodData !== null ?

                                                                prodData.sellerProductId.productType === "grocery" ?
                                                                    <h6 className="qty">{prodData.packSize} {prodData.packSizeMeasureUnit}</h6>
                                                                    :
                                                                    <>
                                                                        {
                                                                            prodData.attributeLabels[0] !== undefined ?
                                                                                <h6 className="qty" style={{ margin: 0, fontSize: 14 }}>{prodData.attributeLabels[0] !== undefined ? prodData.attributeLabels[0].length > 15 ? prodData.attributeLabels[0].substring(0, 15) + '...' : prodData.attributeLabels[0] : null} <span className=" dimention" style={{ margin: 0 }}>
                                                                                    {prodData.attributeValues.map((eachValue, eachValueIndex) => {
                                                                                        return (
                                                                                            <span key={eachValueIndex}>{eachValue}{eachValueIndex !== prodData.attributeValues[0].length - 1 ? ' , ' : null}</span>
                                                                                        )
                                                                                    })}
                                                                                </span></h6>
                                                                                :
                                                                                null
                                                                        }
                                                                    </>
                                                                :
                                                                null
                                                        }

                                                        <p className="priceTag"> QAR {parseFloat(prodData.sellerPrice).toFixed(2)} </p>
                                                        {/*<div className="rating">
                                                            <img src="./images/Full-Star-icon.svg" alt="homepageImages" />
                                                            <img src="./images/Full-Star-icon.svg" alt="homepageImages" />
                                                            <img src="./images/Full-Star-icon.svg" alt="homepageImages" />
                                                            <img src="./images/Full-Star-icon.svg" alt="homepageImages" />
                                                            <img src="./images/Full-Star-icon.svg" alt="homepageImages" />
                                                            <span className="totalUser">(11201)</span>
                                                        </div>*/}
                                                        {
                                                            prodData.sellerProductId.productType === "grocery" ?
                                                                <div className="action-button">
                                                                    <a href="#/" onClick={() => this.addToCart(prodData)} className="btn cart_new"><img src={cart_icon} alt={cart_icon} className="img-responsive" />
                                                                        <span> Add to cart </span></a>
                                                                </div>
                                                                :
                                                                null
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                        )
                                    } else {
                                        return null;
                                    }
                                })
                                :
                                <div className="col-md-12 col-sm-12 flex-screen">
                                    <div className="card product-card" style={{ padding: '30px' }}>
                                        <h3 style={{ textAlign: 'center' }}>No Product Found</h3>
                                    </div>
                                </div>
                        }
                    </div>
                </div>
            )
        }
    }
}
const mapStateToProps = (state) => ({
    CartProducts: state.Cart,
})

export default connect(
    mapStateToProps,
    {  AddToCat }
)(ProductVariantListing);
