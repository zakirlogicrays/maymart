import React, { Component } from 'react';
import LoginGoogle from 'react-google-login';
import axios from "axios";
import jwt_decode from "jwt-decode";
import { connect } from 'react-redux';
import { AddToCat, UpdateCart } from '../../data/reducers/cart';

class GoogleLogin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            successMessage: '',
            success: false,
            errorMessage: '',
            error: false
        };
    }
    signup = async (res) => {
        console.log(res.error)
        if (res.error) {
            console.log(res.error)
        } else {

            let registerData = {
                "fname": res.profileObj.givenName,
                "lname": res.profileObj.familyName,
                "email": res.profileObj.email,
                "UserType": "customer",
                "providerType": "google"
            };
            console.log(registerData);

            await axios.post(`${window.$URL}user/customerGooglesignup`, registerData)
                .then(response => {
                    if (response.status === 200) {
                        if (response.data.success) {
                            this.setState({
                                success: true,
                                successMessage: response.data.message
                            }, async () => {
                                localStorage.setItem("token", response.data.token);

                                const decoded = jwt_decode(response.data.token);
                                let lastVisit = await localStorage.getItem("lastVisited");
                                let lastVisitParse = JSON.parse(lastVisit);
                                if (lastVisitParse) {
                                    lastVisitParse.customerId = decoded.userid;
                                    this.addToCart(lastVisitParse);
                                } else {
                                    this.props.UpdateCart();
                                    setTimeout(() => {
                                        window.location.href = `${process.env.PUBLIC_URL}/`;
                                    }, 1000)
                                }
                            })
                        } else {
                            this.setState({
                                error: true,
                                errorMessage: response.data.message
                            })
                        }
                    }
                })
                .catch(error => {
                    console.log(error);
                })
        }
    }

    addToCart = async (lastVisitParse) => {
        this.props.AddToCat(lastVisitParse);
    }

    render() {
        const responseGoogle = (response) => {
            console.log(response);
            //var res = response.profileObj;
            //console.log(res);
            // debugger;
            this.signup(response);
        }
        return (
            <>
                <LoginGoogle
                    clientId="274718621768-15rtskdkkdfmvbefvttmuq6dvmv0vh6q.apps.googleusercontent.com"
                    buttonText="Google"
                    render={renderProps => (
                        <button onClick={renderProps.onClick} className="btn google-plus" ><i className="fa fa-google-plus" /> Google</button>
                    )}
                    onSuccess={responseGoogle}
                    onFailure={responseGoogle} ></LoginGoogle>
            </>
        );
    }
}


const mapStateToProps = (state) => ({
    CartProducts: state.Cart,
})

export default connect(
    mapStateToProps,
    { AddToCat, UpdateCart }
)(GoogleLogin);

