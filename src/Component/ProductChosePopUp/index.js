import React, { Component } from 'react';
import { connect } from 'react-redux';
import { AddToLocation } from '../../data/reducers/location';
import { Carousel } from 'react-responsive-carousel';
import AmulButter from '../../assets/images/amul-butter-popup-img.png';

class ProductChosePopUp extends Component {
    state = {
        firstPage: 2
    }



    saveDefaultAddress = () => {
        this.props.hideLocationModal();
    }


    render() {
        const { modalStatus, imageData, chosenData } = this.props;
        return (
            <div 
              className={modalStatus ? "modal fade show" : "modal fade"} 
              style={modalStatus ? { display: 'block' } : { display: 'none' }} id="exampleModal" tabIndex="-1" role="dialog" 
              aria-labelledby="exampleModalLabel" 
              aria-hidden="true"
              onClick={this.saveDefaultAddress}
            >
                <div 
                   onClick={e => {
                        e.stopPropagation();
                    }} 
                    className="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable home-product-img-modal-main"
                >
                    <div className="modal-content address-popup">
                        {
                            window.location.pathname.split("/")[4] === 'grocery' ?
                                null
                                :
                                <div className="modal-header">
                                    <h5 className="modal-title">{this.props.productName}</h5>
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.saveDefaultAddress}>
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                        }
                        <div className="modal-body" style={{display: "flex", alignItems: "center"}}>
                            <Carousel
                                // autoPlay
                                autoFocus={true}
                                infiniteLoop
                                showStatus={false}
                                showIndicators={imageData?.length > 1 ? true : false}
                                key={imageData?.length > 1 ? true : false}
                                isSelected={0}
                            >
                                { imageData?.map((item) => (
                                    <div style={{transform: `translate3d(-${this.state.firstPage}%, 0px, 0px)`}}>
                                        <img style={{width: "50%", marginBottom: 10, marginTop: 10}} src={`https://mymart.qa:7779/uploads/${item.originalURL}`} alt={AmulButter} />
                                    </div>
                                ))}
                            </Carousel>
                        </div>
                        <div className="modal-footer location-footer" style={{transform: "translate3d(-0%, 0px, 0px)"}} onClick={() => window.location.href = `${process.env.PUBLIC_URL}/storeDetails/${chosenData?._id}`}>
                            <button type="button" data-dismiss="modal" aria-label="Close" onClick={this.saveDefaultAddress} className="btn">Go to Products Details</button>
                        </div>

                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    location: state.Location,
})

export default connect(
    mapStateToProps,
    { AddToLocation }
)(ProductChosePopUp);