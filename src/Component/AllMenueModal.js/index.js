import React, { Component } from 'react';
import { connect } from 'react-redux';
import { AddToLocation } from '../../data/reducers/location';
import MultiCarousel from 'react-multi-carousel';


import Category1 from "../../assets/images/grocery 1.svg";
import Category2 from "../../assets/images/All-categorys.svg";
import Category3 from "../../assets/images/Maymall.svg";
import Category4 from "../../assets/images/Promotions.svg";
import Category5 from "../../assets/images/Vouchers.svg";
import Category6 from "../../assets/images/Scheduled Cart.svg";
import Category7 from "../../assets/images/Partner Store.svg";
import Category8 from "../../assets/images/All Menu.svg";

const responsiveCategory = {
    superLargeDesktop: {
        // the naming can be any, depends on you.
        breakpoint: { max: 4000, min: 3000 },
        items: 8
    },
    desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 7
    },
    tablet: {
        breakpoint: { max: 1024, min: 575 },
        items: 4.5
    },
    mobile: {
        breakpoint: { max: 576, min: 0 },
        items: 4,
    }
};

class AllMenueModal extends Component {
    state = {

    }



    saveDefaultAddress = () => {
        this.props.hideLocationModal();
    }


    render() {
        const { modalStatus } = this.props;
        return (
            <div className={modalStatus ? "modal fade show" : "modal fade"} style={modalStatus ? { display: 'block' } : { display: 'none' }} id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable home-grocery-modal-main">
                    <div className="modal-content address-popup">
                        {
                            window.location.pathname.split("/")[4] === 'grocery' ?
                                null
                                :
                                <div className="modal-header">
                                    <h5 className="modal-title">All Menu</h5>
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.saveDefaultAddress}>
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                        }

                        <div className="modal-body home-grocery-modal">
                            <div className="home-grocery">
                                <MultiCarousel responsive={responsiveCategory}>
                                    <a href="#">
                                        <div className="home-category-box">
                                            <div className="home-category-box-img">
                                                <img src={Category1} alt={Category1} />
                                            </div>
                                            <p>Grocery</p>
                                        </div>
                                    </a>
                                    <a href="#">
                                        <div className="home-category-box">
                                            <div className="home-category-box-img">
                                                <img src={Category3} alt={Category3} />
                                            </div>
                                            <p>MyMall</p>
                                        </div>
                                    </a>
                                    <a href="#">
                                        <div className="home-category-box">
                                            <div className="home-category-box-img">
                                                <img src={Category2} alt={Category2} />
                                            </div>
                                            <p>All Categories</p>
                                        </div>
                                    </a>
                                    <a href="#">
                                        <div className="home-category-box">
                                            <div className="home-category-box-img">
                                                <img src={Category4} alt={Category4} />
                                            </div>
                                            <p>Promotions</p>
                                        </div>
                                    </a>
                                    <a href="#">
                                        <div className="home-category-box">
                                            <div className="home-category-box-img">
                                                <img src={Category5} alt={Category5} />
                                            </div>
                                            <p>Vouchers</p>
                                        </div>
                                    </a>
                                    <a href="#">
                                        <div className="home-category-box">
                                            <div className="home-category-box-img">
                                                <img src={Category6} alt={Category6} />
                                            </div>
                                            <p>Scheduled Cart</p>
                                        </div>
                                    </a>
                                    <a href="#">
                                        <div className="home-category-box">
                                            <div className="home-category-box-img" onClick={() => window.location.href = `${process.env.PUBLIC_URL}/categories`}>
                                                <img src={Category7} alt={Category7} />
                                            </div>
                                            <p>Partner Store</p>
                                        </div>
                                    </a>
                                    {/* <a href="#">
                                        <div className="home-category-box">
                                            <div className="home-category-box-img">
                                                <img src={Category8} alt={Category8} />
                                            </div>
                                            <p>All Menu</p>
                                        </div>
                                    </a> */}
                                </MultiCarousel>
                            </div>
                        </div>
                        <div className="modal-footer">
                           <button type="button" data-dismiss="modal" aria-label="Close" onClick={this.saveDefaultAddress} className="btn">Close</button>
                        </div>

                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    location: state.Location,
})

export default connect(
    mapStateToProps,
    { AddToLocation }
)(AllMenueModal);