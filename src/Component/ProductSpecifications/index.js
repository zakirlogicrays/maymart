import React, {Component} from 'react';
import chatIcon from "../../assets/images/chat-store.png";

export default class ProductSpecification extends Component{

    createMarkup = (html) => {
        return {
            __html: html
        };
    };
    
    render(){
        const {productDetailsArray} = this.props;
        return(
            <section className="payment-section-wrapper m-0">
                    <div className="payment-inner-wr">
                        <div className="row">
                            <div className="col-lg-6 col-md-6 col-sm-12">
                                <div className="p-d-inner-wr">
                                    <h1 className="pd-title">Product Attributes</h1>
                                    <div className="list-types" dangerouslySetInnerHTML={this.createMarkup(productDetailsArray.sellerProductId.productDescription)} />

                                    <h1 className="pd-title">About Product</h1>
                                    <p className="list-types">{productDetailsArray.sellerProductId.aboutProduct}</p>
                                </div>
                            </div>
                            <div className="col-lg-6 col-md-6 col-sm-12">
                                <div className="p-d-inner-wr">
                                    <h1 className="pd-title">What's in the Box</h1>
                                    <p className="list-types">{productDetailsArray.sellerProductId.highlights}</p>

                                    <div className="seller-details">
                                        {
                                            productDetailsArray.sellerProductId.sellerId !== null && productDetailsArray.sellerProductId.sellerId.SellerDetails.storeLogo !== null && productDetailsArray.sellerProductId.sellerId.SellerDetails.storeLogo !== undefined && productDetailsArray.sellerProductId.sellerId.SellerDetails.storeLogo.length > 0 ?
                                                <>
                                                    <p className="sold_by">Sold by</p>
                                                    <div className="seller-logo">
                                                        <img alt="imageName" src={`${window.$ImageURL}` + productDetailsArray.sellerProductId.sellerId.SellerDetails.storeLogo.originalURL} />
                                                    </div>
                                                </>
                                                :
                                                null
                                        }

                                        <div className="seller-chat">
                                            <a href={`${process.env.PUBLIC_URL}/messagedetails/${productDetailsArray.sellerProductId.sellerId._id}`} className="btn chat"> <img alt="imageName" src={chatIcon} />Chat</a>
                                            {
                                                productDetailsArray.sellerProductId.sellerId !== null ?
                                                    <a href={`${process.env.PUBLIC_URL}/storeDetails/${productDetailsArray.sellerProductId.sellerId._id}`} className="btn store"> Visit Store</a>
                                                    :
                                                    null
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </section>
        )
    }
}