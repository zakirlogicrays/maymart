import React, { Component } from 'react';
import cart_icon from "../../assets/images/cart_icon.svg";
import RecurringModal from "../../Component/RecurringModal";
import { AddToCat } from '../../data/reducers/cart';
import { connect } from 'react-redux';

import { ToastContainer,toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import moment from 'moment';

const noPreview = "http://nodeserver.mydevfactory.com:7779/uploads/no-image-480x480.png";

class ProductDetailsContent extends Component {
    state = {
        labelsArray: [],
        valuesArray: [],
        recurringModalStatus: false,
        buyNow: false
    }

    componentDidMount(){
        const { productDetailsArray, variationsArray, AttributesArray } = this.props;
        let valuesOptionArray = [];
        let labelsArray = [];
        if(productDetailsArray !== null && productDetailsArray.length > 0 && productDetailsArray.sellerProductId.productType === 'grocery'){ 
            labelsArray.push('Pack Size')
            let innerValueArray = [];
            let innerValueDetailsArray = [];
            for(let i=0; i<= variationsArray.length-1; i++){
                if(!innerValueArray.includes(variationsArray[i].packSize)){
                    innerValueArray.push(variationsArray[i].packSize);
                        innerValueDetailsArray.push({
                            _id: variationsArray[i]._id,
                            value: variationsArray[i].packSize +' '+ variationsArray[i].packSizeMeasureUnit,
                            image: noPreview
                        })
                }
            } 
            valuesOptionArray.push(innerValueDetailsArray);
        }else{
            for(let i=0; i<= AttributesArray.length-1; i++){
                let innerValueArray = [];
                let innerValueDetailsArray = [];
                for(let j=0; j<= variationsArray.length-1; j++){
                    if(!innerValueArray.includes(variationsArray[j].attributeValues[i])){
                        innerValueArray.push(variationsArray[j].attributeValues[i]);
                        innerValueDetailsArray.push({
                            _id: variationsArray[j]._id,
                            value: variationsArray[j].attributeValues[i],
                            image: variationsArray[j].variationImageId !== undefined && variationsArray[j].variationImageId.length > 0 ? variationsArray[j].variationImageId[0].originalURL : noPreview
                        })
                    }
                }
                valuesOptionArray.push(innerValueDetailsArray);
            }
        }

        this.setState({
            labelsArray: labelsArray,
            valuesArray: valuesOptionArray,
        })
    }

    addToCart = async (e) => {
        e.preventDefault();
        const { productDetailsArray, qty } = this.props;
        let addToCartData = {
            productVariantId: productDetailsArray._id,
            quantity: qty,
            productId: productDetailsArray.sellerProductId._id,
            sellerId: productDetailsArray.sellerProductId.sellerId._id,
            brand: productDetailsArray.sellerProductId.brand
        }
        this.props.AddToCat(addToCartData);
    }

    buyNow = async (e) => {
        e.preventDefault();
        const { productDetailsArray, qty } = this.props;
        let addToCartData = {
            productVariantId: productDetailsArray._id,
            quantity: qty,
            productId: productDetailsArray.sellerProductId._id,
            sellerId: productDetailsArray.sellerProductId.sellerId._id,
            brand: productDetailsArray.sellerProductId.brand
        }
        this.setState({
            buynow: true
        },() => {
            this.props.AddToCat(addToCartData);
        })
    }

    quantitySubtract = () => {
        this.props.quantitySubtractProps()
    }

    quantityAdd  = () => {
        this.props.quantityAddProps()
    }

    componentDidUpdate(prevProps) {
        if (this.props.CartProducts.cartItem !== prevProps.CartProducts.cartItem) { 
            if(this.props.CartProducts.cartSuceess){
                // this.getToast('Successfully Added To The Cart')
                toast.success('Product added to cart Successfully', {
                    position: "bottom-center",
                    autoClose: 3000,
                    hideProgressBar: true,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });
            }
            // if(this.state.buyNow){
            //     window.location.href = `${process.env.PUBLIC_URL}/cart`
            // }
        }
    }
    getDateValid = (prodArray) => {
        let returnValidate = false;
        if(prodArray.startDate !== "" && prodArray.endDate !== ""){
            let convertStartDate = moment(prodArray.startDate).unix() * 1000;
            let convertEndDate = moment(prodArray.endDate).unix() * 1000;
            let systemTime = moment(new Date()).unix() * 1000;
            if(systemTime <= convertEndDate && systemTime >= convertStartDate){
            returnValidate = true;
            }
        }
        return returnValidate;
    }
    render() {
        const {productDetailsArray,qty, productVariantId} = this.props;
        const {labelsArray,valuesArray} = this.state;

        if(productDetailsArray !== null){
            return (
                <div className="product-more-details">
                    <ToastContainer />
                    <div className="price-estimate">
                        <div className="price-bar">
                            <div className="price-content">
                                {
                                    productDetailsArray.specialPriceProvided && this.getDateValid(productDetailsArray) ?
                                        <>
                                            <h2>qar {productDetailsArray.specialPrice.toFixed(2)}</h2>
                                            <p> <span>qar {productDetailsArray.Price.toFixed(2)}</span> {'-' + parseInt(((productDetailsArray.Price - productDetailsArray.specialPrice) / productDetailsArray.Price) * 100)}% off</p>
                                        </>
                                        :
                                        <h2>qar {productDetailsArray.Price.toFixed(2)}</h2>
                                }
                            </div>
                        </div>
                        {
                            productDetailsArray.deliveryEstimate ? 
                            <div className="estimate-time">
                                <p>Estimate : {productDetailsArray.deliveryEstimate}</p>
                            </div>
                            :
                            null
                        }
                    </div>
                    <div className="size-an-all">
                        {
                            productDetailsArray.sellerProductId.productType === 'grocery' ?
                                <>
                                    <div className="size-item">
                                        <p>pack size</p>
                                        <h1>{productDetailsArray.packSize + productDetailsArray.packSizeMeasureUnit.toLowerCase()}</h1>
                                    </div>
                                    {
                                        productDetailsArray.productId.freshness ?
                                        <div className="size-item ">
                                            <p>Freshness: </p>
                                            <h1>{productDetailsArray.productId.freshness} {productDetailsArray.productId.freshnessDaysOrWeek ? productDetailsArray.productId.freshnessDaysOrWeek : 'Days'}</h1>
                                        </div>
                                        :
                                        null
                                    }
                                </>
                                :
                                productDetailsArray.sellerProductId.attributeLabels.length > 0 ?
                                    productDetailsArray.sellerProductId.attributeLabels.map((eachLabel, eachIndex) => {
                                        return (
                                            <div className="size-item" key={eachIndex}>
                                                <p>{eachLabel}</p>
                                                {
                                                    productDetailsArray.sellerProductId.attributeValues[eachIndex].map((eachValue, eachValueIndex) => {
                                                        return (
                                                            <strong key={eachValueIndex}>{eachValue} {eachValueIndex !== productDetailsArray.sellerProductId.attributeValues[eachIndex].length - 1 ? ' , ' : null}</strong>
                                                        )
                                                    })
                                                }
                                            </div>
                                        )
                                    })
                                    :
                                    null
                        }
    
                        <div className="size-item">
                            <p>Country of Origin: </p>
                            <h1>{productDetailsArray.sellerProductId.country}</h1>
                        </div>
                    </div>
                    {
                        labelsArray.map((eachLabel, eachLabelIndex) => {
                            return (
                                <div className="variation-box mt-3" key={eachLabelIndex}>
                                    <div className="row">
                                        <div className="col-md-3">
                                            <p className="v-t">{eachLabel}</p>
                                        </div>
                                        <div className="col-md-9 pl-0">
                                            <p className="product-level">{productDetailsArray.attributeValues[eachLabelIndex]}</p>
                                            <ul className=" product-variation">
                                                {
                                                    valuesArray[eachLabelIndex].map((eachValue, eachValueIndex) => {
                                                        
                                                        return (
                                                            <li className={eachLabel === "Color Family" ? productVariantId === eachValue._id ? "product-image active" : "product-image" : productVariantId === eachValue._id ? "product-image product-storage active" : "product-image product-storage"} data-toggle="tooltip" data-placement="top" title="Black" key={eachValueIndex}>
                                                                {
                                                                    eachLabel === 'Color Family' ?
                                                                        <a href={`${process.env.PUBLIC_URL}/productDetails/${eachValue._id}`}><img src={window.$ImageURL + eachValue.image} alt="variantImage" /></a>
                                                                        :
                                                                        <a href={`${process.env.PUBLIC_URL}/productDetails/${eachValue._id}`}>{eachValue.value}</a>
                                                                }
                                                            </li>
                                                        )
                                                    })
                                                }
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            )
                        })
                    }
    
                    <div className="variation-box mt-3">
                        <div className="row">
                            <div className="col-md-2">
                                <p className="v-t">Quantity</p>
                            </div>
                            <div className="col-md-7 pl-0">
                                <div className=" cart-card quantity-box m-0">
                                    <div className="incrementbox">
                                        <button className="btn action" onClick={() => this.quantitySubtract()}><span>-</span></button>
                                        <span className="result">{qty}</span>
                                        <button className="btn action" onClick={() => this.quantityAdd()}><span>+</span></button>
                                    </div>
                                </div>
                            </div>
                            {/*<div className="col-md-3 pl-0">
                                <button className="no-background" onClick={() => this.setState({ recurringModalStatus: true })}>Schedule product</button>
                            </div>*/}
                        </div>
                    </div>
                    <div className="button-action">
                        <button className="btn cart_new " type="button" onClick={this.addToCart}>
                            <img src={cart_icon} alt="imageName" className="img-responsive" />
                            <span> Add to Cart </span>
                        </button>
                        {/*<button className="btn buynow" type="button" onClick={this.buyNow}> Buy Now </button>*/}
                    </div>
                    <RecurringModal
                     packSize={productDetailsArray.packSize}
                        packSizeMeasureUnit = {productDetailsArray.packSizeMeasureUnit}
                     modalStatus={this.state.recurringModalStatus} 

                     productVariantId={ productDetailsArray._id}
                     sellerId= {productDetailsArray.sellerProductId.sellerId._id}
                     modalHide={(val) => this.setState({ recurringModalStatus: val })} />
                </div>
            )
        }else{
            return(
                
                    <h1 className="np-f">No Product Found</h1>
               
            )
        }
        
    }
}

const mapStateToProps = (state) => ({
    CartProducts: state.Cart,
})

export default connect(
    mapStateToProps,
    {  AddToCat }
)(ProductDetailsContent);