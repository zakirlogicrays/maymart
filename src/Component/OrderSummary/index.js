import React, { Component } from "react";
import axios from "axios";
import jwt_decode from "jwt-decode";
import LocationModal from "../LocationModal";
import { connect } from "react-redux";

class OrderSummary extends Component {
  state = {
    couponCode: "",
    couponCodeError: "",
    userAddress: [],
    showLocationModal: false,
  };

  componentDidMount() {
    this.fetchUserAddress();
  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      this.fetchUserAddress();
    }
  }

  fetchUserAddress = async () => {
    let checkToken = await localStorage.getItem("token");
    if (checkToken) {
      const decoded = jwt_decode(checkToken);
      let customerId = decoded.userid;
      await axios
      .post(`${window.$URL}userAddress/fetch-user-address`, { customerId })
      .then((response) => {
        if(response.data.data.length > 0){
          const DefaultAddress = response.data.data.filter(
            (data) => data.Default === true
          );
          this.setState({
            userAddress: DefaultAddress,
          },() => {
            this.props.getDeliveryAddress(DefaultAddress[0]?._id);
          });
        }
      })
      .catch((error) => {
        console.log(error.data);
      });
    }
  };

  onChange = (e) => {
    e.preventDefault();
    this.setState({
      couponCodeError: "",
    });
    const { name, value } = e.target;
    this.setState({
      [name]: value,
    });
  };

  // fetchAddress = async() => {
  //     await axios.get(`${window.$URL}userAddress/${this.props.selectedAddress}`)
  //     .then(response => {
  //         console.log('response',response.data)
  //         if(response.data.success){
  //             this.setState({
  //                 userAddress: response.data.data
  //             })
  //         }
  //     })
  //     .catch(error => {
  //         console.log(error);
  //     })
  // }

  couponAppliedFunc = () => {
    const { couponCode } = this.state;
    let regSpace = new RegExp(/\s/);
    if (regSpace.test(couponCode)) {
      this.setState({
        couponCodeError: "Please enter voucher code",
      });
    } else {
      this.props.couponAppliedCheck(couponCode);
    }
  };

  removeCouponFunc = () => {
    this.props.removeCoupon();
  };

  proceedNextTab = () => {
    let token = localStorage.getItem("token");
    if (token) {
      if(this.props.tabName === 'cart' && this.props.selectedAddress === null){
        this.openLocationModal();
      } else {
        this.props.processNext();
      }
    } else {
      window.open(`${process.env.PUBLIC_URL}/login`);
    }
  };
  openLocationModal = () => {
    this.setState({
      showLocationModal: true,
    });
  };

  hideLocModal = () => {
    this.setState({
      showLocationModal: false,
    },() => {
      this.fetchUserAddress();
    });
  };

  render() {
    const {
      items,
      subTotal,
      shippingFee,
      total,
      couponApplied,
      couponAmount,
      location,
      finalTotal,
    } = this.props;
    const { couponCodeError, userAddress, showLocationModal } = this.state;
    return (
      <div className="col-md-4">
        <div className="cart-card checkout-wrapper">
          <div className="shipment-inner billing-section">
            <p className="Ship-Bill">Shipping &amp; Billing</p>
            {/* <p className="buyer-name">{location.Address}</p> */}
            {/* <button className="btn edit" onClick={() => this.openLocationModal()}>Edit</button> */}
            {/* <button className="btn edit" onClick={() => this.openLocationModal()}><i className="fa fa-pencil" aria-hidden="true" style={{height:20,width:20}}/></button> */}
            {/* <p className="address-1">{userAddress.Address}</p> */}
            {userAddress.map((address, index) => {
              return (
                <div className="buyer-address">
                  {/* <p> Bill to the same address</p> */}
                  <p className="buyer-name">
                    {location.locationCoordinates.length > 0
                      ? location.locationCoordinates[0].name
                      : address.Address}
                  </p>
                  <button
                    className="btn edit"
                    style={{ height: "20 px", width: "20 px" }}
                    onClick={() => this.openLocationModal()}
                  >
                    <i
                      className="fa fa-pencil"
                      aria-hidden="true"
                      style={{ height: "20 px", width: "20 px" }}
                    />
                  </button>
                  <p>{address.MobileNo}</p>
                  <p>{address.Name}</p>
                </div>
              );
            })}

            {/* <div className="buyer-address">
                            <p> Bill to the same address</p>
                            <p>123 456 7890</p>
                            <p>broji@dummy.com</p>
                            <button className="btn edit"><i className="fa fa-pencil" aria-hidden="true" /></button>
                        </div> */}
            <LocationModal
              modalStatus={showLocationModal}
              hideLocationModal={() => this.hideLocModal()}
            />
            <div className="cart-total subtotal" style={{ marginTop: 10 }}>
              <p>Subtotal ({items} items) </p>
              <span>QAR {subTotal}</span>
            </div>
            <div className="onf-sm-view">
              <div className="cart-total">
                <p>Shipping Fee </p>
                <span>QAR {shippingFee}</span>
              </div>
              {!couponApplied ? (
                total > 0 ? (
                  <>
                    {couponCodeError !== "" ? (
                      <span className="text-danger">{couponCodeError}</span>
                    ) : null}
                    <div className="cupon-wr">
                      <input
                        className="form-control"
                        type="search"
                        placeholder="Enter Voucher Code"
                        name="couponCode"
                        onChange={this.onChange}
                        aria-label="Search"
                      />
                      <button
                        className="btn apply-code"
                        type="button"
                        onClick={this.couponAppliedFunc}
                      >
                        Apply
                      </button>
                    </div>
                  </>
                ) : null
              ) : (
                <div className="cart-total">
                  <p>Coupon Applied </p>
                  <span>QAR {couponAmount}</span>
                  <i
                    className="fa fa-times"
                    style={{ color: "red" }}
                    onClick={this.removeCouponFunc}
                  ></i>
                </div>
              )}
            </div>
            <div className="total-price">
              <p>Total</p>
              <p>QAR {finalTotal}</p>
            </div>
          </div>

          <button
            className="btn checkout"
            onClick={() => this.proceedNextTab()}
          >
            <span className="large-checkout"> proceed to checkout</span>
            <span className="small-checkout">checkout</span>
          </button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  location: state.Location,
});

export default connect(mapStateToProps, {})(OrderSummary);
