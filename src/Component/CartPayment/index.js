import React, { Component } from "react";
import axios from 'axios';
import renderHTML from 'react-render-html';
const paymentGatewayLink = 'https://demopaymentapi.qpayi.com/api/gateway/v1.0';

export default class CartPayment extends Component {
    state = {
        cardPaymentHTML: '',
        paymentType: 'COD'
    }

    getPaymentGetwayForm = async() => {
       //const {total, couponApplied, couponAmount, selectedAddress} = this.props;

        let payload = JSON.stringify({
            "action" : "capture",
            "secretKey" : "2-/TMTOGfAc67IrI",
            "gatewayId" : "015298936",
            "referenceId":"154452",
            "mode":"test",
            "description":"Sample api testing Qpay ",
            "returnUrl":"https://www.brainiuminfotsech.com",
            "name":"brainium",
            "email":"prakashshaw1406@gmail.com",
            "phone":"7278201724",
            "address":"New Al-Waab Street , Doha 12962 , Qatar. Address: Al Waab Area , Khaliji 11 Street , Doha , Doha 12962 , Qatar",
            "state":"Qatar","postal_code":"12962","country":"QA","currency":"QAR","city":"Doha",
            "signature":"12345",
            "amount": 989
        });
        const proxyurl = 'https://cors-anywhere.herokuapp.com/';
        var config = {
            method: 'post',
            url: proxyurl + paymentGatewayLink,
            headers: { 
              'Content-Type': 'application/json',
            },
            data : payload
          };

        await axios(config)
        .then(response => {
            if(response.statusText === 'OK'){
                this.setState({
                    cardPaymentHTML: response.data
                })
            }
        })
        .catch(error => {
            console.log(error);
        })
    }

    createMarkup = (html) => {
        return {
            __html: html 
        };
    };

    onChange = (e) => {
        const {name,value} = e.target;
        this.setState({
            [name] : value
        },() => {
            if(value === 'Online'){
                this.getPaymentGetwayForm()
            }
        })
    }


    render() {
        const {cardPaymentHTML, paymentType} = this.state;
        return (
            <div className="col-lg-8">
                <div className="shipment-Block">
                    <div className="shipment-wr">
                        <div className="top-header">
                            <div className="row">
                                <div className="col-lg-5 col-md-5 col-sm-12">
                                    <h2 className="gross-title">Payment Method</h2>
                                </div>
                                <div className="col-lg-7 col-md-7 col-sm-12">
                                    <div className="check-form">
                                        <div className="form-check">
                                            <input className="form-check-input" type="radio" name="paymentType" id="gridRadios1" value="Online" onChange={this.onChange} disabled readOnly />
                                            <label className="form-check-label" htmlFor="gridRadios1"> Credit Card / Debit Card</label>
                                        </div>
                                        <div className="form-check">
                                            <input className="form-check-input" type="radio" name="paymentType" id="gridRadios2" value="COD" defaultChecked onChange={this.onChange} />
                                            <label className="form-check-label" htmlFor="gridRadios2"> Cash On Delivery</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {
                    paymentType === 'Online' ?
                    <div className="cart-card" >
                        <div className="address-section">
                            <h1>Payment Gateway</h1>
                            {renderHTML(cardPaymentHTML)}
                        </div>
                    </div>
                    :
                    null
                }
            </div>
        )
    }

}    