import React, { Component } from 'react';
import Loader from "../../Component/Loader";
import ReactStars from "react-rating-stars-component";

class RatingReview extends Component {

    render() {
        const { ProductRatingData, loading, productDetailsArray } = this.props;
        console.log({productDetailsArray});
        if (loading) {
            return (
                <Loader />
            )
        } else {
            return (
                <div>
                    <div className="tab-pane fade show active" id="Item-Info" role="tabpanel" aria-labelledby="nav-home-tab">
                        <div className="inner-review-container">
                            <div className="rating-section">
                                <h3 className="rating-title">Rating &amp; Reviews of {productDetailsArray.sellerProductId.productName}</h3>
                                <div className="reiew-contaier">
                                    <div className="content-left">
                                        <p className="review_count">{ProductRatingData.totalProductAveragerating} <span>/</span> <span className="out-of">5</span>
                                        </p>
                                        <div className="rating">
                                            <span className="star-yellow">★</span>
                                            <span className="star-yellow">★</span>
                                            <span className="star-yellow">★</span>
                                            <span className="star-yellow">★</span>
                                            <span className="star-gray">☆</span>
                                            <span className="totalUser">{ProductRatingData.totalRatingCount} Ratings</span>
                                        </div>
                                    </div>
                                    <div className="content-right-main">
                                    <div className="content-right">
                                            <div className="rating">
                                               <ReactStars value={5} size={30} isHalf={true} edit={false} />
                                                {/* <span className="star-yellow">★</span>
                                                <span className="star-yellow">★</span>
                                                <span className="star-yellow">★</span>
                                                <span className="star-gray">☆</span> */}
                                            </div>
                                            <div className="bar">
                                                <div className="rating-progressbar">
                                                    <div className="progress">
                                                        <div className="progress-bar" role="progressbar" aria-valuenow={70} aria-valuemin={0} aria-valuemax={100} style={{ width: '70%' }}>
                                                            <span className="sr-only">70% Complete</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <h2 className="number">{ProductRatingData.ratingCountFive}</h2>
                                        </div>
                                        <div className="content-right">
                                            <div className="rating">
                                              <ReactStars value={4} size={30} isHalf={true} edit={false} />
                                                {/* <span className="star-yellow">★</span>
                                                <span className="star-yellow">★</span>
                                                <span className="star-yellow">★</span>
                                                <span className="star-gray">☆</span> */}
                                            </div>
                                            <div className="bar">
                                                <div className="rating-progressbar">
                                                    <div className="progress">
                                                        <div className="progress-bar" role="progressbar" aria-valuenow={70} aria-valuemin={0} aria-valuemax={100} style={{ width: '70%' }}>
                                                            <span className="sr-only">70% Complete</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <h2 className="number">{ProductRatingData.ratingCountFour}</h2>
                                        </div>
                                        <div className="content-right">
                                            <div className="rating">
                                               <ReactStars value={3} size={30} isHalf={true} edit={false} />
                                                {/* <span className="star-yellow">★</span>
                                                <span className="star-yellow">★</span>
                                                <span className="star-gray">☆</span>
                                                <span className="star-gray">☆</span> */}
                                            </div>
                                            <div className="bar">
                                                <div className="rating-progressbar">
                                                    <div className="progress">
                                                        <div className="progress-bar" role="progressbar" aria-valuenow={50} aria-valuemin={0} aria-valuemax={100} style={{ width: '50%' }}>
                                                            <span className="sr-only">70% Complete</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <h2 className="number">{ProductRatingData.ratingCountTharee}</h2>
                                        </div>
                                        <div className="content-right">
                                            <div className="rating">
                                               <ReactStars value={2} size={30} isHalf={true} edit={false} />
                                                {/* <span className="star-yellow">★</span>
                                                <span className="star-gray">☆</span>
                                                <span className="star-gray">☆</span>
                                                <span className="star-gray">☆</span> */}
                                            </div>
                                            <div className="bar">
                                                <div className="rating-progressbar">
                                                    <div className="progress">
                                                        <div className="progress-bar" role="progressbar" aria-valuenow={30} aria-valuemin={0} aria-valuemax={100} style={{ width: '30%' }}>
                                                            <span className="sr-only">70% Complete</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <h2 className="number">{ProductRatingData.ratingCounTwo}</h2>
                                        </div>
                                        <div className="content-right">
                                            <div className="rating">
                                              <ReactStars value={1} size={30} isHalf={true} edit={false} />
                                                {/* <span className="star-gray">☆</span>
                                                <span className="star-gray">☆</span>
                                                <span className="star-gray">☆</span>
                                                <span className="star-gray">☆</span> */}
                                            </div>
                                            <div className="bar">
                                                <div className="rating-progressbar">
                                                    <div className="progress">
                                                        <div className="progress-bar" role="progressbar" aria-valuenow={10} aria-valuemin={0} aria-valuemax={100} style={{ width: '10%' }}>
                                                            <span className="sr-only">10% Complete</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <h2 className="number">{ProductRatingData.ratingCountOne}</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/* review start */}
                            <div className="product-r-wr">
                                <h2 className="title-wr">Product Reviews</h2>
                                {
                                    ProductRatingData.ProductRatingReviewDetail !== null && ProductRatingData.ProductRatingReviewDetail.length > 0 ?
                                        ProductRatingData.ProductRatingReviewDetail.map((ratingdata, index) => {
                                            return (
                                                <div className="reviwe-items" key={index}>
                                                    <div className="rating">
                                                        <span className="star-yellow"><ReactStars value={ratingdata.rating} size={30} isHalf={true} edit={false} /></span>
                                                    </div>
                                                    <div className="user-veryfied">
                                                        <span className="by_user">by {ratingdata.customerID.Firstname} {ratingdata.customerID.Lastname}</span>
                                                        <span className="verified-user-with"> verified purchase </span>
                                                    </div>
                                                    <p className="review-desc">{ratingdata.review}</p>
                                                    <h5 className="date-and-time">22 aug 2020</h5>
                                                </div>
                                            )
                                        })
                                        : ''
                                }

                                {/* <div className="reviwe-items">
                                    <div className="rating">
                                        <span className="star-yellow">★</span>
                                        <span className="star-yellow">★</span>
                                        <span className="star-yellow">★</span>
                                        <span className="star-yellow">★</span>
                                        <span className="star-gray">☆</span>
                                    </div>
                                    <div className="user-veryfied">
                                        <span className="by_user">by griffine</span>
                                        <span className="verified-user-with"> verified purchase </span>
                                    </div>
                                    <p className="review-desc">Good quality fresh fruits. We order again</p>
                                    <h5 className="date-and-time">22 aug 2020</h5>
                                </div> */}
                                {/* <div className="reviwe-items">
                                    <div className="rating">
                                        <span className="star-yellow">★</span>
                                        <span className="star-yellow">★</span>
                                        <span className="star-yellow">★</span>
                                        <span className="star-yellow">★</span>
                                        <span className="star-gray">☆</span>
                                    </div>
                                    <div className="user-veryfied">
                                        <span className="by_user">by griffine</span>
                                        <span className="verified-user-with"> verified purchase </span>
                                    </div>
                                    <p className="review-desc">Good quality fresh fruits. We order again</p>
                                    <h5 className="date-and-time">22 aug 2020</h5>
                                </div> */}
                                {/* <div className="reviwe-items">
                                    <div className="rating">
                                        <span className="star-yellow">★</span>
                                        <span className="star-yellow">★</span>
                                        <span className="star-yellow">★</span>
                                        <span className="star-yellow">★</span>
                                        <span className="star-gray">☆</span>
                                    </div>
                                    <div className="user-veryfied">
                                        <span className="by_user">by griffine</span>
                                        <span className="verified-user-with"> verified purchase </span>
                                    </div>
                                    <p className="review-desc">Good quality fresh fruits. We order again</p>
                                    <h5 className="date-and-time">22 aug 2020</h5>
                                </div> */}
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
    }
}

export default RatingReview;