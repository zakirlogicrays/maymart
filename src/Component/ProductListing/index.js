import React, { Component } from 'react';
import cart_icon from "../../assets/images/cart_icon.svg";
import Loader from "../../Component/Loader";
import { AddToCat, UpdateCart, UpdateWithOutCart } from '../../data/reducers/cart';
import { connect } from 'react-redux';
import axios from "axios";
import jwt_decode from "jwt-decode";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
//import ReactStars from "react-rating-stars-component";
import StarRatingComponent from 'react-star-rating-component';
import GreenMartIcon from '../../assets/images/greenmart-Store.svg';
import moment from 'moment';
import HeartIcon from '../../assets/images/Like.svg';
import HeartIconSelected from '../../assets/images/Like-pink.svg';
import AmulButter from '../../assets/images/amulButter.png';
import MultiCarousel from 'react-multi-carousel';
import { Carousel } from 'react-responsive-carousel';

const responsiveImage = {
    superLargeDesktop: {
        // the naming can be any, depends on you.
        breakpoint: { max: 4000, min: 3000 },
        items: 1
    },
    desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 1
    },
    tabletPro: {
        breakpoint: { max: 1024, min: 992 },
        items: 1
    },
    tablet: {
        breakpoint: { max: 991, min: 320 },
        items: 1
    },
    mobile: {
        breakpoint: { max: 576, min: 0 },
        items: 1,
    }
};


class ProductListing extends Component {
    constructor(props) {
        super(props);
        this.state = {
            wishListedArray: [],
            cartArray: [],
            ratingsArray: [],
            loggedIn: false,
            showAll: false,
            loadMore: true,
            hoveredProduct: undefined,
            multiCarousel: true,
            widthofscreen: undefined
        }
    }

    getWindowDimensions() {
        const { innerWidth: width } = window;
        return {
            width
        };
    }

    componentDidMount() {
        //console.log("window.location.href ------>", window.location.href.includes("productListing/newAndTranding"));
        let widthofscreen = this.getWindowDimensions()
        this.setState({widthofscreen: widthofscreen})
        this.setState({multiCarousel: window.location.href.includes("productListing/choosen")})
        this.fetchWishList();
        this.fetchRating();
        this.fetchCart();
    }

    componentDidUpdate(prevProps) {
        if (this.props.CartProducts.cartCount !== prevProps.CartProducts.cartCount) {
            if (this.props.CartProducts.cartSuceess) {
                // this.getToast('Successfully Added To The Cart')
                toast.success('Product added to cart Successfully', {
                    position: "bottom-center",
                    autoClose: 3000,
                    hideProgressBar: true,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });
            }
            this.componentDidMount()
        }
        if (this.props.productArray !== prevProps.productArray) {
            this.fetchRating();
        }
    }

    async fetchWishList() {
        let checkToken = await localStorage.getItem("token");
        if (checkToken) {
            const decoded = jwt_decode(checkToken);
            let customerId = decoded.userid;
            this.setState({
                loggedIn: true
            })
            await axios.post(`${window.$URL}wishlist/fetchWishList`, { customerId })
                .then(response => {
                    if (response.status === 200) {
                        if (response.data.success) {
                            this.setState({
                                wishListedArray: response.data.data
                            })
                        } else {
                            console.log('errror on fetch');
                        }
                    }
                })
                .catch(error => {
                    console.log(error);
                })
        }
    }

    async fetchRating() {
        const { productArray, loading } = this.props;
        if (!loading) {
            let ratingsArray = [];
            for (let i = 0; i <= productArray.length - 1; i++) {
                if (productArray[i].sellerProductVariantId[0] !== undefined) {
                    await axios.get(`${window.$URL}ratingReview/getEachProductrating/${productArray[i].sellerProductVariantId[0]._id}`)
                        .then(response => {
                            if (response.status === 200) {
                                ratingsArray.push({ totalRating: response.data.totalRating, averageRating: response.data.averageRating })
                            }
                        })
                        .catch(error => {
                            console.log(error);
                        })
                } else {
                    ratingsArray.push({ totalRating: 0, averageRating: 0 })
                }
            }
            this.setState({
                ratingsArray
            })
        }
    }

    async fetchCart() {
        let checkToken = await localStorage.getItem("token");
        if (checkToken) {
            const decoded = jwt_decode(checkToken);
            let customerId = decoded.userid;
            this.setState({
                loggedIn: true
            })
            await axios.get(`${window.$URL}cart/fetchCartWithoutPopulate/${customerId}`)
                .then(response => {
                    if (response.status === 200) {
                        this.setState({
                            cartArray: response.data
                        })
                    }
                })
                .catch(error => {
                    console.log(error);
                })
        } else {
            let tempToken = await localStorage.getItem("Temptoken");
            if (tempToken) {
                await axios.get(`${window.$URL}cart/fetchtempCartWithoutPopulate/${tempToken}`)
                    .then(res => {
                        this.setState({
                            cartArray: res.data
                        })
                    })
            }
        }
    }


    async addWishList(productId, wishListStatus) {
        let checkToken = await localStorage.getItem("token");
        if (checkToken) {
            const decoded = jwt_decode(checkToken);
            let customerId = decoded.userid;

            if (wishListStatus) {
                const allItems = this.props.productArray;
                var selectedProduct = allItems.filter(item => {
                    return item.sellerProductVariantId[0]._id === productId;
                })
                if (selectedProduct[0].sellerProductVariantId[0]._id === productId) {
                    let wishListData = {

                        "productVariantId": productId,
                        "customerId": customerId
                    };
                    await axios.post(`${window.$URL}wishlist/addwishlist`, wishListData)
                        .then(response => {
                            if (response.status === 200) {
                                if (response.data.success) {
                                    toast.success(response.data.message, {
                                        position: "bottom-center",
                                        autoClose: 3000,
                                        hideProgressBar: false,
                                        closeOnClick: true,
                                        pauseOnHover: true,
                                        draggable: true,
                                        progress: undefined,
                                    });

                                    this.componentDidMount();
                                } else {
                                    toast.error(response.data.message, {
                                        position: "bottom-center",
                                        autoClose: 3000,
                                        hideProgressBar: true,
                                        closeOnClick: true,
                                        pauseOnHover: true,
                                        draggable: true,
                                        progress: undefined,
                                    });

                                }
                            }
                        })
                        .catch(error => {
                            console.log(error);
                        })
                }
            } else {
                let DeletedId = {
                    "productVariantId": productId,
                    "customerId": customerId
                };
                await axios.post(`${window.$URL}wishlist/deletewishlist`, DeletedId)
                    .then(response => {
                        if (response.status === 200) {
                            if (response.data.success) {
                                toast.error(response.data.message, {
                                    position: "bottom-center",
                                    autoClose: 3000,
                                    hideProgressBar: true,
                                    closeOnClick: true,
                                    pauseOnHover: true,
                                    draggable: true,
                                    progress: undefined,
                                });

                                this.componentDidMount();
                            } else {
                                toast.error(response.data.message, {
                                    position: "bottom-center",
                                    autoClose: 3000,
                                    hideProgressBar: true,
                                    closeOnClick: true,
                                    pauseOnHover: true,
                                    draggable: true,
                                    progress: undefined,
                                });

                            }
                        }
                    })
                    .catch(error => {
                        console.log(error);
                    })

            }
        } else {
            window.location.href = `${process.env.PUBLIC_URL}/login`
        }
    }

    fetchVariationProduct = (variationArray, productType) => {
        if (productType === "nonGrocery") {
            return variationArray.map((eachVariation, eachVariationIndex) => {
                var imageData;
                if (eachVariation.variationImageId.length > 0) {
                    imageData = `${window.$ImageURL}` + eachVariation.variationImageId[0].originalURL
                } else {
                    imageData = `${window.$ImageURL}no-image-480x480.png`
                }
                return (
                    <a href={`${process.env.PUBLIC_URL}/productDetails/${eachVariation._id}`} className="product-variation-image" key={eachVariationIndex}>
                        <img src={imageData} className="img-responsive" alt="homepageImages" />
                    </a>
                )
            })
        } else {
            return variationArray.map((eachVariation, eachVariationIndex) => {
                return (
                    <a href={`${process.env.PUBLIC_URL}/productDetails/${eachVariation._id}`} className="product-variation-text" key={eachVariationIndex}>
                        <span className="qty">{eachVariation.packSize} {eachVariation.packSizeMeasureUnit !== 'Custom' ? eachVariation.packSizeMeasureUnit : null}</span>
                    </a>
                )
            })
        }

    }

    addToCart = async (produtArray) => {

        let addToCartData = {
            productVariantId: produtArray.sellerProductVariantId[0]._id,
            quantity: 1,
            productId: produtArray._id,
            sellerId: produtArray.sellerId._id,
            brand: produtArray.brand
        }
        this.props.AddToCat(addToCartData);

    }

    showCartButton = (prodData) => {
        const { cartArray } = this.state;
        let filterCartArray = cartArray.filter((eachProduct) => {
            return eachProduct.productId.includes(prodData._id)
        })
        console.log(filterCartArray)
        if (filterCartArray.length > 0) {
            let findIndex = filterCartArray[0].productId.indexOf(prodData._id);
            return (
                <div className=" cart-card quantity-box m-0 action-button">
                    <div className="incrementbox">
                        <button className="btn action" onClick={() => this.quantitySubtract(filterCartArray[0]._id, findIndex)}><span>-</span></button>
                        <span className="result">{filterCartArray[0].quantity[findIndex]}</span>
                        <button className="btn action" onClick={() => this.quantityAdd(filterCartArray[0]._id, findIndex)}><span>+</span></button>
                    </div>
                </div>
            )
        } else {
            return (
                <div className="action-button">
                    <button onClick={() => this.addToCart(prodData)} className="btn cart_new">
                        <img src={cart_icon} alt={cart_icon} className="img-responsive" />
                        <span> Add to cart </span>
                    </button>
                </div>
            )
        }
    }

    quantityAdd = async (cartId, variantIndex) => {

        let checkToken = await localStorage.getItem("token");
        if (checkToken) {
            const decoded = jwt_decode(checkToken);
            await axios.post(`${window.$URL}cart/addQuantity`, { customerID: decoded.userid, cartId, variantIndex })
                .then((response) => {
                    if (response.data.success) {
                        this.props.UpdateCart();
                        toast.success(response.data.message, {
                            position: "bottom-center",
                            autoClose: 3000,
                            hideProgressBar: true,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });
                    } else {
                        toast.error(response.data.message, {
                            position: "bottom-center",
                            autoClose: 3000,
                            hideProgressBar: true,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });
                    }
                })
                .catch(error => {
                    console.log(error);
                })
        } else {
            let tempToken = await localStorage.getItem("Temptoken");
            if (tempToken) {
                await axios.post(`${window.$URL}cart/addwithoutQuantity`, { tempID: tempToken, cartId, variantIndex })
                    .then(response => {
                        if (response.data.success) {
                            if (response.data.success) {
                                this.props.UpdateWithOutCart();
                                toast.success(response.data.message, {
                                    position: "bottom-center",
                                    autoClose: 3000,
                                    hideProgressBar: true,
                                    closeOnClick: true,
                                    pauseOnHover: true,
                                    draggable: true,
                                    progress: undefined,
                                });
                            } else {
                                toast.error(response.data.message, {
                                    position: "bottom-center",
                                    autoClose: 3000,
                                    hideProgressBar: true,
                                    closeOnClick: true,
                                    pauseOnHover: true,
                                    draggable: true,
                                    progress: undefined,
                                });
                            }
                        }
                    })
                    .catch(error => {
                        console.log(error.data)
                    })
            }
        }

    }

    quantitySubtract = async (cartId, variantIndex) => {
        let checkToken = await localStorage.getItem("token");
        if (checkToken) {
            const decoded = jwt_decode(checkToken);
            await axios.post(`${window.$URL}cart/subtractQuantity`, { customerID: decoded.userid, cartId, variantIndex })
                .then((response) => {
                    if (response.data.success) {
                        this.props.UpdateCart();
                        toast.error(response.data.message, {
                            position: "bottom-center",
                            autoClose: 3000,
                            hideProgressBar: true,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });
                    } else {
                        toast.error(response.data.message, {
                            position: "bottom-center",
                            autoClose: 3000,
                            hideProgressBar: true,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });
                    }
                })
                .catch(error => {
                    console.log(error);
                })
        } else {
            let tempToken = await localStorage.getItem("Temptoken");
            if (tempToken) {
                await axios.post(`${window.$URL}cart/subtractwithoutQuantity`, { tempID: tempToken, cartId, variantIndex })
                    .then(response => {
                        if (response.data.success) {
                            if (response.data.success) {
                                this.props.UpdateWithOutCart();
                                toast.error(response.data.message, {
                                    position: "bottom-center",
                                    autoClose: 3000,
                                    hideProgressBar: true,
                                    closeOnClick: true,
                                    pauseOnHover: true,
                                    draggable: true,
                                    progress: undefined,
                                });
                            } else {
                                toast.error(response.data.message, {
                                    position: "bottom-center",
                                    autoClose: 3000,
                                    hideProgressBar: true,
                                    closeOnClick: true,
                                    pauseOnHover: true,
                                    draggable: true,
                                    progress: undefined,
                                });
                            }
                        }
                    })
                    .catch(error => {
                        console.log(error.data)
                    })
            }
        }

    }

    redirectToDetails = async (variantId, sellerProductId) => {
        await axios.post(`${window.$URL}sellerProducts/addClick`, { sellerProductId })
            .then(response => {
                if (response.status === 200) {
                    window.location.href = `${process.env.PUBLIC_URL}/productDetails/${variantId}`;
                }
            })
            .catch(error => {
                console.log(error);
            })
    }

    getDateValid = (prodArray) => {
        let returnValidate = false;
        if (prodArray.startDate !== "" && prodArray.endDate !== "") {
            let convertStartDate = moment(prodArray.startDate).unix() * 1000;
            let convertEndDate = moment(prodArray.endDate).unix() * 1000;
            let systemTime = moment(new Date()).unix() * 1000;
            if (systemTime <= convertEndDate && systemTime >= convertStartDate) {
                returnValidate = true;
            }
        }
        return returnValidate;
    }


    render() {
        const { productArray, loading, gridView } = this.props;
        const { wishListedArray, ratingsArray } = this.state;
       // console.log("multiCarousel -------->", this.state.multiCarousel)
        if (loading) {
            return (
                <Loader />
            )
        } else {
            return (
                <div className="product-container">
                    {/* {console.log("productArray ------>", productArray)} */}
                    <ToastContainer />
                    <div className={gridView ? "row-view-catagory-list column-view-slider-categorylist" : "row choosen-product"}>
                        {
                            productArray.length > 0 ?
                                productArray.slice(0, this.state.showAll ? (productArray.length + 1 > 20 ? 20 : productArray.length + 1) : 10)?.map((prodData, prodIndex) => {
                                    var imageData;
                                    if (prodData.productImageId.length < 1 || prodData === undefined) {
                                        imageData = `${window.$ImageURL}no-image-480x480.png`
                                    } else if (prodData.primaryProductImage !== null && prodData.primaryProductImage !== undefined) {
                                        imageData = `${window.$ImageURL}` + prodData.primaryProductImage.originalURL
                                    } else {
                                        imageData = `${window.$ImageURL}` + prodData.productImageId[0].originalURL
                                    }

                                    if (prodData.sellerProductVariantId[0]) {
                                        return (
                                            <div className="col-md-3 mt-30 flex-screen reduce-padding only-cat-view home-product-img-modal-main" key={prodIndex} onMouseEnter={() => this.setState({ hoveredProduct: prodIndex })}>
                                                <div className="card product-card no-qty">
                                                    {/* {
                                                        prodData.sellerProductVariantId[0].specialPriceProvided && prodData.sellerProductVariantId[0].specialPriceProvided && this.getDateValid(prodData.sellerProductVariantId[0]) ?
                                                            <div className="offer-area">
                                                                <span>Offer</span>
                                                            </div>
                                                            :
                                                            null
                                                    } */}
                                                    {/* <div 
                                                        //onClick={() => this.redirectToDetails(prodData.sellerProductVariantId[0]._id, prodData._id)} 
                                                        className="product-image finger-pointer"
                                                     > */}
                                                    {!this.state.multiCarousel ? (
                                                        <MultiCarousel
                                                            showDots={true}
                                                            className="home-product-slider"
                                                            responsive={responsiveImage}
                                                            arrows={this.state.widthofscreen?.width >  991 ? this.state.hoveredProduct !== prodIndex ? false : true : true}
                                                            // arrows={this.state.hoveredProduct !== prodIndex ? false : true}
                                                            draggable={this.state.hoveredProduct !== prodIndex ? false : true}
                                                            showDots={this.state.widthofscreen?.width >  991 ? prodData?.productImageId.length > 1 ? this.state.hoveredProduct !== prodIndex ? false : true : false : prodData?.productImageId.length > 1 ? true : false}
                                                            //showDots={prodData?.productImageId.length > 1 ? (this.state.hoveredProduct !== prodIndex ? false : true) : false}
                                                        >
                                                            {prodData?.productImageId?.map((item) => (
                                                                <img onClick={() => this.props.onClickChosenProductPopUp(prodData)} src={`https://mymart.qa:7779/uploads/${item.originalURL}`} className="img-responsive" alt="homepageImages"/>  
                                                            ))
                                                            }   
                                                        </MultiCarousel>
                                                         ) : (
                                                             <div>
                                                                {gridView ? ( 
                                                                <Carousel
                                                                    // autoPlay
                                                                    infiniteLoop
                                                                    showStatus={false}
                                                                >
                                                                    {prodData?.productImageId?.map((item) => (
                                                                        <img onClick={() => this.props.onClickChosenProductPopUp(prodData)} src={`https://mymart.qa:7779/uploads/${item.originalURL}`} className="img-responsive" alt="homepageImages"/>  
                                                                    ))
                                                                    }  
                                                                </Carousel>
                                                                 ) : (
                                                                    <MultiCarousel
                                                                        showDots={true}
                                                                        className="home-product-slider"
                                                                        responsive={responsiveImage}
                                                                        arrows={this.state.hoveredProduct !== prodIndex ? false : true}
                                                                        draggable={this.state.hoveredProduct !== prodIndex ? false : true}
                                                                        showDots={prodData?.productImageId.length > 1 ? (this.state.hoveredProduct !== prodIndex ? false : true) : false}
                                                                    >
                                                                        {prodData?.productImageId?.map((item) => (
                                                                            <img onClick={() => this.props.onClickChosenProductPopUp(prodData)} src={`https://mymart.qa:7779/uploads/${item.originalURL}`} className="img-responsive" alt="homepageImages"/>  
                                                                        ))
                                                                        }   
                                                                    </MultiCarousel>
                                                                 )}
                                                            </div>
                                                        )}
                                                    {/* </div> */}
                                                    <div className="iner-container" onClick={() => this.redirectToDetails(prodData.sellerProductVariantId[0]._id, prodData._id)} >

                                                        <div className="price-with-fav">
                                                            {
                                                                // prodData.sellerProductVariantId[0].specialPriceProvided && this.getDateValid(prodData.sellerProductVariantId[0]) ?
                                                                prodData.sellerProductVariantId[0].specialPriceProvided ? <h4 className="offer_quote" > Offer </h4> : <h4></h4>
                                                                // :
                                                                // null
                                                            }
                                                            <button type="button" className="favorite_button">
                                                                {wishListedArray.includes(prodData.sellerProductVariantId[0]._id)
                                                                    ? <img onClick={() => this.addWishList(prodData.sellerProductVariantId[0]._id, false)} src={HeartIconSelected} alt={HeartIconSelected} />
                                                                    : <img onClick={() => this.addWishList(prodData.sellerProductVariantId[0]._id, true)} src={HeartIcon} alt={HeartIcon} />
                                                                }
                                                            </button>
                                                        </div>
                                                        {/* {this.fetchVariationProduct(prodData.sellerProductVariantId, prodData.productType)} */}
                                                        {
                                                            prodData !== null ?
                                                                <div className="product-cart-title">
                                                                    <h5 className="card-title">
                                                                        <div className="finger-pointer" onClick={() => this.redirectToDetails(prodData.sellerProductVariantId[0]._id, prodData._id)}>{prodData.productName.length > 35 ? prodData.productName.substring(0, 35) + '...' : prodData.productName}</div>
                                                                    </h5>
                                                                    <p className="description">{prodData?.category[0]}</p>
                                                                </div>
                                                                :
                                                                null
                                                        }
                                                        <div className="rating-ltr">
                                                            <ul>
                                                                <li className="ratting">
                                                                    <span className="rating">
                                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                                        <p>4.5</p>
                                                                    </span>
                                                                    <span className="sold"> 270 Sold </span>
                                                                </li>
                                                                <li className="liter">
                                                                    <span className="liter-true">{prodData.sellerProductVariantId[0].productId.packSize + " " + prodData.sellerProductVariantId[0].productId.packSizeMeasureUnit}</span>
                                                                    {/* <span className="liter-false">1.5 L</span> */}
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div className="greenmart-content">
                                                            <div className="img-text">
                                                                <img src={GreenMartIcon} alt={GreenMartIcon} />
                                                                <span> by <a href="#"> {`${prodData.sellerId.SellerDetails.shopName}`} (1.5km) </a> </span>
                                                            </div>
                                                        </div>
                                                        {/* {
                                                            prodData !== null ?

                                                                prodData.productType === "grocery" ?
                                                                    <h6 className="qty">{prodData.sellerProductVariantId[0].packSize}{prodData.sellerProductVariantId[0].packSizeMeasureUnit !== 'Custom' ? prodData.sellerProductVariantId[0].packSizeMeasureUnit.toLowerCase() : null}</h6>
                                                                    :
                                                                    <>
                                                                        {
                                                                            prodData.attributeLabels[0] !== undefined ?
                                                                                <h6 className="qty storage" >{prodData.attributeLabels[0] !== undefined ? prodData.attributeLabels[0].length > 15 ? prodData.attributeLabels[0].substring(0, 25) + '...' : prodData.attributeLabels[0] : null} <span className=" dimention"  >
                                                                                    {prodData.attributeValues[0].map((eachValue, eachValueIndex) => {
                                                                                        return (
                                                                                            <span key={eachValueIndex}>{eachValue}{eachValueIndex !== prodData.attributeValues[0].length - 1 ? ' , ' : null}</span>
                                                                                        )
                                                                                    })}
                                                                                </span></h6>
                                                                                :
                                                                                null
                                                                        }
                                                                    </>
                                                                :
                                                                null
                                                        } */}
                                                        {
                                                            // prodData.sellerProductVariantId[0].specialPriceProvided && this.getDateValid(prodData.sellerProductVariantId[0]) ?
                                                            <>
                                                                <p className="priceTag"> QAR {parseFloat(prodData.sellerPrice).toFixed(2)} </p>
                                                                <div className="offer-price">
                                                                    <div className="qar-main">
                                                                        <p className="mt-cutoff">QAR {parseFloat(prodData.price).toFixed(2)} </p>
                                                                        <h4 className="offer_margin" > {`- ` + parseInt(prodData.myMartMargin)}% Off </h4>
                                                                    </div>
                                                                    {prodData?.country && <p className="singapore-text">{prodData?.country}</p>}
                                                                </div>
                                                            </>
                                                            // :
                                                            // <p className="priceTag"> QAR {parseFloat(prodData.sellerProductVariantId[0].Price).toFixed(2)} </p>
                                                        }
                                                        {/* {
                                                            ratingsArray[prodIndex] !== undefined && ratingsArray[prodIndex].totalRating > 0 ? 
                                                            <div className="rating">
                                                                <StarRatingComponent name="ratings" value={ratingsArray[prodIndex] !== undefined ? ratingsArray[prodIndex].averageRating : 0} />
                                                                <span className="totalUser">({ratingsArray[prodIndex] !== undefined ? ratingsArray[prodIndex].totalRating : 0})</span>
                                                            </div>
                                                            :
                                                            <div className="rating" style={{height: 20}}></div>
                                                        }
                                                        {
                                                            prodData.productType === "grocery" ?
                                                                this.showCartButton(prodData)
                                                                :
                                                                null
                                                        } */}
                                                    </div>
                                                </div>
                                            </div>
                                        )
                                    } else {
                                        return null;
                                    }
                                })
                                :
                                     <h3 className="no-aviable-prod">No Product Found</h3>
                        }
                        {(productArray.length >= 10 && this.state.loadMore) && <div onClick={() => { this.setState({ showAll: !this.state.showAll, loadMore: false }) }} className="home-load-more-btn"><a>Load More</a></div>}
                    </div>
                </div>
            )
        }
    }
}
const mapStateToProps = (state) => ({
    CartProducts: state.Cart,
})

export default connect(
    mapStateToProps,
    { AddToCat, UpdateCart, UpdateWithOutCart }
)(ProductListing);