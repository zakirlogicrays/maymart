import React, { Component } from "react";
import TimeSlot from "../TimeSlot";
import TimeSlotModal from '../TimeSlotModal'
import CartDeliveryProduct from '../CartDeliveryProduct'
import axios from "axios";
import orangesImage from "../../assets/images/oranges.png";
import { connect } from "react-redux";
import jwt_decode from "jwt-decode";
import moment from 'moment';

function calculateDistance(lat1, lon1, lat2, lon2, unit) {
  var radlat1 = (Math.PI * lat1) / 180;
  var radlat2 = (Math.PI * lat2) / 180;
  var theta = lon1 - lon2;
  var radtheta = (Math.PI * theta) / 180;
  var dist =
    Math.sin(radlat1) * Math.sin(radlat2) +
    Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
  dist = Math.acos(dist);
  dist = (dist * 180) / Math.PI;
  dist = dist * 60 * 1.1515;
  if (unit === "K") {
    dist = dist * 1.609344;
  }
  if (unit === "N") {
    dist = dist * 0.8684;
  }
  return dist;
}


class CartDeliverySlot extends Component {
  state = {
    cartArray: [],
    totalCartArray: [],
    choosenProduct: [],
    loading: true,

    quantityArray: [],
    myMartMarginArray: [],
    myMartProfitArray: [],
    PriceArray: [],
    sellerIDArray: [],
    selectedAddress: null,
    selectedTimeSlot: [],
    subTotalAmount: 0,
    shippingFee: 0,
    totalAmount: 0,
    couponApplied: false,
    couponAmount: 0,

    successMessage: '',
    errorMessage: '',
    timeSlotShow: false,
    deliveryRule: [],
    deliveryRuleSelected: [],
    timeSlot: 0,
    deliveryFromMyMart: "yes",
    showTimeSlotModal: false,
    ViewSelectedSlot: ''

  };

  componentDidMount() {
    this.getDeliveryRule();
    this.fetchCartData();

  }
  fetchCartData = async () => {
    let checkToken = await localStorage.getItem("token");
    if (checkToken) {
      const decoded = jwt_decode(checkToken);
      if (decoded.userType === 'customer') {
        await axios.get(`${window.$URL}cart/fetchCart/${decoded.userid}`)
          .then(res => {
            console.log(res);
            //let selectedArray = [];
            if (res.status === 200) {
              /*for (let i = 0; i <= res.data.length - 1; i++) {
                  for(let j = 0; j <= res.data[i].productVariantId.length-1; j++){
                      selectedArray.push(res.data[i].productVariantId[j]._id);
                  }
              }*/
              this.setState({
                cartArray: res.data,
                totalCartArray: res.data,
                loading: false,
                subTotalAmount: 0,
                totalAmount: 0
              }, () => {
                this.showItems('all');
              })
            }
          })
          .catch(error => {
            console.log(error)
          })
      }
    } else {
      this.setState({
        cartArray: [],
        loading: false
      })
    }
  }

  showItems = (type) => {
    const { totalCartArray } = this.state;
    let filterCart = [];
    if (type === 'all') {
      filterCart = totalCartArray.filter(() => {
        return true;
      })
    } else if (type === 'grocery') {
      filterCart = totalCartArray.filter((eachCart) => {
        return eachCart.productId.some((eachProduct) => {
          return eachProduct.category.includes("Grocery");
        })
      })
    }
    this.setState({
      cartArray: filterCart,
      quantityArray: [],
      subTotalAmount: 0,
      totalAmount: 0
    }, () => {
      this.calculatePrice();
    })
  }

  getDateValid = (prodArray) => {
    let returnValidate = false;
    if (prodArray.startDate !== "" && prodArray.endDate !== "") {
      let convertStartDate = moment(prodArray.startDate).unix() * 1000;
      let convertEndDate = moment(prodArray.endDate).unix() * 1000;
      let systemTime = moment(new Date()).unix() * 1000;
      if (systemTime <= convertEndDate && systemTime >= convertStartDate) {
        returnValidate = true;
      }
    }
    return returnValidate;
  }

  calculatePrice() {
    const { cartArray, couponAmount, couponApplied, shippingFee } = this.state;
    const { selectedProduct } = this.props
    let subTotalAmount = this.state.subTotalAmount;
    let totalAmount = this.state.totalAmount;

    for (let i = 0; i <= cartArray.length - 1; i++) {
      for (let j = 0; j <= cartArray[i].productVariantId.length - 1; j++) {
        if (selectedProduct.includes(cartArray[i].productVariantId[j]._id)) {

          subTotalAmount +=
            cartArray[i].productVariantId[j].specialPriceProvided === 'true' && this.getDateValid(cartArray[i].productVariantId[j])
              ?
              parseFloat(cartArray[i].productVariantId[j].specialPrice) * parseFloat(cartArray[i].quantity[j])
              :
              parseFloat(cartArray[i].productVariantId[j].Price) * parseFloat(cartArray[i].quantity[j])

          totalAmount +=
            cartArray[i].productVariantId[j].specialPriceProvided === 'true' && this.getDateValid(cartArray[i].productVariantId[j])
              ?
              parseFloat(cartArray[i].productVariantId[j].specialPrice) * parseFloat(cartArray[i].quantity[j])
              :
              parseFloat(cartArray[i].productVariantId[j].Price) * parseFloat(cartArray[i].quantity[j])
        }
        this.setState({
          subTotalAmount: parseFloat(subTotalAmount).toFixed(2),
          totalAmount: parseFloat(totalAmount).toFixed(2),
        })
      }
    }
    if (couponApplied) {
      this.setState({
        totalAmount: parseFloat(totalAmount + shippingFee - couponAmount).toFixed(2),
      })
    } else {
      this.setState({
        totalAmount: parseFloat(totalAmount + shippingFee).toFixed(2),
      }, () => {
        console.log(this.state.totalAmount);
      })
    }
  }

  showItems = (type) => {
    const { totalCartArray } = this.state;
    let filterCart = [];
    if (type === 'all') {
      filterCart = totalCartArray.filter(() => {
        return true;
      })
    } else if (type === 'grocery') {
      filterCart = totalCartArray.filter((eachCart) => {
        return eachCart.productId.some((eachProduct) => {
          return eachProduct.category.includes("Grocery");
        })
      })
    }
    this.setState({
      cartArray: filterCart,
      quantityArray: [],
      subTotalAmount: 0,
      totalAmount: 0
    }, () => {
      this.calculatePrice();
    })
  }

  getChooseForYou = async () => {
    await axios.post(`${window.$URL}sellerProducts/getAllProducts`)
      .then(response => {
        this.setState({
          choosenProduct: response.data
        })
      })
      .catch(error => {
        console.log(error.data)
      })
  }

  toggleSelectProductIndividualVariant = (e, eachVariant) => {
    let selectedProdArray = this.state.selectedProduct;
    const { checked } = e.target;
    if (checked) {
      selectedProdArray.push(eachVariant._id);
    } else {
      let findIndex = selectedProdArray.indexOf(eachVariant._id);
      selectedProdArray.splice(findIndex, 1);
    }
    this.setState({
      selectedProduct: selectedProdArray,
      subTotalAmount: 0,
      totalAmount: 0,
    }, () => {
      this.calculatePrice()
    })
  }

  toggleSelectProductVariant = (e, cartProductArray) => {
    let selectedProdArray = this.state.selectedProduct;
    let quantityArray = this.state.quantityArray;
    let myMartMarginArray = this.state.myMartMarginArray;
    let myMartProfitArray = this.state.myMartProfitArray;
    let PriceArray = this.state.PriceArray;
    let sellerIDArray = this.state.sellerIDArray;
    const { checked } = e.target;
    if (checked) {
      for (let j = 0; j <= cartProductArray.productVariantId.length - 1; j++) {
        selectedProdArray.push(cartProductArray.productVariantId[j]._id);
        quantityArray.push(cartProductArray.quantity[j]);
        myMartMarginArray.push(cartProductArray.productVariantId[j].myMartMargin);
        myMartProfitArray.push(cartProductArray.productVariantId[j].myMartProfit);
        PriceArray.push(cartProductArray.productVariantId[j].Price);
        sellerIDArray.push(cartProductArray.productId[j].sellerId);
      }
    } else {
      for (let j = 0; j <= cartProductArray.productVariantId.length - 1; j++) {
        let findIndex = selectedProdArray.indexOf(cartProductArray.productVariantId[j]._id);
        selectedProdArray.splice(findIndex, 1);
        quantityArray.splice(findIndex, 1);
        myMartMarginArray.splice(findIndex, 1);
        myMartProfitArray.splice(findIndex, 1);
        PriceArray.splice(findIndex, 1);
        sellerIDArray.splice(findIndex, 1);
      }
    }
    this.setState({
      selectedProduct: selectedProdArray,
      quantityArray,
      myMartMarginArray,
      myMartProfitArray,
      PriceArray,
      sellerIDArray,
      subTotalAmount: 0,
      totalAmount: 0,
    }, () => {
      this.calculatePrice()
    })
  }

  selectAllProd = (e) => {
    const { checked } = e.target;
    const { cartArray } = this.state;
    let selectedArray = [];
    let quantityArray = [];
    let myMartMarginArray = [];
    let myMartProfitArray = [];
    let PriceArray = [];
    let sellerIDArray = [];
    if (checked) {
      for (let i = 0; i <= cartArray.length - 1; i++) {
        for (let j = 0; j <= cartArray[i].productVariantId.length - 1; j++) {
          selectedArray.push(cartArray[i].productVariantId[j]._id);
          quantityArray.push(cartArray[i].quantity[j]);
          myMartMarginArray.push(cartArray[i].productVariantId[j].myMartMargin);
          myMartProfitArray.push(cartArray[i].productVariantId[j].myMartProfit);
          PriceArray.push(cartArray[i].productVariantId[j].Price);
          sellerIDArray.push(cartArray[i].productId[j].sellerId);
        }
      }
    } else {
      selectedArray = [];
      quantityArray = [];
    }
    this.setState({
      selectedProduct: selectedArray,
      quantityArray,
      myMartMarginArray,
      myMartProfitArray,
      PriceArray,
      sellerIDArray,
      subTotalAmount: 0,
      totalAmount: 0,
    }, () => {
      this.calculatePrice()
    })
  }

  getmymartShippingCheck = (location, radius) => {
    // console.log('delivery location',location)
    // console.log('delivery radius',radius)
    // console.log('userlocation',this.props.Location)
    const { Location } = this.props;

    var distance = calculateDistance(Location.locationCoordinates[0].coordinates.lat, Location.locationCoordinates[0].coordinates.lng, location.coordinates[1], location.coordinates[0], "K");
    console.log('get distance', distance)
    console.log('get radius', radius)
    if (distance < radius) {
      this.setState({
        deliveryFromMyMart: 'yes'
      })
    } else {
      this.setState({
        deliveryFromMyMart: 'no'
      })
    }
  }

  getDeliveryRule = async () => {
    await axios
      .get(`${window.$URL}deliveryRule/fetchDeliveryRule`)
      .then((response) => {
        if (response.data.success) {
          this.setState({
            deliveryRule: response.data.data,
            deliveryRuleSelected: response.data.data[0],
            timeSlot: response.data.data[0].timeSlot,

          });
        }
        this.getmymartShippingCheck(response.data.data[0].Location, response.data.data[0].Radius)
      })
      .catch((error) => {
        console.log(error.data);
      });
  };

  timeSlot = (slot, index) => {
    this.setState({
      timeSlot: slot,
      deliveryRuleSelected: this.state.deliveryRule[index],
    });
  };

  toggleTimeSlot = () => {
    const { showTimeSlotModal } = this.state;
    if (showTimeSlotModal) {
      this.setState({
        showTimeSlotModal: false,
        timeSlotShow: false
      });
    } else {
      this.setState({
        showTimeSlotModal: true,
        timeSlotShow: true
      });
    }
  };
  deliveryTimeandType = (deliveryData, shippingType) => {
    this.props.updateDeliverytimeType(deliveryData, shippingType)
  }
  timeSlotSelected = (slot) => {
    const { selectedProduct } = this.props
    if (this.state.deliveryFromMyMart === 'yes') {
      var deliveryData = [];
      var shippingType = [];
      for (let i = 0; i < selectedProduct.length; i++) {
        deliveryData.push("")
        shippingType.push("Mymart")
      }
      console.log({ selectedProduct, deliveryData, shippingType });
      this.props.updateDeliverytimeType(deliveryData, shippingType)
    }
    this.props.getDeliveryTimeSlot(slot);
    console.log("delivery slot",slot);
    let slotTime = `${slot.date} by ${slot.from} - ${slot.to}`;
    this.setState({
      ViewSelectedSlot: slotTime,
      showTimeSlotModal: false,
      timeSlotShow: false
    })
  };

  onChange = (e) => {
    const { name, value } = e.target;
    this.setState({
      [name]: value,
    });
  };
  modifydata = (selectedProduct) => {
    var anotherData = selectedProduct
    if (selectedProduct.length > 0) {
      selectedProduct.map((slData, slIndex) => {
        slData.productId.map((prData, prIndex) => {
          if (prData.productType === 'grocery') {
            anotherData[slIndex].productId.splice(prIndex, 1)
            anotherData[slIndex].productVariantId.splice(prIndex, 1)
            anotherData[slIndex].quantity.splice(prIndex, 1)
            anotherData[slIndex].productVariantArray.splice(prIndex, 1)
          }
        })
      })
    }
    return anotherData
  }
  render() {
    const {
      deliveryRule,
      deliveryRuleSelected,
      timeSlot,
      timeSlotShow,
      deliveryFromMyMart,
      showTimeSlotModal
    } = this.state;
    console.log('mymart delivery', deliveryFromMyMart)
    const { loading, cartArray, totalCartArray } = this.state;
    const { selectedProduct } = this.props
    console.log('mymart prod', selectedProduct)
    let withOutGroceryProduct
    if (this.state.timeSlotShow) {
      withOutGroceryProduct = this.modifydata(selectedProduct)
    } else {
      withOutGroceryProduct = []
    }
    console.log('modify product', withOutGroceryProduct)

    return (
      <div className="col-md-8">
        <div className="shipment-Block">
          <div className="shipment-wr">
            <div className="top-header">
              <div className="row">
                <div className="col-lg-9 col-md-8 col-sm-12">
                  <h2 className="gross-title"> Mymart Groceries Shipment</h2>
                  <p className="gros-subtitle">
                    Want all your groceries delivered together?
                  </p>
                </div>
                <div className="col-lg-3  col-md-4 col-sm-12">
                  <div className="check-form">
                    <div className="form-check">
                      <input
                        className="form-check-input"
                        type="radio"
                        name="deliveryFromMyMart"
                        id="gridRadios1"
                        value="yes"
                        checked={deliveryFromMyMart === 'yes' ? true : false}
                        onChange={this.onChange}
                      />
                      <label className="form-check-label" htmlFor="gridRadios1">
                        {" "}
                        Yes
                      </label>
                    </div>
                    <div className="form-check">
                      <input
                        className="form-check-input"
                        type="radio"
                        name="deliveryFromMyMart"
                        id="gridRadios2"
                        value="no"
                        checked={deliveryFromMyMart === 'no' ? true : false}
                        onChange={this.onChange}
                      />
                      <label className="form-check-label" htmlFor="gridRadios2">
                        {" "}
                        No
                      </label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div
              className="shipment-inner"
              style={
                deliveryFromMyMart === "yes"
                  ? { display: "block" }
                  : { display: "none" }
              }
            >
              <p className="choose-delivery-opt">Choose your delivery option</p>
              <div className="row">
                {deliveryRule.map((data, index) => {
                  return (
                    <div
                      className="col-lg-4 col-md-4 col-6 equal"
                      key={index}
                    >
                      <label className="schedule-slot">
                        <input
                          type="radio"
                          name="test"
                          value={data.timmeSlot}
                          checked={data.timeSlot === timeSlot ? true : false}
                          onChange={() => this.timeSlot(data.timeSlot, index)}
                        />
                        <div className="sch-slot-slct">
                          <h2>{`QAR ${data.deliveryCharge}`}</h2>
                          <h3>Scheduled ({data.timeSlot}-hour slots) </h3>
                          <p>Select your preferred slot</p>
                        </div>
                      </label>
                    </div>
                  );
                })}
              </div>
              <div className="slect-time-slot-wr">
                <p>Delivery Time</p>
                <button
                  className="btn btn-delivery"
                  onClick={this.toggleTimeSlot}
                >
                  {" "}
                  Select your slot
                </button>
                <p style={{marginTop:10}}>{this.state.ViewSelectedSlot !== '' ? 'Est, Arrival' : null} {this.state.ViewSelectedSlot !== '' ? this.state.ViewSelectedSlot : null}</p>
              </div>
                
             




              {timeSlotShow ? (
                <TimeSlot
                  modalStatus={showTimeSlotModal}
                  deliveryRule={deliveryRule}
                  deliveryRuleSelected={deliveryRuleSelected}
                  timeSlot={timeSlot}
                  selectedTimeSlot={(slot) => this.timeSlotSelected(slot)}

                  getTimeSlot={this.timeSlot}
                />

              ) : null}

              {/* <TimeSlotModal modalStatus={showTimeSlotModal} timeSlot={timeSlot} deliveryRuleSelected={deliveryRuleSelected} deliveryRule={deliveryRule}/> */}

            </div>
            <div className="container" style={
              deliveryFromMyMart === "no"
                ? { display: "block" }
                : { display: "none" }
            }>
              <div className="cart-block-content">
                <div className="cart-items-content">
                  <div className="row">
                    <CartDeliveryProduct
                      totalCartArray={totalCartArray}
                      cartArray={cartArray}
                      selectedProduct={selectedProduct}
                      selectedTimeSlot={(slot) => this.timeSlotSelected(slot)}
                      getanotherType={(deliveryData, shippingType) => this.deliveryTimeandType(deliveryData, shippingType)}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  Location: state.Location,
});

export default connect(mapStateToProps, {})(CartDeliverySlot);
