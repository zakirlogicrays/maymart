import React, { Component } from 'react';
import GoogleMaps from "../GoogleMaps";
import GoogleMapAddress from "../GoogleMapAddress";
import { connect } from 'react-redux';
import { AddToLocation } from '../../data/reducers/location';
import LocationTextBox from "../LocationTextBox";
import jwt_decode from 'jwt-decode';
import AddressBook from "../UserAddress"
import { BiCurrentLocation } from 'react-icons/bi';
import { IoMdLocate } from 'react-icons/io';
import { GiPositionMarker } from 'react-icons/gi';
import SelectedStoresImg from '../../assets/images/selected-stores-img.png';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import LocationBoxIcon from '../../assets/images/bx_bx-current-location.png';
import { login } from './../../util/APIUtills';


class AddNewAddress extends Component {
    state = {
        locationCoordinates: {
            lat: 22.309425,
            lng: 72.136230
        },
        locateMe: [],
        Address: '',
        loggedIn: false,
        postion: undefined,
        buildingNo: undefined,
        zone: undefined,
        Street: undefined
    }

    componentDidMount() {
        const { location } = this.props;
        if (location.locationCoordinates.length > 0) {
            this.setState({
                Address: location.locationCoordinates[0].name,
                locationCoordinates: location.locationCoordinates[0].coordinates
            })
        } else {
            this.setState({
                Address: 'Doha, Qatar'
            })
        }
        this.checkUserLogin();
    }

    checkUserLogin = async () => {
        let checkToken = await localStorage.getItem("token");
        if (checkToken) {
            const decoded = jwt_decode(checkToken);
            if (decoded.userType === "customer") {
                this.setState({
                    loggedIn: true
                })
            }
        }
    }

    onMarkerDragEnd = async (coordinate) => {
        console.log("here -----")
        await fetch(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${coordinate.lat},${coordinate.lng}&key=${process.env.REACT_APP_GOOGLE_API_KEY}& type=locality&region=qa`)
            .then(response => response.json())
            .then(data => {
                console.log("data ------>", data);
                if (data.status === 'OK') {
                    let filterCountry = data.results[0].address_components.filter(eachData => {
                        return eachData.types.includes('country');
                    });
                    console.log("filterCountry ---->", filterCountry);
                    if (filterCountry[0].short_name.toLowerCase() === 'qa') {
                        console.log("data.results[0].formatted_address ---->", data.results[0].formatted_address);
                        this.setState({
                            Address: data.results[0].formatted_address,
                            locationCoordinates: coordinate,
                            locateMe: coordinate
                        })
                    }
                }
            })
            .catch(error => {
                console.log(error);
            })
    }

    getCoordinates = (locationArray, locationName) => {
        this.setState({
            locationCoordinates: locationArray,
            Address: locationName,
            postion: locationArray
        })
    }

    saveLocation = () => {
        const { locationCoordinates, Address } = this.state;
        let detailsArray = [{
            coordinates: locationCoordinates,
            name: Address
        }];
        this.props.AddToLocation(detailsArray);
        this.props.hideLocationModal();
    }

    saveDefaultAddress = () => {
        this.props.hideLocationModal();
    }

    getImmediate() {
        return new Promise((resolve, reject) => 
            navigator.geolocation.getCurrentPosition(
              position => {
                const lat = position.coords.latitude;
                const lng = position.coords.longitude;
                resolve({ lat, lng});
              },
              error => (error.message),
              {
                enableHighAccuracy: true,
                timeout: 20000,
                maximumAge: 1000
              }
            )
        );
    }
    async saveNow(){
        let coordinates = await this.getImmediate();
        console.log("coordinates ---->", coordinates);
        //save to the database
         this.setState({postion: coordinates})
      }



    getUserLocation = () => {
        navigator.geolocation.getCurrentPosition(({ coords: { latitude: lat, longitude: lng } }) => {
            let coordinates = {
                lat: lat,
                lng: lng
            }
            this.onMarkerDragEnd(coordinates);
        })
    }

    render() {
        const { locationCoordinates, Address, loggedIn, postion } = this.state;
        const { modalStatus } = this.props;
        return (
            <div className={modalStatus ? "modal fade show" : "modal fade"} style={modalStatus ? { display: 'block' } : { display: 'none' }} id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
                    <div className="modal-content address-popup add-new-address-tabs">
                        {
                            window.location.pathname.split("/")[4] === 'grocery' ?
                                null
                                :
                                <div className="modal-header">
                                    <h5 className="modal-title">Add New Address</h5>
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.saveDefaultAddress}>
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                        }
                        <div className="modal-body modalHeight ">
                            <div className="add-new-address-tabs">
                                <Tabs>
                                    <TabList>
                                        <Tab>Choose via Map</Tab>
                                        <Tab>Choose via Blue Plate</Tab>
                                    </TabList>

                                    <TabPanel>
                                        {
                                            loggedIn ?
                                            <div className="add-new-address-map">
                                                    <div className="address-search-and-location">
                                                        {!this.state.postion &&<div className="custom-location-box" onClick={() => this.saveNow()}>
                                                            <img src={LocationBoxIcon} alt={LocationBoxIcon} /> <span>Define my current location</span>
                                                        </div>}
                                                        {!this.state.postion && <LocationTextBox fetchLocation={(locArray, name) => this.getCoordinates(locArray, name)} Address={Address} />}
                                                    </div>
                                                    <div className="map" style={{opacity: this.state.postion ? 1 : 0.5}}>
                                                        <GoogleMapAddress coordinates={this.state.postion} draggable={true} getCurrentPosition={(coord) => this.onMarkerDragEnd(coord)} />
                                                        <button type="button" onClick={this.getUserLocation} className="btn signup_with_gmail pull-right rounded-0"><BiCurrentLocation /></button>
                                                    </div>
                                            </div>
                                                :
                                                <div className="add-new-address-map">
                                                    <div className="address-search-and-location">
                                                        {!this.state.postion &&<div className="custom-location-box" onClick={() => this.saveNow()}>
                                                            <img src={LocationBoxIcon} alt={LocationBoxIcon} /> <span>Define my current location</span>
                                                        </div>}
                                                        {!this.state.postion && <LocationTextBox fetchLocation={(locArray, name) => this.getCoordinates(locArray, name)} Address={Address} />}
                                                    </div>
                                                    <div className="map" style={{opacity: this.state.postion ? 1 : 0.5}}>
                                                        <GoogleMapAddress coordinates={this.state.postion} draggable={true} getCurrentPosition={(coord) => this.onMarkerDragEnd(coord)} />
                                                        <button type="button" onClick={this.getUserLocation} className="btn signup_with_gmail pull-right rounded-0"><BiCurrentLocation /></button>
                                                    </div>
                                                </div>
                                        }
                                    </TabPanel>
                                    <TabPanel>
                                        {
                                            loggedIn ?
                                            <div className="add-new-address-map-step-2">
                                            <div className="searched-location-info">
                                                <h2>Blue Plate address</h2>
                                                <p>Please enter your blue plate details to retrieve your location from the database of <br></br>
                                                    Ministry of Municipality & Environment</p>
                                                <ul>
                                                    <li>
                                                        <span>Building No.</span>
                                                        <input type="text" placeholder="ex: 478" style={{border: 'none'}} className="plat-address-input" onChange={(e) => this.setState({buildingNo: e.target.value})}/>
                                                        {/* <p>ex: 478</p> */}
                                                    </li>
                                                    <li>
                                                        <span>Zone</span>
                                                        <input type="text" placeholder="ex:  90" style={{border: 'none'}} className="plat-address-input" onChange={(e) => this.setState({zone: e.target.value})}/>
                                                        {/* <p>ex:  90</p> */}
                                                    </li>
                                                    <li>
                                                        <span>Street</span>
                                                        <input type="text" placeholder="ex:  625" style={{border: 'none'}} className="plat-address-input" onChange={(e) => this.setState({Street: e.target.value})}/>
                                                        {/* <p>ex:  625</p> */}
                                                    </li>
                                                </ul>
                                            </div>
                                            <div className="map">
                                                <GoogleMaps coordinates={locationCoordinates} draggable={true} getCurrentPosition={(coord) => this.onMarkerDragEnd(coord)} />
                                                <button type="button" onClick={this.getUserLocation} className="btn signup_with_gmail pull-right rounded-0"><BiCurrentLocation /></button>
                                            </div>
                                        </div>
                                                :
                                                <div className="add-new-address-map-step-2">
                                                    <div className="searched-location-info">
                                                        <h2>Blue Plate address</h2>
                                                        <p>Please enter your blue plate details to retrieve your location from the database of <br></br>
                                                            Ministry of Municipality & Environment</p>
                                                        <ul>
                                                            <li>
                                                                <span>Building No.</span>
                                                                <input type="text" placeholder="ex: 478" style={{border: 'none'}} className="plat-address-input" onChange={(e) => this.setState({buildingNo: e.target.value})}/>
                                                                {/* <p>ex: 478</p> */}
                                                            </li>
                                                            <li>
                                                                <span>Zone</span>
                                                                <input type="text" placeholder="ex:  90" style={{border: 'none'}} className="plat-address-input" onChange={(e) => this.setState({zone: e.target.value})}/>
                                                                {/* <p>ex:  90</p> */}
                                                            </li>
                                                            <li>
                                                                <span>Street</span>
                                                                <input type="text" placeholder="ex:  625" style={{border: 'none'}} className="plat-address-input" onChange={(e) => this.setState({Street: e.target.value})}/>
                                                                {/* <p>ex:  625</p> */}
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div className="map">
                                                        <GoogleMaps coordinates={locationCoordinates} draggable={true} getCurrentPosition={(coord) => this.onMarkerDragEnd(coord)} />
                                                        <button type="button" onClick={this.getUserLocation} className="btn signup_with_gmail pull-right rounded-0"><BiCurrentLocation /></button>
                                                    </div>
                                                </div>
                                        }
                                    </TabPanel>
                                </Tabs>
                            </div>
                            {/* <div className="location-footer">
                                <p className=" notably"><IoMdLocate /> To get your current location or click desired location in map to bring the <GiPositionMarker /></p>
                            </div> */}


                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn cancel">Cancel</button>
                            <button type="button" className="btn next">Next</button>

                        </div>

                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    location: state.Location,
})

export default connect(
    mapStateToProps,
    { AddToLocation }
)(AddNewAddress);