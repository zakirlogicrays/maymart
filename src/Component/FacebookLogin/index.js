import React, { Component } from 'react';
import axios from "axios";
import jwt_decode from "jwt-decode";
import FacebookLoginApp from 'react-facebook-login';
import { connect } from 'react-redux';
import { AddToCat, UpdateCart } from '../../data/reducers/cart';


class FacebookLogin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            successMessage: '',
            success: false,
            errorMessage: '',
            error: false
        };
    }

    responseFacebook = (response) => {
        console.log(response);
        //var res = response.profileObj;  

        this.signup(response);
    }
    signup = async (res) => {
        console.log(res.error)
        if (res.error) {
            console.log(res.error)
        } else {


            // const responseFacebook = {  
            //     Name: res.name,  
            //     email: res.email,  
            //     token: res.accessToken,  
            //     Image: res.picture.data.url,  
            //     ProviderId: 'Facebook'  

            //   }
            //   debugger;  
            //   axios.post('http://localhost:60200/Api/Login/SocialmediaData', responseFacebook)  
            //       .then((result) => {  
            //       let responseJson = result;  
            //       console.log(result.data.name);  
            //       alert("data");  
            //       sessionStorage.setItem("userData", JSON.stringify(result));  
            //       this.props.history.push('/Dashboard')  
            //       });      facebook
            var displayname = res.name;
            var tmp = displayname.split(" ");
            displayname = tmp[0] + " " + tmp[tmp.length - 1];
            var firstName = displayname.split(' ').slice(0, -1).join(' ');
            var lastName = displayname.split(' ').slice(-1).join(' ');

            let registerData = {
                "fname": firstName,
                "lname": lastName,
                "email": res.email,
                "UserType": "customer",
                "providerType": "facebook"
            };
            console.log(registerData);

            await axios.post(`${window.$URL}user/customerGooglesignup`, registerData)
                .then(response => {
                    if (response.status === 200) {
                        if (response.data.success) {
                            this.setState({
                                success: true,
                                successMessage: response.data.message
                            }, async () => {
                                localStorage.setItem("token", response.data.token);

                                const decoded = jwt_decode(response.data.token);
                                let lastVisit = await localStorage.getItem("lastVisited");
                                let lastVisitParse = JSON.parse(lastVisit);
                                if (lastVisitParse) {
                                    lastVisitParse.customerId = decoded.userid;
                                    this.addToCart(lastVisitParse);
                                } else {
                                    this.props.UpdateCart();
                                    setTimeout(() => {
                                        window.location.href = `${process.env.PUBLIC_URL}/`;
                                    }, 1000)
                                }
                            })
                        } else {
                            this.setState({
                                error: true,
                                errorMessage: response.data.message
                            })
                        }
                    }
                })
                .catch(error => {
                    console.log(error);
                })
        }
    }

    addToCart = async (lastVisitParse) => {
        this.props.AddToCat(lastVisitParse);
    }

    render() {

        return (
            <div className="facebook">
                <FacebookLoginApp
                    buttonStyle={{ padding: "6px" }}
                    appId="1138698449910763"
                    autoLoad={false}
                    fields="name,email,picture"
                    callback={this.responseFacebook}
                    icon="fa-facebook"
                    textButton=" Facebook"
                />
            </div>
        );
    }
}


const mapStateToProps = (state) => ({
    CartProducts: state.Cart,
})

export default connect(
    mapStateToProps,
    { AddToCat, UpdateCart }
)(FacebookLogin);

