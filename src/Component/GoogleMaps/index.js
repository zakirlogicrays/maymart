import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';

import markerPointer from "../../assets/images/MyLocationCenter.png";
import centerPointer from "../../assets/images/Store Location.png"

import locationtooltipimg from "../../assets/images/locationtooltipimg.png"
import locationtime from "../../assets/images/locationtime.png"

const AnyReactComponent = ({ img_src, item, onMouseOver, selectedMark, onMouseLeave }) => <div style={{width: 50}}>
        {item?.id === selectedMark?.id && 
        <div className="location-tooltip" style={{zIndex: 10}}>
          <ul>
            <li>
              <div className="location-item">
                <div className="location-img" >
                  <img src={locationtooltipimg} alt={locationtooltipimg} />
                </div>
                <div className="location-text">
                  <p>Greenmart <span>1.5 Km</span> </p>
                  <span>Grocery</span>
                </div>
              </div>
            </li>
            <li className="delivery-text"> Delivery by <span className="time-pm">5 PM</span> <strong>Today</strong> </li>
            <li className="time-text"> <img src={locationtime} alt={locationtime} /> <span className="bottom-time">10AM - 10AM</span> </li>
          </ul>
          
        </div>}
        <img onMouseOver={() => onMouseOver(item)} onMouseLeave={() => onMouseLeave(item)} src={img_src} className="map-pointer" />
   </div>;
const AnyReactComponentCenter = ({ img_src, onClick }) => 
<div style={{width: 100}}><img src={img_src} className="map-pointer" /></div>;

export default class GoogleMaps extends Component {
  state = {
    markers: [
      // {
      //   lat: 22.309425,
      //   lng: 72.136230
      // },
      {
        id: 1,
        lat: 22.3039,
        lng: 70.8022
      },
      // {
      //   lat: 22.3032,
      //   lng: 70.7874
      // },
      {
        id: 2,
        lat: 22.301875,
        lng: 70.777872
      },
      {
        id: 3,
        lat: 22.26218,
        lng: 70.809589
      },
      {
        id: 4,
        lat: 22.302867,
        lng: 70.816499
      },
      {
        id: 5,
        lat: 22.2936479,
        lng: 70.8008461
      }
  ],
  selectedMark: undefined
  }


  // componentDidUpdate(prevProps){
  //  if(prevProps.coordinates.lat !== this.props.coordinates.lat){
  //    this.setState({
  //      markers : this.props.coordinates
  //    })
  //  }
  // }

  markerClicked(marker) {
    let markerArray = [];
    markerArray.push({
      lat: marker.lat,
      lng: marker.lng
    });
    this.setState({
      markers: markerArray
    }, () => {
      let coordinates = {
        lat: marker.lat,
        lng: marker.lng
      }
      this.props.getCurrentPosition(coordinates);
    })
  }

  _onChildMouseEnter = () => {
    console.log("here in the hello");
  }

  render() {
    const { coordinates, draggable } = this.props;
    const { markers } = this.state;
    if (draggable) {
      return (
        <GoogleMapReact
          bootstrapURLKeys={{ key: process.env.REACT_APP_GOOGLE_API_KEY }}
          center={markers.length > 0 ? markers[1] : coordinates}
          defaultZoom={14}
          //onClick={this.markerClicked.bind(this)}
          onGoogleApiLoaded={({ map, maps }) =>
              new maps.Circle({
                strokeColor: 'rgb(238, 150, 35)',
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: 'rgb(238, 150, 35)',
                fillOpacity: 0.16,
                map,
                center: {lat: 22.3032, lng: 70.7874},
                radius: 1200,
          })}
        >
          {
                  <AnyReactComponentCenter
                    lat={22.3032}
                    lng={70.7874}
                    img_src={markerPointer}
                    // onClick={(e) => console.log("e ------->", e)}
                  />
          }
          {
            markers.length > 0 ?
              markers.map((marker, i) => {
                return (
                  <AnyReactComponent
                    lat={marker.lat}
                    lng={marker.lng}
                    img_src={centerPointer}
                    item={marker}
                    onMouseOver={(e) => this.setState({selectedMark: e})}
                    onMouseLeave={(e) => this.setState({selectedMark: undefined})}
                    selectedMark={this?.state?.selectedMark}
                  />

                )
              })
              :
              <AnyReactComponent
                lat={coordinates.lat}
                lng={coordinates.lng}
                img_src={markerPointer}
              />
          }
        </GoogleMapReact>
      );
    } else {
      return (
        <GoogleMapReact
          bootstrapURLKeys={{ key: process.env.REACT_APP_GOOGLE_API_KEY }}
          center={coordinates}
          defaultZoom={12}
        >
          <AnyReactComponent
            lat={coordinates.lat}
            lng={coordinates.lng}
            img_src={markerPointer}
          />
        </GoogleMapReact>
      );
    }

  }
}