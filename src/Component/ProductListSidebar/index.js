import React, { Component } from "react";
import ReactStars from "react-rating-stars-component";
import axios from "axios";
const ProductRating = {
    size: 30,
    value: 0,
    edit: true,
    isHalf: true
};
export default class ProductListSidebar extends Component {
    state = {
        searchCat: [],
        searchCountry: [],
        searchBrand: [],
        checkedCat: [],
        checkedCon: [],
        checkedBrand: [],
        checkedAttribute: [],
        checkedAttributeOption: [],
        maxval: '',
        minval: '',
        sidebarToggle: false,
        toggleShow: [],
        categoryLevels: [],
        dummyCategoriesLevel1Data: [
            {
                parent: "Electronic Devices",
                categoryName: "Electronic Devices"
            },
            {
                parent: "Electronic Devices",
                categoryName: "smartPhone"
            },
            {
                parent: "Electronic Devices",
                categoryName: "Laptop & Computer"
            },
            {
                parent: "Electronic Devices",
                categoryName: "Camera"
            },
            {
                parent: "Electronic Devices",
                categoryName: "PC"
            },
            {
                parent: "Drinks",
                categoryName: "Drinks"
            },
            {
                parent: "Drinks",
                categoryName: "cock"
            },
            {
                parent: "Drinks",
                categoryName: "thums up"
            },
            {
                parent: "Drinks",
                categoryName: "sprite"
            },
            {
                parent: "Drinks",
                categoryName: "mountent due"
            },            
        ],
        filterdSubCategory: [],
        selectedCategory: undefined,
        viewAll: false
    }

    async componentDidMount() {
        this.getsearchCatData(this.props.catData)
        this.getsearchConData(this.props.contryData)
        this.getsearchBrndData(this.props.brandData)
    }

    async componentDidUpdate(prevProps) {
        //  this.setState({checkedBrand : this.props.filteredBrand})
        if (prevProps.catData !== this.props.catData) {
            this.getsearchCatData(this.props.catData)
        }
        if (prevProps.contryData !== this.props.contryData) {
            this.getsearchConData(this.props.contryData)
        }
        if (prevProps.brandData !== this.props.brandData) {
            this.getsearchBrndData(this.props.brandData)
        }
    }

    

    getsearchCatData = (data) => {
        if (data.length > 0) {
            this.setState({
                searchCat: data
            })
        }
    }

    getsearchConData = (data) => {
        if (data.length > 0) {
            this.setState({
                searchCountry: data
            })
        }
    }

    getsearchBrndData = (data) => {
        if (data.length > 0) {
            this.setState({
                searchBrand: data
            })
        }
    }

    handleToggle = (catData, filtername) => {
        const { checkedCat } = this.state;
        const currentIndex = checkedCat.indexOf(catData);
        const newChecked = [...checkedCat];
        if (currentIndex === -1) {
            newChecked.push(catData)
        } else {
            newChecked.splice(currentIndex, 1)
        }

        this.setState({ checkedCat: newChecked })
        this.props.showFilteredResult(newChecked, filtername)
    }

    // handleTogglecountry = (conData, filtername) => {
    //     const { checkedCon } = this.state;
    //     const currentIndex = checkedCon.indexOf(conData);
    //     const newChecked = [...checkedCon];

    //     if (currentIndex === -1) {
    //         newChecked.push(conData)
    //     } else {
    //         newChecked.splice(currentIndex, 1)
    //     }
    //     this.setState({ checkedCon: newChecked })
    //     this.props.showFilteredcountResult(newChecked, filtername)
    // }

    // handleTogglebrand = (brandData, filtername) => {
    //     //console.log("brandData ------>", brandData);
    //     const { checkedBrand } = this.state
    //     const currentIndex = checkedBrand.indexOf(brandData)
    //     const newChecked = [...checkedBrand]
    //     if (currentIndex === -1) {
    //         newChecked.push(brandData)
    //     } else {
    //         newChecked.splice(currentIndex, 1)
    //     }
    //     this.setState({ checkedBrand: newChecked })
    //     this.props.showFilteredbrandResult(newChecked, filtername)
    // }

    handleToggleAttribute = (option,filtername) => {
        const {checkedAttribute, checkedAttributeOption} = this.state;
        let attributeIndex = checkedAttribute.indexOf(filtername);
        if(attributeIndex === -1){
            let currentIndex = checkedAttribute.length;
            checkedAttribute.push(filtername);
            checkedAttributeOption[currentIndex] = [option];
        }else{
            if(!checkedAttributeOption[attributeIndex].includes(option)){
                checkedAttributeOption[attributeIndex].push(option);
            }else{
                let getOptionIndex = checkedAttributeOption[attributeIndex].indexOf(option)
                checkedAttributeOption[attributeIndex].splice(getOptionIndex, 1);
                if(checkedAttributeOption[attributeIndex].length < 1){
                    checkedAttribute.splice(attributeIndex, 1);
                    checkedAttributeOption.splice(attributeIndex, 1);
                }
            }
        }
        this.setState({ checkedAttribute, checkedAttributeOption })
        this.props.showFilteredAttributesResult(checkedAttribute, checkedAttributeOption)
    }

    getChange = (e) => {
        e.preventDefault();
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    getminval = (e) => {
        e.preventDefault();
        const {minval, maxval} = this.state;
        let priceRange = {minval, maxval};
        if(minval !== '' && maxval !== ''){
            this.props.showFilteredPriceResult(priceRange,'PriceRange')
        }
    }

    handleSidebarMobileToggle = () => {
        this.setState({ sidebarToggle: !this.state.sidebarToggle });
    };

    toggle = (type) => {
        let toggleShowArray = this.state.toggleShow;
    
        if (toggleShowArray.includes(type)) {
          let findIndex = toggleShowArray.indexOf(type);
          if (findIndex !== -1) {
            toggleShowArray.splice(findIndex, 1);
          }
        } else {
          toggleShowArray.push(type);
        }
        this.setState({
          toggleShow: toggleShowArray
        })
    }

    ratingSubmit = async (ratingVal) => {
        this.props.showFilteredRatingResult(ratingVal,'rating')
    }

    findSubCategory = async (parent) => {
        let filterData = this.state.dummyCategoriesLevel1Data.filter((item) => (item.parent === parent && item.parent !== item.categoryName) && item);
        console.log("filterData ---->", filterData);
        this.setState({filterdSubCategory: filterData, selectedCategory: parent})
    }

    render() {
        const {sidebarToggle,toggleShow} = this.state;
        const {otherAttributes} = this.props;
        console.log("checkedBrand ---->", this.props.checkedBrand);
        // console.log("searchCountry ------>", this.state.searchCountry);
        return (
            <div className="col-md-3 leftPannel">
                <div className="sm-search-address filter-new-wr">
                        <button className="btn filter" onClick={this.handleSidebarMobileToggle}>
                        <i className="fa fa-filter" aria-hidden="true"></i>
                        </button>
                    </div>

                    <div className="lv-menu-sec"  >
                        <div className={sidebarToggle ? "menu-show " : "menu-hide"}  >
                            <button className="btn cancel" onClick={this.handleSidebarMobileToggle}>X</button>
                            <div className="inner-filter-container">
                            <div className="ca-filter">
                                <p className="title">Category <i className={toggleShow.includes('Category') ? "fa fa-angle-up" : "fa fa-angle-down"} aria-hidden="true" onClick={() => this.toggle('Category')}></i></p>
                                <div className={toggleShow.includes('Category') ? "collapse-content show" : "collapse-content"}>
                                    {this.state.searchCat.map((catData, catindex) => {
                                        return (
                                            <button className={this.state.checkedCat.includes(catData) ? "tsgs active": "tsgs inactive"} key={catindex} onClick={() => this.handleToggle(catData, 'category')}>{catData}</button>
                                        )
                                    })}
                                </div>
                            </div>
                            <div className="ca-filter">
                                <p className="title">Country of origin <i className={toggleShow.includes('Country of origin') ? "fa fa-angle-up" : "fa fa-angle-down"} aria-hidden="true" onClick={() => this.toggle('Country of origin')}></i></p>
                                <div className={toggleShow.includes('Country of origin') ? "collapse-content show" : "collapse-content"}>
                                    {
                                        this.state.searchCountry.map((conData, conIndex) => {
                                            return (
                                                <button className={this.state.checkedCon.includes(conData) ?"tsgs active":'tsgs inactive'} onClick={() => this.props.handleTogglecountry(conData, 'country')} key={conIndex}>{conData}</button>
                                            )
                                        })
                                    }
                                </div>
                            </div>
                            <div className="ca-filter">
                                <p className="title">Brand <i className={toggleShow.includes('Brand') ? "fa fa-angle-up" : "fa fa-angle-down"} aria-hidden="true" onClick={() => this.toggle('Brand')}></i></p>
                                <div className={toggleShow.includes('Brand') ? "collapse-content show" : "collapse-content"}>
                                {
                                    this.state.searchBrand.map((brandData, brandindex) => {
                                        return (
                                            <button className={this.props?.checkedBrand?.includes(brandData) ?"tsgs active":'tsgs inactive'} onClick={() => this.props.handleTogglebrand(brandData, 'brand')} key={brandindex}>{brandData}</button>
                                        )
                                    })
                                }
                                </div>
                            </div>
                            {
                                Object.keys(otherAttributes).map((eachAttri, eachAttriIndex) => {
                                    return(
                                        <div className="ca-filter" key={eachAttriIndex}>
                                            <p className="title">{eachAttri} <i className={toggleShow.includes(eachAttri) ? "fa fa-angle-up" : "fa fa-angle-down"} aria-hidden="true" onClick={() => this.toggle(eachAttri)}></i></p>
                                            <div className={toggleShow.includes(eachAttri) ? "collapse-content show" : "collapse-content"}>
                                                {
                                                    Object.keys(otherAttributes[eachAttri]).map((options, optionIndex) => {
                                                        return (
                                                            <button className="tsgs" onClick={() => this.handleToggleAttribute(otherAttributes[eachAttri][optionIndex], eachAttri)} key={optionIndex}>{otherAttributes[eachAttri][optionIndex]}</button>
                                                        )
                                                    })
                                                }
                                            </div>
                                        </div>
                                    )
                                })
                            }
                            

                            <div className="ca-filter">
                                <p className="title">Price</p>
                                <div className="min-max-wrapper">
                                <input type="number" className="form-control" placeholder="min" onChange={this.getChange} />
                                <span className="lines"> </span>
                                <input type="text" className="form-control" placeholder="max" onChange={this.getChange} />
                                <button className="btn " onClick={this.getminval}><i className="fa fa-play" aria-hidden="true"></i></button>
                                </div>
                            </div>
                            <div className="st-action-button">
                                <button type="button" className="btn" onClick={() => window.location.reload()}>Reset</button>
                                <button type="button" className="btn" onClick={this.handleSidebarMobileToggle}>Done</button>
                            </div> </div>
                        </div>
                    </div>
                    

                <div className="list_left_bg">
                    {/* <div className="filter-section">
                        <h2 className="filter">filter:</h2>
                        <p className="clearall" onClick={() => window.location.reload()}>Clear all </p>
                    </div> */}
                    
                   
                    
                    {
                        !this.state.searchCat.length > 0
                            ?
                            <div className="product-cat-section">
                                <p className="cat-title-text">Category</p>
                                <div className="prod-list scrollVerical" style={{ overflow: 'auto' }}>
                                    <form>
                                        {this.state.searchCat.map((catData, catindex) => {
                                            return (
                                                <div className="form-group" style={{ border: 'none' }} key={catindex}>
                                                    <label className="c-s-checkbox ">
                                                        {catData}
                                                        <input type="checkbox" onChange={() => this.handleToggle(catData, 'category')} />
                                                        <span className="checkmark"></span>
                                                    </label>
                                                </div>
                                            )
                                        })}
                                    </form>
                                </div>
                            </div>
                            :
                            null
                    }

                    <div className="product-cat-section">
                        <p className="cat-title-text">Filter by</p>

                        <div className="prod-list">
                            <div className="min_max_wr">
                                <div className="form-group min_max">
                                    <input
                                        type="text"
                                        className="form-control"
                                        placeholder="Min"
                                        name="minval"
                                        onChange={this.getChange}
                                    />
                                </div>
                                <div className="minus">-</div>
                                <div className="form-group min_max">
                                    <input
                                        type="text"
                                        className="form-control"
                                        placeholder="Max"
                                        name="maxval"
                                        onChange={this.getChange}
                                    />
                                </div>
                                <button className="btn playBtn" onClick={this.getminval}>
                                    <i className="fa fa-play"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    {/* <!-- end min max --> */}
                    {
                        this.state.searchCountry.length > 0
                            ?
                            <div className="product-cat-section">
                                <p className="cat-title-text">Categories</p>
                                <div className="prod-list">
                                    {this.state.dummyCategoriesLevel1Data?.map((item) => {
                                        if(item.parent === item.categoryName){
                                            return(
                                                <div style={{marginTop: 15, marginBottom: 15, cursor: "pointer" }} onClick={() => this.findSubCategory(item.parent)}>
                                                    <div style={{display: "flex", justifyContent: "space-between"}}>
                                                        <p>{item.parent}</p>
                                                        <i className="fa fa-angle-right" aria-hidden="true" style={{marginTop: 6}}></i>
                                                    </div>
                                                    {this.state.selectedCategory === item.parent && 
                                                    <div>
                                                        {this.state.filterdSubCategory?.slice(0, this.state.viewAll ? this.state.filterdSubCategory.length : 3)?.map((item) => (
                                                            <p style={{color: "lightgray"}}>{item.categoryName}</p>
                                                        ))}
                                                        <p onClick={() => this.setState({viewAll: !this.state.viewAll})} style={{color: "blue"}}>View All</p>
                                                    </div>
                                                    }
                                                </div>
                                            )
                                        }
                                    })}
                                </div>
                            </div>
                            :
                            null
                    }
                    {
                        this.state.searchCountry.length > 0
                            ?
                            <div className="product-cat-section">
                                <p className="cat-title-text">Country of origin</p>
                                <div className="prod-list">
                                    <form>
                                        {
                                            this.state.searchCountry.map((conData, conIndex) => {
                                                return (
                                                    <div className="form-group" key={conIndex}>
                                                        <label className="c-s-checkbox ">
                                                            {conData}
                                                            <input type="checkbox" onChange={() => this.props.handleTogglecountry(conData, 'country')} id={conData} type="checkbox" checked={this.props?.checkedCon?.some(chckcon => chckcon === conData)}/>
                                                            <span className="checkmark"></span>
                                                        </label>
                                                    </div>
                                                )
                                            })
                                        }
                                    </form>
                                </div>
                            </div>
                            :
                            null
                    }
                    {
                        this.state.searchBrand.length > 0
                            ?
                            <div className="product-cat-section">
                                <p className="cat-title-text">Brand</p>
                                {
                                    this.state.searchBrand.map((brandData, brandindex) => {
                                        return (
                                            <div className="prod-list" key={brandindex}>
                                                <form>
                                                    <div className="form-group">
                                                        <label className="c-s-checkbox ">
                                                            {" "}
                                                            {brandData}
                                                            <input onChange={() => this.props.handleTogglebrand(brandData, 'brand')} id={brandData} type="checkbox" checked={this.props?.checkedBrand?.some(chckBrand => chckBrand === brandData)} />
                                                            <span className="checkmark"></span>
                                                        </label>
                                                    </div>
                                                </form>
                                            </div>
                                        )
                                    })
                                }

                            </div>
                            :
                            null
                    }

                    {
                        Object.keys(otherAttributes).length > 0
                            ?
                            <>
                            {
                                Object.keys(otherAttributes).map((eachAttri, eachAttriIndex) => {
                                    return(
                                        <div className="product-cat-section" key={eachAttriIndex}>
                                            <p className="cat-title-text">{eachAttri}</p>
                                            {
                                                Object.keys(otherAttributes[eachAttri]).map((options, optionIndex) => {
                                                    return (
                                                        <div className="prod-list" key={optionIndex}>
                                                            <form>
                                                                <div className="form-group">
                                                                    <label className="c-s-checkbox ">
                                                                        {" "}
                                                                        {otherAttributes[eachAttri][optionIndex]}
                                                                        <input type="checkbox" onChange={() => this.handleToggleAttribute(otherAttributes[eachAttri][optionIndex], eachAttri)} />
                                                                        <span className="checkmark"></span>
                                                                    </label>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    )
                                                })
                                            }

                                        </div>
                                    )
                                })
                            }
                            
                            </>
                            :
                            null
                    }

                    <div className="product-cat-section">
                        <p className="cat-title-text">Rating</p>

                        <div className="prod-list">
                            <ReactStars onChange={(val) => this.ratingSubmit(val)} {...ProductRating} />
                            {/*<div className="rating_new">
                                <span className="star-full">
                                    <i className="fa fa-star" aria-hidden="true"></i>
                                </span>
                                <span className="star-full">
                                    <i className="fa fa-star" aria-hidden="true"></i>
                                </span>
                                <span className="star-full">
                                    <i className="fa fa-star" aria-hidden="true"></i>
                                </span>
                                <span className="star-full">
                                    <i className="fa fa-star" aria-hidden="true"></i>
                                </span>
                                <span className="star-full">
                                    <i className="fa fa-star" aria-hidden="true"></i>
                                </span>
                            </div>

                            <div className="rating_new">
                                <span className="star-full">
                                    <i className="fa fa-star" aria-hidden="true"></i>
                                </span>
                                <span className="star-full">
                                    <i className="fa fa-star" aria-hidden="true"></i>
                                </span>
                                <span className="star-full">
                                    <i className="fa fa-star" aria-hidden="true"></i>
                                </span>
                                <span className="star-full">
                                    <i className="fa fa-star" aria-hidden="true"></i>
                                </span>
                                <span className="star-full">
                                    <i className="fa fa-star" aria-hidden="true"></i>
                                </span>
                                <span className="totalUser">
                                    <i className="fa fa-star-o" aria-hidden="true"></i>
                                </span>
                            </div>*/}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}