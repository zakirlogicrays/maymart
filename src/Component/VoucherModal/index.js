import React, {Component} from 'react';
import Calendar from 'react-calendar-multiday';
import moment from 'moment';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import axios from "axios";
import jwt_decode from "jwt-decode"; 

function dates(startDate, endDate, pattern) {
    var recurrencePattern = parseInt(pattern); // 2 weeks
    var daysSelected = [0,1,2,3,4,5,6]; //0 - sunday, 1 - monday
    var datesObj = [];
    while (startDate <= endDate) {
  
      if (daysSelected.includes(startDate.getDay())) {
        datesObj.push(new Date(startDate.getFullYear() + "-" + (startDate.getMonth() + 1) + "-" + startDate.getDate()).toISOString());
      }
  
      if (datesObj.length > 0) {
        startDate.setDate(startDate.getDate() + recurrencePattern);
      } else {
        startDate.setDate(startDate.getDate() + 1);
      }
    }
    return datesObj;
  }
  
  

var today = new Date();
var currentDate =today.getFullYear()+ '-' + (today.getMonth() + 1)+ '-' +today.getDate() ;

export default class VoucherModal extends Component{
    constructor(props) {
        super(props)
        this.state = {
            selectedVoucher:'',
            voucherAmountnow: 0.00,
            prevvoucherAmount:0.00,
            prevselectedVoucher:'',
            totalSelectedVoucher:[]
        }
      }
      state = this.state

    closeModal = () => {
        this.props.getCloseModal()
    }

    onSelectCustom = async(e,data) => {
        console.log('select Data',data)
        const {modalStatus,voucherData,voucherAmount} = this.props;
        const {totalSelectedVoucher} = this.state
        const { checked } = e.target;

        if(checked){
            var couponamount
            if(data.couponType === 'QAR'){
                const totalAmount = data.couponAmount
                const maximumamount = data.maxDiscount
             
                if(totalAmount < maximumamount){
                 couponamount = totalAmount
                }else{
                 couponamount = maximumamount
                }
             }else{
                 const percentamount = (voucherAmount * data.couponAmount) /100
                 const maximumamount = data.maxDiscount
                 if(percentamount < maximumamount){
                  couponamount = percentamount
                 }else{
                  couponamount = maximumamount
                 }
            
        }
        this.setState({
            voucherAmount:couponamount,
            selectedVoucher:data.couponName,
            totalSelectedVoucher:[...this.state.totalSelectedVoucher,data.couponName]
        })
        
        }else{

            const found = totalSelectedVoucher.indexOf(data.couponName)
            if(found !== -1){
                totalSelectedVoucher.splice(found,1)
            }
            if(data.couponType === 'QAR'){
                const totalAmount = data.couponAmount
                const maximumamount = data.maxDiscount
                var couponamount
                if(totalAmount < maximumamount){
                 couponamount = totalAmount
                }else{
                 couponamount = maximumamount
                }
                 this.setState({
                    voucherAmount: couponamount,
                    selectedVoucher:data.couponName,
                    totalSelectedVoucher
                 })
             }else{
                 const percentamount = (voucherAmount * data.couponAmount) /100
                 const maximumamount = data.maxDiscount
                 var couponamount
                 if(percentamount < maximumamount){
                  couponamount = percentamount
                 }else{
                  couponamount = maximumamount
                 }
                this.setState({
                    voucherAmount: couponamount,
                    selectedVoucher:data.couponName,
                    totalSelectedVoucher
                })
        }
          
        }
    }

    getVoucher = () => {
        const { selectedVoucher,voucherAmount,totalSelectedVoucher} = this.state
        if(totalSelectedVoucher.indexOf(selectedVoucher) !== -1){
            this.props.getVoucherData(selectedVoucher,voucherAmount)
        }else{
            this.props.closeVoucherData(selectedVoucher,voucherAmount)
        }
        
        this.props.getCloseModal()
    }

    render(){
        const {modalStatus,voucherData,voucherAmount} = this.props;
        return(
            <div className={modalStatus ? "modal fade show" : "modal fade"} style={modalStatus ? { display: 'block', paddingRight: 16} : { display: 'none', paddingRight: 16 }} id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-header">
                        <h4 className="modal-title"> Choose Voucher </h4>
                        <button type="button" className="close" onClick={this.closeModal} aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div className="modal-body"style={{maxHeight: 'calc(100vh - 200px)',overflow: 'auto'}}>
                    {
                        voucherData.length > 0
                        ?
                        voucherData.map((vData,vIndex) => {
                            return(
                                <>
                                    <div className="voucher-modal-wr">
                                    <input className="form-check-input" checked={this.state.totalSelectedVoucher.indexOf(vData.couponName) !== -1 ?true : false} onChange={(val) => this.onSelectCustom(val,vData)}  type="checkbox" id="inlineCheckbox2"  />
                                    <img src={`${window.$ImageURL}` + vData.couponBannerImageId[0].originalURL} className="img-fluid voucher-image" alt="productImage"  />
                                    <label className="voucher-name" htmlFor="inlineCheckbox2">{vData.couponName}  </label>
                                    </div>
                                    
                                </>
                               
                            )
                        })
                       
                        :
                            <>
                            <div className="row mb-4 ml-4">
                            <label className="form-check-label" htmlFor="inlineCheckbox2">No Voucher Found </label>
                            </div>
                            <div className="row">
                            </div>
                            </>
                 
                    }
                     </div>
                    <div className="modal-footer voucher-modal-footer">
                        {/* <button type="button" className="btn btn-default" onClick={this.closeModal}>Close</button> */}
                        <button type="button" className="btn btn-add-address" onClick={this.getVoucher}>Apply</button>
                    </div>
                </div>
            </div>
        </div> 
       
        )
    }
}