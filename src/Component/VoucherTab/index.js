import React, { Component } from "react";
import axios from "axios";
import Loader from "../../Component/Loader";
import jwt_decode from "jwt-decode";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';



export default class VoucherTab extends Component {
  state = {
    vouchersData: [],
    loading: true,
    couponArray: []
  }
  componentDidMount() {
    const { dataArray } = this.props;
    this.fetchSellerProducts(dataArray);
    this.fetchCoupons()

  }
  fetchSellerProducts = async (catName) => {
    await axios.post(`${window.$URL}couponDiscount/fetch-seller-products`, { catName })
      .then((response) => {
        console.log(response, "sellerProducts");

        //console.log(response.data)
        this.setState({
          vouchersData: response.data,
          loading: false,
        });

      })
      .catch((error) => {
        console.log(error);
      });

  }
  fetchCoupons = async () => {
    let checkToken = await localStorage.getItem("token");
    if (checkToken) {
      const decoded = jwt_decode(checkToken);
      let customerId = decoded.userid;
      await axios.post(`${window.$URL}couponDiscount/customer-fetchCoupons/`, { customerId })
        .then(response => {
          //console.log(response,'array')
          let colletedIds = [];
          for (let i = 0; i < response.data.data.length; i++) {
            if (!colletedIds.includes(response.data.data[i].voucherId._id)) {
              colletedIds.push(response.data.data[i].voucherId._id)
            }
          }
          if (response.data.success) {

            this.setState({
              couponArray: colletedIds
            })
          }
        })
        .catch(error => {
          console.log(error);
        })
    }
  }


  async collectvoucher(voucherId, sellerId) {
    //alert(voucherId);
    let checkToken = await localStorage.getItem("token");
    if (checkToken) {
      const decoded = jwt_decode(checkToken);
      let customerId = decoded.userid;
      if (sellerId != null) {
        let voucherClaimData = {

          "voucherId": voucherId,
          "sellerId": sellerId,
          "customerId": customerId
        };
        await axios.post(`${window.$URL}couponDiscount/customer-voucher-claim`, voucherClaimData)
          .then(response => {
            if (response.status === 200) {
              if (response.data.success) {
                toast.success(response.data.message, {
                  position: "bottom-center",
                  autoClose: 3000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true,
                  progress: undefined,
                });
                this.componentDidMount();

              } else {
                toast.error(response.data.message, {
                  position: "bottom-center",
                  autoClose: 3000,
                  hideProgressBar: true,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true,
                  progress: undefined,
                });

              }
            }
          })
          .catch(error => {
            console.log(error);
          })
      }
    }
    else {
      window.location.href = `${process.env.PUBLIC_URL}/login`
    }

  }



  render() {
    const { vouchersData, loading, couponArray } = this.state;
    if (loading) {
      return (
        <div className="tab-pane fade show active" id="pills-explore" role="tabpanel" aria-labelledby="pills-explore-tab">
          <div className="inside-tab-wr">
            <Loader />
          </div>
        </div>
      )
    } else {

      return (

        <div className="tab-pane fade show active" id="pills-explore" role="tabpanel" aria-labelledby="pills-explore-tab">
          <div className="inside-tab-wr">
            {/* <div className="tab-act-btn-wr">
              <button className="btn btnaction active">All{dataArray}</button>
              <button className="btn btnaction">MyMart</button>
              <button className="btn btnaction">No Min. Spend</button>
            </div> */}
            <div className="row">
              <ToastContainer />
              {
                vouchersData.length > 0 ?
                  vouchersData.map((data, index) => {
                    let storeImageURL = '';
                    if (data.storeLogo !== null) {
                      storeImageURL = window.$ImageURL + data.storeLogo.originalURL
                    } else {
                      storeImageURL = `${window.$ImageURL}no-image-480x480.png`
                    }
                    return (
                      <div className="col-lg-6" key={index}>
                        <div className="expl-vouchers">
                          <div className="section-left">
                            <div className="new-store-head">
                              <img src={storeImageURL} className="img-fluid" alt={data.storeName} style={{ maxWidth: 30 }} />
                              <h2>{data.storeName}</h2>
                            </div>
                            <div className="row">
                              {data.products.map((product, index) => {

                                let imageURL = '';
                                if (product.primaryProductImage !== null) {
                                  imageURL = window.$ImageURL + product.primaryProductImage.originalURL
                                } else {
                                  imageURL = `${window.$ImageURL}no-image-480x480.png`
                                }
                                return (
                                  <div className="col-4" key={index}>
                                    <div className="pr-level">
                                      <img src={imageURL} className="img-fluid" alt={product.productName} />
                                      {
                                        product.couponType === '%' ?
                                          <p className="mt-price"> QAR {parseFloat((product.price / 100) * data.couponAmount).toFixed(2)}</p>
                                          :
                                          <p className="mt-price">QAR {parseFloat(product.price - data.couponAmount).toFixed(2)}</p>
                                      }

                                      <p className="mt-cutoff"> QAR {parseFloat(product.price).toFixed(2)}</p>



                                    </div>
                                  </div>
                                );
                              })}

                            </div>
                          </div>
                          <div className="section-right">
                            <span className="shaps top" />
                            <span className="shaps bottom" />
                            <p className="price-off">{data.couponType} {data.couponType === '%'? data.couponAmount:parseFloat(data.couponAmount).toFixed(2)}<br /> OFF</p>
                            {
                              couponArray.includes(data.couponId) ?
                                <button disabled className="btn btncollect">Collected</button>
                                :
                                <button onClick={() => this.collectvoucher(data.couponId, data.products[0].sellerId ? data.products[0].sellerId : null)} className="btn btncollect">Collect</button>
                            }

                          </div>
                        </div>
                      </div>
                    );
                  })
                  :
                  <div className="col-lg-12">
                    <div className="expl-vouchers">
                      <div className="section-left">
                        <div className="new-store-head">
                          <h2>No Voucher Found</h2>
                        </div>
                      </div>
                    </div>
                  </div>
              }
            </div>
          </div>
        </div>
      )
    }
  }
}