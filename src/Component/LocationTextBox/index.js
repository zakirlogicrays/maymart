import React, { Component } from 'react';
import PlacesAutocomplete, {
    geocodeByAddress,
    getLatLng,
  } from 'react-places-autocomplete';
import './style.css';

export default class LocationTextBox extends Component {

    state = {
        selectedLocation: '',
        coordinates: {
            lat: 0,
            lng: 0
        },
        address: ''
    }

    componentDidUpdate(prevProps){
        if(prevProps.Address !== this.props.Address){
            this.setState({
                address: this.props.Address
            })
        }
    }
 
    handleChange = address => {
        this.setState({ address });
    };

    handleSelect = address => {
        geocodeByAddress(address)
        .then(results => getLatLng(results[0]), this.setState({selectedLocation: address}))
        .then(latLng => this.setState({coordinates: latLng}))
        .then(() => {
            //console.log(this.state.selectedLocation, this.state.coordinates)
            this.props.fetchLocation(this.state.coordinates, this.state.selectedLocation)
        })
        .catch(error => console.error('Error', error));
    };

    render() {
        return (
            <PlacesAutocomplete
                value={this.state.address}
                onChange={this.handleChange}
                onSelect={this.handleSelect}
                query={{
                    key: process.env.REACT_APP_GOOGLE_API_KEY,
                    language: 'en',
                }}
                searchOptions={{
                    componentRestrictions: {country: ['qa']}
                }}
            >
                {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
                <div className="text-autocomplete">
                    <input
                    {...getInputProps({
                        placeholder: 'Search location',
                        className: 'location-search-input',
                    })}
                    />
                    <div className="autocomplete-dropdown-container">
                    {loading && <div>Loading...</div>}
                    {suggestions.map(suggestion => {
                        const className = suggestion.active
                        ? 'suggestion-item--active'
                        : 'suggestion-item';
                        // inline style for demonstration purpose
                        const style = suggestion.active
                        ? { backgroundColor: '#fafafa', cursor: 'pointer' }
                        : { backgroundColor: '#ffffff', cursor: 'pointer' };
                        return (
                        <div 
                            {...getSuggestionItemProps(suggestion, {
                            className,
                            style,
                            })}
                        >
                            <span>{suggestion.description}</span>
                        </div>
                        );
                    })}
                    </div>
                </div>
                )}
            </PlacesAutocomplete>
        )
    }
}