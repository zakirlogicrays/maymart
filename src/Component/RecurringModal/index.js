import React, {Component} from 'react';
import Calendar from 'react-calendar-multiday';
import moment from 'moment';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import axios from "axios";
import jwt_decode from "jwt-decode"; 

function dates(startDate, endDate, pattern) {
    var recurrencePattern = parseInt(pattern); // 2 weeks
    var daysSelected = [0,1,2,3,4,5,6]; //0 - sunday, 1 - monday
    var datesObj = [];
    while (startDate <= endDate) {
  
      if (daysSelected.includes(startDate.getDay())) {
        datesObj.push(new Date(startDate.getFullYear() + "-" + (startDate.getMonth() + 1) + "-" + startDate.getDate()).toISOString());
      }
  
      if (datesObj.length > 0) {
        startDate.setDate(startDate.getDate() + recurrencePattern);
      } else {
        startDate.setDate(startDate.getDate() + 1);
      }
    }
    return datesObj;
  }
  
  

var today = new Date();
var currentDate =today.getFullYear()+ '-' + (today.getMonth() + 1)+ '-' +today.getDate() ;

export default class RecurringModal extends Component{
    
    state = {
        modalFailed: '',
        modalSuccess: '',
        timeStotFromError: '',
        timeStotToError: '',
        allDates:[],
        timeStotFrom:'',
        timeStotTo:'',
        setStartDate:'',
        setEndDate:'',
        checked1: false,
        checked3: false,
        checked2: false,
        checked7: false,
        selectedDates:[],
        customeSelect:false,
        calendarDataChange:true,
        scheduleType:''




    }
     

    dateChange = (date) => {
        let datesSelected = [];
        for(let i=0; i<= date.selected.length-1; i++){
            let eachDate = date.selected[i];
            let convertDate = moment(moment(eachDate)).unix();
            datesSelected.push(convertDate);
        }
        
        this.setState({
            allDates: datesSelected
        })
        console.log(this.state.allDates);
    }

    closeModal = () => {
        this.props.modalHide(false)
    }

    onChange = e => {
        const {name, value} = e.target;
        this.setState({
            [name] : value
        })
    };

    
    gettimeStotFrom = (time) => {

       // console.log(time.target.value);
        //console.log(moment(time.target.value).unix())
        //6040fcdfe6133f5655213085
        this.setState({
            timeStotFrom: time.target.value
        })

    }
    getStartDate= (time) => {

        console.log(time.target.value,'getStartDate');
       

         this.setState({
             setStartDate: time.target.value
         })
 
     }

     getEndate = (time) => {
        console.log(time.target.value,'getEndate');
        this.setState({
            calendarDataChange:false
        })   
        this.setState({
            setEndDate: time.target.value,
            calendarDataChange:false,
            checked1: false,
            checked3: false,
            checked2: false,
            checked7: false,
            
        })
          
    }
    
    gettimeStotTo = (time) => {

        this.setState({
            timeStotTo: time.target.value
        })
    }

    handleChange(tags){
        this.setState({
            attrValue: tags
        })
    }

    validate = () => {
        this.setState({
            timeStotFromError: '',
            timeStotToError: '',
         
        })
     
        let timeStotToError = '';
        let timeStotFromError = '';

        if (this.state.timeStotFrom === '') {
            timeStotFromError = "Please Enter Time Stot From"
        }
        if (timeStotFromError) {
            this.setState({ timeStotFromError })
            return false
        }
        if (this.state.timeStotTo === '') {
            timeStotToError = "Please Enter Time Stot To"
        }
        if (timeStotToError) {
            this.setState({ timeStotToError })
            return false
        }

        
        return true
    }

    placeOrder = async(e) => {
        
        e.preventDefault();
        
        let checkValidation = this.validate();

        if (checkValidation) {
        
        let checkToken = await localStorage.getItem("token");
        if (checkToken) {
        const decoded = jwt_decode(checkToken);
        const customerId= decoded.userid;

        const { allDates, timeStotFrom, timeStotTo,setEndDate,setStartDate,scheduleType } = this.state;

                let scheduleData = {
                    productVariantId: this.props.productVariantId, 
                    scheduleDates: allDates,
                    timeSlotFrom: timeStotFrom,
                    timeSlotTo: timeStotTo,
                    customerId: customerId,
                    packSize: this.props.packSize,
                    packSizeMeasureUnit: this.props.packSizeMeasureUnit, 
                    sellerId: this.props.sellerId,
                    setStartDate:setStartDate?moment(moment(setStartDate)).unix():setStartDate,
                    setEndDate:setEndDate?moment(moment(setEndDate)).unix():setEndDate,
                    scheduleType:scheduleType 
                   
                };
                console.log(scheduleData)   
                await axios.post(`${window.$URL}schedule-product/save-schedule-data`, scheduleData) 
                .then(response => {
                    if (response.status === 200) {
                        if (response.data.success) {
                            toast.success(response.data.message, {
                                position: "bottom-center",
                                autoClose: 3000,
                                hideProgressBar: true,
                                closeOnClick: true,
                                pauseOnHover: true,
                                draggable: true,
                                progress: undefined,
                            });
                            this.closeModal();
                        } else {
                            toast.error(response.data.message, {
                                position: "bottom-center",
                                autoClose: 3000,
                                hideProgressBar: true,
                                closeOnClick: true,
                                pauseOnHover: true,
                                draggable: true,
                                progress: undefined,
                            });
                        }
                    }
                })
                .catch(error => {
                    console.log(error);
                })
            }
        }
    }

    onChangeDateType = (e) => {
       
    this.setState({
        calendarDataChange:false,
        checked1: false,
        checked3: false,
        checked2: false,
        checked7: false,

    })    
    var value =e.target.value;  
    if(e.target.checked === true)
    {   
     
    const {selectedDates} = this.state;

    const start = new Date(this.state.setStartDate);
    const end = new Date(this.state.setEndDate);
    let getDates = dates(start,end, value);
    //date convert to unix
    let datesSelectedFromStartToEndtDate = [];
    for(let i=0; i<=getDates.length-1; i++){
        let eachDate = getDates[i];
        let convertDate = moment(moment(eachDate)).unix();
        datesSelectedFromStartToEndtDate.push(convertDate);
    }
    console.log(getDates,'getDates',datesSelectedFromStartToEndtDate,'unix',value,'value',moment(moment(start)).unix(),moment(moment(end)).unix());

    this.setState({
        ["checked" + (value)]: e.target.checked,
        scheduleType:value,//for schedule type //everyday,7days,3days,alternativeday 
        calendarDataChange:true,
        selectedDates:getDates,
        allDates: datesSelectedFromStartToEndtDate
       
    })
    
    }
    else
    {

        this.setState({
            ["checked" + (value)]:false,
            calendarDataChange:false,
            checked1: false,
            checked3: false,
            checked2: false,
            checked7: false,

        })
    }
    }


    

    onSelectCustom = (e) => {
       
    var value =e.target.value;  
    if(e.target.checked === true)
    {   
     
    this.setState({
        customeSelect: e.target.checked,
        calendarDataChange:false,
        setStartDate:'',
        setEndDate:'',
        checked1: false,
        checked3: false,
        checked2: false,
        checked7: false,
        scheduleType:'',


        
    })
    
    }
    else
    {

        this.setState({
            customeSelect:false,
            // allDates:[],
            
        
        })
    }
    }
    render(){
        const {modalStatus,timeStotFromError,timeStotToError} = this.props;
        const{customeSelect,selectedDates,calendarDataChange} = this.state;
        console.log(calendarDataChange,customeSelect,'status')
        console.log(selectedDates,'dates');
        return(
            <div className={modalStatus ? "modal fade show" : "modal fade"} style={modalStatus ? { display: 'block', paddingRight: 16} : { display: 'none', paddingRight: 16 }} id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-header">
                        <h4 className="modal-title"> Schedule Product </h4>
                        <button type="button" className="close" onClick={this.closeModal} aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div className="modal-body"style={{maxHeight: 'calc(100vh - 200px)',overflow: 'auto'}}>
                    {customeSelect ? null
                        
                        
                        :
                        <>
                      <div className="row">
                            <div className="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                            <p className="v-t">Start Date</p>
                                <input type="date"   onChange={(event) => this.getStartDate( event)}  className="form-control input-sm mb-4"/>
                              
                            </div>
                            <div className="col-lg-5 col-md-5 col-sm-5 col-xs-5 pull-right">
                            <p className="v-t">End Date</p>
                                <input type="date" onChange={(event) => this.getEndate( event)}  className="form-control input-sm mb-4"/>
                                
                            </div>
                        </div>
                    
                        <div className="row  mb-4 ml-4">
                            <div className="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                            {/* <p className="v-t">Start Date</p> */}
                            <input className="form-check-input"     checked={this.state.checked1} onChange={this.onChangeDateType} type="checkbox" id="inlineCheckbox2" value="1" />
                            <label className="form-check-label" htmlFor="inlineCheckbox2">Everyday</label>
                            </div>
                            <div className="col-lg-5 col-md-5 col-sm-5 col-xs-5 pull-right">
                            {/* <p className="v-t">End Date</p> */}
                            <input className="form-check-input" checked={this.state.checked3} onChange={this.onChangeDateType} type="checkbox" id="inlineCheckbox2" value="3" />
                            <label className="form-check-label" htmlFor="inlineCheckbox2">Every 3 days</label>
                            </div>
                        </div>
                        <div className="row  mb-4 ml-4">
                            <div className="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                            {/* <p className="v-t">Start Date</p> */}
                            <input className="form-check-input" checked={this.state.checked2}  onChange={this.onChangeDateType} type="checkbox" id="inlineCheckbox2" value="2" />
                            <label className="form-check-label" htmlFor="inlineCheckbox2">Alternate Day</label>
                            </div>
                            <div className="col-lg-5 col-md-5 col-sm-5 col-xs-5 pull-right">
                            {/* <p className="v-t">End Date</p> */}
                            <input className="form-check-input" checked={this.state.checked7} onChange={this.onChangeDateType}  type="checkbox" id="inlineCheckbox2" value="7" />
                            <label className="form-check-label" htmlFor="inlineCheckbox2">Every 7 Days </label>
                            </div>
                        </div>
                      </>
                        }

                        <div className="row mb-4 ml-4">
                        <input className="form-check-input" checked={this.state.customeSelect} onChange={this.onSelectCustom}  type="checkbox" id="inlineCheckbox2" value="7" />
                        <label className="form-check-label" htmlFor="inlineCheckbox2">Custom Date Select </label>
                        </div>

                        {customeSelect ? 
                        <>
                        <div className="row mb-4">
                            <p className="v-t">Choose Dates</p>
                            <Calendar
                                reset={true}
                                isMultiple={true}
                                onChange={this.dateChange}
                                channels={{}}  
                            />
                        </div>
                        </>
                        :
                        <div className="row mb-4">
                            <p className="v-t">Selected Dates</p>
                            {calendarDataChange?
                            <Calendar
                               
                              
                               
                             //selected= {["2021-04-16T00:00:00-03:00"]}
                             selected= {selectedDates}
                              
                            />
                            :<Calendar/>
                            }
                        </div>
                      
                        }
                        <div className="row">
                            <div className="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                            <p className="v-t">Select Time Range From</p>
                                <input type="time"  onChange={(event) => this.gettimeStotFrom( event)}  className="form-control input-sm mb-4"/>
                                {timeStotFromError ? <span style={{ color: 'red' }}> {timeStotFromError} </span> : null}
                            </div>
                            <div className="col-lg-5 col-md-5 col-sm-5 col-xs-5 pull-right">
                            <p className="v-t">Select Time Range To</p>
                                <input type="time" onChange={(event) => this.gettimeStotTo( event)}  className="form-control input-sm mb-4"/>
                                {timeStotToError ? <span style={{ color: 'red' }}> {timeStotToError} </span> : null}
                            </div>
                        </div>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-default" onClick={this.closeModal}>Close</button>
                        <button type="button" className="btn btn-primary" onClick={this.placeOrder}>Send</button>
                    </div>
                </div>
            </div>
        </div> 
        )
    }
}