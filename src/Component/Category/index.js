import React, {Component} from "react";

export default class Category extends Component{
    getfilterData = (variables, segment) => {
        this.props.filterCategoryWise(variables, segment)
    }
    render(){
        const {categoryDetails} = this.props;
        return(
            <div className="categories-wrapper">
                {
                    categoryDetails.map((data,index) => {
                        return(
                            <div onClick={() => this.getfilterData([data.categoryName],'category')} className="product-category" key={index}>
                                <div className="inner_category">
                                    <div className="image-product">
                                        <img src={data.categoryImageId !== undefined && data.categoryImageId !== null ? window.$ImageURL + data.categoryImageId.originalURL : "http://nodeserver.mydevfactory.com:7779/uploads/no-image-480x480.png"} alt={data.categoryImageId !== undefined && data.categoryImageId !== null ? data.categoryImageId.originalURL : "Category Image"} className="img-fluid" />
                                    </div>
                                    <p>{data.categoryName}</p>
                                </div>
                            </div>
                        )
                    })
                }
                
            </div>
        )
    }
}