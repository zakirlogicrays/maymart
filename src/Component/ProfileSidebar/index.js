import React, { Component } from "react";
import jwt_decode from 'jwt-decode';
import { AddToCat,UpdateloginCart,UpdateCart,UpdatelogoutCart } from '../../data/reducers/cart';
import { connect } from 'react-redux';
import logoSmall from "../../assets/images/myMart-logo.svg";

class ProfileSidebar extends Component {
    state = {
        pageName: window.location.pathname.split("/"),
        manageAccount: false,
        smallMenu: true
    }
    toggleMessage = () => {
        this.setState({
            smallMenu: !this.state.smallMenu
        })
      }
    async componentDidMount(){
        let checkToken = await localStorage.getItem("token");
        if(checkToken){
            const decoded = jwt_decode(checkToken);
            if(decoded.userType !== 'customer'){
                window.location.href = `${process.env.PUBLIC_URL}/`;
            }
        }else{
            window.location.href = `${process.env.PUBLIC_URL}/`;
        }
        
    }

    logout = async () => {
        await localStorage.removeItem("token");
        this.props.UpdatelogoutCart()
        window.location.href = `${process.env.PUBLIC_URL}/`;
    }

    toggleSidebar = (type, currentState) => {
        if(currentState){
            this.setState({
                [type] : false
            })
        }else{
            this.setState({
                [type] : true
            })
        }
    }

    render() {
        const {manageAccount,pageName} = this.state;
        console.log(pageName);
        return (

            <>
            <div className="small-menu-part">
                 <button onClick={this.toggleMessage} className="btn btn-hamburger">
                   <i class="fa fa-bars" aria-hidden="true"></i> 
                   </button>
                 <img src={logoSmall} className="sm-logo-menu" />
             </div>
            <div className={`col-lg-3 left-menu ${this.state.smallMenu ? "hide_inmenu" : "showMenu"}`}>
            <button className="btn btn-close-new" onClick={this.toggleMessage}><i class="fa fa-times" aria-hidden="true"></i></button>
                <nav className="inner-left-menu-w">
                    <ul className="nav">
                        <li className="nav-item">
                            <button className={pageName[1] === 'myProfile' || pageName[1] === 'address-book' || pageName[1] === 'applied-vouchers' || manageAccount ? "nav-link btn-block arrow-down active" : "nav-link btn-block arrow-down"} type="button" onClick={() => this.toggleSidebar('manageAccount', manageAccount)} data-toggle="collapse" aria-expanded="false" aria-controls="ManageAccount"> Manage My Account</button>
                            <div className={pageName[1] === 'myProfile' || pageName[1] === 'address-book' || pageName[1] === 'applied-vouchers' || manageAccount ? "collapse sub-nav show" : "collapse sub-nav"} id="ManageAccount">
                                <a href={`${process.env.PUBLIC_URL}/myProfile`} className={pageName[1] === 'myProfile' ? "nav-link active" : "nav-link"}> My Profile </a>
                                <a href={`${process.env.PUBLIC_URL}/address-book`} className={pageName[1] === 'address-book' ? "nav-link active" : "nav-link"}> My Address Book </a>
                                <a href="#/" className="nav-link"> My Payment Options </a>
                                <a href={`${process.env.PUBLIC_URL}/applied-vouchers`} className={pageName[1] === 'applied-vouchers' ? "nav-link active" : "nav-link"} > Vouchers </a>
                            </div>
                        </li>
                        <li className="nav-item">
                            <a className={pageName[1] === 'myOrder' ? "nav-link active" : "nav-link"} href={`${process.env.PUBLIC_URL}/myOrder`}>My Orders</a>
                        </li>
                        <li className="nav-item">
                            <a className={pageName[1] === 'myWishlist' ? "nav-link active" : "nav-link"} href={`${process.env.PUBLIC_URL}/myWishlist`}>My Wishlist &amp; Followed Stores</a>
                        </li>
                        <li className="nav-item">
                            <a className={pageName[1] === 'myReviews' ? "nav-link active" : "nav-link"} href="#/">My Reviews</a> 
                        </li>
                        <li className="nav-item">
                            <a className={pageName[1] === 'change-password' ? "nav-link active" : "nav-link"} href={`${process.env.PUBLIC_URL}/change-password`}>Change Password</a>  
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#/" onClick={this.logout}>Logout</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </>
        )
    }
}
const mapStateToProps = (state) => ({
    CartProducts: state.Cart,
})

export default connect(
    mapStateToProps,
    {  AddToCat, UpdateloginCart,UpdateCart,UpdatelogoutCart }
)(ProfileSidebar);