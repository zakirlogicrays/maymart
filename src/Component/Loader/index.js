import React, {Component} from "react";
import Loader from "react-loader-spinner";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";

export default class commonLoader extends Component{
    render(){
        const {loaderSize} = this.props
        return(
            <div className="body-block">
                <div className="container" style={{textAlign:'center', justifyContent:'center'}}>
                    <Loader
                        type="ThreeDots"
                        color="#EE9621"
                        height={loaderSize !== undefined ? loaderSize : 150}
                        width={loaderSize !== undefined ? loaderSize : 150}
                    />
                </div>
            </div>
        )
    }
}