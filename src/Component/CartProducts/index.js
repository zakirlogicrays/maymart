import React, { Component } from "react";
import bestPriceImage from "../../assets/images/best-pricetag.png"
import SweetAlert from 'react-bootstrap-sweetalert';
import axios from 'axios';
import VoucherModal from '../VoucherModal';



export default class CartProducts extends Component {
  constructor(props) {
    super(props)
    this.state = {
      showItemTag: 'all',
      showSweetAlert: false,
      showSweetAlertDeleteAll: false,
      deleteCart: '',
      deleteCartIndex: '',
      voucherModal: false,
      voucherData: [],
      voucherAmount: 0.00,
      totalCouponamount: 0,
      selectedVoucherList: [],
      selectVoucherindex: ''
    }
  }
  state = this.state


  cartProductlength = () => {
    const { cartArray } = this.props;
    let count = 0;
    for (let i = 0; i <= cartArray.length - 1; i++) {
      for (let j = 0; j <= cartArray[i].productVariantId.length - 1; j++) {
        count++
      }
    }
    return count;
  }

  componentDidUpdate(prevProps) {
    if (prevProps.couponapplied !== this.props.couponapplied) {
      if (this.props.couponapplied === false)
        this.setState({ selectedVoucherList: [] })
    }
  }

  selectedProductVariantArray = (productVariantArray) => {
    const { selectedProduct } = this.props;
    let checked = false;
    let returnTypeArray = [];
    for (let i = 0; i <= productVariantArray.productVariantId.length - 1; i++) {
      if (selectedProduct.includes(productVariantArray.productVariantId[i]._id)) {
        returnTypeArray.push(true);
      } else {
        returnTypeArray.push(false);
      }
    }
    if (returnTypeArray.includes(false)) {
      checked = false;
    } else {
      checked = true;
    }
    return checked;
  }

  fetchProductImage = (productArray) => {
    var imageData;
    if (productArray.primaryProductImage !== null && productArray.primaryProductImage !== undefined) {
      imageData = `${window.$ImageURL}` + productArray.primaryProductImage.originalURL
    } else if (productArray.productImageId.length > 0) {
      imageData = `${window.$ImageURL}` + productArray.productImageId[0].originalURL
    } else {
      imageData = `${window.$ImageURL}no-image-480x480.png`
    }
    return imageData;
  }

  toggleSelectProductVariant = (e, cartProductArray) => {
    this.props.productVariantSelect(e, cartProductArray);
  }

  toggleSelectProductIndividualVariant = (e, eachVariant, prodVarient) => {
    this.props.productVariantIndividualSelect(e, eachVariant, prodVarient);
  }

  selectAllProd = (e) => {
    this.props.allSelect(e);
  }

  deleteCart = (cartId, index) => {
    this.setState({
      showSweetAlert: true,
      deleteCart: cartId,
      deleteCartIndex: index
    })
    // this.props.deleteCartData(cartId, index);
  }

  getDeletefromCart = () => {
    const { deleteCart, deleteCartIndex } = this.state
    this.setState({
      showSweetAlert: false,
    }, () => {
      this.props.deleteCartData(deleteCart, deleteCartIndex);
    })

  }

  quantityAdd = (cartId, index) => {
    this.props.addQuantity(cartId, index);
  }

  quantitySubtract = (cartId, index) => {
    this.props.subtractQuantity(cartId, index);
  }

  showItems = (productType) => {
    this.setState({
      showItemTag: productType
    })
    this.props.showProductFilter(productType);
  }

  getDeliveryAmount = (variantData) => {
    return (
      <>

      </>
    )
  }
  onCancel() {
    this.setState({
      showSweetAlert: false,
      showSweetAlertDeleteAll: false,
    })
  }

  deleteAllCart = () => {
    this.setState({
      showSweetAlertDeleteAll: true,
    })
  }

  selecteddeleteCartall = () => {
    this.setState({
      showSweetAlertDeleteAll: false
    },() => {
      this.props.getselectedDel();
    })
  }

  selectedstate = () => {
    const { cartArray } = this.props;
    const { selectedVoucherList } = this.state
    if (selectedVoucherList.length === 0) {
      var dataarray = []
      if (selectedVoucherList.length === 0) {
        for (var i = 0; i < cartArray.length; i++) {
          dataarray.push('')
        }
      }
      this.setState({
        selectedVoucherList: dataarray
      })
    }
  }

  getVoucher = async (index, data) => {
    this.selectedstate()
    var ProdcutCategory = []
    var totalAmount = 0.00;

    const sellerId = data.sellerId._id
    data.productVariantId.map((varData, varindex) => {
      var price
      if (varData.specialPriceProvided === 'true') {
        price = parseFloat(varData.specialPrice).toFixed(2)
      } else {
        price = parseFloat(varData.Price).toFixed(2)
      }
      totalAmount = parseFloat(totalAmount) + parseFloat(price)
    })
    data.productId.map((prodData, prodIndex) => {
      const selectedCategoy = prodData.category
      if (selectedCategoy.length > 0) {
        selectedCategoy.map((secCat, secIndex) => {
          ProdcutCategory.push(secCat)
        })
      }
    })
    ProdcutCategory = [...new Set(ProdcutCategory)];
    this.setState({
      voucherAmount: totalAmount,
      selectVoucherindex: index
    })
    const object = {
      "sellerId": sellerId,
      "ProductCategory": ProdcutCategory
    }
    this.getPost(object)
  }

  getPost = async (object) => {
    await axios.post(`${window.$URL}cart/GetCoupon`, object)
      .then(response => {
        console.log(response.data)
        this.setState({
          voucherModal: true,
          voucherData: response.data
        })

      })
      .catch(error => {
        console.log(error.data)
      })
  }

  getDataVoucher = async (selVoucher, voucheramount) => {
    const { totalCouponamount, selectVoucherindex, selectedVoucherList } = this.state
    var anotherAmount = Math.round(parseFloat(totalCouponamount) + parseFloat(voucheramount))

    selectedVoucherList[selectVoucherindex] = selVoucher
    this.setState({
      totalCouponamount: anotherAmount,
      selectedVoucherList,
      selectVoucherindex: ''
    })
    this.props.getyooCoupon(anotherAmount)
  }

  closeDataVoucher = (selVoucher, voucheramount) => {
    const { totalCouponamount, selectVoucherindex, selectedVoucherList } = this.state
    var anotherAmount
    if (totalCouponamount > 0) {
      anotherAmount = Math.round(parseFloat(totalCouponamount) - parseFloat(voucheramount))
    } else {
      anotherAmount = 0
    }
    selectedVoucherList[selectVoucherindex] = ''
    this.setState({
      totalCouponamount: anotherAmount,
      selectedVoucherList,
      selectVoucherindex: ''
    })
    this.props.getyooCoupon(anotherAmount)
  }

  closeVoucherModal = () => {
    this.setState({
      voucherModal: false
    })
  }
  render() {
    const { cartArray, totalCartArray, selectedProduct } = this.props;
    const { showItemTag, showSweetAlert, selectedVoucherList, showSweetAlertDeleteAll } = this.state;
    return (
      <div className="col-md-8">
        <VoucherModal
          modalStatus={this.state.voucherModal}
          voucherData={this.state.voucherData}
          voucherAmount={this.state.voucherAmount}
          getVoucherData={this.getDataVoucher}
          closeVoucherData={this.closeDataVoucher}
          getCloseModal={this.closeVoucherModal}
        />  
        {showSweetAlert ?
          <SweetAlert
            warning
            showCancel
            confirmBtnText="Yes, delete it!"
            confirmBtnBsStyle="danger"
            title="Confirm Delete Products?"
            onConfirm={() => this.getDeletefromCart()}
            onCancel={() => this.onCancel()}
            focusCancelBtn
          />
          :
          null
        }
        {showSweetAlertDeleteAll ?
          <SweetAlert
            warning
            showCancel
            confirmBtnText="Yes, delete it!"
            confirmBtnBsStyle="danger"
            title="Confirm Delete Products?"
            onConfirm={() => this.selecteddeleteCartall()}
            onCancel={() => this.onCancel()}
            focusCancelBtn
          />
          :
          null
        }
        {
          totalCartArray.length > 0 ?
            <ul className="nav">
              <li className="nav-item">
                <button className={showItemTag === 'all' ? "nav-link active" : "nav-link"} type="button" onClick={() => this.showItems('all')}>ALL Items </button>
              </li>
              <li className="nav-item">
                <button className={showItemTag === 'grocery' ? "nav-link active" : "nav-link"} type="button" onClick={() => this.showItems('grocery')}>Grocery Items Only </button>
              </li>
              <li className="nav-item">
                <button className={showItemTag === 'nearest' ? "nav-link active" : "nav-link"} type="button" onClick={() => this.showItems('nearest')}>Nearest Grocery Only</button>
              </li>
              <li className="nav-item">
                <button className={showItemTag === 'freeDelivery' ? "nav-link active" : "nav-link"} type="button" onClick={() => this.showItems('freeDelivery')}>Free Delivery Items Only</button>
              </li>
            </ul>


            :
            null
        }

        <div className="mobile-filterBy">
          <p className="labels">Filter cart by</p>
          <select className="form-control">
            <option>All Items</option>
            <option>Grocery Items</option>
            <option>Nearest Store Items</option>
          </select>
        </div>


        {
          cartArray.length > 0 ?
            <div className="cart-card">
              <div className="delete-wrapper">
                <div className="chkbx">
                  <label className="c-s-checkbox ">
                    <input type="checkbox" onChange={(val) => this.selectAllProd(val)} checked={selectedProduct.length === this.cartProductlength() ? true : false} />
                    <span className="checkmark" />
                  </label>
                  <span className="all-check">SELECT ALL (<span> {selectedProduct.length} ITEM(S)</span>)</span>
                </div>
                <button className="btn btn-dlt" onClick={() => this.deleteAllCart()}>DELETE </button>
              </div>
            </div>
            :
            null
        }
        {
          cartArray.length > 0 ?
            cartArray.map((data, index) => {
              return (
                <div className="cart-card" key={index}>
                  <div className="delete-wrapper header">
                    <div className="chkbx add-4-item">
                      <label className="c-s-checkbox ">
                        <input type="checkbox" onChange={(val) => this.toggleSelectProductVariant(val, data)} checked={this.selectedProductVariantArray(data)} />
                        <span className="checkmark" />
                      </label>
                      <button className="btn">{data.sellerId.SellerDetails.shopName}</button>
                      {/*<span className="all-check">{data.brandName} <i className="fa fa-angle-right" aria-hidden="true" /></span>*/}
                    </div>
                    <button className="btn btn-dlt" onClick={() => this.getVoucher(index, data)}>{selectedVoucherList.length > 0 ?
                      selectedVoucherList[index] !== '' ? selectedVoucherList[index] :
                        'Get Voucher' : 'Get Voucher'} <i className="fa fa-angle-right" aria-hidden="true" /></button>
                  </div>
                  {
                    data.productVariantId.map((eachVariant, eachVariantIndex) => {
                      return (
                        <div className="row content-main" key={eachVariantIndex}>
                          <div className="col-lg-7 col-md-12">
                            <div className="more-offer">{this.getDeliveryAmount(eachVariant)}</div>
                            <div className="img-section">
                              <div className="chkbx">
                                <label className="c-s-checkbox ">
                                  <input type="checkbox" onChange={(val) => this.toggleSelectProductIndividualVariant(val, eachVariant, data.productId[eachVariantIndex])} checked={selectedProduct.includes(eachVariant._id) ? true : false} />
                                  <span className="checkmark" />
                                </label>
                              </div>
                              <div className="image-content">
                                <a href={`${process.env.PUBLIC_URL}/productDetails/${eachVariant._id}`}><img src={this.fetchProductImage(data.productId[eachVariantIndex])} className="img-fluid" alt="productImage" /></a>
                              </div>
                              <div className="content-wrapper">
                                <a href={`${process.env.PUBLIC_URL}/productDetails/${eachVariant._id}`}><h2 className="title">{data.productId[eachVariantIndex].productName} </h2></a>
                                <p className="fresh-fruit">
                                  {
                                    data.productId[eachVariantIndex].category.map((eachCat, eachIndex) => {
                                      return (
                                        <a href={`${process.env.PUBLIC_URL}/productListing/category/${eachCat}`} key={eachIndex}> {eachCat} {eachIndex !== data.productId[eachVariantIndex].category.length - 1 ? " > " : null}</a>
                                      )
                                    })
                                  }
                                </p>
                                {
                                  eachVariant.manageStock === "true" ?
                                    <p className="item-left">Only {eachVariant.currentInventory} item(s)in stock</p>
                                    :
                                    null
                                }
                                {/* <div className="offerbox">
                                  <img className="offer-image" src={bestPriceImage} alt={bestPriceImage} />
                                  <div className="content">
                                    <div className="cont-head">Save<span> QAR 2.00 </span>if you are willing to wait a little bit for this product. Tomorrow delivery</div>
                                    <p className="item-left">Tommorow Delivery</p>
                                    <button className="btn view-details">View Details</button>
                                  </div>
                                </div> */}
                              </div>
                            </div>
                          </div>
                          <div className="col-lg-3">
                            <div className="price-mark">
                              <p className="price">qar {eachVariant.specialPriceProvided === 'true' ? parseFloat(eachVariant.specialPrice).toFixed(2) : parseFloat(eachVariant.Price).toFixed(2)}</p>
                              {
                                eachVariant.specialPriceProvided === 'true' ?
                                  <>
                                    <p className="price cutoff-price">qar {parseFloat(eachVariant.Price).toFixed(2)}</p>
                                    <span className="less-percent">-{parseFloat(eachVariant.specialPriceDiscount).toFixed(2)}%</span>
                                  </>
                                  :
                                  null
                              }
                              <div className="like-delete">
                                <i className="fa fa-trash-o" aria-hidden="true" onClick={() => this.deleteCart(data._id, eachVariantIndex)} />
                              </div>
                            </div>
                          </div>
                          <div className="col-lg-2">
                            <div className="quantity-box">
                              <p>Earliest Time : <span>28 Sep</span></p>
                              <div className="incrementbox">
                                <button className="btn action" onClick={() => this.quantitySubtract(data._id, eachVariantIndex)}><span>-</span></button>
                                <span className="result">{data.quantity[eachVariantIndex]}</span>
                                <button className="btn action" onClick={() => this.quantityAdd(data._id, eachVariantIndex)}><span>+</span></button>
                              </div>
                            </div>
                          </div>
                        </div>
                      )
                    })
                  }
                </div>
              )
            })
            :
            <div className="cart-card">
              <div className="delete-wrapper">
                <h2 className="title">Your Cart is Empty </h2>
              </div>
            </div>
        }
      </div>
    )
  }

}