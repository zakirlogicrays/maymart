import React, { Component } from 'react';
import GoogleMaps from "../GoogleMaps";
import { connect } from 'react-redux';
import { AddToLocation } from '../../data/reducers/location';
import LocationTextBox from "../LocationTextBox";
import jwt_decode from 'jwt-decode';
import AddressBook from "../UserAddress"
import { BiCurrentLocation } from 'react-icons/bi';
import { IoMdLocate } from 'react-icons/io';
import { GiPositionMarker } from 'react-icons/gi';
import SelectedStoresImg from '../../assets/images/selected-stores-img.png';


class LocationModal extends Component {
    state = {
        locationCoordinates: {
            lat: 22.309425,
            lng: 72.136230
        },
        locateMe: [],
        Address: '',
        loggedIn: false,
    }

    componentDidMount() {
        const { location } = this.props;
        if (location.locationCoordinates.length > 0) {
            this.setState({
                Address: location.locationCoordinates[0].name,
                locationCoordinates: location.locationCoordinates[0].coordinates
            })
        } else {
            this.setState({
                Address: 'Doha, Qatar'
            })
        }
        this.checkUserLogin();
    }

    checkUserLogin = async () => {
        let checkToken = await localStorage.getItem("token");
        if (checkToken) {
            const decoded = jwt_decode(checkToken);
            if (decoded.userType === "customer") {
                this.setState({
                    loggedIn: true
                })
            }
        }
    }

    onMarkerDragEnd = async (coordinate) => {
        await fetch(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${coordinate.lat},${coordinate.lng}&key=${process.env.REACT_APP_GOOGLE_API_KEY}& type=locality&region=qa`)
            .then(response => response.json())
            .then(data => {
                if (data.status === 'OK') {
                    let filterCountry = data.results[0].address_components.filter(eachData => {
                        return eachData.types.includes('country');
                    });
                    if (filterCountry[0].short_name.toLowerCase() === 'qa') {
                        this.setState({
                            Address: data.results[0].formatted_address,
                            locationCoordinates: coordinate,
                            locateMe: coordinate
                        })
                    }
                }
            })
            .catch(error => {
                console.log(error);
            })
    }

    getCoordinates = (locationArray, locationName) => {
        this.setState({
            locationCoordinates: locationArray,
            Address: locationName
        })
    }

    saveLocation = () => {
        const { locationCoordinates, Address } = this.state;
        let detailsArray = [{
            coordinates: locationCoordinates,
            name: Address
        }];
        this.props.AddToLocation(detailsArray);
        this.props.hideLocationModal();
    }

    saveDefaultAddress = () => {
        this.props.hideLocationModal();
    }

    getUserLocation = () => {
        navigator.geolocation.getCurrentPosition(({ coords: { latitude: lat, longitude: lng } }) => {
            let coordinates = {
                lat: lat,
                lng: lng
            }
            this.onMarkerDragEnd(coordinates);
        })
    }

    render() {
        const { locationCoordinates, Address, loggedIn } = this.state;
        const { modalStatus } = this.props;
        return (
            <div className={modalStatus ? "modal fade show" : "modal fade"} style={modalStatus ? { display: 'block' } : { display: 'none' }} id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable">
                    <div className="modal-content address-popup">
                        {
                            window.location.pathname.split("/")[4] === 'grocery' ?
                                null
                                :
                                <div className="modal-header">
                                    <h5 className="modal-title">Search & Select Stores by Location nearby <strong>Old Airport Road</strong> </h5>
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.saveDefaultAddress}>
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                        }
                        <div className="modal-body modalHeight ">
                            {/* <div className="location-footer">
                                <p className=" notably"><IoMdLocate /> To get your current location or click desired location in map to bring the <GiPositionMarker /></p>
                            </div> */}
                            {
                                loggedIn ?
                                    <AddressBook colLayout={'col-md-12 col-sm-12 col-xs-12 col-lg-12'} setDefaultLocationInReducer={this.saveDefaultAddress} />
                                    :
                                    <>
                                        <LocationTextBox fetchLocation={(locArray, name) => this.getCoordinates(locArray, name)} Address={Address} />
                                        <div className="map">
                                            <GoogleMaps coordinates={locationCoordinates} draggable={true} getCurrentPosition={(coord) => this.onMarkerDragEnd(coord)} />
                                            <button type="button" onClick={this.getUserLocation} className="btn signup_with_gmail pull-right rounded-0"><BiCurrentLocation /></button>
                                        </div>
                                    </>
                            }

                        </div>
                        <div className="modal-footer location-footer">
                            <button type="button" onClick={this.saveLocation} className="btn btn-add-address pull-right  ">Deliver Here</button>
                            <div className="bottom-selected-stores">
                                <h2>Selected Stores</h2>
                                <div className="store-selected">
                                    <ul>
                                        <li> <span className="store-left"><img src={SelectedStoresImg} alt={SelectedStoresImg} /> <span>Greenmart</span> </span> <span className="store-right"> <span> <strong>Barwa City </strong>2km </span> <a href="#">×</a> </span></li>
                                        <li> <span className="store-left"><img src={SelectedStoresImg} alt={SelectedStoresImg} /> <span>Greenmart</span> </span> <span className="store-right"> <span> <strong>Barwa City </strong>2km </span> <a href="#">×</a> </span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    location: state.Location,
})

export default connect(
    mapStateToProps,
    { AddToLocation }
)(LocationModal);