import React, { Component } from "react";
//import axios from "axios";
import fullstar from "../../assets/images/Full-Star-icon.svg";


export default class SellerProduct extends Component{
    state = {
        Skip: 0,
        Limit: 9,
        postSize: '',
        ProductList: this.props.prodData,
      }
    render(){
        const getPrimaryImage = (data) => {
            var imageData;
            if (data.productId.productImageId[0] === undefined) {
                imageData = `${window.$ImageURL}no-image-480x480.png`
            } else {
                imageData = `${window.$ImageURL}` + data.productId.productImageId[0].originalURL
            }
            return(
                imageData
            )
        }
        return(
            <div class="col-md-3 col-sm-12 flex-screen">
                <div class="card product-card">
                    <a href={`${process.env.PUBLIC_URL}/productDetails/${this.state.ProductList.productId._id}`}>
                        <div class="offer-area">
                            <span>{this.state.ProductList.productId.brandName.name !== null?this.state.ProductList.productId.brandName.name:'No Brand'}</span>
                        </div>
                        <div class="product-image">
                            <img src={getPrimaryImage(this.state.ProductList)} class="img-responsive" alt="imageName" />
                        </div>
                        <div class="price-with-fav">
                            {/* <h4 class="offer_quote">32% Off </h4> */}

                            <a class="favorite_button" style={{position:'absolute'}} href="#/">
                                <i class="fa fa-heart-o" aria-hidden="true"></i>
                                {/* <!-- <i class="fa fa-heart" aria-hidden="true"></i> --> */}
                            </a>
                        </div>
                        <h5 class="card-title">
                        {this.state.ProductList.productId.productname.substring(0,12)}{this.state.ProductList.productId.productname.substring(0,10) > 12 ? '...':''}
                        </h5>
                        {/* <h6 class="qty">1kg</h6>
                        <h6 class=" qty dimention">4D</h6> */}

                        <p class="priceTag">
                            ${this.state.ProductList.sellerPrice !== null ?this.state.ProductList.sellerPrice:'0.00' }
                            {/* <label>$80</label> */}
                        </p>
                        <div class="rating">
                            <img src={fullstar} alt="imageName" />
                            <img src={fullstar} alt="imageName" />
                            <img src={fullstar} alt="imageName" />
                            <img src={fullstar} alt="imageName" />
                            <img src={fullstar} alt="imageName" />
                            <span class="totalUser">(11201)</span>
                        </div>

                        <div class="action-button">
                            <a href="#/" class="btn cart_new">
                                <img
                                    src="./images/cart_icon.svg"
                                    alt="imageName"
                                    class="img-responsive"
                                />
                                <span> Add to cart </span>
                            </a>
                            {/* <!-- cart-increase --> */}

                            <div class="cart-increse">
                                <button class="btn">-</button>
                                <div class="text-content">8</div>
                                <button class="btn">+</button>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        )
    }
}