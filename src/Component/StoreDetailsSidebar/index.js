import React, { Component } from "react";
import ReactStars from "react-rating-stars-component";
const ProductRating = {
    size: 30,
    value: 0,
    edit: true,
    isHalf: true
};
export default class ProductListSidebar extends Component {
    state = {
        searchCat: [],
        searchCountry: [],
        searchBrand: [],
        checkedCat: [],
        checkedCon: [],
        checkedBrand: [],
        maxval: '',
        minval: '',
        sidebarToggle: false,
        toggleShow: [],
    }

    async componentDidMount() {
        this.getsearchCatData(this.props.catData)
        this.getsearchConData(this.props.contryData)
        this.getsearchBrndData(this.props.brandData)
    }

    async componentDidUpdate(prevProps) {
        if (prevProps.catData !== this.props.catData) {
            this.getsearchCatData(this.props.catData)
        }
        if (prevProps.contryData !== this.props.contryData) {
            this.getsearchConData(this.props.contryData)
        }
        if (prevProps.brandData !== this.props.brandData) {
            this.getsearchBrndData(this.props.brandData)
        }
    }

    getsearchCatData = (data) => {
        if (data.length > 0) {
            this.setState({
                searchCat: data
            })
        }
    }

    getsearchConData = (data) => {
        if (data.length > 0) {
            this.setState({
                searchCountry: data
            })
        }
    }

    getsearchBrndData = (data) => {
        if (data.length > 0) {
            this.setState({
                searchBrand: data
            })
        }
    }

    handleToggle = (catData, filtername) => {
        const { checkedCat } = this.state;
        const currentIndex = checkedCat.indexOf(catData);
        const newChecked = [...checkedCat];
        if (currentIndex === -1) {
            newChecked.push(catData)
        } else {
            newChecked.splice(currentIndex, 1)
        }

        this.setState({ checkedCat: newChecked })
        this.props.showFilteredResult(newChecked, filtername)
    }

    handleTogglecountry = (conData, filtername) => {
        const { checkedCon } = this.state;
        const currentIndex = checkedCon.indexOf(conData);
        const newChecked = [...checkedCon];
        if (currentIndex === -1) {
            newChecked.push(conData)
        } else {
            newChecked.splice(currentIndex, 1)
        }
        this.setState({ checkedCon: newChecked })
        this.props.showFilteredcountResult(newChecked, filtername)
    }

    handleTogglebrand = (brandData, filtername) => {
        const { checkedBrand } = this.state
        const currentIndex = checkedBrand.indexOf(brandData)
        const newChecked = [...checkedBrand]
        if (currentIndex === -1) {
            newChecked.push(brandData)
        } else {
            newChecked.splice(currentIndex, 1)
        }
        this.setState({ checkedBrand: newChecked })
        this.props.showFilteredbrandResult(newChecked, filtername)
    }

    getChange = (e) => {
        e.preventDefault();
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    getminval = (e) => {
        e.preventDefault();
        const {minval, maxval} = this.state;
        let priceRange = {minval, maxval};
        if(minval !== '' && maxval !== ''){
            this.props.showFilteredPriceResult(priceRange,'PriceRange')
        }
    }

    handleSidebarMobileToggle = () => {
        this.setState({ sidebarToggle: !this.state.sidebarToggle });
    };

    toggle = (type) => {
        let toggleShowArray = this.state.toggleShow;
    
        if (toggleShowArray.includes(type)) {
          let findIndex = toggleShowArray.indexOf(type);
          if (findIndex !== -1) {
            toggleShowArray.splice(findIndex, 1);
          }
        } else {
          toggleShowArray.push(type);
        }
        this.setState({
          toggleShow: toggleShowArray
        })
    }

    ratingSubmit = async (ratingVal) => {
        this.props.showFilteredRatingResult(ratingVal,'rating')
    }

    render() {
        const {sidebarToggle,toggleShow} = this.state;
        return (
            <div className="col-md-3 leftPannel">
                <div className="sm-search-address filter-new-wr">
                        <button className="btn filter" onClick={this.handleSidebarMobileToggle}>
                        <i className="fa fa-filter" aria-hidden="true"></i>
                        </button>
                    </div>

                    <div className="lv-menu-sec"  >
                        <div className={sidebarToggle ? "menu-show " : "menu-hide"}  >
                            <button className="btn cancel" onClick={this.handleSidebarMobileToggle}>X</button>
                            <div className="inner-filter-container">
                            <div className="ca-filter">
                                <p className="title">Category <i className={toggleShow.includes('Category') ? "fa fa-angle-up" : "fa fa-angle-down"} aria-hidden="true" onClick={() => this.toggle('Category')}></i></p>
                                <div className={toggleShow.includes('Category') ? "collapse-content show" : "collapse-content"}>
                                    {this.state.searchCat.map((catData, catindex) => {
                                        return (
                                            <button className="tsgs" key={catindex} onClick={() => this.handleToggle(catData, 'category')}>{catData}</button>
                                        )
                                    })}
                                </div>
                            </div>
                            <div className="ca-filter">
                                <p className="title">Country of origin <i className={toggleShow.includes('Country of origin') ? "fa fa-angle-up" : "fa fa-angle-down"} aria-hidden="true" onClick={() => this.toggle('Country of origin')}></i></p>
                                <div className={toggleShow.includes('Country of origin') ? "collapse-content show" : "collapse-content"}>
                                    {
                                        this.state.searchCountry.map((conData, conIndex) => {
                                            return (
                                                <button className={this.state.checkedCon.includes(conData) ?"tsgs active":'tsgs inactive'} onClick={() => this.handleTogglecountry(conData, 'country')} key={conIndex}>{conData}</button>
                                            )
                                        })
                                    }
                                </div>
                            </div>
                            <div className="ca-filter">
                                <p className="title">Brand <i className={toggleShow.includes('Brand') ? "fa fa-angle-up" : "fa fa-angle-down"} aria-hidden="true" onClick={() => this.toggle('Brand')}></i></p>
                                <div className={toggleShow.includes('Brand') ? "collapse-content show" : "collapse-content"}>
                                {
                                    this.state.searchBrand.map((brandData, brandindex) => {
                                        return (
                                            <button className="tsgs" onClick={() => this.handleTogglebrand(brandData, 'brand')} key={brandindex}>{brandData}</button>
                                        )
                                    })
                                }
                                </div>
                            </div>
                            <div className="ca-filter">
                                <p className="title">Price</p>
                                <div className="min-max-wrapper">
                                <input type="number" className="form-control" placeholder="min" onChange={this.getChange} />
                                <span className="lines"> </span>
                                <input type="text" className="form-control" placeholder="max" onChange={this.getChange} />
                                <button className="btn " onClick={this.getminval}><i className="fa fa-play" aria-hidden="true"></i></button>
                                </div>
                            </div>
                            <div className="st-action-button">
                                <button type="button" className="btn" onClick={() => window.location.reload()}>Reset</button>
                                <button type="button" className="btn" onClick={this.handleSidebarMobileToggle}>Done</button>
                            </div> </div>
                        </div>
                    </div>
                    

                <div className="list_left_bg">
                    <div className="filter-section">
                        <h2 className="filter">filter:</h2>
                        <p className="clearall" onClick={() => window.location.reload()}>Clear all </p>
                    </div>
                    
                   
                    
                    {
                        this.state.searchCat.length > 0
                            ?
                            <div className="product-cat-section">
                                <p className="cat-title-text">Category</p>
                                <div className="prod-list scrollVerical" style={{ overflow: 'auto' }}>
                                    <form>
                                        {this.state.searchCat.map((catData, catindex) => {
                                            return (
                                                <div className="form-group" style={{ border: 'none' }} key={catindex}>
                                                    <label className="c-s-checkbox ">
                                                        {catData}
                                                        <input type="checkbox" onChange={() => this.handleToggle(catData, 'category')} />
                                                        <span className="checkmark"></span>
                                                    </label>
                                                </div>
                                            )
                                        })}
                                    </form>
                                </div>
                            </div>
                            :
                            null
                    }

                    <div className="product-cat-section">
                        <p className="cat-title-text">Filter by price</p>

                        <div className="prod-list">
                            <div className="min_max_wr">
                                <div className="form-group min_max">
                                    <input
                                        type="text"
                                        className="form-control"
                                        placeholder="Min"
                                        name="minval"
                                        onChange={this.getChange}
                                    />
                                </div>
                                <div className="minus">-</div>
                                <div className="form-group min_max">
                                    <input
                                        type="text"
                                        className="form-control"
                                        placeholder="Max"
                                        name="maxval"
                                        onChange={this.getChange}
                                    />
                                </div>
                                <button className="btn playBtn" onClick={this.getminval}>
                                    <i className="fa fa-play"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    {/* <!-- end min max --> */}
                    {
                        this.state.searchCountry.length > 0
                            ?
                            <div className="product-cat-section">
                                <p className="cat-title-text">Country of origin</p>
                                <div className="prod-list">
                                    <form>
                                        {
                                            this.state.searchCountry.map((conData, conIndex) => {
                                                return (
                                                    <div className="form-group" key={conIndex}>
                                                        <label className="c-s-checkbox ">
                                                            {conData}
                                                            <input type="checkbox" onChange={() => this.handleTogglecountry(conData, 'country')} />
                                                            <span className="checkmark"></span>
                                                        </label>
                                                    </div>
                                                )
                                            })
                                        }
                                    </form>
                                </div>
                            </div>
                            :
                            null
                    }
                    {
                        this.state.searchBrand.length > 0
                            ?
                            <div className="product-cat-section">
                                <p className="cat-title-text">Brand</p>
                                {
                                    this.state.searchBrand.map((brandData, brandindex) => {
                                        return (
                                            <div className="prod-list" key={brandindex}>
                                                <form>
                                                    <div className="form-group">
                                                        <label className="c-s-checkbox ">
                                                            {" "}
                                                            {brandData}
                                                            <input type="checkbox" onChange={() => this.handleTogglebrand(brandData, 'brand')} />
                                                            <span className="checkmark"></span>
                                                        </label>
                                                    </div>
                                                </form>
                                            </div>
                                        )
                                    })
                                }

                            </div>
                            :
                            null
                    }

                    <div className="product-cat-section">
                        <p className="cat-title-text">Rating</p>

                        <div className="prod-list">
                            <ReactStars onChange={(val) => this.ratingSubmit(val)} {...ProductRating} />
                            {/*<div className="rating_new">
                                <span className="star-full">
                                    <i className="fa fa-star" aria-hidden="true"></i>
                                </span>
                                <span className="star-full">
                                    <i className="fa fa-star" aria-hidden="true"></i>
                                </span>
                                <span className="star-full">
                                    <i className="fa fa-star" aria-hidden="true"></i>
                                </span>
                                <span className="star-full">
                                    <i className="fa fa-star" aria-hidden="true"></i>
                                </span>
                                <span className="star-full">
                                    <i className="fa fa-star" aria-hidden="true"></i>
                                </span>
                            </div>

                            <div className="rating_new">
                                <span className="star-full">
                                    <i className="fa fa-star" aria-hidden="true"></i>
                                </span>
                                <span className="star-full">
                                    <i className="fa fa-star" aria-hidden="true"></i>
                                </span>
                                <span className="star-full">
                                    <i className="fa fa-star" aria-hidden="true"></i>
                                </span>
                                <span className="star-full">
                                    <i className="fa fa-star" aria-hidden="true"></i>
                                </span>
                                <span className="star-full">
                                    <i className="fa fa-star" aria-hidden="true"></i>
                                </span>
                                <span className="totalUser">
                                    <i className="fa fa-star-o" aria-hidden="true"></i>
                                </span>
                            </div>*/}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}