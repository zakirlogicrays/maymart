import React, { Component } from "react";
import axios from "axios";
import jwt_decode from "jwt-decode";
import AddAddress from "./addAddress";
import EditAddress from "./editAddress";
import Loader from "../../Component/Loader";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { connect } from 'react-redux';
import { UpdateLocation } from '../../data/reducers/location';

class UserAddress extends Component {
    state = {
        editAddressId: '',
        userAddressArray: [],
        addAddressHide: true,
        editAddressHide: true,
        loading: true,
        pageName: window.location.pathname.split("/"),
        defaultAddress: null,
        defaultAddressReducer: '',
        defaultCoordinatesReducer: []
    }

    async componentDidMount() {

        let checkToken = await localStorage.getItem("token");
        if (checkToken) {
            const decoded = jwt_decode(checkToken);
            let customerId = decoded.userid;

            await axios.post(`${window.$URL}userAddress/fetch-user-address`, { customerId })
            .then(response => {
                if (response.status === 200) {
                    if (response.data.success) {
                        this.setState({
                            userAddressArray: response.data.data,
                            loading: false
                        },() => {
                            if(this.state.userAddressArray.length > 0){
                                this.fetchDeliveryAddress();
                            }
                        })
                    } else {
                        console.log('errror on fetch');
                    }
                }
            })
            .catch(error => {
                console.log(error);
            })
        }
    }

    fetchDeliveryAddress = () => {
        const {userAddressArray,pageName} = this.state;
        let filterAddress = userAddressArray.filter((eachAddr) => {
            return eachAddr.Default === true
        })
        if(pageName[2] === 'cart'){
            // this.props.getDeliveryAddress(filterAddress[0]._id)
        }
    }

    addAddress = () => {
        this.setState({
            editAddressId : '',
            addAddressHide: false,
            editAddressHide: true
        })
    }

    editAddressId = (editId) => {
        this.setState({
            editAddressId : editId,
            addAddressHide: true,
            editAddressHide: false
        })
    }

    cancelEdit = () => {
        this.setState({
            editAddressId : '',
            editAddressHide: true
        },() => {
            this.componentDidMount();
        })
    }

    cancelAdd = () => {
        this.setState({
            addAddressHide: true,
        },() => {
            this.componentDidMount();
        })
    }

    setDefaultAddress = async(addressId,dataArray) => {
        let checkToken = await localStorage.getItem("token");
        const decoded = jwt_decode(checkToken);
        let customerId = decoded.userid;
        
        await axios.post(`${window.$URL}userAddress/default/${customerId}/${addressId}`)
        .then(response => {
            if (response.status === 200) {
                if (response.data.success) {
                    let locationCoordinates= {
                        lat: dataArray.Location.coordinates[1],
                        lng: dataArray.Location.coordinates[0]
                    };
                    let Address = '';
                    if(dataArray.AddressType !== undefined && dataArray.AddressType === 'Choose on Map'){
                        Address = dataArray.Address;
                    }else{
                        Address = dataArray.Locality;
                    }
                    let detailsArray = [{
                        coordinates: locationCoordinates,
                        name: Address
                    }];
                    this.props.UpdateLocation(detailsArray);
                    if(this.props.setDefaultLocationInReducer !== undefined){
                        this.props.setDefaultLocationInReducer('hide');
                    }
                    /*toast.success(response.data.message, {
                        position: "bottom-center",
                        autoClose: 3000,
                        hideProgressBar: true,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });*/
                   this.componentDidMount();
                }
            }
        })
        .catch(error => {
            console.log(error);
        })
    }

    deleteAddressId = async(addressId) => {
        await axios.delete(`${window.$URL}userAddress/${addressId}`)
        .then(response => {
            if (response.status === 200) {
                if (response.data.success) {
                    toast.success(response.data.message, {
                        position: "bottom-center",
                        autoClose: 3000,
                        hideProgressBar: true,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                   this.componentDidMount();
                }
            }
        })
        .catch(error => {
            console.log(error);
        })
    }

    render() {
        const { editAddressId, userAddressArray, addAddressHide, editAddressHide, loading } = this.state;
        return (
            <div className={`${this.props.colLayout} my-order-wr`}>
                 <ToastContainer />
                <div className='row'>
                    <h2 className="col-md-8 col-sm-6 col-6 order-title">My Address</h2> 
                    <div className="col-md-4 col-sm-6 col-6 addAddress_button_wr" >
                        <button type="button" onClick={this.addAddress} className="btn btn-add-address">ADD ADDRESS</button>
                    </div>
                </div>

                <div className="cart-card addressbook-wr add" style={addAddressHide || editAddressId !== '' ? { display: 'none' } : { display: 'block' }}>
                    <div className="address-section">
                        <AddAddress cancelAdd={() => this.cancelAdd()}  />
                    </div>
                </div>
                <div className="cart-card addressbook-wr edit" style={editAddressHide ? { display: 'none' } : { display: 'block' }}>
                    <div className="address-section">
                        {
                            editAddressId !== '' ?
                            <EditAddress editAddressId={editAddressId} userAddressArray={userAddressArray} cancelAction={() => this.cancelEdit()} />
                            :
                            null
                        }
                    </div>
                </div>
                <div className="row mb-4">
                    {
                        loading ? 
                        <div className="col-md-12 col-sm-12 flex-screen" style={{justifyContent:'center'}}>
                            <Loader />
                        </div>
                        :
                        userAddressArray.length > 0 ?
                            userAddressArray.map((data,index) => (

                            <div className="col-md-4 col-sm-6 flex-screen" key={index}>
                                <div className={data.Default ? "card address-card active" : "card address-card"} >
                                    <div className="card-body">
                                        <p className="name">  {data.Name}  </p>
                                        <p className="number">{data.MobileNo}</p>
                                        {
                                            data.AddressType !== undefined && data.AddressType === 'Choose on Map' ? 
                                            <p className="address">{data.Address}</p>
                                            :
                                            <p className="address"><b>Floor</b>: {data.Floor} <b>Flat:</b> {data.Flat}<br /><b>Building:</b> {data.Building}, <b>Zone:</b> {data.Zone}, <b>Street:</b> {data.Street} <br /> <b>Locality:</b> {data.Locality}</p>
                                        }
                                    </div>
                                    <div className="action-btn">
                                        <button className="btn edit" onClick={() => this.editAddressId(data._id)}>Edit</button>
                                        <button className="btn dlt" onClick={() => this.deleteAddressId(data._id)} >Delete</button>
                                    </div>
                                    {
                                        data.Default ? 
                                        <button className="btn save-default active-button" >Default Address</button>
                                        :
                                        <button className="btn save-default" onClick={() => this.setDefaultAddress(data._id,data)} >Save as default</button>
                                    }
                                </div>
                            </div>
                        ))
                        :
                        <div className="col-md-12 col-sm-12 flex-screen">
                            <div className="card product-card" style={{ padding: '15px' }}>
                                <h3 style={{ textAlign: 'center' }}>No Address Found</h3>
                            </div>
                        </div>
                    }
                </div>

            </div>
        )
    }
}  
const mapStateToProps = (state) => ({
  location: state.Location,
})

export default connect(
  mapStateToProps,
  {UpdateLocation}
)(UserAddress);  