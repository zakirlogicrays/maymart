import React, { Component } from 'react';
import axios from "axios";
import jwt_decode from "jwt-decode";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import GoogleMaps from "../GoogleMaps";
import LocationTextBox from '../LocationTextBox';

export default class EditAddress extends Component {
    state = {
        Name: '',
        MobileNo: '',
        AddressType: '',
        Zone: '',
        Street: '',
        Building: '',
        Floor: '',
        Flat: '',
        Locality: '',
        Address: '',
        Type: '',
        User: '',
        Default: false,
        latitude: 0,
        longitude: 0,

        NameError: '',
        MobileNoError: '',
        AddressTypeError: '',
        StreetError: '',
        BuildingError: '',
        FloorError: '',
        FlatError: '',
        LocalityError: '',
        AddressError: '',
        TypeError: '',

        successMessage: '',
        success: false,
        errorMessage: '',
        error: false,
        locationCoordinates: {
            lat: 25.2854473,
            lng: 51.53103979999999
        },

        locationOnMapCoordinates: {
            lat: 25.2854473,
            lng: 51.53103979999999
        },
    }

    componentDidMount() {
        let addressId = this.props.editAddressId;
        let userAddressArray = this.props.userAddressArray;
        this.editAddress(addressId, userAddressArray);
    }

    onChange = (e) => {
        const { name, value, checked } = e.target;
        if (name === 'Default') {
            this.setState({
                [name]: checked
            })
        } else {
            this.setState({
                [name]: value
            }, () => {
                if (name === 'Zone' || name === 'Street' || name === 'Building') {
                    this.fetchLocation();
                }
            });
        }
    }

    fetchLocation = async () => {
        const { Zone, Street, Building } = this.state;
        await axios.get(`https://services.gisqatar.org.qa/server/rest/services/Vector/QARS_wgs84/MapServer/0/query?where=zone_no%3D${Zone}+and+street_no%3D${Street}+and+building_no%3D${Building}&text=&objectIds=&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelIntersects&relationParam=&outFields=X_COORD%2CY_COORD&returnGeometry=true&returnTrueCurves=false&maxAllowableOffset=&geometryPrecision=&outSR=&having=&returnIdsOnly=false&returnCountOnly=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&returnZ=false&returnM=false&gdbVersion=&historicMoment=&returnDistinctValues=false&resultOffset=&resultRecordCount=&queryByDistance=&returnExtentOnly=false&datumTransformation=&parameterValues=&rangeValues=&quantizationParameters=&f=pjson`)
            .then(result => {
                if (result.data.features.length > 0) {
                    this.setState({
                        latitude: result.data.features[0].attributes.Y_COORD,
                        longitude: result.data.features[0].attributes.X_COORD,
                    }, () => {
                        this.setState({
                            locationCoordinates: {
                                lat: result.data.features[0].attributes.Y_COORD,
                                lng: result.data.features[0].attributes.X_COORD
                            }
                        })
                    })
                }
            })
            .catch(error => {
                console.log(error);
            })
    }

    validate = () => {
        this.setState({
            NameError: '',
            MobileNoError: '',
            AddressTypeError: '',

            StreetError: '',
            ZoneError: '',

            BuildingError: '',
            /*FloorError: '',

            FlatError: '',
            LocalityError: '',*/
            AddressError: '',
            TypeError: ''
        });
        // const regex = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
        let NameError = '';
        let MobileNoError = '';
        let AddressTypeError = '';
        let StreetError = '';
        let ZoneError = '';
        let BuildingError = '';
        /*let FloorError = '';

        let FlatError = '';
        let LocalityError = '';*/
        let AddressError = '';
        let TypeError = '';




        if (this.state.Name.length < 1) {
            NameError = "Please Enter Name";
        }
        if (NameError) {
            this.setState({ NameError })
            return false
        }

        if (this.state.MobileNo.length < 1) {
            MobileNoError = "Please Enter Mobile No";
        }
        if (MobileNoError) {
            this.setState({ MobileNoError })
            return false
        }

        if (this.state.AddressType.length < 1) {
            AddressTypeError = "Please Choose Address Type";
        }
        if (AddressTypeError) {
            this.setState({ AddressTypeError })
            return false
        }

        if(this.state.AddressType === 'Choose on Map'){
            if (this.state.Address.length < 1) {
                AddressError = "Please Enter Address";
            }
    
            if (AddressError) {
                this.setState({ AddressError })
                return false
            }
        }else if(this.state.AddressType === 'Blue Plate Details'){
            if (this.state.Street.length < 1) {
                StreetError = "Please Enter Street";
            }
            if (StreetError) {
                this.setState({ StreetError })
                return false
            }
    
            if (this.state.Zone.length < 1) {
                ZoneError = "Please Enter Zone";
            }
    
            if (ZoneError) {
                this.setState({ ZoneError })
                return false
            }
    
            if (this.state.Building.length < 1) {
                BuildingError = "Please Enter Building";
            }
            if (BuildingError) {
                this.setState({ BuildingError })
                return false
            }
            /*if (this.state.Floor.length < 1) {
                FloorError = "Please enter Floor";
            }
            if (FloorError) {
                this.setState({ FloorError })
                return false
            }
    
    
    
            if (this.state.Flat.length < 1) {
                FlatError = "Please Enter Flat";
            }
            if (FlatError) {
                this.setState({ FlatError })
                return false
            }
    
            if (this.state.Locality.length < 1) {
                LocalityError = "Please Enter Locality";
            }
            if (LocalityError) {
                this.setState({ LocalityError })
                return false
            }*/
        }

        if (this.state.Type === '') {
            TypeError = "Choose Address Type";
        }
        if (TypeError) {
            this.setState({ TypeError })
            return false
        }

        this.setState({
            NameError,
            MobileNoError,
            AddressTypeError,
            StreetError,
            ZoneError,
            BuildingError,
            /*FloorError,
            FlatError,
            LocalityError,*/
            AddressError,
            TypeError


        })
        return true
    }

    async editAddress(addressId, userAddressArray) {
        this.setState({
            addAddressHide: true
        })

        let checkToken = await localStorage.getItem("token");
        if (checkToken) {
            var selectedAdress = userAddressArray.filter(item => {
                return item._id === addressId;
            })
            this.setState({
                editForm: true,
                EditAddressId: addressId,
                editedAddressArray: selectedAdress[0],
                Name: selectedAdress[0].Name,
                MobileNo: selectedAdress[0].MobileNo,
                Zone: selectedAdress[0].Zone,
                Street: selectedAdress[0].Street,
                Building: selectedAdress[0].Building,
                Floor: selectedAdress[0].Floor,
                Flat: selectedAdress[0].Flat,
                Locality: selectedAdress[0].Locality,
                Address: selectedAdress[0].Address,
                Type: selectedAdress[0].Type,
                User: selectedAdress[0].User,
                Default: selectedAdress[0].Default,
                AddressType: selectedAdress[0].AddressType,
                locationCoordinates: {
                    lat: selectedAdress[0].Location !== undefined ? selectedAdress[0].Location.coordinates[1] : 0,
                    lng: selectedAdress[0].Location !== undefined ? selectedAdress[0].Location.coordinates[0] : 0
                },
                locationOnMapCoordinates: {
                    lat: selectedAdress[0].chooseOnMapLocation !== undefined ? selectedAdress[0].chooseOnMapLocation.coordinates[1] : 0,
                    lng: selectedAdress[0].chooseOnMapLocation !== undefined ? selectedAdress[0].chooseOnMapLocation.coordinates[0] : 0
                }
            })
        }
    }

    async cancelEdit() {
        this.setState({
            editForm: false,
        }, () => {
            this.props.cancelAction(true);
        })
    }

    updateAddress = async (e, addressId) => {

        e.preventDefault();
        let checkToken = await localStorage.getItem("token");
        if (checkToken) {
            const decoded = jwt_decode(checkToken);
            let customerId = decoded.userid;
            //alert(addressId);return false;
            this.setState({
                success: false,
                error: false,
                successMessage: '',
                errorMessage: ''
            })
            let checkValidation = this.validate();
            if (checkValidation) {

                const { Name, Street, MobileNo, Building, Floor, Flat, Locality, Zone, Address, Type, Default, latitude, longitude, locationOnMapCoordinates,AddressType } = this.state;

                let addressData = {
                    Name: Name,
                    MobileNo: MobileNo,
                    Zone: Zone,
                    Street: Street,
                    Building: Building,
                    Floor: Floor,
                    Flat: Flat,
                    Locality: Locality,
                    Address: Address,
                    Type: Type,
                    User: customerId,
                    Default: Default,
                    AddressType,
                    Location: {
                        coordinates: [
                            longitude,
                            latitude
                        ]
                    },
                    chooseOnMapLocation: {
                        coordinates: [
                            locationOnMapCoordinates.lng,
                            locationOnMapCoordinates.lat
                        ]
                    },
                    _id: addressId
                };
                

                console.log(addressData);
                await axios.put(`${window.$URL}userAddress/update-user-address`, addressData)
                .then(response => {
                    if (response.status === 200) {
                        if (response.data.success) {
                            toast.success(response.data.message, {
                                position: "bottom-center",
                                autoClose: 3000,
                                hideProgressBar: true,
                                closeOnClick: true,
                                pauseOnHover: true,
                                draggable: true,
                                progress: undefined,
                            });
                            this.setState({
                                editForm: false,
                            }, () => {
                                this.props.cancelAction(true);
                            })
                        } else {
                            toast.error(response.data.message, {
                                position: "bottom-center",
                                autoClose: 3000,
                                hideProgressBar: true,
                                closeOnClick: true,
                                pauseOnHover: true,
                                draggable: true,
                                progress: undefined,
                            }, () => {
                                this.props.cancelAction(true);
                            });
                        }
                    }
                })
                .catch(error => {
                    console.log(error);
                })
            }
        }

    }

    onMarkerDragEnd = (coordinate) => {
        this.setState({
            locationOnMapCoordinates: coordinate
        }, async () => {
            await fetch(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${coordinate.lat},${coordinate.lng}&key=${process.env.REACT_APP_GOOGLE_API_KEY}&type=locality`)
                .then(response => response.json())
                .then(data => {
                    if (data.status === 'OK') {
                        this.setState({
                            Address: data.results[0].formatted_address
                        })
                    }
                })
                .catch(error => {
                    console.log(error);
                })
        })
    }

    getCoordinates = (locationArray, locationName) => {
        this.setState({
            locationOnMapCoordinates: locationArray,
            Address: locationName
        })
    }

    render() {
        const { ZoneError, NameError, MobileNoError, StreetError, BuildingError, FloorError, FlatError, LocalityError, /*AddressError,*/ AddressTypeError, TypeError, success, error, successMessage, errorMessage, editForm, editedAddressArray, locationCoordinates, locationOnMapCoordinates, Address, AddressType } = this.state;
        return (
            <>
                <form onSubmit={e => this.updateAddress(e, editedAddressArray._id)} style={editForm ? { display: "block" } : { display: "none" }}   >
                   

                        <ToastContainer />
                        <h1>Contact Details</h1>
                        {
                            success ?
                                <p style={{ color: 'green', fontSize: 18 }}> {successMessage} </p>
                                :
                                null
                        }
                        {
                            error ?
                                <p style={{ color: 'red', fontSize: 18 }}> {errorMessage} </p>
                                :
                                null
                        }
                        <div className="row">
                            <div className="col-6">
                                <div className="form-group">
                                    <label className="input-heading" htmlFor="exampleInputEmail1">Name</label>
                                    <input type="text" className="form-control" name="Name" defaultValue={this.state.Name} onChange={this.onChange} placeholder="Name*" />
                                    {NameError ? <span style={{ color: 'red' }}> {NameError} </span> : null}
                                </div>
                            </div>
                            <div className="col-6">
                                <div className="form-group mb-4">
                                    <label className="input-heading" htmlFor="exampleInputEmail1">Mobile No</label>
                                    <input type="text" className="form-control" name="MobileNo" defaultValue={this.state.MobileNo} onChange={this.onChange} placeholder="Mobile No*" />
                                    {MobileNoError ? <span style={{ color: 'red' }}> {MobileNoError} </span> : null}

                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-6">
                                <div className="form-group">
                                    <label className="c-s-checkbox ">Blue Plate Details
                                        <input type="radio" name="AddressType" value="Blue Plate Details" onChange={this.onChange} checked={AddressType === 'Blue Plate Details' ? true : false} />
                                        <span className="checkmark" />
                                    </label>
                                    {AddressTypeError ? <span style={{ color: 'red' }}> {AddressTypeError} </span> : null}
                                </div>
                            </div>
                            <div className="col-6">
                                <div className="form-group">
                                    <label className="c-s-checkbox ">Choose on Map
                                        <input type="radio" name="AddressType" value="Choose on Map" onChange={this.onChange} checked={AddressType === 'Choose on Map' ? true : false}  />
                                        <span className="checkmark" />
                                    </label>
                                    {AddressTypeError ? <span style={{ color: 'red' }}> {AddressTypeError} </span> : null}
                                </div>
                            </div>
                        </div>
                        
                        <div className="row" style={AddressType === 'Blue Plate Details' ? { display: 'flex' } : { display: 'none' }}>
                        <div className="col-6">
                            <div className="form-row">
                                <div className="form-group col-md-6">
                                    <label className="input-heading" htmlFor="exampleInputEmail1">Zone</label>
                                    <input type="text" className="form-control" name="Zone" defaultValue={this.state.Zone} onChange={this.onChange} placeholder="Zone*" />
                                    {ZoneError ? <span style={{ color: 'red' }}> {ZoneError} </span> : null}

                                </div>
                                <div className="form-group col-md-6">
                                    <label className="input-heading" htmlFor="exampleInputEmail1">Street</label>
                                    <input type="text" className="form-control" name="Street" defaultValue={this.state.Street} onChange={this.onChange} placeholder="Street*" />
                                    {StreetError ? <span style={{ color: 'red' }}> {StreetError} </span> : null}

                                </div>
                            </div>
                            <div className="form-row">
                                <div className="form-group col-md-6">
                                    <label className="input-heading" htmlFor="exampleInputEmail1">Building</label>
                                    <input type="text" className="form-control" name="Building" defaultValue={this.state.Building} onChange={this.onChange} placeholder="Building*" />
                                    {BuildingError ? <span style={{ color: 'red' }}> {BuildingError} </span> : null}

                                </div>
                                <div className="form-group col-md-6">
                                    <label className="input-heading" htmlFor="exampleInputEmail1">Floor</label>
                                    <input type="text" className="form-control" name="Floor" defaultValue={this.state.Floor} onChange={this.onChange} placeholder="Floor" />
                                    {FloorError ? <span style={{ color: 'red' }}> {FloorError} </span> : null}

                                </div>
                            </div>
                            <div className="form-row">

                                <div className="form-group col-md-6">
                                    <label className="input-heading" htmlFor="exampleInputEmail1">Flat</label>
                                    <input type="text" className="form-control" name="Flat" defaultValue={this.state.Flat} onChange={this.onChange} placeholder="Flat" />
                                    {FlatError ? <span style={{ color: 'red' }}> {FlatError} </span> : null}

                                </div>
                                <div className="form-group col-md-6">
                                    <label className="input-heading" htmlFor="exampleInputEmail1">Locality/Area</label>
                                    <input type="text" className="form-control" name="Locality" defaultValue={this.state.Locality} onChange={this.onChange} placeholder="Locality/ Area" />
                                    {LocalityError ? <span style={{ color: 'red' }}> {LocalityError} </span> : null}

                                </div>
                            </div>

                        </div>
                        <div className="col-6">
                            <GoogleMaps coordinates={locationCoordinates} draggable={false} />
                        </div>
                    </div>
                    
                    <div className="row" style={AddressType === 'Choose on Map' ? { display: 'flex' } : { display: 'none' }}>
                        <div className="col-6">
                        <div className="form-row">
                                <div className="form-group col-md-12">
                                    <label className="input-heading" >Please enter your location or click and drag the <i className="fa fa-map-marker" style={{color: 'red', fontSize:25, fontWeight:'bold'}} /> to your desired location</label>
                                    <LocationTextBox fetchLocation={(locArray,name) => this.getCoordinates(locArray,name)} Address={Address} />
                                </div>
                            </div>
                            <div className="form-row">
                                <div className="form-group col-md-6">
                                    <label className="input-heading" htmlFor="exampleInputEmail1">Floor</label>
                                    <input type="text" className="form-control" name="Floor" onChange={this.onChange} placeholder="Floor" />
                                    {FloorError ? <span style={{ color: 'red' }}> {FloorError} </span> : null}

                                </div>
                                <div className="form-group col-md-6">
                                    <label className="input-heading" htmlFor="exampleInputEmail1">Flat</label>
                                    <input type="text" className="form-control" name="Flat" onChange={this.onChange} placeholder="Flat" />
                                    {FlatError ? <span style={{ color: 'red' }}> {FlatError} </span> : null}

                                </div>
                            </div>
                            <div className="form-row">
                                <div className="form-group col-md-12">
                                    <label className="input-heading" htmlFor="exampleInputEmail1">Locality/Area</label>
                                    <input type="text" className="form-control" name="Locality" onChange={this.onChange} placeholder="Locality/ Area" />
                                    {LocalityError ? <span style={{ color: 'red' }}> {LocalityError} </span> : null}
                                </div>
                            </div>
                        </div>
                        <div className="col-6">
                            <GoogleMaps coordinates={locationOnMapCoordinates} draggable={true} getCurrentPosition={(coord) => this.onMarkerDragEnd(coord)} />
                        </div>
                    </div>
                    {/* end address */}
                    <h1>Save Address As</h1>
                    <div className="save-address">
                        <label className="schedule-slot mr-3">
                            <input type="radio" name="Type" checked={this.state.Type === 'Office' ? true : false} onChange={this.onChange} defaultValue="Office" />
                            <div className="sch-slot-slct">
                                <h2><i className="fa fa-suitcase" aria-hidden="true" />Office</h2>
                            </div>
                        </label>
                        <label className="schedule-slot">
                            <input type="radio" name="Type" onChange={this.onChange} checked={this.state.Type === 'Home' ? true : false} defaultValue="Home" />
                            <div className="sch-slot-slct">
                                <h2> <i className="fa fa-home" aria-hidden="true" /> Home</h2>
                            </div>
                        </label>
                        {TypeError ? <span style={{ color: 'red' }}> {TypeError} </span> : null}
                    </div>
                    <div className="form-group">
                        <label className="c-s-checkbox ">Make this is my default address
                        <input type="checkbox" checked={this.state.Default === true ? true : false} name="Default" onChange={this.onChange} />
                            <span className="checkmark" />
                        </label>
                    </div>
                    <div className="row">
                        <div className="col-6">
                            <button type="button" onClick={() => this.cancelEdit()} className="btn btn-signup btn-block rounded-0 mt-4">CANCEL</button>
                        </div>
                        <div className="col-6">
                            <button type="submit" className="btn btn-signup btn-block rounded-0 mt-4">UPDATE ADDRESS</button>
                        </div>
                    </div>
                </form>
            </>
        )
    }
}