import React, {Component} from 'react';

export default class ProductAttributes extends Component{

    createMarkup = (html) => {
        return {
            __html: html
        };
    };
    
    render(){
        const {productDetailsArray} = this.props;
        return(
            <section className="payment-section-wrapper m-0">
                    <div className="payment-inner-wr">
                        <div className="p-d-inner-wr">
                            <div className="list-types" dangerouslySetInnerHTML={this.createMarkup(productDetailsArray.sellerProductId.productDescription)} />
                        </div>
                    </div>
            </section>
        )
    }
}