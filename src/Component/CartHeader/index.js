import React, {Component} from "react";

export default class CartHeader extends Component{
    state = {
        tabname: 'cart'
    }
    componentDidUpdate(prevProps){
      if(this.props.setActiveHeader !== prevProps.setActiveHeader){
        this.setState({
          tabname: this.props.setActiveHeader
        })
      }
    }
    showDetails = (pageType) => {
        this.setState({
            tabname: pageType
        })
        this.props.showTab(pageType);
    }

    render(){
        const {cartArray} = this.props;
        const {tabname} = this.state;
        return(
            <>
            {
                cartArray.length > 0 ?
                <ul className="nav nav-pills mb-3" id="pills-tab" role="tablist">
                  <li className="nav-item" role="presentation">
                    <button className={tabname === 'cart' ? "nav-link active" : "nav-link btn"} id="pills-cart-tab" data-toggle="pill" onClick={() => this.showDetails('cart')} role="tab" aria-controls="pills-cart" aria-selected="true">cart</button>
                    <span className="line-space"><span>-----</span>-----</span>
                  </li>
                  {/* <li className="nav-item" role="presentation">
                    <button className={tabname === 'address' ? "nav-link active" : "nav-link"} id="pills-address-tab" data-toggle="pill" onClick={() => this.showDetails('address')} role="tab" aria-controls="pills-address" aria-selected="false">address </button>
                    <span className="line-space"><span>-----</span>-----</span>
                  </li> */}
                  <li className="nav-item" role="presentation">
                    <button className={tabname === 'delivery' ? "nav-link active" : "nav-link"} id="pills-delivery-tab" data-toggle="pill" onClick={() => this.showDetails('delivery')} role="tab" aria-controls="pills-delivery" aria-selected="false">delivery </button>
                    <span className="line-space"><span>-----</span>-----</span>
                  </li>
                  <li className="nav-item" role="presentation">
                    <button className={tabname === 'payment' ? "nav-link active" : "nav-link"} id="pills-payment-tab" data-toggle="pill" onClick={() => this.showDetails('payment')} role="tab" aria-controls="pills-payment" aria-selected="false">payment </button>
                  </li>
                </ul>
                :
                null
              }
            </>
        )
    }
}