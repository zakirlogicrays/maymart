import React, { useState, useEffect } from 'react';
import back_arrow from "../../assets/images/back-arrow.png";
import user_image from "../../assets/images/user-image.png";
import axios from 'axios';
import firebase from '../../Firebase';
import jwt_decode from 'jwt-decode';

const MessageContact = (props) => {

    const [contacts, setContacts] = useState([])
    const [testState,setTestState] = useState([])
    const ref = firebase.firestore().collection('ChatDetails')

    useEffect(() => {
        getContactList()
    }, [])

    const getContactList = async () => {
        ref.orderBy('CreatedAt', 'desc').onSnapshot(querySnapshot => {
            let ContactData = []
            querySnapshot.docs.map(doc => {
                ContactData.push(doc.data());
                return true;
            })
            getFetch(ContactData)
        })
    }

    const getFetch = async (ContactData) => {
        let checkToken = await localStorage.getItem("token");
        const decoded = jwt_decode(checkToken);
        const userId = decoded.userid
        const Trimdata = ContactData.filter(data => data.ReceiverId === userId || data.SenderId === userId )
        let GetSameContact = groupBy(Trimdata)
        getFinalizeContact(GetSameContact)
    }

    const getFinalizeContact = async (ContactDetails) => {
        let ConDetails = ContactDetails
        let checkToken = await localStorage.getItem("token");
        const decoded = jwt_decode(checkToken);
        const userId = decoded.userid
        ConDetails.map((condata,conindex) => {
            let talkerId;
            if(condata.ReceiverId === userId ){
                talkerId = condata.SenderId
            }else{
                talkerId = condata.ReceiverId
            }
             axios.get(`${window.$URL}user/Details/${talkerId}`)
            .then(response => {
               const object= {
                    talkerFirstname:response.data.Firstname,
                    talkerLastname:response.data.Lastname,
                    image:response.data.profileImageId,
                    message:condata.message,
                    date:condata.CreatedAt,
                    SenderId:condata.SenderId,
                    ReceiverId:condata.ReceiverId
               }

               setTestState(prevState => {
                return [...prevState, object]
              })
            })
            .catch(error => {
                console.log(error.data)
            })
            return true;
        })
        setContacts(ConDetails)
    }

    const groupBy =  (array) => {
        // empty object is the initial value for result object
        let checkToken = localStorage.getItem("token");
        const decoded = jwt_decode(checkToken);
        const userId = decoded.userid
        var selectedArray = []
        var selectedArrayData= []
        array.map((data,index) => {
            if(selectedArray.length === 0){
                if(data['SenderId'] === userId ){
                    selectedArray.push(data['ReceiverId'])
                    selectedArrayData.push(data)
                }else{
                    selectedArray.push(data['SenderId'])
                    selectedArrayData.push(data)
                }
            }else{
                let searchData;
                if(data['SenderId'] === userId ){
                    searchData = data['ReceiverId']
                }else{
                    searchData = data['SenderId']
                }
                const found = selectedArray.indexOf(searchData)
                if(found === -1){
                    if(data['SenderId'] === userId ){
                        selectedArray.push(data['ReceiverId'])
                        selectedArrayData.push(data)
                    }else{
                        selectedArray.push(data['SenderId'])
                        selectedArrayData.push(data)
                    }
                    // selectedArray.push(data[key])
                    // selectedArrayData.push(data)
                }else{
                    selectedArrayData[found] = data
                    selectedArray[found] = searchData
                   
                }
            }
            return true;
        })
        return selectedArrayData;
        // console.log(selectedArray)
        // console.log(selectedArrayData)
      };
    // const getChat = async (data) => {
    //     let checkToken = localStorage.getItem("token");
    //     const decoded = jwt_decode(checkToken);
    //     const userId = decoded.userid
    //     let dataId;
    //     if(data.SenderId === userId){
    //         dataId = data.ReceiverId
    //     }else{
    //         dataId = data.SenderId
    //     }
    //     // window.location.href(`${process.env.PUBLIC_URL}/messagedetails/${dataId}`); 
    //     history.push(`/messagedetails/${dataId}`)
    // }

    return (
       
        <div className="col-md-4 col-12 message-wrapper">
            <div className="product-m-c">
                <div className="manage-header">
                    <h2 className="title-stock">Message</h2>
                </div>

                <div class="message-header">
                    <img src={back_arrow} alt="back-arrow" class="back-arrow" />
                        Chats
                    </div>

                <div class="msg-body">
                    {
                        testState.length > 0
                        ?
                        testState.map((data,conIndex) => {
                            console.log(data)
                            // console.log(data)
                            // console.log(data.talkerFirstname != ""  ? data['talkerFirstname'] : 'asche na')
                            // console.log(data['talkerFirstname'])
                         
                            // let checkToken = localStorage.getItem("token");
                            // const decoded = jwt_decode(checkToken);
                            // const userId = decoded.userid
                            // let dataId;
                            // if(data.SenderId === userId){
                            //     dataId = data.ReceiverId
                            // }else{
                            //     dataId = data.SenderId
                            // }
                            // const Link = `${process.env.PUBLIC_URL}/messagedetails/${dataId}`
                            const Link = ``
                                
                            return(
                                <a href={Link} class="single-msg-item" >
                                    <div class="user-image">
                                        <img src={user_image} class="user-image" alt={user_image} />
                                    </div>
                                    <div class="content-sec">
                                        <h2 class="users_name">{data.talkerFirstname} {data.talkerLastname}</h2>
                                        <h5 class="time">{data.date}</h5>
                                        <p class="message-text">{data.message}</p>
                                    </div>
                                </a>
                            )
                        })
                        :
                        <a href={`${process.env.PUBLIC_URL}/messageDetails/604f734c92ddac069420edb9`} class="single-msg-item">
                            <div class="content-sec">
                                <h2 class="users_name" style={{textAlign:'center'}}> Empty Message List</h2>
                            </div>
                        </a>
                    }
                </div>
            </div>
        </div>
        
    )

}

export default MessageContact;