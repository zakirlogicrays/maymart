import React, { Component } from "react";
import bestPriceImage from "../../assets/images/best-pricetag.png";
import jwt_decode from "jwt-decode";
import { connect } from "react-redux";
import deliveryBox from "../../assets/images/delivery-box.png";
import checkMark from "../../assets/images/check-mark.png";
import Loader from "../Loader";

function calculateDistance(lat1, lon1, lat2, lon2, unit) {
  var radlat1 = (Math.PI * lat1) / 180;
  var radlat2 = (Math.PI * lat2) / 180;
  var theta = lon1 - lon2;
  var radtheta = (Math.PI * theta) / 180;
  var dist =
    Math.sin(radlat1) * Math.sin(radlat2) +
    Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
  dist = Math.acos(dist);
  dist = (dist * 180) / Math.PI;
  dist = dist * 60 * 1.1515;
  if (unit === "K") {
    dist = dist * 1.609344;
  }
  if (unit === "N") {
    dist = dist * 0.8684;
  }
  return dist;
}

// function getDistance(lat1, lon1, lat2, lon2){
//   const prevLatInRad = toRad(lat1);
//   const prevLongInRad = toRad(lon1);
//   const latInRad = toRad(lat2);
//   const longInRad = toRad(lon2);
//   return (
//     // In kilometers
//     6377.830272 *
//     Math.acos(
//       Math.sin(prevLatInRad) * Math.sin(latInRad) +
//         Math.cos(prevLatInRad) * Math.cos(latInRad) * Math.cos(longInRad - prevLongInRad),
//     )
//   );

// }

// function toRad(angle) {
//   return (angle * Math.PI) / 180;
// }

function calculateDeliveryPrice(deliveryRule, distance) {
  var deliveryPrice = 0;
  // if(deliveryRule.length > 0){
  //   for(var i= 0; i<deliveryRule.length;){
  //       co
  //   }
  // }
}

class CartDeliveryProducts extends Component {
  // state = {
  //     showItemTag: 'all',
  //     CartData: this.props.cartArray
  //   }
  constructor(props) {
    super(props);

    this.state = {
      showItemTag: "all",
      CartData: this.props.cartArray,
      selectSellerTerms: false,
      deliveryTime: [],
      totalPrice: 0,
      shippingType: [],
      shiipingIndex: [],
      loading: true,
    };
    // this.calculateShipping(this.props.cartArray)
  }
  state = this.state;

  async componentDidMount() { }

  async componentDidUpdate(prevProps) {
    if (this.props.selectedProduct !== prevProps.selectedProduct) {
      this.componentDidMount();
      if (this.props.selectedProduct.length > 0) {
        this.setState({
          loading: false
        })
      }
    }
  }

  fetchProductImage = (productArray) => {
    var imageData;
    if (
      productArray.primaryProductImage !== null &&
      productArray.primaryProductImage !== undefined
    ) {
      imageData =
        `${window.$ImageURL}` + productArray.primaryProductImage.originalURL;
    } else if (productArray.productImageId.length > 0) {
      imageData =
        `${window.$ImageURL}` + productArray.productImageId[0].originalURL;
    } else {
      imageData = `${window.$ImageURL}no-image-480x480.png`;
    }
    return imageData;
  };

  toggleSelectProductVariant = (e, cartProductArray) => {
    this.props.productVariantSelect(e, cartProductArray);
  };

  toggleSelectProductIndividualVariant = (e, eachVariant) => {
    this.props.productVariantIndividualSelect(e, eachVariant);
  };

  selectAllProd = (e) => {
    this.props.allSelect(e);
  };

  deleteCart = (cartId, index) => {
    this.props.deleteCartData(cartId, index);
  };

  quantityAdd = (cartId, index) => {
    this.props.addQuantity(cartId, index);
  };

  quantitySubtract = (cartId, index) => {
    this.props.subtractQuantity(cartId, index);
  };

  showItems = (productType) => {
    this.setState({
      showItemTag: productType,
    });
    this.props.showProductFilter(productType);
  };

  getDeliveryAmount = (variantData) => {
    return (
      <>
        <span></span>
      </>
    );
  };
  calculateShipping = (selectedProduct) => {
    const { Location } = this.props;
    var slotSelected = [];
    var deliveryData = [];
    var shippingType = [];
    var shiipingIndex = []
    var totalPrice = 0;

    for (let i = 0; i < selectedProduct.length; i++) {
      deliveryData.push('')
      shiipingIndex.push('')
      deliveryData[i] = []
    }
    if (selectedProduct.length > 0) {
      for (let i = 0; i < selectedProduct.length; i++) {
        var distance = calculateDistance(
          Location.locationCoordinates[0].coordinates.lat,
          Location.locationCoordinates[0].coordinates.lng,
          selectedProduct[i].sellerIDArray[0].warehouseAddressID.Location
            .coordinates[1],
          selectedProduct[i].sellerIDArray[0].warehouseAddressID.Location
            .coordinates[0],
          "K"
        );

        var initialDistance = 0;
        if (selectedProduct[i].sellerIDArray[0].deliveryRule.length > 0) {
          for (var j = 0; j < selectedProduct[i].sellerIDArray[0].deliveryRule.length; j++) {
            console.log(selectedProduct[i].sellerIDArray[0].deliveryRule);
            const deliverydistance = selectedProduct[i].sellerIDArray[0].deliveryRule[j].distance;
            // const distanceinmtr = Math.round(distance * 1000);
            const distanceinmtr = Math.round(distance);
            console.log('initial distance ', i, initialDistance)
            console.log('delivery distance ', i, selectedProduct[i].sellerIDArray[0].deliveryRule[j].distance)
            console.log('distance meter ', i, distanceinmtr)
            if (distanceinmtr > initialDistance || distanceinmtr <= deliverydistance) {
              deliveryData[i].push({
                minHoursDelivery: selectedProduct[i].sellerIDArray[0].deliveryRule[j].minHoursDelivery,
                maxHoursDelivery: selectedProduct[i].sellerIDArray[0].deliveryRule[j].maxHoursDelivery,
                minDaysDelivery: selectedProduct[i].sellerIDArray[0].deliveryRule[j].minDaysDelivery,
                maxDaysDelivery: selectedProduct[i].sellerIDArray[0].deliveryRule[j].maxDaysDelivery,
                deliveryCharge: selectedProduct[i].sellerIDArray[0].deliveryRule[j].deliveryCharge,
                id: selectedProduct[i].sellerIDArray[0].deliveryRule[j]._id
              });
              totalPrice += parseFloat(selectedProduct[i].sellerIDArray[0].deliveryRule[j].deliveryCharge);
              shippingType.push("seller");
            }
            // if (j ===selectedProduct[i].sellerIDArray[0].deliveryRule.length - 1) {
            //   deliveryData[i].push({ 
            //     minHoursDelivery:selectedProduct[i].sellerIDArray[0].deliveryRule[j].minHoursDelivery,
            //     maxHoursDelivery:selectedProduct[i].sellerIDArray[0].deliveryRule[j].maxHoursDelivery,
            //     minDaysDelivery:selectedProduct[i].sellerIDArray[0].deliveryRule[j].minDaysDelivery,
            //     maxDaysDelivery:selectedProduct[i].sellerIDArray[0].deliveryRule[j].maxDaysDelivery,
            //     deliveryCharge:selectedProduct[i].sellerIDArray[0].deliveryRule[j].deliveryCharge,
            //     id:selectedProduct[i].sellerIDArray[0].deliveryRule[j]._id
            //   });
            //   deliveryData[i].push({
            //     minHoursDelivery:1,
            //     maxHoursDelivery:3,
            //     minDaysDelivery:1,
            //     maxDaysDelivery:2,
            //     deliveryCharge:30,
            //     id:[i]
            //   });
            //   shippingType.push("seller");
            //   totalPrice += parseFloat(selectedProduct[i].sellerIDArray[0].deliveryRule[j].deliveryCharge);
            // }
            initialDistance = parseFloat(selectedProduct[i].sellerIDArray[0].deliveryRule[j].distance) + 1;
          }
        }
      }
    }
    console.log("totalPrice", totalPrice);
    slotSelected = {
      from: "",
      to: "",
      date: "",
      concatSlotFrom: "",
      concatSlotTo: "",
      shippingFee: totalPrice,
    };

    console.log("delivery data", deliveryData);
    if (slotSelected) {
      // this.props.selectedTimeSlot(slotSelected);
      // this.props.getanotherType(deliveryData, shippingType);
    }
    this.setState({ selectSellerTerms: true, deliveryTime: deliveryData, shippingType: shippingType, shiipingIndex: shiipingIndex });
  };

  getPrice = (data, index) => {
    const { totalPrice, deliveryTime, shippingType, shiipingIndex } = this.state;
    // console.log('shipping index',shiipingIndex[index])
    var newtotalPrice;
    if (shiipingIndex[index] === '') {
      shiipingIndex[index] = data.id
      newtotalPrice = parseFloat(totalPrice) + parseFloat(data.deliveryCharge)
    } else {
      if (shiipingIndex[index] !== data.id) {
        const getVal = deliveryTime[index].filter(data => data.id === shiipingIndex[index])
        const prevMinusPrice = parseFloat(totalPrice) - parseFloat(getVal[0].deliveryCharge)
        newtotalPrice = parseFloat(prevMinusPrice) + parseFloat(data.deliveryCharge)
        shiipingIndex[index] = data.id
      } else {
        newtotalPrice = totalPrice
      }
    }
    const slotSelected = {
      from: "",
      to: "",
      date: "",
      concatSlotFrom: "",
      concatSlotTo: "",
      shippingFee: newtotalPrice,
    };
    this.setState({
      totalPrice: newtotalPrice,
      shiipingIndex
    })
    this.props.selectedTimeSlot(slotSelected);
    this.props.getanotherType(shiipingIndex, shippingType);

  }

  render() {
    const { cartArray, totalCartArray, selectedProduct } = this.props;
    const { showItemTag, selectSellerTerms, shiipingIndex, loading } = this.state;
    console.log("selectd Product another", selectedProduct);
    const formatAMPM = (date) => {
      var hours = date.getHours();
      var minutes = date.getMinutes();
      var ampm = hours >= 12 ? "pm" : "am";
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? "0" + minutes : minutes;
      const object = {
        time: hours,
        slot: ampm,
      };
      return object;
    };
    const getTimeIndex = (timedata) => {
      let timestamp = Date.now();
      const getAMPM = formatAMPM(new Date());

      if (getAMPM.slot === "pm" && getAMPM.time > 6) {
        const maximumHours = timedata.maxHoursDelivery;
        const maximumDays = timedata.minDaysDelivery * 24;
        const totalMaxDays =
          parseFloat(maximumHours) + parseFloat(maximumDays) + parseFloat(14);
        const maxtime =
          parseFloat(timestamp) + parseFloat(totalMaxDays * 60 * 60 * 1000);

        const minimumHours = timedata.minHoursDelivery;
        const minimumDays = timedata.minDaysDelivery * 24;
        const totalMinDays =
          parseFloat(minimumHours) + parseFloat(minimumDays) + parseFloat(14);
        const mintime =
          parseFloat(timestamp) + parseFloat(totalMinDays * 60 * 60 * 1000);

        // const maximumTime = (parseFloat(timestamp) + parseFloat((1* 4 * 60 * 60 * 1000)));
        // const totaldate = (parseFloat(timestamp) + parseFloat((48 * 60 * 60 * 1000)));
        // let date = new Intl.DateTimeFormat('en-US', { year: 'numeric', month: '2-digit', day: '2-digit', hour: '2-digit', minute: '2-digit' }).format(timestamp)
        let maximum = new Intl.DateTimeFormat("en-US", {
          month: 'long',
          day: '2-digit',
          year: 'numeric',
        }).format(maxtime);
        let minimum = new Intl.DateTimeFormat("en-US", {
          month: 'long',
          day: '2-digit',
          year: 'numeric',
        }).format(mintime);
        const totalTime = minimum;
        // console.log('maximum',maximum)
        // console.log('another',total)
        return totalTime;
      } else {
        const maximumHours = timedata.maxHoursDelivery;
        const maximumDays = timedata.minDaysDelivery * 24;
        const totalMaxDays = parseFloat(maximumHours) + parseFloat(maximumDays);
        const maxtime =
          parseFloat(timestamp) + parseFloat(totalMaxDays * 60 * 60 * 1000);

        const minimumHours = timedata.minHoursDelivery;
        const minimumDays = timedata.minDaysDelivery * 24;
        const totalMinDays = parseFloat(minimumHours) + parseFloat(minimumDays);
        const mintime =
          parseFloat(timestamp) + parseFloat(totalMinDays * 60 * 60 * 1000);

        // const maximumTime = (parseFloat(timestamp) + parseFloat((1* 4 * 60 * 60 * 1000)));
        // const totaldate = (parseFloat(timestamp) + parseFloat((48 * 60 * 60 * 1000)));
        // let date = new Intl.DateTimeFormat('en-US', { year: 'numeric', month: '2-digit', day: '2-digit', hour: '2-digit', minute: '2-digit' }).format(timestamp)
        let maximum = new Intl.DateTimeFormat("en-US", {
          month: 'long',
          day: '2-digit',
          year: 'numeric',
        }).format(maxtime);
        let minimum = new Intl.DateTimeFormat("en-US", {
          month: 'long',
          day: '2-digit',
          year: 'numeric',
        }).format(mintime);
        const totalTime = minimum;
        // console.log('maximum',maximum)
        // console.log('another',total)
        return totalTime;
      }
    };

    const getIndex = (index) => {
      const finalindex = parseFloat(index) + 1
      return (
        finalindex
      )
    }
    if (loading) {
      return (
        <Loader />
      )
    } else {
      return (
        <div className="col-md-12 ">
          {selectedProduct.length > 0 ? (
            <div className="cart-card">
              <div className="delete-wrapper">
                <div className="chkbx">
                  <label className="c-s-checkbox ">
                    <input
                      type="checkbox"
                      onChange={() => this.calculateShipping(selectedProduct)}
                      checked={selectSellerTerms ? true : false}
                    />
                    <span className="checkmark" />
                  </label>
                  <span className="all-check">
                    Select To Approve Seller Terms
                  </span>
                </div>
              </div>
            </div>
          ) : null}

          {selectedProduct.length > 0 ? (
            selectedProduct.map((data, index) => {
              return (
                <div className="cart-card si-newInner" key={index}>
                  <div className="pack-details">
                    <div className="pack-with-image">
                      <img src={deliveryBox} className="img-fluid" />
                      <h4 class="pack-head">Package {getIndex(index)} of {selectedProduct.length}</h4>
                    </div>
                    <p class="paked-by">Shipped by {data.sellerIDArray[0].SellerDetails.shopName}</p>
                  </div>
                  <div className="inner-padding">
                    <p class="choose-delivery-opt">
                      Choose your delivery option
                    </p>
                    {
                      selectSellerTerms ?
                        <div class="row">
                          {
                            this.state.deliveryTime[index].map((delData, delIndex) => {
                              return (
                                <div class="col-6 equal">
                                  <label class="schedule-slot">
                                    <input type="radio" name={`test_${index}`} value={delData.id} checked={delData.id === shiipingIndex[index] ? true : false} onClick={() => this.getPrice(delData, index)} />

                                    <div class="sch-slot-slct">
                                      <h2 className="modified-price"><img src={checkMark} /> {delData.deliveryCharge === 0 ? 'free' : `QAR ${delData.deliveryCharge}`}</h2>
                                      <h3 className="hd">Home Delivery</h3>
                                      <p>Est, Arrival : {getTimeIndex(delData)}</p>
                                    </div>
                                  </label>
                                </div>
                              )
                            })
                          }

                          {/* <div class="col-lg-6 col-md-6 col-sm-12 equal">
                        <label class="schedule-slot">
                          <input type="radio" name="test" value="big" />

                          <div class="sch-slot-slct">
                          <h2 className="modified-price"><img src={checkMark}/> free</h2>
                            <h3 className="hd">Delivery</h3>
                            <p>Est, Arrival : 26 -29 Sep</p>
                          </div>
                        </label>
                      </div>  */}
                        </div>
                        : null
                    }


                    <div className="delete-wrapper header">
                      <div className="chkbx add-4-item">
                        <button className="btn">
                          {data.sellerIDArray[0].SellerDetails.shopName}
                        </button>
                      </div>
                      {/* {selectSellerTerms ? (
                      <button className="btn btn-dlt">
                        Delivery Time <br />
                        <span>
                          {getTimeIndex(this.state.deliveryTime[index])}
                        </span>
                      </button>
                    ) : null} */}
                    </div>
                    {data.productVariantArray.length > 0 ||
                      data.productVariantArray === null
                      ? data.productVariantArray.map((eachVariant, eachVariantIndex) => {
                        return (
                          // <>
                          // </>
                          <div
                            className="row content-main"
                            key={eachVariantIndex}
                          >
                            <div className="col-lg-7 col-md-12">
                              <div className="more-offer">
                                {this.getDeliveryAmount(eachVariant)}
                              </div>
                              <div className="img-section">
                                <div className="image-content">
                                  <img
                                    src={this.fetchProductImage(
                                      data.productId[eachVariantIndex]
                                    )}
                                    className="img-fluid"
                                    alt="productImage"
                                  />
                                </div>
                                <div className="content-wrapper">
                                  <h2 className="title">
                                    {
                                      data.productId[eachVariantIndex]
                                        .productName
                                    }{" "}
                                  </h2>
                                  <p className="fresh-fruit">
                                    {data.productId[
                                      eachVariantIndex
                                    ].category.map((eachCat, eachIndex) => {
                                      return (
                                        <a
                                          href={`${process.env.PUBLIC_URL}/productListing/category/${eachCat}`}
                                          key={eachIndex}
                                        >
                                          {" "}
                                          {eachCat}{" "}
                                          {eachIndex !==
                                            data.productId[eachVariantIndex]
                                              .category.length -
                                            1
                                            ? " > "
                                            : null}
                                        </a>
                                      );
                                    })}
                                  </p>
                                  {eachVariant && eachVariant.manageStock === "true" ? (
                                    <p className="item-left">
                                      Only {eachVariant.currentInventory}{" "}
                                      item(s)in stock
                                    </p>
                                  ) : null}
                                </div>
                              </div>
                            </div>
                            <div className="col-lg-3">
                              <div className="price-mark">
                                <p className="price">
                                  qar{" "}
                                  {eachVariant && eachVariant.specialPriceProvided === "true"
                                    ? parseFloat(
                                      eachVariant.specialPrice
                                    ).toFixed(2)
                                    : parseFloat(eachVariant.Price).toFixed(
                                      2
                                    )}
                                </p>
                                {eachVariant && eachVariant.specialPriceProvided ===
                                  "true" ? (
                                  <>
                                    <p className="price cutoff-price">
                                      qar{" "}
                                      {parseFloat(eachVariant.Price).toFixed(
                                        2
                                      )}
                                    </p>
                                    <span className="less-percent">
                                      -
                                      {parseFloat(
                                        eachVariant.specialPriceDiscount
                                      ).toFixed(2)}
                                      %
                                    </span>
                                  </>
                                ) : null}
                              </div>
                            </div>
                            <div className="col-lg-2">
                              <div className="quantity-box">
                                {/* <p>Earliest Time : <span>28 Sep</span></p> */}
                                <div className="incrementbox">
                                  {/* <button className="btn action" onClick={() => this.quantitySubtract(data._id, eachVariantIndex)}><span>-</span></button> */}
                                  <span className="result">
                                    {data.quantity[eachVariantIndex]}
                                  </span>
                                  {/* <button className="btn action" onClick={() => this.quantityAdd(data._id, eachVariantIndex)}><span>+</span></button> */}
                                </div>
                              </div>
                            </div>
                          </div>
                        );
                      }
                      )
                      : null}
                  </div>
                </div>
              );
            })
          ) : (
            <div className="cart-card">
              <div className="delete-wrapper">
                <h2 className="title">Your Cart is Empty </h2>
              </div>
            </div>
          )}
        </div>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  Location: state.Location,
});

export default connect(mapStateToProps, {})(CartDeliveryProducts);
