import React, { Component } from 'react';
import axios from "axios";
import { connect } from 'react-redux';
import { AddToLocation } from '../../data/reducers/location';

import ArrowLeft from '../../assets/images/Arrow - Left.png';
import SearchForMobile from '../../assets/images/Search-mobile.png';
import ClearSearch from '../../assets/images/search-clear.png';

import searchListIcon1 from '../../assets/images/search-list-icon1.png';
import searchListIcon2 from '../../assets/images/search-list-icon2.png';
import searchListIcon3 from '../../assets/images/search-list-icon3.png';
import searchListIcon4 from '../../assets/images/search-list-icon4.png';


class SearchModalForMobile extends Component {
    state = {
        searchedArray: [],
        searchKeyword:'',
        brandFound: false,
        storeFound: false,
        searchValue: undefined
    }

    searchKey = async (e) => {
        const { value } = e.target;
        this.setState({
          showList: true,
          searchValue: value
        })
        if (value.length > 0 && this.state.showList) {
          await axios.get(`https://mymart.qa:7775/api/search/searchKeyword/${value}`)
            .then(response => {
              if (response.status === 200) {
                this.setState({
                  searchedArray: response.data,
                  searchKeyword: value
                })
              }
            })
            .catch(error => {
              console.log(error);
            })
            this.setState({brandFound: this.state?.searchedArray?.filter((item) => (item.type === "Brand" && true))})
            this.setState({storeFound: this.state?.searchedArray?.filter((item) => (item.type === "Store" && true))})
        } else {
          this.setState({
            searchedArray: [],
          })
        }
      }

      getHighlightedText(name) {
        const { searchKeyword } = this.state;
        if (searchKeyword.length > 1) {
          const parts = name?.split(new RegExp(`(${searchKeyword})`, 'gi'));
          return <span>{parts?.map(part => part.toLowerCase() === searchKeyword.toLowerCase() ? <b>{part}</b> : part)}</span>;
        } else {
          return (
            name
          )
        }
      }

      handleSearchClicked = (data) => {
        console.log("here ----->", data);
        const bodyData = {userId:"5f173552743dec5203b612c5", keyword:data._id}
        axios.post('https://mymart.qa:7775/api/product/addSearchKeyword', bodyData)
            .then(response => {
              this.setState({
                searchedArray: [],
                showList: false
              })
            });
      }

    render() {
        const { searchedArray } = this.state
        const { modalStatus } = this.props;
        return (
            <div className={modalStatus ? "modal fade show search-modal-mobile" : "modal fade search-modal-mobile"} style={modalStatus ? { display: 'block' } : { display: 'none' }} id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered modal-lg search-popup-mobile" role="document">
                    <div className="modal-content">
                        {
                            window.location.pathname.split("/")[4] === 'grocery' ?
                                null
                                :
                                <div className="modal-header">

                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={() => this.props.hideLocationModal()}>
                                        <img src={ArrowLeft} alt={ArrowLeft} />
                                    </button>
                                    <div className="search-for-mobile">
                                        <div className="form-group">
                                            <input 
                                               autoFocus={true}
                                               type="text" 
                                               placeholder={`Search in ${this.props.rendomeName}`} 
                                               className="search-input" 
                                               onChange={(val) => this.searchKey(val)}
                                               value={this.state.searchValue}
                                            />
                                            <button type="reset" className="clear" onClick={() => this.setState({searchValue: "", searchedArray: []})}> <img src={ClearSearch} alt={ClearSearch} /> </button>
                                            <button type="submit" className="btn"> <img src={SearchForMobile} alt={SearchForMobile} /> </button>
                                        </div>
                                    </div>
                                </div>
                        }
                        <div className="mobile-search-results">
                            <ul className="search-suggestion">
                                {searchedArray?.slice(0,5)?.map((item) => (
                                    <li onClick={() => {
                                         this.handleSearchClicked(item)
                                         window.location.href = `${process.env.PUBLIC_URL}/productListing/category/${item._id}`
                                        }}>
                                        <div className="search-suggestion-list-box">
                                            <img className="img1" src={searchListIcon1} alt={searchListIcon1} />
                                            <div className="searched-text">
                                                <p>{this.getHighlightedText(item?.name?.split("-")[0])}</p>
                                                <span>{item.category !== undefined && item.category !== null ? ` in ${item.category.split(",")[1]}` : null}</span>
                                            </div>
                                            <img className="img2" src={searchListIcon2} alt={searchListIcon2} />
                                        </div>
                                    </li>
                                ))}
                                {this.state.searchedArray.length > 0 && <div className="see-all-product-results">
                                    <a className="btn">See All Product Results  <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                </div>}
                            </ul>
                            <ul className="search-suggestion">
                            {searchedArray?.map((data, index) => {
                                return(
                                    <div>
                                        {data.type === "Brand" && <li onClick={() => {
                                            this.handleSearchClicked(data)
                                            window.location.href = `${process.env.PUBLIC_URL}/productListing/brand/${data._id}`
                                        }}>
                                            <div className="search-suggestion-list-box">
                                            {data?.brandImage?.originalURL && <img src={`${"https://mymart.qa:7779/uploads/"}` + data?.brandImage?.originalURL} alt={""} style={{ width: 25 }} />}
                                                <div className="searched-text">
                                                    {data?.brandImage?.originalURL && <a style={{ color: "#5A5A5A" }}> <strong>{data?.name?.split("-")[0]}</strong> </a>}
                                                </div>
                                            </div>
                                        </li>}
                                    </div>
                            )})}
 
                                {(this.state?.brandFound.length > 0 && this.state.searchedArray.length > 0)  && <div className="see-all-product-results">
                                    <a className="btn">See All Store Results  <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                </div>}
                            </ul>
                            <ul className="search-suggestion">
                            {searchedArray?.map((data, index) => {
                                return(
                                    <div>
                                        {data.type === "Store" && <li onClick={() => {
                                            window.location.href = `${process.env.PUBLIC_URL}/storeDetails/${data._id}`
                                            this.handleSearchClicked(data)
                                        }}>
                                            <div className="search-suggestion-list-box">
                                            {data?.sellerImage?.originalURL && <img src={`${"https://mymart.qa:7779/uploads/"}` + data?.sellerImage?.originalURL} alt={""} style={{ width: 25 }} />}
                                                <div className="searched-text">
                                                    {data?.sellerImage?.originalURL && <a style={{ color: "#5A5A5A" }}> <strong>{data?.name}</strong> </a>}
                                                </div>
                                            </div>
                                        </li>}
                                    </div>
                            )})}
                                
                                {(this.state.storeFound.length > 0 && this.state.searchedArray.length > 0 ) && <div className="see-all-product-results">
                                    <a className="btn">See All Store Results  <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                </div>}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    location: state.Location,
})

export default connect(
    mapStateToProps,
    { AddToLocation }
)(SearchModalForMobile);