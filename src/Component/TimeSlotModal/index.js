import React, { Component } from "react";
import MultiCarousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import TimeSlot from "../TimeSlot";
import axios from "axios";
import orangesImage from "../../assets/images/oranges.png";
var moment = require('moment-timezone');

const responsive = {
    superLargeDesktop: {
        // the naming can be any, depends on you.
        breakpoint: { max: 4000, min: 3000 },
        items: 3
    },
    desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 3
    },
    tablet: {
        breakpoint: { max: 1024, min: 464 },
        items: 2
    },
    mobile: {
        breakpoint: { max: 464, min: 0 },
        items: 2
    }
};


export default class TimeSlotModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            timeSlotShow: false,
            deliveryRule: this.props.deliveryRule,
            deliveryRuleSelected: this.props.deliveryRuleSelected,
            timeSlot: this.props.timeSlot,
            deliveryFromMyMart: "yes",
            showTimeSlotModal: false,
            deliverySlot: [],
            slotSelected: null,
        }
        //...
    }
    state = this.state
    // state = {
    //     timeSlotShow: false,
    //     deliveryRule: this.props.deliveryRule,
    //     deliveryRuleSelected: this.props.deliveryRuleSelected,
    //     timeSlot: this.props.timeSlot,
    //     deliveryFromMyMart: "yes",
    //     showTimeSlotModal:false

    //   };

    async componentDidMount() {
        this.renderTimeSlot();
    }

    renderTimeSlot = () => {
        const { timeSlot, deliveryRuleSelected } = this.props;
        const DATE_FORMAT_AVLBLTY = 'ddd DD MMM YYYY';
        const TIME_ZONE = 'Singapore';
        const TODAY = moment();
        const ONE = 1;
        const END_DAY = deliveryRuleSelected.deliverySlotDays;
        const DAYS = 'days';
        const END_DAYS_IN_FUTURE = moment().add(END_DAY, DAYS);
        const TIME_SLOT_DIFF = timeSlot;
        let deliverySlot = [];

        for (const temp = moment(TODAY); temp.isBefore(END_DAYS_IN_FUTURE); temp.add(ONE, DAYS)) {
            deliverySlot.push({
                date: temp.tz(TIME_ZONE).format(DATE_FORMAT_AVLBLTY),
                slot: this.getTimeStops('11:00', '00:00', TIME_SLOT_DIFF),
                shippingFee: deliveryRuleSelected.deliveryCharge
            });
        }
        this.setState({
            deliverySlot
        })
    }

    getTimeStops(start, end, splitHour) {
        var startTime = moment(start, 'hh:mm');
        var endTime = moment(end, 'hh:mm');

        if (endTime.isBefore(startTime)) {
            endTime.add(1, 'day');
        }

        var timeStops = [];

        while (startTime <= endTime) {
            timeStops.push(new moment(startTime).format('hh A'));
            startTime.add(splitHour, 'hours');
        }
        return timeStops;
    }

    selectedSlot = (slotStart, slotEnd, dateSelected, shippingFee) => {

        let concatSlotFrom = moment(moment(dateSelected).format('YYYY-MM-DD') + ' ' + moment(slotStart, ["hh A"]).format('HH:mm:ss')).unix();
        let concatSlotTo = moment(moment(dateSelected).format('YYYY-MM-DD') + ' ' + moment(slotEnd, ["hh A"]).format('HH:mm:ss')).unix();

        this.setState({
            slotSelected: {
                from: slotStart,
                to: slotEnd,
                date: dateSelected,
                concatSlotFrom: concatSlotFrom,
                concatSlotTo: concatSlotTo,
                shippingFee: shippingFee
            }
        }, () => {
            this.timeSlotSelected();
        })
    }

    timeSlotSelected = () => {
        this.props.selectedTimeSlot(this.state.slotSelected);
    }

      timeSlot = (slot, index) => {
       this.props.getTimeSlot(slot,index)
      };
    render() {

        const { deliverySlot, slotSelected } = this.state;
        console.log('delivery slot',deliverySlot)
        const { modalStatus, timeSlot, deliveryRuleSelected, deliveryRule } = this.props;
        alert(timeSlot)
        return (
            <div className={modalStatus ? "modal fade show" : "modal fade "} style={modalStatus ? { display: 'block' } : { display: 'none' }} id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered modal-lg">
                    <div className="modal-content ">
                        <div className="modal-body modalHeight">
                            <p className="choose-delivery-opt">Choose your delivery option</p>
                            <div className="row">
                                {deliveryRule.map((data, index) => {
                                    return (
                                        <div
                                            className="col-lg-4 col-md-4 col-sm-12 equal"
                                            key={index}
                                        >
                                            <label className="schedule-slot">
                                                <input
                                                    type="radio"
                                                    name="test"
                                                    value={data.timmeSlot}
                                                    checked={data.timeSlot === timeSlot ? true : false}
                                                    onChange={() => this.timeSlot(data.timeSlot, index)}
                                                />
                                                <div className="sch-slot-slct">
                                                    <h2>{`QAR ${data.deliveryCharge}`}</h2>
                                                    <h3>Scheduled ({data.timeSlot}-hour slots) </h3>
                                                    <p>Select your preferred slot</p>
                                                </div>
                                            </label>
                                        </div>
                                    );
                                })}
                            </div>
                            <div className="slot-main">

                                <MultiCarousel
                                    swipeable={false}
                                    draggable={false}
                                    showDots={false}
                                    responsive={responsive}
                                    ssr={true} // means to render carousel on server-side.
                                    infinite={false}
                                    autoPlaySpeed={1000}
                                    keyBoardControl={true}
                                    customTransition="all .5"
                                    transitionDuration={500}
                                    containerClass="carousel-container"
                                    itemClass="carousel-item-padding-40-px"
                                >
                                    {
                                        deliverySlot.map((data, index) => {
                                            return (
                                                <div className="slot-wrapper" key={index}>
                                                    <p className="slot-date"> {data.date} </p>
                                                    {
                                                        data.slot.map((eachSlot, eachIndex) => {
                                                            if (data.slot[eachIndex + 1] !== undefined) {
                                                                return (
                                                                    <button className={slotSelected && slotSelected.from === eachSlot && slotSelected.date === data.date ? "btn slotbtn available-slot booked-slot" : "btn slotbtn available-slot"} key={eachIndex} onClick={() => this.selectedSlot(eachSlot, data.slot[eachIndex + 1], data.date, data.shippingFee)}>{eachSlot} - {data.slot[eachIndex + 1]}
                                                                        {/*<span className="fill-fast">
                                                            <p>+$12</p>
                                                            <span className="grey-text"><i className="fa fa-angle-double-up" aria-hidden="true" /> Filling Fast</span>
                                                        </span>*/}
                                                                    </button>
                                                                )
                                                            } else {
                                                                return null;
                                                            }
                                                        })
                                                    }
                                                </div>
                                            )
                                        })
                                    }
                                </MultiCarousel>
                            </div>
                            {/* <TimeSlot
                            deliveryRule={deliveryRule}
                            deliveryRuleSelected={deliveryRuleSelected}
                            timeSlot={timeSlot}
                            selectedTimeSlot={(slot) => this.timeSlotSelected(slot)}
                        /> */}
                        </div>
                        <div className="modal-footer">
                            <button type="button" onClick={this.saveLocation} className="btn btn-signup pull-right rounded-0">Lets Go</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}