import React from "react";
import PropTypes from "prop-types";
import { Route, Redirect } from "react-router-dom";
import jwt_decode from 'jwt-decode';

import DefaultLayout from "../_layouts/default";
//import AuthLayout from "../_layouts/auth";

export default function RouteWrapper({
  component: Component,
  isPrivate,
  ...rest
}) {
  
  let checkToken = localStorage.getItem("token");
  let signed;
  if(checkToken){
    let decoded = jwt_decode(checkToken);
    signed = decoded.userType === 'customer' ? true : false;
  }else{
    signed = false;
  }
  /**
   * Redirect user to SignIn page if he tries to access a private route
   * without authentication.
   */

  if (isPrivate && !signed) {
    return <Redirect to={`${process.env.PUBLIC_URL}/login`} />;
  }

  /**
   * Redirect user to Main page if he tries to access a non private route
   * (SignIn or SignUp) after being authenticated.
   */
  /*if (!isPrivate && signed) {
    return <Redirect to={`${process.env.PUBLIC_URL}/`} />;
  }*/

  const Layout = DefaultLayout;

  /**
   * If not included on both previous cases, redirect user to the desired route.
   */
  return (
    <Route
      {...rest}
      render={props => (
        <Layout>
          <Component {...props} />
        </Layout>
      )}
    />
  );
}

RouteWrapper.propTypes = {
  isPrivate: PropTypes.bool,
  component: PropTypes.oneOfType([PropTypes.element, PropTypes.func]).isRequired
};

RouteWrapper.defaultProps = {
  isPrivate: false
};
