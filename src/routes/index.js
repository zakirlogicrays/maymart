import React from "react";
import { Switch } from "react-router-dom";
import Route from "./Route";

import Homepage from '../pages/Homepage';
import Groceries from '../pages/Groceries';
import HomepageCategoryWise from '../pages/HomepageCategoryWise';
import Login from '../pages/Login';
import Register from '../pages/Register';
import Productlisting from '../pages/product-listing';
import ProductDetails from '../pages/ProductDetails';

import StoreDetails from '../pages/StoreDetails';
import ForgotPassword from '../pages/Forgot-Password';
import ResetPassword from '../pages/Reset-Password';
import MyCart from "../pages/MyCart";
/*import Cart from '../pages/Cart';
import CartAddress from '../pages/CartAddress';
import CartPayment from '../pages/CartPayment';*/

import Checkout from '../pages/Checkout';
import Myorder from '../pages/MyOrder';
import MyProfile from '../pages/MyProfile';
import MyWishList from '../pages/MyWishList'; 
import MobileVerification from '../pages/MobileVerification'; 
import EmailVerification from '../pages/EmailVerfication';
import UserAddress from '../pages/UserAddress';
import OrderDetails from '../pages/OrderDetails';
import AboutUs from '../pages/AboutUs';
import PrivayPolicy from '../pages/PrivacyPolicy';
import termsAndCondition from '../pages/TermsAndCondition';
import SellerFaq from '../pages/SellerFaq';
import CustomerFaq from '../pages/CustomerFaq';
import ContactUs from '../pages/ContactUs';
import LegalNotice from '../pages/LegalNotice';
import Categories from '../pages/Categories';
import MessageDetails from '../pages/MessageDetails';
import Vouchers from '../pages/Vouchers';
import AppliedVouchers from '../pages/AppliedVouchers';
import ChangePassword from '../pages/ChangePassword';

import EmailVerify from '../pages/Email-Verify';
import ShippingAndDelivery from '../pages/ShippingAndDelivery'; 
import ReturnsAndRefunds from '../pages/ReturnsAndRefunds'; 
import PaymentMethods from '../pages/PaymentMethods'; 
import Payments from '../pages/Payments'; 







export default function Routes() {
    return(
        <Switch>
 
            <Route path={`${process.env.PUBLIC_URL}/type/:categoryType`} exact component={HomepageCategoryWise} />
            <Route path={`${process.env.PUBLIC_URL}/login`} exact component={Login}/>
            <Route path={`${process.env.PUBLIC_URL}/register`} exact component={Register}/>
            <Route path={`${process.env.PUBLIC_URL}/forgot-password`} exact component={ForgotPassword}/>
            <Route path={`${process.env.PUBLIC_URL}/reset-password/:token`} exact component={ResetPassword}/>
            <Route path={`${process.env.PUBLIC_URL}/productListing/:segment/:id`} exact component={Productlisting}/>
            <Route path={`${process.env.PUBLIC_URL}/productDetails/:id`} exact component={ProductDetails}/>
            <Route path={`${process.env.PUBLIC_URL}/storeDetails/:storeId`} exact component={StoreDetails} />
            <Route path={`${process.env.PUBLIC_URL}/cart`} exact component={MyCart}/>
            <Route path={`${process.env.PUBLIC_URL}/checkout`} exact component={Checkout}/>
            <Route path={`${process.env.PUBLIC_URL}/myOrder`} exact component={Myorder}/>
            <Route path={`${process.env.PUBLIC_URL}/myProfile`} exact component={MyProfile}/>
            <Route path={`${process.env.PUBLIC_URL}/myWishlist`} exact component={MyWishList}/>
            <Route path={`${process.env.PUBLIC_URL}/mobileVerification`} exact component={MobileVerification}/>
            <Route path={`${process.env.PUBLIC_URL}/emailVerification`} exact component={EmailVerification}/>
            <Route path={`${process.env.PUBLIC_URL}/address-book`} exact component={UserAddress}/>
            <Route path={`${process.env.PUBLIC_URL}/order-details`} exact component={OrderDetails}/>
            <Route path={`${process.env.PUBLIC_URL}/About-us`} exact component={AboutUs}/>
            <Route path={`${process.env.PUBLIC_URL}/Privacy-Policy`} exact component={PrivayPolicy}/>
            <Route path={`${process.env.PUBLIC_URL}/groceries`} exact component={Groceries}/>

            
            <Route path={`${process.env.PUBLIC_URL}/shipping-delivery`} exact component={ShippingAndDelivery}/>
            <Route path={`${process.env.PUBLIC_URL}/returns-refunds`} exact component={ReturnsAndRefunds}/>
            <Route path={`${process.env.PUBLIC_URL}/payment-methods`} exact component={PaymentMethods}/> 

            <Route path={`${process.env.PUBLIC_URL}/Terms-and-Condition`} exact component={termsAndCondition}/> 
            <Route path={`${process.env.PUBLIC_URL}/seller-faqs`} exact component={SellerFaq}/>
            <Route path={`${process.env.PUBLIC_URL}/customer-faqs`} exact component={CustomerFaq}/>
            <Route path={`${process.env.PUBLIC_URL}/contact-us`} exact component={ContactUs}/>
            <Route path={`${process.env.PUBLIC_URL}/legal-notice`} exact component={LegalNotice}/>
            <Route path={`${process.env.PUBLIC_URL}/categories`} exact component={Categories} />
            <Route path={`${process.env.PUBLIC_URL}/messagedetails/:sellerId`} exact component={MessageDetails} />
            <Route path={`${process.env.PUBLIC_URL}/vouchers`} exact component={Vouchers} />
            <Route path={`${process.env.PUBLIC_URL}/applied-vouchers`} exact component={AppliedVouchers} />  
            <Route path={`${process.env.PUBLIC_URL}/change-password`} exact component={ChangePassword}/>
            <Route path={`${process.env.PUBLIC_URL}/verify-email/:token`} exact component={EmailVerify}/>
            <Route path={`${process.env.PUBLIC_URL}/pay`} exact component={Payments}/>



            <Route path={`${process.env.PUBLIC_URL}/`} exact component={Homepage}/>
            <Route component={Homepage} />
        </Switch>
    )
}